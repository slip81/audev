﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuDiscountApi.Models;
using AuDiscountApi.Service;
using AuDiscountApi.Json;
using Swashbuckle.Swagger.Annotations;
#endregion

namespace AuDiscountApi.Controllers
{
    /// <summary>
    /// Основные методы API
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]
    public class MainController : ApiController
    {
        private readonly IAuDiscountMainService mainService;

        public MainController(IAuDiscountMainService _mainService)
        {
            this.mainService = _mainService;
        }

        private HttpResponseMessage getJsonResponse(object value)
        {
            var settings = new JsonSerializerSettings() { ContractResolver = new NullToEmptyStringResolver(), };
            string json = JsonConvert.SerializeObject(value, settings);
            //string json = JsonConvert.SerializeObject(value);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }

        /// <summary>
        /// Проверка связи с сервисом
        /// </summary>
        [HttpPost]
        [Route("Noop")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop()
        {
            mainService.Noop();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Начать сессию
        /// </summary>
        /// <param name="loginServiceParam">Данные о пользователе</param>
        /// <returns>Код сессии и информация о текущих маркетинговых акциях</returns>
        [HttpPost]
        [Route("Login")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Открывается сессия для заданного пользователя. Пароль должен быть зашифрован, для шифрования в Кассе используется библиотека EncrHelper."
            + " <br/>"
            + " Если для организации настроена и действует на момент старта сессии маркетинговая акция с использованием разовых купонов, то объект campaign_info != null."
            + " <br/>"
            + " Если campaign_info.card_num_default имеет непустое значение, его необходимо сохранить  в переменных класса TDiscountService и передавать во все продажи по Кассе."
            + " <br/>"
            + " Если card_num_default задан, то все равно должна быть возможность “перебить” его другим номером карты, если например покупатель  предъявил свою дисконтную / бонусную карту при совершении покупки. Т.е. есть возможность выбора – либо использовать маркетинговую акцию, либо использовать карту покупателя."
            + " <br/>"
            + " <strong>ВАЖНО !</strong> Одновременное использование карты и акции не предусмотрено.",
            typeof(LoginResult))
        ]
        public HttpResponseMessage Login(LoginServiceParam loginServiceParam)
        {
            var result = mainService.Login(loginServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Закрыть сессию
        /// </summary>
        /// <param name="logoutServiceParam">Код активной сессии</param>
        /// <returns>Признак закрытия сессии</returns>
        [HttpPost]
        [Route("Logout")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Закрывается указанная сессия",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage Logout(LogoutServiceParam logoutServiceParam)
        {
            var result = mainService.Logout(logoutServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Считать карту
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии, номер карты (либо фамилия, либо телефон, либо электронная почта, либо ФИО владельца)</param>
        /// <returns>Дисконтная карта</returns>
        [HttpPost]
        [Route("GetCard")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Поиск карты по номеру, штрих-коду, фамилии, телефону, электронной почте, ФИО владельца."
            + " Вернется карта в случае, если найдена ровно одна карта. В других случаях вернется ошибка.",
            typeof(Card))
        ]
        public HttpResponseMessage GetCard(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCard(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получить список карт
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии, номер карты (либо фамилия, либо телефон, либо электронная почта, либо ФИО владельца)</param>
        /// <returns>Список карт</returns>
        [HttpPost]
        [Route("GetCards")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Возвращается список карт заданной организации по заданным условиям поиска. Максимально допустимое количество карт – 5000, если найдено больше карт – вернется ошибка.",
            typeof(Cards))
        ]
        public HttpResponseMessage GetCards(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCards(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Выдать новую карту
        /// </summary>
        /// <param name="issueCardServiceParam">Код сессии, параметры выдаваемой карты и владельца карты</param>
        /// <returns>Новая выданная карта</returns>
        [HttpPost]
        [Route("IssueCard")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Выдается указанная карта указанному владельцу. Если карта не активирована – она автоматически активируется."
            + " Если указанная карта не найдена по номеру – она автоматически создается, с указанным номером и доп. номером."
            + " В этом случае должны быть обязательно заполнены параметры card_status_id и card_type_id.",
            typeof(Card))
        ]
        public HttpResponseMessage IssueCard(IssueCardServiceParam issueCardServiceParam)
        {
            var result = mainService.IssueCard(issueCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Сбросить или сменить владельца карты
        /// </summary>
        /// <param name="resetCardHolderServiceParam">Код сессии, номер карты, ФИО нового владельца</param>
        /// <returns>Признак успешного сброса или смены владельца карты</returns>
        [HttpPost]
        [Route("ResetCardHolder")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "У указанной карты сбрасывается текущий владелец карты. Если указаны фамилия (или имя, или отчество) нового владельца -"
            + " создается новый владелец и карта привязывается к нему",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage ResetCardHolder(ResetCardHolderServiceParam resetCardHolderServiceParam)
        {
            var result = mainService.ResetCardHolder(resetCardHolderServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Изменить данные владельца карты
        /// </summary>
        /// <param name="updateCardHolderServiceParam">Код сессии, номер карты, новые ФИО, адрес,телефон, email владельца</param>
        /// <returns>Признак успешного изменения данных владельца карты</returns>
        [HttpPost]
        [Route("UpdateCardHolder")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "У указанной карты изменяются данные владельца карты.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage UpdateCardHolder(ResetCardHolderServiceParam updateCardHolderServiceParam)
        {
            var result = mainService.UpdateCardHolder(updateCardHolderServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Заменить карту
        /// </summary>
        /// <param name="mergeCardServiceParam">Код сессии, номер старой и новой карты, режим переноса номиналов</param>
        /// <returns>Новая карта</returns>
        [HttpPost]
        [Route("MergeCard")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Указанная карта закрывается, взамен ее выдается новая указанная карта."
            + " Если новая указанная карта не найдена по номеру – она автоматически создается,"
            + " с указанным номером и доп. номером, с комментарием “создана взамен карты XXXX”."
            + " Нельзя заменить карту, если она в статусе “Утеряна”, “Закрыта”."
            + " Нельзя заменить карту на карту в статусе “Утеряна”, “Закрыта”. Нельзя заменить карту на карту, у которой уже есть владелец.",
            typeof(Card))
        ]
        public HttpResponseMessage MergeCard(MergeCardServiceParam mergeCardServiceParam)
        {
            var result = mainService.MergeCard(mergeCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Сменить статус карты
        /// </summary>
        /// <param name="changeCardStatusServiceParam">Код сессии, номер карты, код нового статуса карты</param>
        /// <returns>Признак успешной смены статуса</returns>
        [HttpPost]
        [Route("ChangeCardStatus")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Статус указанной карты изменяется на заданный. Допустимые статусы можно получить методом 'Получить список статусов карт'.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage ChangeCardStatus(ChangeCardStatusServiceParam changeCardStatusServiceParam)
        {
            var result = mainService.ChangeCardStatus(changeCardStatusServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получить список статусов карт
        /// </summary>
        /// <param name="mainServiceParam">Код сессии</param>
        /// <returns>Cправочник статусов карт</returns>
        [HttpPost]
        [Route("GetCardStatuses")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Возвращает справочник статусов карт",
            typeof(CardStatuses))
        ]
        public HttpResponseMessage GetCardStatuses(MainServiceParam mainServiceParam)
        {
            var result = mainService.GetCardStatuses(mainServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получить список типов карт
        /// </summary>
        /// <param name="mainServiceParam">Код сессии</param>
        /// <returns>Cправочник типов карт</returns>
        [HttpPost]
        [Route("GetCardTypes")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Возвращает справочник типов карт",
            typeof(CardTypes))
        ]
        public HttpResponseMessage GetCardTypes(MainServiceParam mainServiceParam)
        {
            var result = mainService.GetCardTypes(mainServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Проверить активность карты
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии</param>
        /// <returns>0 если карта активна, если другое число – код статуса карты</returns>
        [HttpPost]
        [Route("IsCardActive")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Быстрая проверка, является ли карта активной, т.е. можно ли выполнять операции по данной карте",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage IsCardActive(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.IsCardActive(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получить процент скидки по карте
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии</param>
        /// <returns>Процент скидки по карте</returns>
        [HttpPost]
        [Route("GetCardDiscountPercent")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Запрос текущего процента скидки по карте. Если карта не является дисконтной"
            + " (например, бонусная или подарочный сертификат) – вернется ошибка.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage GetCardDiscountPercent(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCardDiscountPercent(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получить бонусный процент по карте
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии</param>
        /// <returns>Бонусный процент по карте</returns>
        [HttpPost]
        [Route("GetCardBonusPercent")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Запрос текущего бонусного процента по карте. Если карта не является бонусной"
            + " (например, дисконтная или подарочный сертификат) – вернется ошибка.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage GetCardBonusPercent(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCardBonusPercent(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получить сумму бонусов на карте
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии</param>
        /// <returns>Cумма бонусов по карте</returns>
        [HttpPost]
        [Route("GetCardBonusSum")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Запрос текущей суммы доступных бонусов на карте. Если карта не является бонусной"
            + " (например, дисконтная или подарочный сертификат) – вернется ошибка",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage GetCardBonusSum(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCardBonusSum(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получить сумму денег на карте
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии</param>
        /// <returns>Cумма денег по карте</returns>
        [HttpPost]
        [Route("GetCardMoneySum")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Запрос текущей суммы доступных денег на карте. Если карта не является денежной"
            + " (например, дисконтная или бонусная) – вернется ошибка.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage GetCardMoneySum(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCardMoneySum(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Проверить разрешение на скидку
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии</param>
        /// <returns>Разрешение на скидку</returns>
        [HttpPost]
        [Route("AllowCardDiscount")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Быстрая проверка, возможно ли применение скидки по карте.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage AllowCardDiscount(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.AllowCardDiscount(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Проверить разрешение на оплату бонусами
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии</param>
        /// <returns>Разрешение на оплату бонусами</returns>
        [HttpPost]
        [Route("AllowCardBonus")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Быстрая проверка, возможна ли оплата бонусами по карте.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage AllowCardBonus(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.AllowCardBonus(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Проверить разрешение на оплату деньгами
        /// </summary>
        /// <param name="getCardServiceParam">Код сессии</param>
        /// <returns>Разрешение на оплату деньгами</returns>
        [HttpPost]
        [Route("AllowCardMoney")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Быстрая проверка, возможна ли оплата деньгами по карте.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage AllowCardMoney(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.AllowCardMoney(getCardServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Добавить процент скидки на карту
        /// </summary>
        /// <param name="addCardValueServiceParam">Код сессии, номер карты, добавляемый процент</param>
        /// <returns>Процент скидки по карте</returns>
        [HttpPost]
        [Route("AddCardDiscountPercent")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Добавление процента скидки на карту. Если карта не является дисконтной"
            + " (например, бонусная или подарочный сертификат) – вернется ошибка.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage AddCardDiscountPercent(AddCardValueServiceParam addCardValueServiceParam)
        {
            var result = mainService.AddCardDiscountPercent(addCardValueServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Добавить бонусный процент на карту
        /// </summary>
        /// <param name="addCardValueServiceParam">Код сессии, номер карты, добавляемый процент</param>
        /// <returns>Бонусный процент по карте</returns>
        [HttpPost]
        [Route("AddCardBonusPercent")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Добавление бонусного процента на карту. Если карта не является бонусной"
            + " (например, дисконтная или подарочный сертификат) – вернется ошибка.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage AddCardBonusPercent(AddCardValueServiceParam addCardValueServiceParam)
        {
            var result = mainService.AddCardBonusPercent(addCardValueServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Добавить сумму бонусов на на карту
        /// </summary>
        /// <param name="addCardValueServiceParam">Код сессии, номер карты, добавляемая сумма</param>
        /// <returns>Сумма бонусов по карте</returns>
        [HttpPost]
        [Route("AddCardBonusSum")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Добавление суммы бонусов на карту. Если карта не является бонусной"
            + " (например, дисконтная или подарочный сертификат) – вернется ошибка.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage AddCardBonusSum(AddCardValueServiceParam addCardValueServiceParam)
        {
            var result = mainService.AddCardBonusSum(addCardValueServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Добавить сумму денег на на карту
        /// </summary>
        /// <param name="addCardValueServiceParam">Код сессии, номер карты, добавляемая сумма</param>
        /// <returns>Сумма денег по карте</returns>
        [HttpPost]
        [Route("AddCardMoneySum")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Добавление суммы денег на карту. Если карта не является денежной"
            + " (например, дисконтная или бонусная) – вернется ошибка.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage AddCardMoneySum(AddCardValueServiceParam addCardValueServiceParam)
        {
            var result = mainService.AddCardMoneySum(addCardValueServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получить список операций по карте
        /// </summary>
        /// <param name="getCardTransactionsServiceParam">Код сессии, номер карты, период поиска</param>
        /// <returns>Список операций по карте</returns>
        [HttpPost]
        [Route("GetCardTransactions")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Получение списка операций по указанной карте за указанный диапазон.",
            typeof(CardTransactions))
        ]
        public HttpResponseMessage GetCardTransactions(GetCardTransactionsServiceParam getCardTransactionsServiceParam)
        {
            var result = mainService.GetCardTransactions(getCardTransactionsServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Начать операцию продажи по карте
        /// </summary>
        /// <param name="cardTransactionServiceParam">Код сессии, номер карты, атрибуты продажи</param>
        /// <returns>Результат продажи</returns>
        [HttpPost]
        [Route("CardTransactionStart")]
        [JsonDeserializeExt(Param = "cardTransactionServiceParam", JsonDataType = typeof(CardTransactionStartServiceParam))]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Начало операции продажи по карте. Метод посчитает все, что нужно посчитать, исходя из заданных параметров продажи, создаст объект “операция продажи” в статусе “не подтверждено” и вернет код этой операции вместе с посчитанными данными."
            + " <br/>"
            + " Если в поле distr_by_pos значение 1 – это значит, что примененный дисконтный алгоритм настроен на работу по позициям продажи, и все скидки и бонусы были посчитаны по каждой позиции."
            + " <br/>"
            + " Если в поле distr_by_pos значение 0 - это значит, что примененный дисконтный алгоритм настроен на работу по продаже в целом, и все скидки и бонусы были посчитаны по итоговой сумме продажи."
            + " <br/>"
            + " Если при продаже была применена маркетинговая акция, то объект campaign_data != null."
            + " <br/>"
            + " Если campaign_data.check_text имеет не пустое значение – это текст для печати информации о примененной акции на чеке."
            + " <br/>"
            + " Если campaign_data.card_issued = 1 – после продажи кассир должен выдать покупателю заранее созданный купон, принадлежащий текущей акции."
            + " <br/>"
            + " Если campaign_data.card_applied = 1 – при продаже был использован купон и он теперь не действителен, кассир должен изъять его у покупателя (если это не “виртуальный купон”) "
            + " <br/>"
            + " Если campaign_data.mess имеет не пустое значение – это сообщение для кассира, нужно его где-то в Кассе показать",
            typeof(CardTransactionResult))
        ]
        public HttpResponseMessage CardTransactionStart(CardTransactionStartServiceParam cardTransactionServiceParam)
        {            
            var result = mainService.CardTransactionStart(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Подтвердить операцию продажи по карте
        /// </summary>
        /// <param name="cardTransactionServiceParam">Код сессии, код транзакции, позиция для округления</param>
        /// <returns>Признак успешного подтверждения продажи</returns>
        [HttpPost]
        [Route("CardTransactionCommit")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Подтверждение операции продажи по карте. Метод найдет операцию с заданным кодом,"
            + " находящуюся в статусе 'не подтверждено', переведет ее в статус 'выполнено'"
            + " и обновит информацию по карте.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage CardTransactionCommit(CardTransactionCommitServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CardTransactionCommit(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отменить операцию продажи по карте
        /// </summary>
        /// <param name="cardTransactionServiceParam">Код сессии, код транзакции</param>
        /// <returns>Признак успешной отмены продажи</returns>
        [HttpPost]
        [Route("CardTransactionRollback")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Отмена операции продажи по карте. Метод найдет операцию с заданным кодом,"
            + " находящуюся в статусе 'не подтверждено' и переведет ее в статус 'отменено'."
            + " <br/>"
            + " <strong>ВНИМАНИЕ !</strong> Лучше этим методом не пользоваться,"
            + " т.к. он создает дополнительную нагрузку на сервис, а неподтвержденные операции продаж"
            + " и так автоматически удаляются ежедневно",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage CardTransactionRollback(CardTransactionRollbackServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CardTransactionRollback(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Вернуть товар из продажи по карте
        /// </summary>
        /// <param name="cardTransactionServiceParam">Код сессии, код транзакции, список товаров для возврата</param>
        /// <returns>Результат возврата</returns>
        [HttpPost]
        [Route("CardTransactionRefund")]
        [JsonDeserializeExt(Param = "cardTransactionServiceParam", JsonDataType = typeof(CardTransactionRefundServiceParam))]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Возврат одного или нескольких товаров из продажи по карте."
            + " Метод находит подтвержденную операцию продажи с заданным кодом,"
            + " и проводит операцию возврата каждого заданного товара."
            + " Создастся объект 'операция возврата', связанная с заданной операцией продажи."
            + " <br/>"
            + " В объектах типа CalcUnit, перечисленных к возврату, нужно обязательно заполнить следующие атрибуты:"
            + " <strong>unit_num</strong> – номер позиции, которую надо вернуть"
            + " <strong>unit_count</strong> – кол-во штук в позиции, которые надо вернуть",
            typeof(CardTransactionResult))
        ]
        public HttpResponseMessage CardTransactionRefund(CardTransactionRefundServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CardTransactionRefund(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Начать операцию продажи по сертификату
        /// </summary>
        /// <param name="certTransactionServiceParam">Код сессии, номер сертификата, атрибуты продажи</param>
        /// <returns>Результат продажи</returns>
        [HttpPost]
        [Route("CertTransactionStart")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Метод работает аналогично методу /cardtransactionstart",
            typeof(CertTransactionResult))
        ]
        public HttpResponseMessage CertTransactionStart(CertTransactionStartServiceParam certTransactionServiceParam)
        {
            var result = mainService.CertTransactionStart(certTransactionServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Подтвердить операцию продажи по сертификату
        /// </summary>
        /// <param name="cardTransactionServiceParam">Код сессии, код транзакции, позиция для округления</param>
        /// <returns>Признак успешного подтверждения продажи</returns>
        [HttpPost]
        [Route("CertTransactionCommit")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Подтверждение операции продажи по сертификату. Метод найдет операцию с заданным кодом,"
            + " находящуюся в статусе 'не подтверждено', переведет ее в статус 'выполнено'"
            + " и обновит информацию по сертификату.",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage CertTransactionCommit(CardTransactionCommitServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CertTransactionCommit(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отменить операцию продажи по сертификату
        /// </summary>
        /// <param name="cardTransactionServiceParam">Код сессии, код транзакции</param>
        /// <returns>Признак успешной отмены продажи</returns>
        [HttpPost]
        [Route("CertTransactionRollback")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Отмена операции продажи по сертификату. Метод найдет операцию с заданным кодом,"
            + " находящуюся в статусе 'не подтверждено' и переведет ее в статус 'отменено'."
            + " <br/>"
            + " <strong>ВНИМАНИЕ !</strong> Лучше этим методом не пользоваться,"
            + " т.к. он создает дополнительную нагрузку на сервис, а неподтвержденные операции продаж"
            + " и так автоматически удаляются ежедневно",
            typeof(AuDiscountBaseResult))
        ]
        public HttpResponseMessage CertTransactionRollback(CardTransactionRollbackServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CertTransactionRollback(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Вернуть продажу по сертификату
        /// </summary>
        /// <param name="certTransactionServiceParam">Код сессии, код транзакции</param>
        /// <returns>Результат возврата</returns>
        [HttpPost]
        [Route("CertTransactionRefund")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Возврат всех товаров из продажи по сертификату."
            + " Метод находит подтвержденную операцию продажи с заданным кодом,"
            + " и проводит операцию возврата всей продажи."
            + " Создастся объект 'операция возврата', связанная с заданной операцией продажи.",
            typeof(CertTransactionResult))
        ]
        public HttpResponseMessage CertTransactionRefund(CertTransactionRefundServiceParam certTransactionServiceParam)
        {
            var result = mainService.CertTransactionRefund(certTransactionServiceParam);
            return getJsonResponse(result);
        }
    }

    
    public class JsonDeserializeExtAttribute : ActionFilterAttribute
    {
        public string Param { get; set; }
        public Type JsonDataType { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string inputContent;
            using (var stream = new StreamReader(actionContext.Request.Content.ReadAsStreamAsync().Result, Encoding.GetEncoding("windows-1251")))
            {
                stream.BaseStream.Position = 0;
                inputContent = stream.ReadToEnd();
            }

            var result = JsonConvert.DeserializeObject(inputContent, JsonDataType);
            actionContext.ActionArguments[Param] = result;
        }

    }
   
}
