﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuDiscountApi.Models;
using AuDiscountApi.Service;
using AuDiscountApi.Json;
using System.Web.Http.Description;
#endregion

namespace AuDiscountApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("test")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class TestController : ApiController
    {
        private readonly IAuDiscountTestService mainService;

        public TestController(IAuDiscountTestService _mainService)
        {
            this.mainService = _mainService;
        }

        private HttpResponseMessage getJsonResponse(object value)
        {
            var settings = new JsonSerializerSettings() { ContractResolver = new NullToEmptyStringResolver() };
            string json = JsonConvert.SerializeObject(value, settings);
            //string json = JsonConvert.SerializeObject(value);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }

        [HttpPost]
        [Route("Noop")]
        public HttpResponseMessage Noop()
        {
            mainService.Noop();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("Login")]
        public HttpResponseMessage Login(LoginServiceParam loginServiceParam)
        {
            var result = mainService.Login(loginServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("Logout")]
        public HttpResponseMessage Logout(LogoutServiceParam logoutServiceParam)
        {
            var result = mainService.Logout(logoutServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("GetCard")]
        public HttpResponseMessage GetCard(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCard(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("GetCards")]
        public HttpResponseMessage GetCards(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCards(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("IssueCard")]
        public HttpResponseMessage IssueCard(IssueCardServiceParam issueCardServiceParam)
        {
            var result = mainService.IssueCard(issueCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("ResetCardHolder")]
        public HttpResponseMessage ResetCardHolder(ResetCardHolderServiceParam resetCardHolderServiceParam)
        {
            var result = mainService.ResetCardHolder(resetCardHolderServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("MergeCard")]
        public HttpResponseMessage MergeCard(MergeCardServiceParam mergeCardServiceParam)
        {
            var result = mainService.MergeCard(mergeCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("ChangeCardStatus")]
        public HttpResponseMessage ChangeCardStatus(ChangeCardStatusServiceParam changeCardStatusServiceParam)
        {
            var result = mainService.ChangeCardStatus(changeCardStatusServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("GetCardStatuses")]
        public HttpResponseMessage GetCardStatuses(MainServiceParam mainServiceParam)
        {
            var result = mainService.GetCardStatuses(mainServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("GetCardTypes")]
        public HttpResponseMessage GetCardTypes(MainServiceParam mainServiceParam)
        {
            var result = mainService.GetCardTypes(mainServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("IsCardActive")]
        public HttpResponseMessage IsCardActive(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.IsCardActive(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("GetCardDiscountPercent")]
        public HttpResponseMessage GetCardDiscountPercent(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCardDiscountPercent(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("GetCardBonusPercent")]
        public HttpResponseMessage GetCardBonusPercent(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCardBonusPercent(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("GetCardBonusSum")]
        public HttpResponseMessage GetCardBonusSum(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCardBonusSum(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("GetCardMoneySum")]
        public HttpResponseMessage GetCardMoneySum(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.GetCardMoneySum(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("AllowCardDiscount")]
        public HttpResponseMessage AllowCardDiscount(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.AllowCardDiscount(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("AllowCardBonus")]
        public HttpResponseMessage AllowCardBonus(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.AllowCardBonus(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("AllowCardMoney")]
        public HttpResponseMessage AllowCardMoney(GetCardServiceParam getCardServiceParam)
        {
            var result = mainService.AllowCardMoney(getCardServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("AddCardDiscountPercent")]
        public HttpResponseMessage AddCardDiscountPercent(AddCardValueServiceParam addCardValueServiceParam)
        {
            var result = mainService.AddCardDiscountPercent(addCardValueServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("AddCardBonusPercent")]
        public HttpResponseMessage AddCardBonusPercent(AddCardValueServiceParam addCardValueServiceParam)
        {
            var result = mainService.AddCardBonusPercent(addCardValueServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("AddCardBonusSum")]
        public HttpResponseMessage AddCardBonusSum(AddCardValueServiceParam addCardValueServiceParam)
        {
            var result = mainService.AddCardBonusSum(addCardValueServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("AddCardMoneySum")]
        public HttpResponseMessage AddCardMoneySum(AddCardValueServiceParam addCardValueServiceParam)
        {
            var result = mainService.AddCardMoneySum(addCardValueServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("GetCardTransactions")]
        public HttpResponseMessage GetCardTransactions(GetCardTransactionsServiceParam getCardTransactionsServiceParam)
        {
            var result = mainService.GetCardTransactions(getCardTransactionsServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("CardTransactionStart")]
        [JsonDeserializeExt(Param = "cardTransactionServiceParam", JsonDataType = typeof(CardTransactionStartServiceParam))]
        public HttpResponseMessage CardTransactionStart(CardTransactionStartServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CardTransactionStart(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("CardTransactionCommit")]
        public HttpResponseMessage CardTransactionCommit(CardTransactionCommitServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CardTransactionCommit(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("CardTransactionRollback")]
        public HttpResponseMessage CardTransactionRollback(CardTransactionRollbackServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CardTransactionRollback(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("CardTransactionRefund")]
        [JsonDeserializeExt(Param = "cardTransactionServiceParam", JsonDataType = typeof(CardTransactionRefundServiceParam))]
        public HttpResponseMessage CardTransactionRefund(CardTransactionRefundServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CardTransactionRefund(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("CertTransactionStart")]
        public HttpResponseMessage CertTransactionStart(CertTransactionStartServiceParam certTransactionServiceParam)
        {
            var result = mainService.CertTransactionStart(certTransactionServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("CertTransactionCommit")]
        public HttpResponseMessage CertTransactionCommit(CardTransactionCommitServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CertTransactionCommit(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("CertTransactionRollback")]
        public HttpResponseMessage CertTransactionRollback(CardTransactionRollbackServiceParam cardTransactionServiceParam)
        {
            var result = mainService.CertTransactionRollback(cardTransactionServiceParam);
            return getJsonResponse(result);
        }

        [HttpPost]
        [Route("CertTransactionRefund")]
        public HttpResponseMessage CertTransactionRefund(CertTransactionRefundServiceParam certTransactionServiceParam)
        {
            var result = mainService.CertTransactionRefund(certTransactionServiceParam);
            return getJsonResponse(result);
        }
    }
}
