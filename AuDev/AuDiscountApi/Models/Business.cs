﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class Business : AuDiscountBaseClass
    {
        public Business()
        {
            //
        }

        public long business_id { get; set; }
        public string business_name { get; set; }
        public Nullable<int> add_to_timezone { get; set; }        
        public Nullable<int> card_scan_only { get; set; }
        public short scanner_equals_reader { get; set; }        
        public bool allow_card_add { get; set; }
        public bool allow_phone_num { get; set; }
    }
}