﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CheckPrintInfo
    {
        public CheckPrintInfo()
        {
            //
        }

        public CheckPrintInfo(card_type_check_info item)
        {            
            is_active = item.is_active;
            print_sum_trans = item.print_sum_trans;
            print_bonus_value_trans = item.print_bonus_value_trans;
            print_bonus_value_card = item.print_bonus_value_card;
            print_bonus_percent_card = item.print_bonus_percent_card;
            print_disc_percent_card = item.print_disc_percent_card;
            print_inactive_bonus_value_card = item.print_inactive_bonus_value_card;
            print_used_bonus_value_trans = item.print_used_bonus_value_trans;
            print_programm_descr = item.print_programm_descr;
            print_card_num = item.print_card_num;
            print_cert_sum_card = item.print_cert_sum_card;
            print_cert_sum_trans = item.print_cert_sum_trans;
            print_cert_sum_left = item.print_cert_sum_left;
        }
        
        public bool is_active { get; set; }
        public bool print_sum_trans { get; set; }
        public bool print_bonus_value_trans { get; set; }
        public bool print_bonus_value_card { get; set; }
        public bool print_bonus_percent_card { get; set; }
        public bool print_disc_percent_card { get; set; }        

        // new
        public bool print_inactive_bonus_value_card { get; set; }
        public bool print_used_bonus_value_trans { get; set; }
        public bool print_programm_descr { get; set; }
        public bool print_card_num { get; set; }
        public bool print_cert_sum_card { get; set; }
        public bool print_cert_sum_trans { get; set; }
        public bool print_cert_sum_left { get; set; }
    }
}