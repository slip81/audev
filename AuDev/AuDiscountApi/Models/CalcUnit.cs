﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AuDiscountApi.Models
{
    public class CalcUnit
    {
        public CalcUnit()
        {
            // default values
            group1_property_initialized = 0;
            group2_property_initialized = 0;
            group3_property_initialized = 0;
            unit_max_discount_percent = 100;
            unit_max_bonus_for_card_percent = 100;
            unit_max_bonus_for_pay_percent = 100;
            live_prep = 0;
            unit_price = 0;
            unit_price_tax = 0;
            unit_price_opt = 0;
            unit_price_opt_tax = 0;
            tax_percent = 0;
            tax_summa = 0;
            price_margin_value = 0;
            price_margin_percent = 0;
            artikul = 0;
            mess = "";
            unit_producer_price = 0;
            unit_pack_count = 0;
            date_expire = null;
            producer_name = "";
            producer_id = null;
            farm_group_name = "";
            farm_group_id = null;
            mnn_name = "";
            mnn_id = null;
            ean13 = "";
        }

        public CalcUnit(CalcUnit source_calc_unit)
        {
            unit_num = source_calc_unit.unit_num;
            unit_name = source_calc_unit.unit_name;
            unit_value = source_calc_unit.unit_value;
            unit_max_discount_percent = source_calc_unit.unit_max_discount_percent;
            unit_value_discount = source_calc_unit.unit_value_discount;
            unit_value_with_discount = source_calc_unit.unit_value_with_discount;
            unit_percent_discount = source_calc_unit.unit_percent_discount;
            unit_value_bonus_for_card = source_calc_unit.unit_value_bonus_for_card;
            unit_value_bonus_for_pay = source_calc_unit.unit_value_bonus_for_pay;
            unit_percent_bonus_for_card = source_calc_unit.unit_percent_bonus_for_card;
            unit_percent_bonus_for_pay = source_calc_unit.unit_percent_bonus_for_pay;
            unit_count = source_calc_unit.unit_count;
            unit_type = source_calc_unit.unit_type;
            unit_percent_of_summa = source_calc_unit.unit_percent_of_summa;
            promo = source_calc_unit.promo;

            group1_property_initialized = source_calc_unit.group1_property_initialized;
            group2_property_initialized = source_calc_unit.group2_property_initialized;
            group3_property_initialized = source_calc_unit.group3_property_initialized;

            unit_max_bonus_for_card_percent = group1_property_initialized == 1 ? source_calc_unit.unit_max_bonus_for_card_percent : 100;
            unit_max_bonus_for_pay_percent = group1_property_initialized == 1 ? source_calc_unit.unit_max_bonus_for_pay_percent : 100;
            live_prep = group1_property_initialized == 1 ? source_calc_unit.live_prep : 0;
            price_margin_value = group1_property_initialized == 1 ? source_calc_unit.price_margin_value : 0;
            price_margin_percent = group1_property_initialized == 1 ? source_calc_unit.price_margin_percent : 0;
            artikul = group1_property_initialized == 1 ? source_calc_unit.artikul : 0;
            mess = "";

            unit_price = group2_property_initialized == 1 ? source_calc_unit.unit_price : 0;
            unit_price_tax = group2_property_initialized == 1 ? source_calc_unit.unit_price_tax : 0;
            unit_price_opt = group2_property_initialized == 1 ? source_calc_unit.unit_price_opt : 0;
            unit_price_opt_tax = group2_property_initialized == 1 ? source_calc_unit.unit_price_opt_tax : 0;
            tax_percent = group2_property_initialized == 1 ? source_calc_unit.tax_percent : 0;
            tax_summa = group2_property_initialized == 1 ? source_calc_unit.tax_summa : 0;
            unit_producer_price = group2_property_initialized == 1 ? source_calc_unit.unit_producer_price : 0;
            unit_pack_count = group2_property_initialized == 1 ? source_calc_unit.unit_pack_count : 0;

            date_expire = group3_property_initialized == 1 ? source_calc_unit.date_expire : null;
            producer_name = group3_property_initialized == 1 ? source_calc_unit.producer_name : "";
            producer_id = group3_property_initialized == 1 ? source_calc_unit.producer_id : null;
            farm_group_name = group3_property_initialized == 1 ? source_calc_unit.farm_group_name : "";
            farm_group_id = group3_property_initialized == 1 ? source_calc_unit.farm_group_id : null;
            mnn_name = group3_property_initialized == 1 ? source_calc_unit.mnn_name : "";
            mnn_id = group3_property_initialized == 1 ? source_calc_unit.mnn_id : null;
            ean13 = group3_property_initialized == 1 ? source_calc_unit.ean13 : "";
        }


        // основная группа (всегда инициализирована)
        [JsonProperty(Required = Required.Default)]
        public int unit_num { get; set; }
        [JsonProperty(Required = Required.Default)]
        public string unit_name { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_value { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_max_discount_percent { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_value_discount { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_value_with_discount { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_percent_discount { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_value_bonus_for_card { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_value_bonus_for_pay { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_percent_bonus_for_card { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_percent_bonus_for_pay { get; set; }
        [JsonProperty(Required = Required.Default)]
        //public int unit_count { get; set; }
        public decimal unit_count { get; set; }
        [JsonProperty(Required = Required.Default)]
        public int unit_type { get; set; }

        // всегда считается в сервисе, перед началом применения алгоритма 
        [JsonProperty(Required = Required.Default)]
        public decimal unit_percent_of_summa { get; set; }

        // признак "акционный товар"
        [JsonProperty(Required = Required.Default)]
        public int promo { get; set; }

        [JsonProperty(Required = Required.Default)]
        public int group1_property_initialized { get; set; }
        [JsonProperty(Required = Required.Default)]
        public int group2_property_initialized { get; set; }
        [JsonProperty(Required = Required.Default)]
        public int group3_property_initialized { get; set; }

        // доп. группы (инициализированы в зависимости от значения соответствующего атрибута)

        // доп группа 1
        [JsonProperty(Required = Required.Default)]
        public decimal unit_max_bonus_for_card_percent { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_max_bonus_for_pay_percent { get; set; }
        [JsonProperty(Required = Required.Default)]
        public int live_prep { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal price_margin_value { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal price_margin_percent { get; set; }
        [JsonProperty(Required = Required.Default)]
        public int? artikul { get; set; }
        [JsonProperty(Required = Required.Default)]
        public string mess { get; set; }

        // доп группа 2
        [JsonProperty(Required = Required.Default)]
        public decimal unit_price { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_price_tax { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_price_opt { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_price_opt_tax { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal tax_percent { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal tax_summa { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal unit_producer_price { get; set; }
        [JsonProperty(Required = Required.Default)]
        public int unit_pack_count { get; set; }
        // доп группа 3
        [JsonProperty(Required = Required.Default)]
        public DateTime? date_expire { get; set; }
        [JsonProperty(Required = Required.Default)]
        public string producer_name { get; set; }
        [JsonProperty(Required = Required.Default)]
        public long? producer_id { get; set; }
        [JsonProperty(Required = Required.Default)]
        public string farm_group_name { get; set; }
        [JsonProperty(Required = Required.Default)]
        public long? farm_group_id { get; set; }
        [JsonProperty(Required = Required.Default)]
        public string mnn_name { get; set; }
        [JsonProperty(Required = Required.Default)]
        public long? mnn_id { get; set; }
        [JsonProperty(Required = Required.Default)]
        public string ean13 { get; set; }
    }
    
    public class CalcUnit2
    {        
        public CalcUnit2()
        {
            //
        }
        public int unit_num { get; set; }        
        public string unit_name { get; set; }
    }
}