﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CardStatus
    {
        public CardStatus()
        {
            //card_status
        }

        public long card_status_id { get; set; }
        public string card_status_name { get; set; }
    }

    public class CardStatuses : AuDiscountBaseClass
    {
        public CardStatuses()
        {
            //
        }

        public IEnumerable<CardStatus> card_statuses { get; set; }
    }
}