﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CertTransactionResult : AuDiscountBaseClass
    {
        public CertTransactionResult()
        {
            
        }
        
        public long trans_id { get; set; }
        public decimal trans_sum { get; set; }
        public decimal cert_sum_used { get; set; }
        public decimal cert_sum_left { get; set; }
        public decimal trans_sum_left { get; set; }        
        public string warn { get; set; }
        public CheckPrintInfo print_info { get; set; }
    }
}