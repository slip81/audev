﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CardNominal : AuDiscountBaseClass
    {
        public CardNominal()
        {
            //card_nominal
        }
        
        public Nullable<int> unit_type { get; set; }
        public Nullable<decimal> unit_value { get; set; }
        public Nullable<System.DateTime> date_beg { get; set; }
        public Nullable<System.DateTime> date_end { get; set; }
        public Nullable<System.DateTime> date_expire { get; set; }
    }
}