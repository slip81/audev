﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class Card : AuDiscountBaseClass
    {
        public Card()
        {
            //vw_card
        }

        public long card_id { get; set; }
        public Nullable<System.DateTime> date_beg { get; set; }
        public Nullable<System.DateTime> date_end { get; set; }
        public Nullable<long> curr_trans_count { get; set; }
        public Nullable<long> card_num { get; set; }
        public string card_num2 { get; set; }
        public Nullable<decimal> curr_trans_sum { get; set; }
        public Nullable<decimal> curr_bonus_value { get; set; }
        public Nullable<decimal> curr_bonus_percent { get; set; }        
        public Nullable<decimal> curr_discount_percent { get; set; }
        public Nullable<decimal> curr_money_value { get; set; }        
        public string mess { get; set; }
        public string curr_card_type_name { get; set; }
        public string curr_card_status_name { get; set; }
        public string curr_holder_name { get; set; }
        public string curr_holder_first_name { get; set; }
        public string curr_holder_second_name { get; set; }
        public Nullable<System.DateTime> curr_holder_date_birth { get; set; }
        public Nullable<decimal> curr_inactive_bonus_value { get; set; }
        public Nullable<decimal> curr_all_bonus_value { get; set; }
        public string curr_holder_full_name { get; set; }
        public string curr_holder_address { get; set; }
        public string curr_holder_email { get; set; }
    }

    public class Cards : AuDiscountBaseClass
    {
        public Cards()
        {
            //
        }

        public IEnumerable<Card> cards { get; set; }
    }

    public class CardFull : Card
    {
        public CardFull()
        {
            //
        }

        public string curr_holder_phone_num { get; set; }
    }

    public class CardsFull : AuDiscountBaseClass
    {
        public CardsFull()
        {
            //
        }

        public IEnumerable<CardFull> cards { get; set; }
    }

  
}