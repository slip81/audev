﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Util;

namespace AuDiscountApi.Models
{
    public class AuDiscountBaseClass
    {
        public AuDiscountBaseClass()
        {
            //            
        }
        public ErrInfo error { get; set; }
    }

    public class ErrInfo
    {
        public ErrInfo(long? _id, string _message = "")
        {
            id = _id;
            message = _message;
        }

        public ErrInfo(long? _id, Exception e)
        {
            id = _id;
            if (e == null)
                return;

            message = GlobalUtil.ExceptionInfo(e);
        }

        public long? id { get; set; }
        public string message { get; set; }
    }
}