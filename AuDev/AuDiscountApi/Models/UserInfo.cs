﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuDiscountApi.Models
{
    public class UserInfo
    {
        public UserInfo()
        {
            //
        }

        public bool is_authenticated { get; set; }
        public string login_name { get; set; }        
        public string role_name { get; set; }        
        public long? role_id { get; set; }        
        public long? business_id { get; set; }
    }
}