﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Util;

namespace AuDiscountApi.Models
{
    public class AuDiscountBaseResult : AuDiscountBaseClass
    {
        public AuDiscountBaseResult()
        {
            //            
        }

        public string result { get; set; }
    }
}