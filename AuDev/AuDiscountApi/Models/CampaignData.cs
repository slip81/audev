﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CampaignData
    {
        public CampaignData()
        {
            //
        }

        public string check_text { get; set; }
        public int card_issued { get; set; }
        public int card_applied { get; set; }
        public string mess { get; set; }
    }
}