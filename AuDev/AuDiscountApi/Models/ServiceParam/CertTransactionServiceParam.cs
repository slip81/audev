﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CertTransactionStartServiceParam : MainServiceParam
    {
        public CertTransactionStartServiceParam()
        {
            //
        }

        public string card_num { get; set; }
        public decimal trans_sum { get; set; }
        public decimal sum_for_pay { get; set; }
        public short card_input_mode { get; set; }
        public short is_test_mode { get; set; }
    }

    public class CertTransactionRefundServiceParam : MainServiceParam
    {
        public CertTransactionRefundServiceParam()
        {
            //
        }

        public long trans_id { get; set; }
    }
}