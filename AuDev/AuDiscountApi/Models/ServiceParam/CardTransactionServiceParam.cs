﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    /// <summary>
    /// Параметры начала продажи
    /// </summary>
    public class CardTransactionStartServiceParam : MainServiceParam
    {
        public CardTransactionStartServiceParam()
        {
            //
        }

        /// <summary>
        /// Номер карты
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public string card_num { get; set; }

        /// <summary>
        /// Список товаров
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public List<CalcUnit> calc_unit_list { get; set; }

        /// <summary>
        /// Сумма бонусов к оплате (0 - все доступные, -1 - никаких)
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public decimal bonus_for_pay { get; set; }

        /// <summary>
        /// Режим считывания карты (0 – вручную, 1 – сканером штрих-кодов, 2 – карт-ридером)
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public short card_input_mode { get; set; }

        /// <summary>
        /// Режим продажи (0 – рабочий режим, 1 – тестовый режим)
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public short is_test_mode { get; set; }
    }

    /*
    public class CardTransactionStartServiceParam2 : MainServiceParam
    {
        public CardTransactionStartServiceParam2()
        {
            //
        }

        [JsonProperty(Required = Required.Default)]
        public string card_num { get; set; }
        [JsonProperty(Required = Required.Default)]
        public List<CalcUnit2> calc_unit_list { get; set; }
        [JsonProperty(Required = Required.Default)]
        public decimal bonus_for_pay { get; set; }
        [JsonProperty(Required = Required.Default)]
        public short card_input_mode { get; set; }
        [JsonProperty(Required = Required.Default)]
        public short is_test_mode { get; set; }
    }

    public class CardTransactionStartServiceParam3
    {
        public CardTransactionStartServiceParam3()
        {
            //
        }

        public List<CalcUnit2> calc_unit_list { get; set; }
    }
    */

    /// <summary>
    /// Параметры подтверждения продажи
    /// </summary>
    public class CardTransactionCommitServiceParam : MainServiceParam
    {
        public CardTransactionCommitServiceParam()
        {
            //
        }

        /// <summary>
        /// Код транзакции продажи
        /// </summary>
        public long trans_id {get; set;}
        
        /// <summary>
        /// Позиция для округления
        /// </summary>
        public CalcRound round_info { get; set; }
    }

    /// <summary>
    /// Параметры отмены продажи
    /// </summary>
    public class CardTransactionRollbackServiceParam : MainServiceParam
    {
        public CardTransactionRollbackServiceParam()
        {
            //
        }

        /// <summary>
        /// Код транзакции продажи
        /// </summary>
        public long trans_id { get; set; }
    }

    /// <summary>
    /// Параметры возврата продажи
    /// </summary>
    public class CardTransactionRefundServiceParam : MainServiceParam
    {
        public CardTransactionRefundServiceParam()
        {
            //
        }

        /// <summary>
        /// Код транзакции продажи
        /// </summary>
        public long trans_id { get; set; }

        /// <summary>
        /// Список товаров к возврату
        /// </summary>
        public List<CalcUnit> calc_unit_list { get; set; }

    }

    /// <summary>
    /// Параметры отложенной загрузки продажи (не реализовано)
    /// </summary>
    public class CardTransactionUploadServiceParam : MainServiceParam
    {
        public CardTransactionUploadServiceParam()
        {
            //
        }

    }

}