﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class MainServiceParam
    {
        public MainServiceParam()
        {
            //
        }

        [JsonProperty(Required = Required.Default)]
        public string session_id { get; set; }
        [JsonProperty(Required = Required.Default)]
        public string business_id { get; set; }
    }
}