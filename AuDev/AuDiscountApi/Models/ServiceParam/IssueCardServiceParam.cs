﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class IssueCardServiceParam : MainServiceParam //IssueCardParam
    {
        public IssueCardServiceParam()
        {
            //
        }

        public string card_num { get; set; }
        public string card_num2 { get; set; }
        public int? card_status_id { get; set; }
        public int? card_type_id { get; set; }
        public string card_holder_surname { get; set; }
        public string card_holder_name { get; set; }
        public string card_holder_fname { get; set; }
        public Nullable<System.DateTime> date_birth { get; set; }
        public string phone_num { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string comment { get; set; }        
    }
}