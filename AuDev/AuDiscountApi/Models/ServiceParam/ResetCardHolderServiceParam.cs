﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class ResetCardHolderServiceParam : MainServiceParam
    {
        public ResetCardHolderServiceParam()
        {
            //
        }
        
        public string card_num { get; set; }
        public string card_holder_surname { get; set; }
        public string card_holder_name { get; set; }
        public string card_holder_fname { get; set; }
        //
        public string card_holder_phone_num { get; set; }
        public string card_holder_address { get; set; }
        public string card_holder_email { get; set; }
    }
}