﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class GetCardTransactionsServiceParam : MainServiceParam
    {
        public GetCardTransactionsServiceParam()
        {
            //
        }
        
        public string card_num { get; set; }
        public DateTime? date_beg { get; set; }
        public DateTime? date_end { get; set; }
    }
}