﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class LoginServiceParam
    {
        public LoginServiceParam()
        {
            //
        }
        public string user_name {get; set;}
        public string user_pwd { get; set; }
    }
}