﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class MergeCardServiceParam : MainServiceParam
    {
        public MergeCardServiceParam()
        {
            //
        }
        
        public string card_num { get; set; }
        public string new_card_num { get; set; }
        public string new_card_num2 { get; set; }
        public string transfer_nominal_mode { get; set; }
    }
}