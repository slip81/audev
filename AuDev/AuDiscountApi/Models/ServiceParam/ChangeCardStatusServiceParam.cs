﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class ChangeCardStatusServiceParam : MainServiceParam
    {
        public ChangeCardStatusServiceParam()
        {
            //
        }
        
        public string card_num { get; set; }
        public int? card_status_id { get; set; }
    }
}