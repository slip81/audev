﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    /// <summary>
    /// Параметры добавления номинала на карты
    /// </summary>
    public class AddCardValueServiceParam : GetCardServiceParam
    {
        public AddCardValueServiceParam()
        {
            //
        }
        
        /// <summary>
        /// Величина добавляемого номинала
        /// </summary>
        public string value { get; set; }

        /// <summary>
        ///Признак: 1 - заменить значение номинала, иначе - добавить к номиналу
        /// </summary>
        public int? forReplace { get; set; }
    }
}