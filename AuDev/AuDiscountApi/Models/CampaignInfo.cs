﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CampaignInfo
    {
        public CampaignInfo()
        {
            //
        }

        public CampaignInfo(string _campaign_descr, string _card_num_default)
        {
            descr = _campaign_descr;            
            card_num_default = _card_num_default;
        }

        public string descr { get; set; }        
        public string card_num_default { get; set; }
    }
}