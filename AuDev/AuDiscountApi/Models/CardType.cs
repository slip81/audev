﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CardType
    {
        public CardType()
        {
            //card_status
        }

        public long card_type_id { get; set; }
        public string card_type_name { get; set; }
    }

    public class CardTypes : AuDiscountBaseClass
    {
        public CardTypes()
        {
            //
        }

        public IEnumerable<CardType> card_types { get; set; }
    }
}