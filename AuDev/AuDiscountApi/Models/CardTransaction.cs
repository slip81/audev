﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CardTransaction
    {
        public CardTransaction()
        {
            //vw_card_transaction
        }

        public long card_trans_id { get; set; }
        public Nullable<long> card_id { get; set; }
        public Nullable<long> trans_num { get; set; }
        public Nullable<System.DateTime> date_beg { get; set; }
        public Nullable<decimal> trans_sum { get; set; }
        public short trans_kind { get; set; }
        public Nullable<int> status { get; set; }
        public string user_name { get; set; }
        public Nullable<System.DateTime> date_check { get; set; }
        public Nullable<long> business_id { get; set; }
        public Nullable<System.DateTime> status_date { get; set; }
        public Nullable<short> is_delayed { get; set; }
        public Nullable<decimal> curr_card_sum { get; set; }
        public Nullable<long> card_num { get; set; }
        public string card_num2 { get; set; }
        public string curr_card_type_name { get; set; }
        public Nullable<System.DateTime> date_check_date_only { get; set; }
        public Nullable<decimal> curr_card_bonus_value { get; set; }
        public Nullable<decimal> curr_card_percent_value { get; set; }
        public Nullable<decimal> trans_card_bonus_value { get; set; }
        public Nullable<decimal> trans_card_percent_value { get; set; }
        public Nullable<decimal> trans_sum_discount { get; set; }
        public Nullable<decimal> trans_sum_with_discount { get; set; }
        public Nullable<decimal> trans_sum_bonus_for_card { get; set; }
        public Nullable<decimal> trans_sum_bonus_for_pay { get; set; }
        public string org_name { get; set; }
    }

    public class CardTransactions : AuDiscountBaseClass
    {
        public CardTransactions()
        {
            //vw_card_transaction
        }

        public IEnumerable<CardTransaction> card_transactions { get; set; }
    }
}