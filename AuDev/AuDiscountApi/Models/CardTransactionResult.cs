﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class CardTransactionResult : AuDiscountBaseClass
    {
        public CardTransactionResult()
        {
            distr_by_pos = 1;
        }
        
        public long trans_id { get; set; }        
        public List<CalcUnit> calc_unit_result { get; set; }        
        public decimal curr_trans_sum { get; set; }        
        public decimal curr_trans_count { get; set; }        
        public List<CardNominal> card_nominals { get; set; }        
        public CheckPrintInfo print_info { get; set; }
        public string warn { get; set; }
        public int distr_by_pos { get; set; }
        public CampaignData campaign_data { get; set; }

    }

    public class CardTransactionUploadResult : AuDiscountBaseClass
    {
        public CardTransactionUploadResult()
        {
            //
        }
    }
    
}