﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuDiscountApi.Models
{
    #region CalcPrepareCard

    public class CalcPrepareCard
    {
        public CalcPrepareCard()
        {
            //
        }

        // card
        public long card_id { get; set; }
        public Nullable<System.DateTime> date_beg { get; set; }
        public Nullable<System.DateTime> date_end { get; set; }
        public Nullable<long> curr_card_status_id { get; set; }
        public Nullable<long> business_id { get; set; }
        public Nullable<long> curr_trans_count { get; set; }
        public Nullable<long> card_num { get; set; }
        public string card_num2 { get; set; }
        public Nullable<decimal> curr_trans_sum { get; set; }
        public Nullable<long> curr_client_id { get; set; }
        public Nullable<long> curr_card_type_id { get; set; }
        public string warn { get; set; }
        public Nullable<long> source_card_id { get; set; }
        public bool from_au { get; set; }

        // card_status
        public Nullable<long> card_status_group_id { get; set; }

        // programm
        public Nullable<long> programm_id { get; set; }
        public string descr { get; set; }

        // card_type_check_info
        public bool print_is_active { get; set; }
        public bool print_sum_trans { get; set; }
        public bool print_bonus_value_trans { get; set; }
        public bool print_bonus_value_card { get; set; }
        public bool print_bonus_percent_card { get; set; }
        public bool print_disc_percent_card { get; set; }
        // new
        public bool print_inactive_bonus_value_card { get; set; }
        public bool print_used_bonus_value_trans { get; set; }
        public bool print_programm_descr { get; set; }
        public bool print_card_num { get; set; }
        public bool print_cert_sum_card { get; set; }
        public bool print_cert_sum_trans { get; set; }
        public bool print_cert_sum_left { get; set; }

        // business
        public Nullable<int> add_to_timezone { get; set; }
        public Nullable<int> card_scan_only { get; set; }
        public short scanner_equals_reader { get; set; }
        public bool allow_card_add { get; set; }

        // business_org
        public Nullable<long> org_id { get; set; }
        public Nullable<int> bo_add_to_timezone { get; set; }
        public Nullable<int> bo_card_scan_only { get; set; }        
        public Nullable<short> bo_scanner_equals_reader { get; set; }
        public Nullable<int> bo_allow_card_add { get; set; }

        // card_type_business_org
        public Nullable<int> allow_discount { get; set; }
        public Nullable<int> allow_bonus_for_pay { get; set; }
        public Nullable<int> allow_bonus_for_card { get; set; }
    }

    #endregion
}