﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuDiscountApi.Models
{
    public class LoginResult : AuDiscountBaseClass
    {
        public LoginResult()
        {
            //
        }
        
        public string result { get; set; }
        public CampaignInfo campaign_info { get; set; }
    }    
}