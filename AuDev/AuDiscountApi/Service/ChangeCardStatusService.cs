﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {
        public AuDiscountBaseResult ChangeCardStatus(ChangeCardStatusServiceParam changeCardStatusServiceParam)
        {
            try
            {
                return changeCardStatus(changeCardStatusServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }

        }

        private AuDiscountBaseResult changeCardStatus(ChangeCardStatusServiceParam changeCardStatusServiceParam)
        {
            if ((changeCardStatusServiceParam == null) || (String.IsNullOrEmpty(changeCardStatusServiceParam.card_num)) || (!changeCardStatusServiceParam.card_status_id.HasValue))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = changeCardStatusServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            LogSession ls = getSession(changeCardStatusServiceParam);
            if (ls.error != null)
                return new AuDiscountBaseResult() { error = ls.error };

            GetCardServiceParam getCardServiceParam = new GetCardServiceParam() { session_id = changeCardStatusServiceParam.session_id, business_id = changeCardStatusServiceParam.business_id, card_num = changeCardStatusServiceParam.card_num, };
            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            long business_id = long.Parse(changeCardStatusServiceParam.business_id);
            string user_name = ls.user_name;
            long card_status_id = (long)changeCardStatusServiceParam.card_status_id;
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            card card_db = dbContext.card.Where(ss => ss.card_id == card.card_id).FirstOrDefault();
            if (card_db.curr_card_status_id == card_status_id)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_MULTIPLE_RESULT, "На карте уже установлен данный статус") };
            List<card_card_status_rel> card_card_status_rel_list = dbContext.card_card_status_rel.Where(ss => ss.card_id == card.card_id && !ss.date_end.HasValue).ToList();
            if (card_card_status_rel_list != null)
            {
                foreach (var card_card_status_rel in card_card_status_rel_list)
                {
                    card_card_status_rel.date_end = today;
                }
            }
            insertCardCardStatusRel(card_db, card_status_id, today);

            dbContext.SaveChanges();

            return new AuDiscountBaseResult() { result = "0" };
        }
    }
}