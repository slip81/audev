﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {

        public AuDiscountBaseResult ResetCardHolder(ResetCardHolderServiceParam resetCardHolderServiceParam)
        {
            try
            {
                return resetCardHolder(resetCardHolderServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuDiscountBaseResult resetCardHolder(ResetCardHolderServiceParam resetCardHolderServiceParam)
        {
            if ((resetCardHolderServiceParam == null) || (String.IsNullOrEmpty(resetCardHolderServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = resetCardHolderServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            LogSession ls = getSession(resetCardHolderServiceParam);
            if (ls.error != null)
                return new AuDiscountBaseResult() { error = ls.error };

            GetCardServiceParam getCardServiceParam = new GetCardServiceParam() 
            {
                business_id = resetCardHolderServiceParam.business_id,
                session_id = resetCardHolderServiceParam.session_id,
                card_num = resetCardHolderServiceParam.card_num,
            };
            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            long business_id = long.Parse(resetCardHolderServiceParam.business_id);
            string user_name = ls.user_name;
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            card card_db = dbContext.card.Where(ss => ss.card_id == card.card_id).FirstOrDefault();
            //if (!card_db.curr_holder_id.HasValue)
                //return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "У карты нет владельца") };
            
            card_holder_card_rel card_holder_card_rel = dbContext.card_holder_card_rel.Where(ss => ss.card_id == card.card_id && ss.card_holder_id == card_db.curr_holder_id).FirstOrDefault();
            if (card_holder_card_rel != null)
                card_holder_card_rel.date_end = today;

            card_holder curr_card_holder = dbContext.card_holder.Where(ss => ss.card_holder_id == card_db.curr_holder_id).FirstOrDefault();
            
            string card_holder_comment = curr_card_holder != null ? curr_card_holder.comment : null;
            DateTime? card_holder_date_birth = curr_card_holder != null ? curr_card_holder.date_birth : null;
            /*
            string card_holder_email = curr_card_holder != null ? curr_card_holder.email : null;
            string card_holder_address = curr_card_holder != null ? curr_card_holder.address : null;
            string card_holder_phone_num = curr_card_holder != null ? curr_card_holder.phone_num : null;
            */
            card_holder new_card_holder = null;
            string card_holder_surname = resetCardHolderServiceParam.card_holder_surname;
            string card_holder_name = resetCardHolderServiceParam.card_holder_name;
            string card_holder_fname = resetCardHolderServiceParam.card_holder_fname;
            string card_holder_email = resetCardHolderServiceParam.card_holder_email;
            string card_holder_address = resetCardHolderServiceParam.card_holder_address;
            string card_holder_phone_num = resetCardHolderServiceParam.card_holder_phone_num;
            if ((!String.IsNullOrWhiteSpace(card_holder_surname)) || (!String.IsNullOrWhiteSpace(card_holder_name)) || (!String.IsNullOrWhiteSpace(card_holder_fname)))
            {
                new_card_holder = insertCardHolder(card_holder_address, business_id, card_holder_fname, card_holder_name, card_holder_surname, card_holder_comment, card_holder_date_birth, card_holder_email, card_holder_phone_num);
                insertCardHolderCardRel(card_db, new_card_holder, today);
            }

            card_db.card_holder = new_card_holder;
            //card_db.curr_holder_id = null;
            
            dbContext.SaveChanges();

            return new AuDiscountBaseResult() { result = "0" };
        }       
    }
}