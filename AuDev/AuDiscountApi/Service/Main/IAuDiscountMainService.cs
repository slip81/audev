﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDiscountApi.Models;

namespace AuDiscountApi.Service
{
    public interface IAuDiscountMainService
    {
        void Noop();
        //
        LoginResult Login(LoginServiceParam loginServiceParam);
        AuDiscountBaseResult Logout(LogoutServiceParam logoutServiceParam);
        Card GetCard(GetCardServiceParam getCardServiceParam);
        AuDiscountBaseClass GetCards(GetCardServiceParam getCardServiceParam);
        Card IssueCard(IssueCardServiceParam issueCardServiceParam);
        Card MergeCard(MergeCardServiceParam mergeCardServiceParam);
        AuDiscountBaseResult ResetCardHolder(ResetCardHolderServiceParam resetCardHolderServiceParam);
        AuDiscountBaseResult UpdateCardHolder(ResetCardHolderServiceParam updateCardHolderServiceParam);
        AuDiscountBaseResult ChangeCardStatus(ChangeCardStatusServiceParam changeCardStatusServiceParam);
        CardStatuses GetCardStatuses(MainServiceParam mainServiceParam);
        CardTypes GetCardTypes(MainServiceParam mainServiceParam);
        //        
        AuDiscountBaseResult IsCardActive(GetCardServiceParam getCardServiceParam);
        AuDiscountBaseResult GetCardDiscountPercent(GetCardServiceParam getCardServiceParam);
        AuDiscountBaseResult GetCardBonusPercent(GetCardServiceParam getCardServiceParam);
        AuDiscountBaseResult GetCardBonusSum(GetCardServiceParam getCardServiceParam);
        AuDiscountBaseResult GetCardMoneySum(GetCardServiceParam getCardServiceParam);
        //        
        AuDiscountBaseResult AllowCardDiscount(GetCardServiceParam getCardServiceParam);
        AuDiscountBaseResult AllowCardBonus(GetCardServiceParam getCardServiceParam);
        AuDiscountBaseResult AllowCardMoney(GetCardServiceParam getCardServiceParam);
        //
        AuDiscountBaseResult AddCardDiscountPercent(AddCardValueServiceParam addCardValueServiceParam);
        AuDiscountBaseResult AddCardBonusPercent(AddCardValueServiceParam addCardValueServiceParam);
        AuDiscountBaseResult AddCardBonusSum(AddCardValueServiceParam addCardValueServiceParam);
        AuDiscountBaseResult AddCardMoneySum(AddCardValueServiceParam addCardValueServiceParam);
        //
        CardTransactions GetCardTransactions(GetCardTransactionsServiceParam getCardTransactionsServiceParam);
        //        
        CardTransactionResult CardTransactionStart(CardTransactionStartServiceParam cardTransactionServiceParam);
        AuDiscountBaseResult CardTransactionCommit(CardTransactionCommitServiceParam cardTransactionCommitServiceParam);
        AuDiscountBaseResult CardTransactionRollback(CardTransactionRollbackServiceParam cardTransactionRollbackServiceParam);
        CardTransactionResult CardTransactionRefund(CardTransactionRefundServiceParam cardTransactionRefundServiceParam);
        CardTransactionUploadResult CardTransactionUpload(CardTransactionUploadServiceParam cardTransactionUploadServiceParam);
        //        
        CertTransactionResult CertTransactionStart(CertTransactionStartServiceParam certTransactionServiceParam);
        AuDiscountBaseResult CertTransactionCommit(CardTransactionCommitServiceParam cardTransactionServiceParam);
        AuDiscountBaseResult CertTransactionRollback(CardTransactionRollbackServiceParam cardTransactionServiceParam);
        CertTransactionResult CertTransactionRefund(CertTransactionRefundServiceParam certTransactionServiceParam);

    }
}
