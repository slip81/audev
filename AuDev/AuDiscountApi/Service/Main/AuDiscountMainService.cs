﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService : IDisposable, IAuDiscountMainService
    {
        protected AuMainDb dbContext;

        [Ninject.Inject]
        public AutoMapper.IMapper ModelMapper { get; set; }

        public AuDiscountMainService()
        {
            dbContext = new AuMainDb("AuMainDb");
            //dbContext = new AuMainDb("AuTestDb");
        }

        public void Dispose()
        {
            if (dbContext != null)
            {                
                dbContext.Dispose();
            }
        }

        /*
        public IEnumerable<Business> GetBusinessList()
        {
            return dbContext.business.ProjectTo<Business>().AsEnumerable();
        }
        */

        private LogSession getSession(MainServiceParam sessionParam)
        {
            if (sessionParam == null)
                return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код сессии") };
            string session_id = sessionParam.session_id;
            if (String.IsNullOrEmpty(session_id))
                return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код сессии")};
            return getActiveSession(session_id);            
        }

        private LogSession getActiveSession(string session_id)
        {
            log_session ls = null;
            DateTime today = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
            using (AuMainDb dbContext = new AuMainDb())
                ls = dbContext.log_session.Where(ss => ss.session_id.ToLower().Trim().Equals(session_id.ToLower().Trim())).FirstOrDefault();
            if (ls.state != 0)
                return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED, "Сессия закрыта") };

            DateTime session_day = new DateTime(ls.date_beg.Year, ls.date_beg.Month, ls.date_beg.Day);
            if (session_day < today)
            {
                long res1 = closeSession(session_id);
                if (res1 != 0)
                    return new LogSession() { error = new ErrInfo(res1, "Ошибка при попытке закрыть просроченную сессию") };
                else
                    return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED, "Сессия просрочена и закрыта") };
            }

            LogSession result = new LogSession();
            ModelMapper.Map<log_session, LogSession>(ls, result);
            //result.business_name = ls.business != null ? ls.business.business_name : "";
            return result;
        }

        private long closeSession(string session_id)
        {
            if (String.IsNullOrEmpty(session_id))
                return (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;

            DateTime now = DateTime.Now;
            
            LogSession LogSession = null;
            log_session ls = dbContext.log_session.Where(ss => ss.session_id.ToLower().Trim().Equals(session_id.ToLower().Trim())).FirstOrDefault();
            if (ls == null)
                return (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;
            if (ls.state == 0)
            {
                //dbContext.log_session.Remove(ls);
                ls.state = 1;
                ls.date_end = now;
            }
            else
                return (long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED;

            dbContext.SaveChanges();

            ModelMapper.Map<log_session, LogSession>(ls, LogSession);

            //Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, "closed session_id: " + session_id, session_id, ls.user_name); 
            Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, session_id, session_id, LogSession.business_id, "", LogSession, (long)Enums.LogEventType.SESSION_CLOSE, null, now, null, true);

            return 0;
        }

    }
}