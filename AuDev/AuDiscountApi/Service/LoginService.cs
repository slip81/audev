﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {
        #region Login

        public LoginResult Login(LoginServiceParam loginServiceParam)
        {
            try
            {
                return login(loginServiceParam);
            }
            catch (Exception ex)
            {
                return new LoginResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private LoginResult login(LoginServiceParam loginServiceParam)
        {
            EncrHelper2 encr = new EncrHelper2();

            DateTime now = DateTime.Now;

            string user_name = String.IsNullOrEmpty(loginServiceParam.user_name) ? "" : loginServiceParam.user_name.Trim();
            string user_pwd = String.IsNullOrEmpty(loginServiceParam.user_pwd) ? "" : loginServiceParam.user_pwd.Trim();

            UserInfo ui = null;

            // !!!
            // убрать

            long? org_id = null;
            if (user_name.Trim().ToLower().Equals("empty") && (user_pwd.Equals("")))
            {
                ui = new UserInfo();
                ui.is_authenticated = true;
                ui.login_name = user_name;
                //ui.business_id = 957862396381103979;      // Трит
                //org_id = 1012951473354441915;                
                ui.business_id = 1191989625543984144;      // "!Тест_Монолит"
                org_id = 1191989799339164689;
                //ui.business_id = 1202118759788054458;      // "Аптека В1, ООО"
                ui.role_id = (long)Enums.UserRole.ADMIN;                
            }
            else
            {
                ui = new UserInfo();
                ui = Authenticate(user_name, user_pwd);
                if (!(ui.is_authenticated))
                    return new LoginResult() { result = "", error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Авторизация не выполнена") };
            }

            LogSession LogSession = null;

            long? curr_business_id = null;

            // создание session_id
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            string session_id = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            curr_business_id = ui.business_id.GetValueOrDefault(0) <= 0 ? null : ui.business_id;

            var org = (from bo in dbContext.business_org
                       from bou in dbContext.business_org_user
                       where bo.org_id == bou.org_id
                       && bo.business_id == curr_business_id
                       && bou.user_name.Trim().ToLower().Equals(user_name.Trim().ToLower())
                       select bou)
                      .FirstOrDefault();
            org_id = org != null ? (long?)org.org_id : org_id;

            log_session ls = new log_session();
            ls.date_beg = now;
            ls.session_id = session_id.Trim();
            ls.business_id = curr_business_id;
            ls.user_name = user_name;
            ls.user_role = ui.role_id;
            ls.is_admin = ((ui.role_id.GetValueOrDefault(0) == (long)Enums.UserRole.ADMIN) || (ui.role_id.GetValueOrDefault(0) == (long)Enums.UserRole.SUPERADMIN)) ? (short)1 : (short)0;
            ls.state = 0;
            //ls.org_id = org != null ? (long?)org.org_id : null;
            ls.org_id = org_id;
            dbContext.log_session.Add(ls);
            dbContext.SaveChanges();

            LogSession = new LogSession();
            ModelMapper.Map<log_session, LogSession>(ls, LogSession);

            // campaign
            DateTime today = DateTime.Today;

            //campaign campaign = dbContext.campaign.Where(ss => ss.business_id == curr_business_id && ss.date_beg <= today && ss.date_end >= today && ss.state == (int)Enums.CampaignStateEnum.ACTIVE).FirstOrDefault();
            //CampaignInfo campaign_info = campaign != null ? new CampaignInfo(campaign) : null;

            var campaign = (from t1 in dbContext.campaign
                                from t2 in dbContext.campaign_business_org
                                where t1.campaign_id == t2.campaign_id
                                && t2.org_id == org_id
                                && t1.business_id == curr_business_id
                                && t1.date_beg <= today && t1.date_end >= today
                                && t1.state == (int)Enums.CampaignStateEnum.ACTIVE
                                select new { t1.campaign_descr, t2.card_num_default }
                               ).FirstOrDefault();
            CampaignInfo campaignInfo = campaign != null ? new CampaignInfo(campaign.campaign_descr, campaign.card_num_default) : null;

            Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, session_id, session_id, curr_business_id, "", LogSession, (long)Enums.LogEventType.SESSION_START, null, now, null, true);

            LoginResult session = new LoginResult() { result = session_id.Trim(), campaign_info = campaignInfo };

            return session;
        }
        
        #endregion

        #region Authenticate

        private Dictionary<long, string> UserRoles { get; set; }
        private void CreateUserRoles()
        {
            UserRoles = new Dictionary<long, string>();
            UserRoles.Add(101, "Administrators");
            UserRoles.Add(102, "Testers");
            UserRoles.Add(103, "Users");
            UserRoles.Add(104, "Superadmin");
        }

        private UserInfo Authenticate(string user_name, string user_pwd)
        {
            CreateUserRoles();

            EncrHelper2 encr = new EncrHelper2();

            int step_num = 0;

            step_num = 1;
            DirectoryEntry de = new DirectoryEntry();

            bool isAu = false;
            string currRoleName = "";
            long currRoleId = 0;

            DirectorySearcher deSearch;
            SearchResult result;

            DirectoryEntry de_found;
            long business_id_from_user = 0;

            List<long> business_id_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                business_id_list = dbContext.business.Select(ss => ss.business_id).ToList();
            }

            foreach (var userRole in UserRoles)
            {
                isAu = false;
                currRoleName = userRole.Value;
                currRoleId = userRole.Key;

                //!!!
                //v1.2.1.3
                foreach (var business_id in business_id_list)
                {

                    //!!!
                    //v1.2.1.3
                    de.Path = "LDAP://10.0.1.2:389/uid=" + business_id.ToString() + ",ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";
                    //de.Path = "LDAP://10.0.1.2:389/ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";                    

                    //!!!
                    //v1.2.1.3
                    de.Username = "cn=" + user_name + ",uid=" + business_id.ToString() + ",ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";
                    //de.Username = "cn=" + user_name + ",ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";                    

                    de.Password = encr._GetHashValue(user_pwd);
                    de.AuthenticationType = AuthenticationTypes.None;

                    deSearch = new DirectorySearcher();
                    deSearch.SearchRoot = de;

                    try
                    {
                        result = deSearch.FindOne();
                        isAu = true;

                        //!!!
                        //v1.2.1.3
                        business_id_from_user = business_id;
                        break;

                        de_found = result.GetDirectoryEntry();

                        foreach (DirectoryEntry child in de_found.Children)
                        {
                            PropertyCollection pc = child.Properties;
                            IDictionaryEnumerator ide = pc.GetEnumerator();
                            ide.Reset();
                            while (ide.MoveNext())
                            {
                                PropertyValueCollection pvc = ide.Entry.Value as PropertyValueCollection;
                                if ((ide.Entry.Key.ToString().ToLower().Trim().Equals("cn"))
                                    && (pvc.Value.ToString().ToLower().Trim().Equals(user_name.ToLower().Trim())))
                                {
                                    foreach (DirectoryEntry child2 in child.Children)
                                    {
                                        PropertyCollection pc2 = child2.Properties;
                                        IDictionaryEnumerator ide2 = pc2.GetEnumerator();
                                        ide2.Reset();
                                        while (ide2.MoveNext())
                                        {
                                            PropertyValueCollection pvc2 = ide2.Entry.Value as PropertyValueCollection;
                                            if (ide2.Entry.Key.ToString().ToLower().Trim().Equals("uid"))
                                            {
                                                business_id_from_user = Convert.ToInt64(pvc2.Value.ToString());
                                            }
                                            if (business_id_from_user > 0)
                                                break;
                                        }
                                        if (business_id_from_user > 0)
                                            break;
                                    }
                                }

                                if (business_id_from_user > 0)
                                    break;
                            }
                            if (business_id_from_user > 0)
                                break;
                        }
                    }
                    catch
                    {
                        isAu = false;
                    }

                    if (isAu)
                        break;

                    //!!!
                    //v1.2.1.3
                }
                if (isAu)
                    break;
            }

            return new UserInfo()
            {
                is_authenticated = isAu,
                login_name = user_name,
                role_name = currRoleName,
                role_id = currRoleId,
                business_id = business_id_from_user,
            };

        }

        #endregion
    }
}