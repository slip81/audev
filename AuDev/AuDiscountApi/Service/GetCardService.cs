﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;
using System.Text.RegularExpressions;
#endregion

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {
        #region GetCard

        public Card GetCard(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return getCard(getCardServiceParam, false, null);
            }
            catch (Exception ex)
            {
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private Card getCard(GetCardServiceParam getCardServiceParam, bool forCalc, long? org_id)
        {
            if ((getCardServiceParam == null) || (String.IsNullOrEmpty(getCardServiceParam.card_num)))
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            DateTime logStartTime = DateTime.Now;

            long? ls_org_id = org_id;
            LogSession ls = null;
            if (!forCalc)
            {
                ls = getSession(getCardServiceParam);
                if (ls.error != null)
                    return new Card() { error = ls.error };
                ls_org_id = ls.org_id;
            }

            long business_id = long.Parse(getCardServiceParam.business_id);            

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(getCardServiceParam.card_num, out card_num_long);
            if (!parseOk)
                card_num_long = 0;

            List<vw_card> card_list = xgetCards(business_id, ls_org_id, card_num_long, card_num_string);
            if ((card_list == null) || (card_list.Count <= 0))
            {
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с заданным условием") };
            }

            if (card_list.Count > 1)
            {
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_MULTIPLE_RESULT, "Найдено несколько карт с заданным условием") };
            }

            vw_card single_vw_card = card_list.FirstOrDefault();

            if (!forCalc)
            {
                // обновляем номиналы, если на типе карт установлен такой признак                
                var update_nominal = dbContext.card_type.Where(ss => ss.card_type_id == single_vw_card.curr_card_type_id && ss.update_nominal == 1).FirstOrDefault();
                if (update_nominal != null)
                {                    
                    recalcCardNominal(single_vw_card.card_id, ls);
                }

                // место для настройки - можно менять последний параметр true/false
                // подготовка к расчету
                // xCard_CalcBefore(single_card, ls.user_name, true);                                
                card single_card = dbContext.card.Where(ss => ss.card_id == single_vw_card.card_id).FirstOrDefault();
                cardTransactionBefore(single_card, ls.user_name, true);

            }

            // !!!
            // test
            /*
            CardFull res11 = new CardFull();
            return res11;
            */

            var allow_phone_num = dbContext.business.Where(ss => ss.business_id == business_id).Select(ss => ss.allow_phone_num).FirstOrDefault();
            if (allow_phone_num)
            {
                CardFull resultFull = new CardFull();
                ModelMapper.Map<vw_card, CardFull>(single_vw_card, resultFull);
                return resultFull;
            }
            else
            {
                Card result = new Card();
                ModelMapper.Map<vw_card, Card>(single_vw_card, result);
                return result;
            }
            
        }

        #endregion

        #region GetCards

        public AuDiscountBaseClass GetCards(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return getCards(getCardServiceParam);
            }
            catch (Exception ex)
            {
                return new Cards() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuDiscountBaseClass getCards(GetCardServiceParam getCardServiceParam)
        {
            if (getCardServiceParam == null)
                return new Cards() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            DateTime logStartTime = DateTime.Now;

            LogSession ls = getSession(getCardServiceParam);
            if (ls.error != null)
                return new Cards() { error = ls.error };
            long business_id = long.Parse(getCardServiceParam.business_id);

            long? ls_org_id = ls.org_id;

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(getCardServiceParam.card_num, out card_num_long);
            if (!parseOk)
                card_num_long = 0;

            List<vw_card> card_list = xgetCards(business_id, ls_org_id, card_num_long, card_num_string);
            if ((card_list == null) || (card_list.Count <= 0))
                return new Cards() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены карты по заданному условию") };

            int max_card_cnt = 5000;
            if (card_list.Count > max_card_cnt)
                return new Cards() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_MULTIPLE_RESULT, "Невозможно показать карты (найдено " + card_list.Count.ToString() + ", максимально допустимо " + max_card_cnt.ToString() + ")") };

            var allow_phone_num = dbContext.business.Where(ss => ss.business_id == business_id).Select(ss => ss.allow_phone_num).FirstOrDefault();
            if (allow_phone_num)
            {
                return new CardsFull() { cards = ModelMapper.Map<IEnumerable<vw_card>, IEnumerable<CardFull>>(card_list) };
            }
            else
            {
                return new Cards() { cards = ModelMapper.Map<IEnumerable<vw_card>, IEnumerable<Card>>(card_list) };
            }            
        }

        private List<vw_card> xgetCards(long business_id, long? ls_org_id, long card_num_long, string card_num_string)
        {
            List<vw_card> card_list = null;

            string card_holder_surname = null;
            string card_holder_name = null;
            string card_holder_fname = null;

            if (card_num_long > 0)
            {
                // по числовому номеру карты
                card_list = (from c in dbContext.vw_card
                             from ct in dbContext.card_type_business_org
                             where c.business_id == business_id
                             && (c.card_num.HasValue && c.card_num == card_num_long)
                             && c.curr_card_type_id == ct.card_type_id
                             && ct.business_org_id == ls_org_id
                             select c)
                   .OrderBy(ss => ss.business_id)
                   .ThenBy(ss => ss.card_num)
                   .ToList()
                   ;
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if (!String.IsNullOrWhiteSpace(card_num_string))
                {
                    // по строковому номеру карты
                    card_list = (from c in dbContext.vw_card
                                 from ct in dbContext.card_type_business_org
                                 where c.business_id == business_id
                                 && (!String.IsNullOrEmpty(c.card_num2) && c.card_num2.Equals(card_num_string))
                                 && c.curr_card_type_id == ct.card_type_id
                                 && ct.business_org_id == ls_org_id
                                 select c)
                       .OrderBy(ss => ss.business_id)
                       .ThenBy(ss => ss.card_num2)
                       .ToList()
                       ;
                }
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if (!String.IsNullOrWhiteSpace(card_num_string))
                {
                    // по доп. номеру карты
                    card_list = (from c in dbContext.vw_card
                                 from can in dbContext.card_additional_num
                                 from ct in dbContext.card_type_business_org
                                 where c.business_id == business_id
                                 && c.card_id == can.card_id
                                 && (!String.IsNullOrEmpty(can.card_num) && can.card_num.Equals(card_num_string))
                                 && c.curr_card_type_id == ct.card_type_id
                                 && ct.business_org_id == ls_org_id
                                 select c)
                       .OrderBy(ss => ss.business_id)
                       .ThenBy(ss => ss.card_num)
                       .Distinct()
                       .ToList()
                       ;
                }
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if (!String.IsNullOrWhiteSpace(card_num_string))
                {
                    // по фамилии владельца
                    card_list = (from c in dbContext.vw_card
                                 from ct in dbContext.card_type_business_org
                                 from rel in dbContext.card_holder_card_rel
                                 from ch in dbContext.card_holder
                                 where c.business_id == business_id
                                 && c.curr_card_type_id == ct.card_type_id
                                 && ct.business_org_id == ls_org_id
                                 && c.card_id == rel.card_id && !rel.date_end.HasValue
                                 && rel.card_holder_id == ch.card_holder_id
                                 && (!String.IsNullOrEmpty(ch.card_holder_surname) && ch.card_holder_surname.Trim().ToLower().Equals(card_num_string.Trim().ToLower()))
                                 select c)
                       .OrderBy(ss => ss.business_id)
                       .ThenBy(ss => ss.card_num)
                       .Distinct()
                       .ToList()
                       ;
                }
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if (!String.IsNullOrWhiteSpace(card_num_string))
                {
                    // по телефону владельца
                    card_list = (from c in dbContext.vw_card
                                 from ct in dbContext.card_type_business_org
                                 from rel in dbContext.card_holder_card_rel
                                 from ch in dbContext.card_holder
                                 where c.business_id == business_id
                                 && c.curr_card_type_id == ct.card_type_id
                                 && ct.business_org_id == ls_org_id
                                 && c.card_id == rel.card_id && !rel.date_end.HasValue
                                 && rel.card_holder_id == ch.card_holder_id
                                 && (!String.IsNullOrEmpty(ch.phone_num) && ch.phone_num.Trim().ToLower().Equals(card_num_string.Trim().ToLower()))
                                 select c)
                       .OrderBy(ss => ss.business_id)
                       .ThenBy(ss => ss.card_num)
                       .Distinct()
                       .ToList()
                       ;
                }
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if (!String.IsNullOrWhiteSpace(card_num_string))
                {
                    // по телефону владельца - только цифры
                    Regex digitsOnly = new Regex(@"[^\d]");
                    var phone_num_digits = digitsOnly.Replace(card_num_string, "");
                    if (phone_num_digits == null) phone_num_digits = "";
                    card_list = (from c in dbContext.vw_card
                                 from ct in dbContext.card_type_business_org
                                 from rel in dbContext.card_holder_card_rel
                                 from ch in dbContext.card_holder
                                 where c.business_id == business_id
                                 && c.curr_card_type_id == ct.card_type_id
                                 && ct.business_org_id == ls_org_id
                                 && c.card_id == rel.card_id && !rel.date_end.HasValue
                                 && rel.card_holder_id == ch.card_holder_id
                                 && (!String.IsNullOrEmpty(ch.phone_num) && ch.phone_num.Trim().ToLower().Equals(phone_num_digits.Trim().ToLower()))
                                 select c)
                       .OrderBy(ss => ss.business_id)
                       .ThenBy(ss => ss.card_num)
                       .Distinct()
                       .ToList()
                       ;
                }
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if (!String.IsNullOrWhiteSpace(card_num_string))
                {
                    // по телефону владельца - только цифры, без первой цифры 7 или 8
                    Regex digitsOnly = new Regex(@"[^\d]");
                    var phone_num_digits = digitsOnly.Replace(card_num_string, "");
                    if (phone_num_digits == null) phone_num_digits = "";
                    if ((phone_num_digits.Trim().StartsWith("7")) || (phone_num_digits.Trim().StartsWith("8")))
                    {
                        phone_num_digits = phone_num_digits.Trim().Substring(1);
                        card_list = (from c in dbContext.vw_card
                                     from ct in dbContext.card_type_business_org
                                     from rel in dbContext.card_holder_card_rel
                                     from ch in dbContext.card_holder
                                     where c.business_id == business_id
                                     && c.curr_card_type_id == ct.card_type_id
                                     && ct.business_org_id == ls_org_id
                                     && c.card_id == rel.card_id && !rel.date_end.HasValue
                                     && rel.card_holder_id == ch.card_holder_id
                                     && (!String.IsNullOrEmpty(ch.phone_num) && ch.phone_num.Trim().ToLower().Equals(phone_num_digits.Trim().ToLower()))
                                     select c)
                           .OrderBy(ss => ss.business_id)
                           .ThenBy(ss => ss.card_num)
                           .Distinct()
                           .ToList()
                           ;
                    }
                }
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if (!String.IsNullOrWhiteSpace(card_num_string))
                {
                    // по электронной почте владельца
                    card_list = (from c in dbContext.vw_card
                                 from ct in dbContext.card_type_business_org
                                 from rel in dbContext.card_holder_card_rel
                                 from ch in dbContext.card_holder
                                 where c.business_id == business_id
                                 && c.curr_card_type_id == ct.card_type_id
                                 && ct.business_org_id == ls_org_id
                                 && c.card_id == rel.card_id && !rel.date_end.HasValue
                                 && rel.card_holder_id == ch.card_holder_id
                                 && (!String.IsNullOrEmpty(ch.email) && ch.email.Trim().ToLower().Equals(card_num_string.Trim().ToLower()))
                                 select c)
                       .OrderBy(ss => ss.business_id)
                       .ThenBy(ss => ss.card_num)
                       .Distinct()
                       .ToList()
                       ;
                }
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if (!String.IsNullOrWhiteSpace(card_num_string))
                {
                    var card_num_string_splitted1 = card_num_string.Split(' ');
                    if ((card_num_string_splitted1 != null) && (card_num_string_splitted1.Length == 2))
                    {
                        card_holder_surname = card_num_string_splitted1[0].Trim().ToLower();
                        card_holder_name = card_num_string_splitted1[1].Trim().ToLower();
                        // по фамилии и имени владельца
                        card_list = (from c in dbContext.vw_card
                                     from ct in dbContext.card_type_business_org
                                     from rel in dbContext.card_holder_card_rel
                                     from ch in dbContext.card_holder
                                     where c.business_id == business_id
                                     && c.curr_card_type_id == ct.card_type_id
                                     && ct.business_org_id == ls_org_id
                                     && c.card_id == rel.card_id && !rel.date_end.HasValue
                                     && rel.card_holder_id == ch.card_holder_id
                                     && (!String.IsNullOrEmpty(ch.card_holder_surname) && ch.card_holder_surname.Trim().ToLower().Equals(card_holder_surname))
                                     && (!String.IsNullOrEmpty(ch.card_holder_name) && ch.card_holder_name.Trim().ToLower().Equals(card_holder_name))
                                     select c)
                           .OrderBy(ss => ss.business_id)
                           .ThenBy(ss => ss.card_num)
                           .Distinct()
                           .ToList()
                           ;
                    }
                }
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if (!String.IsNullOrWhiteSpace(card_num_string))
                {
                    var card_num_string_splitted1 = card_num_string.Split(' ');
                    if ((card_num_string_splitted1 != null) && (card_num_string_splitted1.Length == 3))
                    {
                        card_holder_surname = card_num_string_splitted1[0].Trim().ToLower();
                        card_holder_name = card_num_string_splitted1[1].Trim().ToLower();
                        card_holder_fname = card_num_string_splitted1[2].Trim().ToLower();
                        // по ФИО владельца
                        card_list = (from c in dbContext.vw_card
                                     from ct in dbContext.card_type_business_org
                                     from rel in dbContext.card_holder_card_rel
                                     from ch in dbContext.card_holder
                                     where c.business_id == business_id
                                     && c.curr_card_type_id == ct.card_type_id
                                     && ct.business_org_id == ls_org_id
                                     && c.card_id == rel.card_id && !rel.date_end.HasValue
                                     && rel.card_holder_id == ch.card_holder_id
                                     && (!String.IsNullOrEmpty(ch.card_holder_surname) && ch.card_holder_surname.Trim().ToLower().Equals(card_holder_surname))
                                     && (!String.IsNullOrEmpty(ch.card_holder_name) && ch.card_holder_name.Trim().ToLower().Equals(card_holder_name))
                                     && (!String.IsNullOrEmpty(ch.card_holder_fname) && ch.card_holder_fname.Trim().ToLower().Equals(card_holder_fname))
                                     select c)
                           .OrderBy(ss => ss.business_id)
                           .ThenBy(ss => ss.card_num)
                           .Distinct()
                           .ToList()
                           ;
                    }
                }
            }
            if ((card_list == null) || (card_list.Count <= 0))
            {
                if ((card_num_long <= 0) && (String.IsNullOrWhiteSpace(card_num_string)))
                {
                    // все карты, доступные для данного отделения организации
                    card_list = (from c in dbContext.vw_card
                                 from ct in dbContext.card_type_business_org
                                 where c.business_id == business_id
                                 && c.curr_card_type_id == ct.card_type_id
                                 && ct.business_org_id == ls_org_id
                                 select c)
                   .OrderBy(ss => ss.business_id)
                   .ThenBy(ss => ss.card_num)
                   .ToList()
                   ;
                }
            }

            return card_list;
        }

        #endregion       

        #region GetCardStatuses

        public CardStatuses GetCardStatuses(MainServiceParam mainServiceParam)
        {
            try
            {
                return getCardStatuses(mainServiceParam);
            }
            catch (Exception ex)
            {
                return new CardStatuses() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }        

        private CardStatuses getCardStatuses(MainServiceParam mainServiceParam)
        {
            LogSession ls = getSession(mainServiceParam);
            if (ls.error != null)
                return new CardStatuses() { error = ls.error };

            var res = dbContext.card_status.ProjectTo<CardStatus>().ToList();

            return new CardStatuses() { card_statuses = res };
        }

        #endregion

        #region GetCardTypes

        public CardTypes GetCardTypes(MainServiceParam mainServiceParam)
        {
            try
            {
                return getCardTypes(mainServiceParam);
            }
            catch (Exception ex)
            {
                return new CardTypes() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private CardTypes getCardTypes(MainServiceParam mainServiceParam)
        {
            LogSession ls = getSession(mainServiceParam);
            if (ls.error != null)
                return new CardTypes() { error = ls.error };
            long business_id = long.Parse(mainServiceParam.business_id);

            var res = dbContext.card_type.Where(ss => ss.business_id == business_id).ProjectTo<CardType>().ToList();

            return new CardTypes() { card_types = res };
        }

        #endregion

        #region IsCardActive

        public AuDiscountBaseResult IsCardActive(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return isCardActive(getCardServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult isCardActive(GetCardServiceParam getCardServiceParam)
        {
            if ((getCardServiceParam == null) || (String.IsNullOrEmpty(getCardServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            var res = (from t1 in dbContext.card
                       from t2 in dbContext.card_status
                       from t3 in dbContext.card_status_group
                       where t1.card_id == card.card_id
                       && t1.curr_card_status_id == t2.card_status_id
                       && t2.card_status_group_id == t3.card_status_group_id
                       select new { card_status_id = t1.curr_card_status_id, is_active = t3.is_active })
                      .FirstOrDefault();            

            return new AuDiscountBaseResult() { result = ((res != null) && (res.is_active == 1)) ? "0" : (res != null ? res.card_status_id.ToString() : "") };
        }

        #endregion
        
        #region GetCardDiscountPercent

        public AuDiscountBaseResult GetCardDiscountPercent(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return getCardDiscountPercent(getCardServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult getCardDiscountPercent(GetCardServiceParam getCardServiceParam)
        {
            if ((getCardServiceParam == null) || (String.IsNullOrEmpty(getCardServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            var res = (from t1 in dbContext.vw_card
                       from t2 in dbContext.card_type
                       where t1.card_id == card.card_id
                       && t1.curr_card_type_id == t2.card_type_id
                       select new { t1.curr_discount_percent, t2.card_type_group_id }
                       ).FirstOrDefault();
            if (res == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не найдена") };

            if ((res.card_type_group_id != (int)Enums.CardTypeGroup.DISCOUNT) && (res.card_type_group_id != (int)Enums.CardTypeGroup.DISCOUNT_BONUS))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не является дисконтной") };

            return new AuDiscountBaseResult() { result = string.Format("{0:0.00}", res.curr_discount_percent.GetValueOrDefault(0).ToString()) };
        }

        #endregion

        #region GetCardBonusPercent

        public AuDiscountBaseResult GetCardBonusPercent(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return getCardBonusPercent(getCardServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult getCardBonusPercent(GetCardServiceParam getCardServiceParam)
        {
            if ((getCardServiceParam == null) || (String.IsNullOrEmpty(getCardServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            var res = (from t1 in dbContext.vw_card
                       from t2 in dbContext.card_type
                       where t1.card_id == card.card_id
                       && t1.curr_card_type_id == t2.card_type_id
                       select new { t1.curr_bonus_percent, t2.card_type_group_id }
                       ).FirstOrDefault();
            if (res == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не найдена") };

            if ((res.card_type_group_id != (int)Enums.CardTypeGroup.BONUS) && (res.card_type_group_id != (int)Enums.CardTypeGroup.DISCOUNT_BONUS))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не является бонусной") };

            return new AuDiscountBaseResult() { result = string.Format("{0:0.00}", res.curr_bonus_percent.GetValueOrDefault(0).ToString()) };
        }

        #endregion

        #region GetCardBonusSum

        public AuDiscountBaseResult GetCardBonusSum(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return getCardBonusSum(getCardServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult getCardBonusSum(GetCardServiceParam getCardServiceParam)
        {
            if ((getCardServiceParam == null) || (String.IsNullOrEmpty(getCardServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            var res = (from t1 in dbContext.vw_card
                       from t2 in dbContext.card_type
                       where t1.card_id == card.card_id
                       && t1.curr_card_type_id == t2.card_type_id
                       select new { t1.curr_bonus_value, t2.card_type_group_id }
                       ).FirstOrDefault();
            if (res == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не найдена") };

            if ((res.card_type_group_id != (int)Enums.CardTypeGroup.BONUS) && (res.card_type_group_id != (int)Enums.CardTypeGroup.DISCOUNT_BONUS))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не является бонусной") };

            return new AuDiscountBaseResult() { result = string.Format("{0:0.00}", res.curr_bonus_value.GetValueOrDefault(0).ToString()) };
        }

        #endregion

        #region GetCardMoneySum

        public AuDiscountBaseResult GetCardMoneySum(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return getCardMoneySum(getCardServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult getCardMoneySum(GetCardServiceParam getCardServiceParam)
        {
            if ((getCardServiceParam == null) || (String.IsNullOrEmpty(getCardServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            var res = (from t1 in dbContext.vw_card
                       from t2 in dbContext.card_type
                       where t1.card_id == card.card_id
                       && t1.curr_card_type_id == t2.card_type_id
                       select new { t1.curr_money_value, t2.card_type_group_id }
                       ).FirstOrDefault();
            if (res == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не найдена") };

            if (res.card_type_group_id != (int)Enums.CardTypeGroup.CERT)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не является денежной") };

            return new AuDiscountBaseResult() { result = string.Format("{0:0.00}", res.curr_money_value.GetValueOrDefault(0).ToString()) };
        }

        #endregion

        #region AllowCardDiscount

        public AuDiscountBaseResult AllowCardDiscount(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return allowCardDiscount(getCardServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult allowCardDiscount(GetCardServiceParam getCardServiceParam)
        {
            if ((getCardServiceParam == null) || (String.IsNullOrEmpty(getCardServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            var res = (from t1 in dbContext.vw_card
                       from t2 in dbContext.card_type
                       where t1.card_id == card.card_id
                       && t1.curr_card_type_id == t2.card_type_id
                       select new { t1.curr_discount_percent, t2.card_type_group_id }
                       ).FirstOrDefault();
            if (res == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не найдена") };

            if ((res.card_type_group_id != (int)Enums.CardTypeGroup.DISCOUNT) && (res.card_type_group_id != (int)Enums.CardTypeGroup.DISCOUNT_BONUS))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не является дисконтной") };

            return new AuDiscountBaseResult() { result = "0" };
        }

        #endregion

        #region AllowCardBonus

        public AuDiscountBaseResult AllowCardBonus(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return allowCardBonus(getCardServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult allowCardBonus(GetCardServiceParam getCardServiceParam)
        {
            if ((getCardServiceParam == null) || (String.IsNullOrEmpty(getCardServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            var res = (from t1 in dbContext.vw_card
                       from t2 in dbContext.card_type
                       where t1.card_id == card.card_id
                       && t1.curr_card_type_id == t2.card_type_id
                       select t2.card_type_group_id
                       ).FirstOrDefault();
            if (res == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не найдена") };

            if ((res != (int)Enums.CardTypeGroup.BONUS) && (res != (int)Enums.CardTypeGroup.DISCOUNT_BONUS))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не является бонусной") };

            return new AuDiscountBaseResult() { result = "0" };
        }

        #endregion

        #region AllowCardMoney

        public AuDiscountBaseResult AllowCardMoney(GetCardServiceParam getCardServiceParam)
        {
            try
            {
                return allowCardMoney(getCardServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult allowCardMoney(GetCardServiceParam getCardServiceParam)
        {
            if ((getCardServiceParam == null) || (String.IsNullOrEmpty(getCardServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = getCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            var res = (from t1 in dbContext.vw_card
                       from t2 in dbContext.card_type
                       where t1.card_id == card.card_id
                       && t1.curr_card_type_id == t2.card_type_id
                       select t2.card_type_group_id
                       ).FirstOrDefault();
            if (res == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не найдена") };

            if (res != (int)Enums.CardTypeGroup.CERT)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта не является денежной") };

            return new AuDiscountBaseResult() { result = "0" };
        }

        #endregion        

        #region RecalcCardNominal

        private void recalcCardNominal(long card_id, LogSession ls)
        {
            /*
                usage in:             
                xGetCardList_byCardNum
                xGetCardList_byClient
                xGetCardValueForPay
                xGetCardList_byAttrs
                xGetCardNominalList_byCard
            */

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            //List<long> id_for_del = new List<long>();
            using (AuMainDb dbContext = new AuMainDb())
            {
                var nominal_inactive_list = dbContext.card_nominal_inactive.Where(ss => ss.card_id == card_id && ss.active_date <= now).ToList();
                if (nominal_inactive_list == null)
                    return;
                var nominal_inactive_list_grouped = nominal_inactive_list
                    .GroupBy(ss => new { ss.unit_type })
                    .Select(ss => new { unit_type = ss.Key.unit_type, unit_value = ss.Sum(tt => tt.unit_value.GetValueOrDefault(0)) })
                    ;
                foreach (var nominal_inactive in nominal_inactive_list_grouped)
                {
                    switch (nominal_inactive.unit_type)
                    {
                        case (int)Enums.CardUnitTypeEnum.BONUS_VALUE:
                            var nominal = dbContext.card_nominal.Where(ss => ss.card_id == card_id && ss.unit_type == nominal_inactive.unit_type && !ss.date_end.HasValue).OrderByDescending(ss => ss.date_beg).FirstOrDefault();
                            if (nominal == null)
                            {
                                nominal = new card_nominal();
                                nominal.card_id = card_id;
                                nominal.crt_date = now;
                                nominal.crt_user = ls.user_name;
                                nominal.date_beg = today;
                                nominal.date_end = null;
                                nominal.unit_type = nominal_inactive.unit_type;
                                nominal.unit_value = nominal_inactive.unit_value;
                            }
                            else
                            {
                                nominal.date_end = today;
                                var new_nominal = new card_nominal();
                                new_nominal.card_id = card_id;
                                new_nominal.crt_date = now;
                                new_nominal.crt_user = ls.user_name;
                                new_nominal.date_beg = today;
                                new_nominal.date_end = null;
                                new_nominal.unit_type = nominal_inactive.unit_type;
                                new_nominal.unit_value = nominal.unit_value.GetValueOrDefault(0) + nominal_inactive.unit_value;
                                dbContext.card_nominal.Add(new_nominal);
                            }
                            //id_for_del.Add(nominal_inactive.inactive_id);
                            break;
                        default:
                            break;
                    }

                    dbContext.SaveChanges();
                }
                //dbContext.card_nominal_inactive.RemoveRange(dbContext.card_nominal_inactive.Where(ss => id_for_del.Contains(ss.inactive_id)));
                dbContext.card_nominal_inactive.RemoveRange(nominal_inactive_list);
                dbContext.SaveChanges();
            }
        }

        #endregion
    }
}