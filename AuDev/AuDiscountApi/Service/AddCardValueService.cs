﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {
        #region AddCardDiscountPercent

        public AuDiscountBaseResult AddCardDiscountPercent(AddCardValueServiceParam addCardValueServiceParam)
        {
            try
            {
                return addCardDiscountPercent(addCardValueServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult addCardDiscountPercent(AddCardValueServiceParam addCardValueServiceParam)
        {
            if ((addCardValueServiceParam == null) || (String.IsNullOrEmpty(addCardValueServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = addCardValueServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            LogSession ls = getSession(addCardValueServiceParam);
            if (ls.error != null)
                return new AuDiscountBaseResult() { error = ls.error };

            Card card = getCard(addCardValueServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            decimal value = 0;
            parseOk = decimal.TryParse(addCardValueServiceParam.value, out value);
            if (!parseOk)
                value = 0;
            if (value == 0)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан процент скидки") };

            var allow = allowCardDiscount(addCardValueServiceParam);
            if (allow.error != null)
                return new AuDiscountBaseResult() { error = allow.error };

            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;

            decimal value_next = value;
            decimal value_prev = 0;
            card_nominal card_nominal_last = dbContext.card_nominal.Where(ss => ss.card_id == card.card_id && ss.unit_type == (int)Enums.CardUnitTypeEnum.DISCOUNT_PERCENT && !ss.date_end.HasValue).FirstOrDefault();
            if (card_nominal_last != null)
            {
                value_prev = card_nominal_last.unit_value.GetValueOrDefault(0);                
                card_nominal_last.date_end = today;
            }
            
            value_next = addCardValueServiceParam.forReplace == 1 ? value : value + value_prev;

            if (value_next < 0)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Процент скидки не может быть отрицательным") };
            if (value_next > 100)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Процент скидки не может быть больше 100") };

            var business = dbContext.business.Where(ss => ss.business_id == ls.business_id).FirstOrDefault();
            if (business == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + ls.business_id.ToString() ) };
            if ((value_prev > 0) && (!business.allow_card_nominal_change))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Стоит запрет на изменение данных по карте") };

            card_nominal card_nominal_new = new card_nominal();
            card_nominal_new.card_id = card.card_id;
            card_nominal_new.crt_date = now;
            card_nominal_new.crt_user = ls.user_name;
            card_nominal_new.date_beg = today;
            card_nominal_new.unit_type = (int)Enums.CardUnitTypeEnum.DISCOUNT_PERCENT;
            card_nominal_new.unit_value = value_next;
            dbContext.card_nominal.Add(card_nominal_new);

            dbContext.SaveChanges();

            var vw_card = dbContext.vw_card.AsNoTracking().Where(ss => ss.card_id == card.card_id).FirstOrDefault();
                
            return new AuDiscountBaseResult() { result = string.Format("{0:0.00}", vw_card.curr_discount_percent.GetValueOrDefault(0).ToString()) };
        }

        #endregion

        #region AddCardBonusPercent

        public AuDiscountBaseResult AddCardBonusPercent(AddCardValueServiceParam addCardValueServiceParam)
        {
            try
            {
                return addCardBonusPercent(addCardValueServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult addCardBonusPercent(AddCardValueServiceParam addCardValueServiceParam)
        {
            if ((addCardValueServiceParam == null) || (String.IsNullOrEmpty(addCardValueServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = addCardValueServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            LogSession ls = getSession(addCardValueServiceParam);
            if (ls.error != null)
                return new AuDiscountBaseResult() { error = ls.error };

            Card card = getCard(addCardValueServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            decimal value = 0;
            parseOk = decimal.TryParse(addCardValueServiceParam.value, out value);
            if (!parseOk)
                value = 0;
            if (value == 0)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан бонусный процент") };

            var allow = allowCardBonus(addCardValueServiceParam);
            if (allow.error != null)
                return new AuDiscountBaseResult() { error = allow.error };

            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;

            decimal value_next = value;
            decimal value_prev = 0;
            card_nominal card_nominal_last = dbContext.card_nominal.Where(ss => ss.card_id == card.card_id 
                && ss.unit_type == (int)Enums.CardUnitTypeEnum.BONUS_PERCENT && !ss.date_end.HasValue).FirstOrDefault();
            if (card_nominal_last != null)
            {
                value_prev = card_nominal_last.unit_value.GetValueOrDefault(0);
                card_nominal_last.date_end = today;
            }

            value_next = addCardValueServiceParam.forReplace == 1 ? value : value + value_prev;

            if (value_next < 0)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Бонусный процент не может быть отрицательным") };
            if (value_next > 100)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Бонусный процент не может быть больше 100") };

            var business = dbContext.business.Where(ss => ss.business_id == ls.business_id).FirstOrDefault();
            if (business == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + ls.business_id.ToString()) };
            if ((value_prev > 0) && (!business.allow_card_nominal_change))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Стоит запрет на изменение данных по карте") };

            card_nominal card_nominal_new = new card_nominal();
            card_nominal_new.card_id = card.card_id;
            card_nominal_new.crt_date = now;
            card_nominal_new.crt_user = ls.user_name;
            card_nominal_new.date_beg = today;
            card_nominal_new.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_PERCENT;
            card_nominal_new.unit_value = value_next;
            dbContext.card_nominal.Add(card_nominal_new);

            dbContext.SaveChanges();

            var vw_card = dbContext.vw_card.AsNoTracking().Where(ss => ss.card_id == card.card_id).FirstOrDefault();

            return new AuDiscountBaseResult() { result = string.Format("{0:0.00}", vw_card.curr_bonus_percent.GetValueOrDefault(0).ToString()) };
        }

        #endregion

        #region AddCardBonusSum

        public AuDiscountBaseResult AddCardBonusSum(AddCardValueServiceParam addCardValueServiceParam)
        {
            try
            {
                return addCardBonusSum(addCardValueServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult addCardBonusSum(AddCardValueServiceParam addCardValueServiceParam)
        {
            if ((addCardValueServiceParam == null) || (String.IsNullOrEmpty(addCardValueServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = addCardValueServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            LogSession ls = getSession(addCardValueServiceParam);
            if (ls.error != null)
                return new AuDiscountBaseResult() { error = ls.error };

            Card card = getCard(addCardValueServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            decimal value = 0;
            parseOk = decimal.TryParse(addCardValueServiceParam.value, out value);
            if (!parseOk)
                value = 0;
            if (value == 0)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан бонусный процент") };

            var allow = allowCardBonus(addCardValueServiceParam);
            if (allow.error != null)
                return new AuDiscountBaseResult() { error = allow.error };

            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;

            decimal value_next = value;
            decimal value_prev = 0;
            card_nominal card_nominal_last = dbContext.card_nominal.Where(ss => ss.card_id == card.card_id
                && ss.unit_type == (int)Enums.CardUnitTypeEnum.BONUS_VALUE && !ss.date_end.HasValue).FirstOrDefault();
            if (card_nominal_last != null)
            {
                value_prev = card_nominal_last.unit_value.GetValueOrDefault(0);
                card_nominal_last.date_end = today;
            }

            value_next = addCardValueServiceParam.forReplace == 1 ? value : value + value_prev;

            if (value_next < 0)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Сумма бонусов не может быть отрицательной") };

            var business = dbContext.business.Where(ss => ss.business_id == ls.business_id).FirstOrDefault();
            if (business == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + ls.business_id.ToString()) };
            if ((value_prev > 0) && (!business.allow_card_nominal_change))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Стоит запрет на изменение данных по карте") };

            card_nominal card_nominal_new = new card_nominal();
            card_nominal_new.card_id = card.card_id;
            card_nominal_new.crt_date = now;
            card_nominal_new.crt_user = ls.user_name;
            card_nominal_new.date_beg = today;
            card_nominal_new.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
            card_nominal_new.unit_value = value_next;
            dbContext.card_nominal.Add(card_nominal_new);

            dbContext.SaveChanges();

            var vw_card = dbContext.vw_card.AsNoTracking().Where(ss => ss.card_id == card.card_id).FirstOrDefault();

            return new AuDiscountBaseResult() { result = string.Format("{0:0.00}", vw_card.curr_bonus_value.GetValueOrDefault(0).ToString()) };
        }

        #endregion

        #region AddCardMoneySum

        public AuDiscountBaseResult AddCardMoneySum(AddCardValueServiceParam addCardValueServiceParam)
        {
            try
            {
                return addCardMoneySum(addCardValueServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult addCardMoneySum(AddCardValueServiceParam addCardValueServiceParam)
        {
            if ((addCardValueServiceParam == null) || (String.IsNullOrEmpty(addCardValueServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = addCardValueServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            LogSession ls = getSession(addCardValueServiceParam);
            if (ls.error != null)
                return new AuDiscountBaseResult() { error = ls.error };

            Card card = getCard(addCardValueServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            decimal value = 0;
            parseOk = decimal.TryParse(addCardValueServiceParam.value, out value);
            if (!parseOk)
                value = 0;
            if (value == 0)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан бонусный процент") };

            var allow = allowCardMoney(addCardValueServiceParam);
            if (allow.error != null)
                return new AuDiscountBaseResult() { error = allow.error };

            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;

            decimal value_next = value;
            decimal value_prev = 0;
            card_nominal card_nominal_last = dbContext.card_nominal.Where(ss => ss.card_id == card.card_id
                && ss.unit_type == (int)Enums.CardUnitTypeEnum.MONEY && !ss.date_end.HasValue).FirstOrDefault();
            if (card_nominal_last != null)
            {
                value_prev = card_nominal_last.unit_value.GetValueOrDefault(0);
                card_nominal_last.date_end = today;
            }

            value_next = addCardValueServiceParam.forReplace == 1 ? value : value + value_prev;

            if (value_next < 0)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Сумма денег на карте не может быть отрицательной") };

            var business = dbContext.business.Where(ss => ss.business_id == ls.business_id).FirstOrDefault();
            if (business == null)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + ls.business_id.ToString()) };
            if ((value_prev > 0) && (!business.allow_card_nominal_change))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Стоит запрет на изменение данных по карте") };

            card_nominal card_nominal_new = new card_nominal();
            card_nominal_new.card_id = card.card_id;
            card_nominal_new.crt_date = now;
            card_nominal_new.crt_user = ls.user_name;
            card_nominal_new.date_beg = today;
            card_nominal_new.unit_type = (int)Enums.CardUnitTypeEnum.MONEY;
            card_nominal_new.unit_value = value_next;
            dbContext.card_nominal.Add(card_nominal_new);

            dbContext.SaveChanges();

            var vw_card = dbContext.vw_card.AsNoTracking().Where(ss => ss.card_id == card.card_id).FirstOrDefault();

            return new AuDiscountBaseResult() { result = string.Format("{0:0.00}", vw_card.curr_money_value.GetValueOrDefault(0).ToString()) };
        }

        #endregion

    }
}