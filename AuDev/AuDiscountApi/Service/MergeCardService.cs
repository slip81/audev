﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {

        public Card MergeCard(MergeCardServiceParam mergeCardServiceParam)
        {
            try
            {
                return mergeCard(mergeCardServiceParam);
            }
            catch (Exception ex)
            {
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private Card mergeCard(MergeCardServiceParam mergeCardServiceParam)
        {
            if ((mergeCardServiceParam == null) || (String.IsNullOrEmpty(mergeCardServiceParam.card_num)) || (String.IsNullOrEmpty(mergeCardServiceParam.new_card_num)))
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = mergeCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер заменяемой карты") };

            long new_card_num_long = 0;
            string new_card_num_string = mergeCardServiceParam.new_card_num.Trim().ToLower();
            parseOk = long.TryParse(new_card_num_string, out new_card_num_long);
            if (!parseOk)
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер новой карты") };

            LogSession ls = getSession(mergeCardServiceParam);
            if (ls.error != null)
                return new Card() { error = ls.error };

            GetCardServiceParam getCardServiceParam = new GetCardServiceParam() { business_id = mergeCardServiceParam.business_id, session_id = mergeCardServiceParam.session_id, card_num = mergeCardServiceParam.card_num };
            Card closing_card = getCard(getCardServiceParam, false, null);
            if (closing_card.error != null)
                return new Card() { error = closing_card.error };

            getCardServiceParam.card_num = mergeCardServiceParam.new_card_num;
            Card opening_card = getCard(getCardServiceParam, false, null);

            long business_id = long.Parse(mergeCardServiceParam.business_id);
            string user_name = ls.user_name;
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            card closing_card_db = null;
            card opening_card_db = null;
            bool create_new_card = false;
            Enums.CardTransferNominalMode transferMode = Enums.CardTransferNominalMode.REPLACE_ALL;

            closing_card_db = dbContext.card.Where(ss => ss.card_id == closing_card.card_id).FirstOrDefault();

            // не давать заменять карту со статусами "Закрыта", "Утеряна" (статус карты, которую заменяем)
            if ((closing_card_db.curr_card_status_id == (long)Enums.CardStatusEnum.LOST) || (closing_card_db.curr_card_status_id == (long)Enums.CardStatusEnum.CLOSED))
            {
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нельзя заменить утерянную/закрытую карту") };                
            }            

            if ((opening_card == null) || (opening_card.error != null))
            {
                create_new_card = true;
            }
            else
            {
                opening_card_db = dbContext.card.Where(ss => ss.card_id == opening_card.card_id).FirstOrDefault();
            }

            // либо создаем новую карту ...
            if (create_new_card)
            {
                opening_card_db = insertCard(business_id, new_card_num_long, mergeCardServiceParam.new_card_num2, today, null, "Выдана взамен карты номер " + card_num_long.ToString(), (int)Enums.CardStatusEnum.ACTIVE, closing_card_db.curr_card_type_id, 0, 0, user_name);

                if (opening_card_db == null)
                {
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Ошибка при создании карты с номером " + new_card_num_long.ToString()) };
                }                
            }
            // ... либо берем существующую
            else
            {
                // нельзя заменять карту на саму себя
                if (opening_card.card_id == closing_card.card_id)
                {
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нельзя заменить карту на саму себя") };
                }

                // не давать заменять карту на карту со статусами "Закрыта", "Утеряна" (статус карты, на которую заменяем)
                if ((opening_card_db.curr_card_status_id == (long)Enums.CardStatusEnum.LOST) || (opening_card_db.curr_card_status_id == (long)Enums.CardStatusEnum.CLOSED))
                {
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нельзя заменить карту на утерянную/закрытую карту") };
                }

                // проверим, что тип карты совпадает
                if (opening_card_db.curr_card_type_id != closing_card_db.curr_card_type_id)
                {
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "У новой карты должен быть тот же тип, что и у заменяемой") };
                }

                // не давать заменять карту на карту, у которой уже есть владелец
                if (opening_card_db.curr_holder_id.HasValue)
                {
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Новая карта уже выдана другому владельцу") };
                }
            }

            dbContext.SaveChanges();

            if ((opening_card == null) || (opening_card.error != null))
            {
                opening_card = new Card();
                var vw_opening_card_db = dbContext.vw_card.Where(ss => ss.card_id == opening_card_db.card_id).FirstOrDefault();
                ModelMapper.Map<vw_card, Card>(vw_opening_card_db, opening_card);
            }


            parseOk = Enum.TryParse<Enums.CardTransferNominalMode>(mergeCardServiceParam.transfer_nominal_mode, out transferMode);
            if (!parseOk)
                transferMode = Enums.CardTransferNominalMode.REPLACE_ALL;

            // переносим владельца                
            opening_card_db.curr_holder_id = closing_card_db.curr_holder_id;
            var curr_card_holder_card_rel = dbContext.card_holder_card_rel.Where(ss => ss.card_holder_id == closing_card_db.curr_holder_id && ss.card_id == closing_card_db.card_id && !ss.date_end.HasValue)
                .OrderByDescending(ss => ss.date_beg).ThenByDescending(ss => ss.rel_id)
                .FirstOrDefault();
            if (curr_card_holder_card_rel != null)
            {
                curr_card_holder_card_rel.date_end = today;
            }
            if (opening_card_db.curr_holder_id.HasValue)
            {
                var curr_client = dbContext.card_holder.Where(ss => ss.card_holder_id == opening_card_db.curr_holder_id).FirstOrDefault();
                insertCardHolderCardRel(opening_card_db, curr_client, today);
                /*
                card_holder_card_rel new_client = new card_holder_card_rel();
                new_client.card_id = new_card.card_id;
                new_client.card_holder_id = (long)new_card.curr_holder_id;
                new_client.date_beg = today;
                new_client.date_end = null;
                dbContext.card_holder_card_rel.Add(new_client);
                */
            }
            closing_card_db.curr_holder_id = null;

            // todo: переносим продажи (или не надо ?)

            // переносим номиналы, накопленную сумму, лимиты
            switch (transferMode)
            {
                case Enums.CardTransferNominalMode.REPLACE_ALL:
                case Enums.CardTransferNominalMode.ADD_ALL:
                    var existing_nominal_list = dbContext.card_nominal.Where(ss => ss.card_id == closing_card.card_id && !ss.date_end.HasValue).ToList();
                    if ((existing_nominal_list != null) && (existing_nominal_list.Count > 0))
                    {
                        var existing_nominals_on_new_card = dbContext.card_nominal.Where(ss => ss.card_id == opening_card.card_id && !ss.date_end.HasValue).ToList();
                        if ((existing_nominals_on_new_card != null) && (existing_nominals_on_new_card.Count > 0))
                        {
                            foreach (var existing_nominal_on_new_card in existing_nominals_on_new_card)
                            {
                                existing_nominal_on_new_card.date_end = today;
                            }
                        }

                        List<card_nominal> new_nominal_list = new List<card_nominal>();
                        foreach (var existing_nominal in existing_nominal_list)
                        {
                            var new_nominal = new card_nominal();
                            new_nominal.card_id = opening_card.card_id;
                            new_nominal.crt_date = now;
                            new_nominal.crt_user = ls.user_name;
                            new_nominal.date_beg = today;
                            new_nominal.date_end = null;
                            if (transferMode == Enums.CardTransferNominalMode.REPLACE_ALL)
                            {
                                new_nominal.unit_type = existing_nominal.unit_type;
                                new_nominal.unit_value = existing_nominal.unit_value;
                            }
                            else
                            {
                                switch ((Enums.CardUnitTypeEnum)existing_nominal.unit_type)
                                {
                                    case Enums.CardUnitTypeEnum.BONUS_VALUE:
                                    case Enums.CardUnitTypeEnum.DISCOUNT_VALUE:
                                    case Enums.CardUnitTypeEnum.MONEY:
                                        new_nominal.unit_type = existing_nominal.unit_type;
                                        var existing_value_on_new_card = dbContext.card_nominal.Where(ss => ss.card_id == opening_card.card_id && ss.unit_type == existing_nominal.unit_type).OrderByDescending(ss => ss.card_nominal_id).FirstOrDefault().unit_value;
                                        new_nominal.unit_value = existing_nominal.unit_value.GetValueOrDefault(0) + existing_value_on_new_card.GetValueOrDefault(0);
                                        break;
                                    default:
                                        new_nominal.unit_type = existing_nominal.unit_type;
                                        new_nominal.unit_value = existing_nominal.unit_value;
                                        break;
                                }
                            }
                            new_nominal_list.Add(new_nominal);
                        }
                        dbContext.card_nominal.AddRange(new_nominal_list);
                    }

                    if (transferMode == Enums.CardTransferNominalMode.REPLACE_ALL)
                    {
                        opening_card_db.curr_trans_count = 0;
                        opening_card_db.curr_trans_sum = closing_card.curr_trans_sum;
                    }
                    else
                    {
                        opening_card_db.curr_trans_count = opening_card_db.curr_trans_count.GetValueOrDefault(0) + closing_card_db.curr_trans_count.GetValueOrDefault(0);
                        opening_card_db.curr_trans_sum = opening_card_db.curr_trans_sum.GetValueOrDefault(0) + closing_card_db.curr_trans_sum.GetValueOrDefault(0);
                    }

                    var existing_limits = dbContext.card_ext1.Where(ss => ss.card_id == closing_card_db.card_id).ToList();
                    if (existing_limits != null)
                    {
                        var existing_limits_copy = existing_limits.Select(ss => new { ss.curr_limit_id, ss.curr_sum, });
                        dbContext.card_ext1.RemoveRange(existing_limits);

                        var new_limits = dbContext.card_ext1.Where(ss => ss.card_id == opening_card_db.card_id);
                        var new_limits_copy = new_limits.Select(ss => new { ss.curr_limit_id, ss.curr_sum, });
                        dbContext.card_ext1.RemoveRange(new_limits);

                        foreach (var existing_limit in existing_limits_copy)
                        {
                            card_ext1 new_limit = new card_ext1();
                            new_limit.card_id = opening_card_db.card_id;
                            new_limit.curr_limit_id = existing_limit.curr_limit_id;
                            if (transferMode == Enums.CardTransferNominalMode.REPLACE_ALL)
                            {
                                new_limit.curr_sum = existing_limit.curr_sum;
                            }
                            else
                            {
                                new_limit.curr_sum = existing_limit.curr_sum.GetValueOrDefault(0) + new_limits_copy.Where(ss => ss.curr_limit_id == existing_limit.curr_limit_id).FirstOrDefault().curr_sum.GetValueOrDefault(0);
                            }
                            dbContext.card_ext1.Add(new_limit);
                        }
                    }
                    break;
                case Enums.CardTransferNominalMode.NONE:
                    break;
                default:
                    break;
            }

            // закрываем старую карту
            card_card_status_rel curr_existing_card_status = dbContext.card_card_status_rel
                .Where(ss => ss.card_id == closing_card_db.card_id && ss.card_status_id == closing_card_db.curr_card_status_id && !ss.date_end.HasValue)
                .OrderByDescending(ss => ss.date_end).ThenByDescending(ss => ss.rel_id)
                .FirstOrDefault();
            if (curr_existing_card_status != null)
            {
                curr_existing_card_status.date_end = today;
            }

            insertCardCardStatusRel(closing_card_db, (long)Enums.CardStatusEnum.CLOSED, today);

            /*
            card_card_status_rel new_existing_card_status = new card_card_status_rel();
            new_existing_card_status.card_id = existing_card.card_id;
            new_existing_card_status.card_status_id = (long)Enums.CardStatusEnum.CLOSED;
            new_existing_card_status.date_beg = today;
            new_existing_card_status.date_end = null;
            dbContext.card_card_status_rel.Add(new_existing_card_status);
            */

            closing_card_db.date_end = today;            
            closing_card_db.mess = "Карта закрыта. Вместо нее выпущена карта номер " + new_card_num_long.ToString();

            dbContext.SaveChanges();

            return opening_card;
        }
    }
}