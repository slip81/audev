﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {
        public Card IssueCard(IssueCardServiceParam issueCardServiceParam)
        {
            try
            {
                return issueCard(issueCardServiceParam);
            }
            catch (Exception ex)
            {
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private Card issueCard(IssueCardServiceParam issueCardServiceParam)
        {

            if ((issueCardServiceParam == null) || (String.IsNullOrEmpty(issueCardServiceParam.card_num)))
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            if (String.IsNullOrEmpty(issueCardServiceParam.card_holder_surname))
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задана фамилия владельца карты") };

            DateTime logStartTime = DateTime.Now;

            /*
            SessionParam sessionParam = issueCardServiceParam.session_param;
            LogSession ls = getSession(sessionParam);
            */
            LogSession ls = getSession(issueCardServiceParam);
            if (ls.error != null)
                return new Card() { error = ls.error };

            // !!!
            var business_org_id = ls.org_id;
            var allow_card_add = dbContext.business_org.Where(ss => ss.org_id == business_org_id).Select(ss => ss.allow_card_add).FirstOrDefault();
            if (!allow_card_add)
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Запрещено создание карты") };

            long business_id = long.Parse(issueCardServiceParam.business_id);
            string user_name = ls.user_name;
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            long card_num_long = 0;
            string card_num_string = issueCardServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан числовой номер карты") };

            string card_num2 = issueCardServiceParam.card_num2;

            Card result = new Card();
            CardFull resultFull = new CardFull();

            var allow_phone_num = dbContext.business.Where(ss => ss.business_id == business_id).Select(ss => ss.allow_phone_num).FirstOrDefault();

            card existing_card = dbContext.card
                .Where(ss => ss.business_id == business_id && ss.card_num.HasValue && ss.card_num == card_num_long)
                .FirstOrDefault();
            if (existing_card != null)
            {
                if (existing_card.curr_holder_id.HasValue)
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT, "Карта с номером " + card_num_long.ToString() + " уже выдана") };

                // привязываем к карте владельца
                card_holder card_holder = insertCardHolder(issueCardServiceParam.address, business_id, issueCardServiceParam.card_holder_fname
                    , issueCardServiceParam.card_holder_name, issueCardServiceParam.card_holder_surname, issueCardServiceParam.comment, issueCardServiceParam.date_birth
                    , issueCardServiceParam.email, issueCardServiceParam.phone_num
                    );
                insertCardHolderCardRel(existing_card, card_holder, today);

                // если карта не активирована - активируем
                long? existing_card_status_id = existing_card.curr_card_status_id;
                if (existing_card_status_id != 2)
                {
                    List<card_card_status_rel> card_card_status_rel_list = dbContext.card_card_status_rel.Where(ss => ss.card_id == existing_card.card_id && !ss.date_end.HasValue).ToList();
                    if (card_card_status_rel_list != null)
                    {
                        foreach (var card_card_status_rel in card_card_status_rel_list)
                        {
                            card_card_status_rel.date_end = today;
                        }
                    }

                    insertCardCardStatusRel(existing_card, 2, today);
                }
                
                dbContext.SaveChanges();
                vw_card vw_card = dbContext.vw_card.Where(ss => ss.card_id == existing_card.card_id).FirstOrDefault();
                if (allow_phone_num)
                {
                    ModelMapper.Map<vw_card, CardFull>(vw_card, resultFull);
                }
                else
                {
                    ModelMapper.Map<vw_card, Card>(vw_card, result);
                }                
            }
            else
            {
                if (!issueCardServiceParam.card_status_id.HasValue)
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан статус карты") };
                if (!issueCardServiceParam.card_type_id.HasValue)
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан тип карты") };

                var card_type_business_org = dbContext.card_type_business_org
                    .Where(ss => ss.business_org_id == ls.org_id && ss.card_type_id == issueCardServiceParam.card_type_id)
                    .FirstOrDefault();
                if (card_type_business_org == null)
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден заданный тип карты [" + issueCardServiceParam.card_type_id.ToString() + "]") };

                    
                card card = insertCard(business_id, card_num_long, card_num2, today, null, null, 
                    issueCardServiceParam.card_status_id, issueCardServiceParam.card_type_id, 
                    0, 0, user_name);

                #region вставка в card_transaction
                /*                
                card_transaction card_transaction = new card_transaction();
                card_transaction.card = card;
                card_transaction.date_beg = DateTime.Now;
                card_transaction.status = (int)Enums.CardTransactionStatus.DONE;
                card_transaction.trans_kind = (int)Enums.CardTransactionType.START_SALDO;
                card_transaction.trans_num = 1;
                card_transaction.trans_sum = cardAdd.curr_trans_sum;
                card_transaction.user_name = user_name;
                dbContext.card_transaction.Add(card_transaction);
                */
                #endregion
                
                insertCardCardTypeRel(card, (long)issueCardServiceParam.card_type_id, today);

                #region вставка в card_nominal (old)
                /*
                card_nominal card_nominal = null;
                List<card_type_unit> card_type_unit_list = dbContext.card_type_unit.Where(ss => ss.card_type_id == (long)issueCardServiceParam.card_type_id).ToList();
                if ((card_type_unit_list != null) && (card_type_unit_list.Count > 0))
                {
                    foreach (var card_type_unit in card_type_unit_list)
                    {
                        card_nominal = new card_nominal();
                        card_nominal.card = card;
                        card_nominal.date_beg = card.date_beg;                        
                        card_nominal.unit_type = card_type_unit.unit_type;
                        switch ((Enums.CardUnitTypeEnum)card_type_unit.unit_type)
                        {
                            case Enums.CardUnitTypeEnum.BONUS_PERCENT:
                                card_nominal.unit_value = cardAdd.curr_bonus_percent;
                                break;
                            case Enums.CardUnitTypeEnum.BONUS_VALUE:
                                card_nominal.unit_value = cardAdd.curr_bonus_value;
                                break;
                            case Enums.CardUnitTypeEnum.DISCOUNT_PERCENT:
                                card_nominal.unit_value = cardAdd.curr_discount_percent;
                                break;
                            case Enums.CardUnitTypeEnum.DISCOUNT_VALUE:
                                card_nominal.unit_value = cardAdd.curr_discount_value;
                                break;
                            case Enums.CardUnitTypeEnum.MONEY:
                                card_nominal.unit_value = cardAdd.curr_money_value;
                                break;
                            default:
                                card_nominal.unit_value = 0;
                                break;
                        }
                        card_nominal.crt_date = DateTime.Now;
                        card_nominal.crt_user = user_name;
                        dbContext.card_nominal.Add(card_nominal);
                    }
                }
                */
                #endregion

                insertCardCardStatusRel(card, (long)issueCardServiceParam.card_status_id, today);

                card_holder card_holder = insertCardHolder(issueCardServiceParam.address, business_id, issueCardServiceParam.card_holder_fname
                    , issueCardServiceParam.card_holder_name, issueCardServiceParam.card_holder_surname, issueCardServiceParam.comment, issueCardServiceParam.date_birth
                    , issueCardServiceParam.email, issueCardServiceParam.phone_num
                    );

                insertCardHolderCardRel(card, card_holder, today);

                dbContext.SaveChanges();
                vw_card vw_card = dbContext.vw_card.Where(ss => ss.card_id == card.card_id).FirstOrDefault();
                if (allow_phone_num)
                {
                    ModelMapper.Map<vw_card, CardFull>(vw_card, resultFull);
                }
                else
                {
                    ModelMapper.Map<vw_card, Card>(vw_card, result);
                }
            }

            return allow_phone_num ? resultFull : result;
        }

        private card insertCard(long business_id, long card_num, string card_num2
            , DateTime? date_beg, DateTime? date_end, string mess, long? card_status_id, long? card_type_id
            , decimal trans_sum, int trans_count, string user_name
            )
        {
            card card = new card();
            card.business_id = business_id;
            card.card_num = card_num;
            card.card_num2 = card_num2;
            card.date_beg = date_beg;
            card.date_end = date_end;
            card.mess = mess;
            card.curr_card_status_id = card_status_id;
            card.curr_card_type_id = card_type_id;
            card.curr_trans_sum = trans_sum;
            card.curr_trans_count = trans_count;
            card.crt_user = user_name;
            card.crt_date = DateTime.Now;            
            card.from_au = true;
            dbContext.card.Add(card);
            return card;
        }

        private void insertCardCardTypeRel(card card, long card_type_id, DateTime date_beg)
        {         
            card_card_type_rel card_card_type_rel = new card_card_type_rel();
            card_card_type_rel.card = card;
            card_card_type_rel.card_type_id = card_type_id;
            card_card_type_rel.date_beg = date_beg;
            card_card_type_rel.date_end = null;
            card_card_type_rel.ord = 1;
            dbContext.card_card_type_rel.Add(card_card_type_rel);
            card.curr_card_type_id = card_type_id;
        }

        private void insertCardCardStatusRel(card card, long card_status_id, DateTime date_beg)
        {            
            card_card_status_rel card_card_status_rel = new card_card_status_rel();
            card_card_status_rel.card = card;
            card_card_status_rel.card_status_id = card_status_id;
            card_card_status_rel.date_beg = date_beg;
            card_card_status_rel.date_end = null;
            dbContext.card_card_status_rel.Add(card_card_status_rel);
            card.curr_card_status_id = card_status_id;
        }

        private card_holder insertCardHolder(string address, long business_id, string card_holder_fname, string card_holder_name,
            string card_holder_surname, string comment, DateTime? date_birth, string email, string phone_num
            )
        {            
            card_holder card_holder = new card_holder();
            card_holder.address = address;
            card_holder.business_id = business_id;
            card_holder.card_holder_fname = card_holder_fname;
            card_holder.card_holder_name = card_holder_name;
            card_holder.card_holder_surname = card_holder_surname;
            card_holder.comment = comment;
            card_holder.date_birth = date_birth;
            card_holder.email = email;
            card_holder.phone_num = phone_num;
            dbContext.card_holder.Add(card_holder);
            return card_holder;
        }

        private void insertCardHolderCardRel(card card, card_holder card_holder, DateTime? date_beg)
        {
            card.card_holder = card_holder;
            card_holder_card_rel card_holder_card_rel = new card_holder_card_rel();
            card_holder_card_rel.card = card;
            card_holder_card_rel.card_holder = card_holder;
            card_holder_card_rel.date_beg = date_beg;
            dbContext.card_holder_card_rel.Add(card_holder_card_rel);
        }
    }
}