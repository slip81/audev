﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {
        public AuDiscountBaseResult Logout(LogoutServiceParam logoutServiceParam)
        {
            try
            {
                return logout(logoutServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuDiscountBaseResult logout(LogoutServiceParam logoutServiceParam)
        {
            string session_id = String.IsNullOrEmpty(logoutServiceParam.session_id) ? "" : logoutServiceParam.session_id.Trim();
            if (String.IsNullOrEmpty(session_id))
                return new AuDiscountBaseResult() { result = "", error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код сессии") };

            DateTime now = DateTime.Now;

            
            log_session ls = dbContext.log_session.Where(ss => ss.session_id.ToLower().Trim().Equals(session_id.ToLower().Trim())).FirstOrDefault();
            if (ls == null)
                return new AuDiscountBaseResult() { result = "", error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена сессия с заданным кодом") };
            if (ls.state == 0)
            {
                //dbContext.log_session.Remove(ls);
                ls.state = 1;
                ls.date_end = now;
            }
            else
                return new AuDiscountBaseResult() { result = "", error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED, "Сессия уже закрыта") };

            dbContext.SaveChanges();

            LogSession LogSession = new LogSession();
            ModelMapper.Map<log_session, LogSession>(ls, LogSession);

            Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, session_id, session_id, LogSession.business_id, "", LogSession, (long)Enums.LogEventType.SESSION_CLOSE, null, now, null, true);

            return new AuDiscountBaseResult() { result = "0", error = null, };
        }
    }
}