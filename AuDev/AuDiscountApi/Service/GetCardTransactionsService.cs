﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {
        #region GetCardTransactions

        public CardTransactions GetCardTransactions(GetCardTransactionsServiceParam getCardTransactionsServiceParam)
        {
            try
            {
                return getCardTransactions(getCardTransactionsServiceParam);
            }
            catch (Exception ex)
            {
                return getCardTransactions_Error(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)));
            }
        }

        private CardTransactions getCardTransactions(GetCardTransactionsServiceParam getCardTransactionsServiceParam)
        {
            if (getCardTransactionsServiceParam == null)
                return getCardTransactions_Error(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры"));

            GetCardServiceParam getCardServiceParam = new GetCardServiceParam() { session_id = getCardTransactionsServiceParam.session_id, business_id = getCardTransactionsServiceParam.business_id, card_num = getCardTransactionsServiceParam.card_num, };
            var card = getCard(getCardServiceParam, false, null);
            if ((card == null) || (card.error != null))
                return getCardTransactions_Error(card.error != null ? card.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта"));
                                    
            long business_id = long.Parse(getCardTransactionsServiceParam.business_id);
            DateTime? date_beg = getCardTransactionsServiceParam.date_beg;
            DateTime? date_end = getCardTransactionsServiceParam.date_end;

            var query = dbContext.vw_card_transaction.Where(ss => ss.business_id == business_id
               && ss.card_id == card.card_id
               && ss.status == (int)Enums.CardTransactionStatus.DONE
               && ((ss.trans_kind == (int)Enums.CardTransactionType.CALC) || (ss.trans_kind == (int)Enums.CardTransactionType.RETURN))
               );
            if (date_beg.HasValue)
                query = query.Where(ss => ss.date_beg >= date_beg);
            if (date_end.HasValue)
                query = query.Where(ss => ss.date_beg <= date_end);
            var transactions = query.OrderByDescending(ss => ss.date_check)
                .ProjectTo<CardTransaction>()
                .ToList()
                ;
            /*
            var transactions = dbContext.vw_card_transaction.Where(ss => ss.business_id == business_id 
                && ss.card_id == card.card_id
                && ss.status == (int)Enums.CardTransactionStatus.DONE
                && ((ss.trans_kind == (int)Enums.CardTransactionType.CALC) || (ss.trans_kind == (int)Enums.CardTransactionType.RETURN))
                && (((ss.date_beg >= date_beg) && (date_beg.HasValue)) || (!date_beg.HasValue))
                && (((ss.date_beg <= date_end) && (date_end.HasValue)) || (!date_end.HasValue))
                )
                .OrderByDescending(ss => ss.date_check)
                .ProjectTo<CardTransaction>()
                .ToList()
                ;
            */

            return new CardTransactions() { card_transactions = transactions };
        }

        private CardTransactions getCardTransactions_Error(ErrInfo errInfo)
        {
            return new CardTransactions() { error = errInfo };
        }

        #endregion
    }
}