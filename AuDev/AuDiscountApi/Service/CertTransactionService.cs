﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {
        #region CertTransactionStart

        public CertTransactionResult CertTransactionStart(CertTransactionStartServiceParam certTransactionServiceParam)
        {
            try
            {
                return certTransactionStart(certTransactionServiceParam, false);
            }
            catch (Exception ex)
            {
                return new CertTransactionResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private CertTransactionResult certTransactionStart(CertTransactionStartServiceParam certTransactionStartServiceParam, bool doCommit)
        {
            #region

            CardTransactionStartServiceParam cardTransactionStartServiceParam = new CardTransactionStartServiceParam();
            List<CalcUnit> calc_unit_list = new List<CalcUnit>() { new CalcUnit()
            {
                group1_property_initialized = 1,
                group2_property_initialized = 0,
                group3_property_initialized = 0,
                unit_count = 1,
                unit_max_bonus_for_card_percent = 100,
                unit_max_bonus_for_pay_percent = 100,
                unit_max_discount_percent = 100,
                unit_name = "оплата сертификатом",
                unit_num = 1,
                unit_pack_count = 1,
                unit_type = 0,
                unit_value = certTransactionStartServiceParam.trans_sum,
            }
            };

            cardTransactionStartServiceParam.bonus_for_pay = certTransactionStartServiceParam.sum_for_pay;
            cardTransactionStartServiceParam.business_id = certTransactionStartServiceParam.business_id;
            cardTransactionStartServiceParam.calc_unit_list = new List<CalcUnit>(calc_unit_list);
            cardTransactionStartServiceParam.card_input_mode = certTransactionStartServiceParam.card_input_mode;
            cardTransactionStartServiceParam.card_num = certTransactionStartServiceParam.card_num;
            cardTransactionStartServiceParam.is_test_mode = certTransactionStartServiceParam.is_test_mode;
            cardTransactionStartServiceParam.session_id = certTransactionStartServiceParam.session_id;

            try
            {
                var res1 = cardTransactionStart(cardTransactionStartServiceParam, false, true);

                CertTransactionResult certTransactionResult = new CertTransactionResult();
                certTransactionResult.error = res1.error;
                certTransactionResult.trans_id = res1.trans_id;
                certTransactionResult.trans_sum = res1.error == null ? res1.calc_unit_result.Where(ss => ss.unit_type == 1).Select(ss => ss.unit_value).FirstOrDefault() : 0;
                certTransactionResult.cert_sum_used = res1.error == null ? res1.calc_unit_result.Where(ss => ss.unit_type == 1).Select(ss => ss.unit_value_discount).FirstOrDefault() : 0;
                certTransactionResult.cert_sum_left = ((res1.error == null) && (res1.card_nominals != null)) ? res1.card_nominals.Where(ss => ss.unit_type == (int)Enums.CardUnitTypeEnum.MONEY).Select(ss => ss.unit_value.GetValueOrDefault(0)).FirstOrDefault() : 0;
                certTransactionResult.trans_sum_left = res1.error == null ? res1.calc_unit_result.Where(ss => ss.unit_type == 1).Select(ss => ss.unit_value_with_discount).FirstOrDefault() : 0;
                certTransactionResult.warn = res1.warn;
                certTransactionResult.print_info = res1.error == null ? res1.print_info : null;

                return certTransactionResult;
            }
            catch (Exception ex)
            {
                return new CertTransactionResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }

            #endregion
        }

        #endregion

        #region CertTransactionCommit

        public AuDiscountBaseResult CertTransactionCommit(CardTransactionCommitServiceParam cardTransactionServiceParam)
        {
            try
            {
                return certTransactionCommit(cardTransactionServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult certTransactionCommit(CardTransactionCommitServiceParam cardTransactionServiceParam)
        {
            return CardTransactionCommit(cardTransactionServiceParam);
        }

        #endregion

        #region CertTransactionRollback

        public AuDiscountBaseResult CertTransactionRollback(CardTransactionRollbackServiceParam cardTransactionServiceParam)
        {
            try
            {
                return certTransactionRollback(cardTransactionServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private AuDiscountBaseResult certTransactionRollback(CardTransactionRollbackServiceParam cardTransactionServiceParam)
        {
            return CardTransactionRollback(cardTransactionServiceParam);
        }

        #endregion

        #region CertTransactionRefund

        public CertTransactionResult CertTransactionRefund(CertTransactionRefundServiceParam certTransactionServiceParam)
        {
            try
            {
                return certTransactionRefund(certTransactionServiceParam);
            }
            catch (Exception ex)
            {
                return new CertTransactionResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }


        private CertTransactionResult certTransactionRefund(CertTransactionRefundServiceParam certTransactionServiceParam)
        {
            CardTransactionRefundServiceParam cardTransactionRefundServiceParam = new CardTransactionRefundServiceParam();
            cardTransactionRefundServiceParam.business_id = certTransactionServiceParam.business_id;
            cardTransactionRefundServiceParam.session_id = certTransactionServiceParam.session_id;
            cardTransactionRefundServiceParam.trans_id = certTransactionServiceParam.trans_id;

            using (AuMainDb dbContext = new AuMainDb())
            {
                List<card_transaction_detail> card_transaction_detail_list = dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == certTransactionServiceParam.trans_id).ToList();
                List<CalcUnit> calc_unit_list = card_transaction_detail_list.Select(ss => new CalcUnit()
                    {
                        unit_count = ss.unit_count.GetValueOrDefault(0),
                        unit_max_bonus_for_card_percent = ss.unit_max_bonus_for_card_percent.GetValueOrDefault(0),
                        unit_max_bonus_for_pay_percent = ss.unit_max_bonus_for_pay_percent.GetValueOrDefault(0),
                        unit_max_discount_percent = ss.unit_max_discount_percent.GetValueOrDefault(0),
                        unit_name = ss.unit_name,
                        unit_num = ss.unit_num.GetValueOrDefault(0),
                        unit_pack_count = 1,
                        unit_type = ss.unit_type.GetValueOrDefault(0),
                        unit_value = ss.unit_value.GetValueOrDefault(0),
                    }
                    ).ToList();

                cardTransactionRefundServiceParam.calc_unit_list = new List<CalcUnit>(calc_unit_list);
            }

            //var res1 = CardTransactionRefund(cardTransactionRefundServiceParam);
            try
            {
                var res1 = cardTransactionRefund(cardTransactionRefundServiceParam, true);

                CertTransactionResult certTransactionResult = new CertTransactionResult();
                certTransactionResult.error = res1.error;
                certTransactionResult.trans_id = res1.trans_id;
                certTransactionResult.trans_sum = res1.error == null ? res1.calc_unit_result.Where(ss => ss.unit_type == 1).Select(ss => ss.unit_value).FirstOrDefault() : 0;
                certTransactionResult.cert_sum_used = res1.error == null ? res1.calc_unit_result.Where(ss => ss.unit_type == 1).Select(ss => ss.unit_value_discount).FirstOrDefault() : 0;
                certTransactionResult.cert_sum_left = ((res1.error == null) && (res1.card_nominals != null)) ? res1.card_nominals.Where(ss => ss.unit_type == (int)Enums.CardUnitTypeEnum.MONEY).Select(ss => ss.unit_value.GetValueOrDefault(0)).FirstOrDefault() : 0;
                certTransactionResult.trans_sum_left = res1.error == null ? res1.calc_unit_result.Where(ss => ss.unit_type == 1).Select(ss => ss.unit_value_with_discount).FirstOrDefault() : 0;
                certTransactionResult.print_info = res1.error == null ? res1.print_info : null;
                certTransactionResult.warn = res1.warn;

                return certTransactionResult;
            }
            catch (Exception ex)
            {
                return new CertTransactionResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }

        }

        #endregion
    }
}