﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {

        public AuDiscountBaseResult UpdateCardHolder(ResetCardHolderServiceParam updateCardHolderServiceParam)
        {
            try
            {
                return updateCardHolder(updateCardHolderServiceParam);
            }
            catch (Exception ex)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuDiscountBaseResult updateCardHolder(ResetCardHolderServiceParam updateCardHolderServiceParam)
        {
            if ((updateCardHolderServiceParam == null) || (String.IsNullOrEmpty(updateCardHolderServiceParam.card_num)))
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            long card_num_long = 0;
            string card_num_string = updateCardHolderServiceParam.card_num.Trim().ToLower();
            bool parseOk = long.TryParse(card_num_string, out card_num_long);
            if (!parseOk)
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер карты") };

            LogSession ls = getSession(updateCardHolderServiceParam);
            if (ls.error != null)
                return new AuDiscountBaseResult() { error = ls.error };

            GetCardServiceParam getCardServiceParam = new GetCardServiceParam() 
            {
                business_id = updateCardHolderServiceParam.business_id,
                session_id = updateCardHolderServiceParam.session_id,
                card_num = updateCardHolderServiceParam.card_num,
            };
            Card card = getCard(getCardServiceParam, false, null);
            if (card.error != null)
                return new AuDiscountBaseResult() { error = card.error };

            long business_id = long.Parse(updateCardHolderServiceParam.business_id);
            string user_name = ls.user_name;
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            card card_db = dbContext.card.Where(ss => ss.card_id == card.card_id).FirstOrDefault();
            //if (!card_db.curr_holder_id.HasValue)
            //return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "У карты нет владельца") };

            card_holder curr_card_holder = dbContext.card_holder.Where(ss => ss.card_holder_id == card_db.curr_holder_id).FirstOrDefault();
            if (curr_card_holder == null)
            {
                return new AuDiscountBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден владелец карты № " + card_num_string) };
            }
            
            /*
            card_holder_card_rel card_holder_card_rel = dbContext.card_holder_card_rel.Where(ss => ss.card_id == card.card_id && ss.card_holder_id == card_db.curr_holder_id).FirstOrDefault();
            if (card_holder_card_rel != null)
                card_holder_card_rel.date_end = today;            
            */

            string card_holder_comment = curr_card_holder.comment;
            DateTime? card_holder_date_birth = curr_card_holder.date_birth;
            //
            string card_holder_surname = updateCardHolderServiceParam.card_holder_surname;
            string card_holder_name = updateCardHolderServiceParam.card_holder_name;
            string card_holder_fname = updateCardHolderServiceParam.card_holder_fname;
            string card_holder_email = updateCardHolderServiceParam.card_holder_email;
            string card_holder_address = updateCardHolderServiceParam.card_holder_address;
            string card_holder_phone_num = updateCardHolderServiceParam.card_holder_phone_num;
            if ((!String.IsNullOrWhiteSpace(card_holder_surname)) || (!String.IsNullOrWhiteSpace(card_holder_name)) || (!String.IsNullOrWhiteSpace(card_holder_fname)))
            {
                curr_card_holder.comment = card_holder_comment;
                curr_card_holder.date_birth = card_holder_date_birth;
                curr_card_holder.card_holder_surname = card_holder_surname;
                curr_card_holder.card_holder_name = card_holder_name;
                curr_card_holder.card_holder_fname = card_holder_fname;
                curr_card_holder.email = card_holder_email;
                curr_card_holder.address = card_holder_address;
                curr_card_holder.phone_num = card_holder_phone_num;
            }
            
            dbContext.SaveChanges();

            return new AuDiscountBaseResult() { result = "0" };
        }       
    }
}