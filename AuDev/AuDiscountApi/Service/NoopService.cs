﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuDiscountApi.Models;
using AuDiscountApi.Log;

namespace AuDiscountApi.Service
{
    public partial class AuDiscountMainService
    {
        #region Noop

        public void Noop()
        {
            try
            {
                noop();
            }
            catch (Exception ex)
            {
                //
            }
        }

        private void noop()
        {
            var version_db = dbContext.version_db.FirstOrDefault();
        }

        #endregion
    }
}