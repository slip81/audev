﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AuDev.Common.Extensions
{
    public static class BoolExtensions
    {
        public static string YesNoString(this bool boolValue)
        {
            return boolValue ? "Да" : "Нет";
        }
    }
}