﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AuDev.Common.Extensions
{
    public static class DictionaryExtensions
    {
        public static string GetStringValue(this Dictionary<string, string> dic, string value)
        {
            return (dic != null && dic.ContainsKey(value)) ? dic[value] : null;
        }

        public static bool GetBoolValue(this Dictionary<string, string> dic, string value)
        {
            return dic != null ? (dic.ContainsKey(value) && !String.IsNullOrEmpty(dic[value])) : false;
        }

        public static List<string> GetListValueFromJSArray(this Dictionary<string, string> dic, string value)
        {
            return (dic != null && dic.ContainsKey(value)) ? dic[value].Replace(']', ' ').Replace('[', ' ').Split(',').ToList().Where(ss => !String.IsNullOrWhiteSpace(ss)).ToList() : null;
        }

        public static double? GetDoubleValue(this Dictionary<string, string> dic, string value)
        {
            return (dic != null && dic.ContainsKey(value) && !String.IsNullOrWhiteSpace(dic[value])) ? (double?)(double.Parse(dic[value])) : null;
        }

        public static DateTime? GetDateTimeValue(this Dictionary<string, string> dic, string value)
        {
            return (dic != null && dic.ContainsKey(value) && !String.IsNullOrWhiteSpace(dic[value])) ? (DateTime?)(DateTime.Parse(dic[value])) : null;
        }

        public static string GetSplitterSize(this Dictionary<string, string> dic, string value, string defSize)
        {
            string result = (dic != null && dic.ContainsKey(value)) ? dic[value] : null;
            result = ((!String.IsNullOrWhiteSpace(result)) && (result.IndexOf(".") > 0)) ? result.Substring(0, result.IndexOf(".")) : result;
            //result = String.IsNullOrWhiteSpace(result) ? "60%" : (result.EndsWith("px") ? result : result + "px");
            result = String.IsNullOrWhiteSpace(result) ? defSize : ((result.EndsWith("px") || result.EndsWith("%")) ? result : result + "px");
            return result;
        }

    }
}