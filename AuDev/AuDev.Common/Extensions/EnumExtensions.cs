﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;

namespace AuDev.Common.Extensions
{
    public static class EnumExtensions
    {
        public static T GetAttribute<T>(this Enum enumValue)
        where T : Attribute
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<T>();
        }

        public static string EnumName<T>(this int? intValue)
        {
            #region MainObjectType
            if (typeof(T) == typeof(Enums.MainObjectType))
            {
                Enums.MainObjectType mainObj = Enums.MainObjectType.NONE;
                try
                {
                    mainObj = (Enums.MainObjectType)intValue;
                    return mainObj.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region ErrCodeEnum
            if (typeof(T) == typeof(Enums.ErrCodeEnum))
            {
                Enums.ErrCodeEnum errCode = Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED;
                try
                {
                    errCode = (Enums.ErrCodeEnum)intValue;
                    return errCode.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region ExchangeItemEnum
            if (typeof(T) == typeof(Enums.ExchangeItemEnum))
            {
                Enums.ExchangeItemEnum exType = Enums.ExchangeItemEnum.Avg;
                try
                {
                    exType = (Enums.ExchangeItemEnum)intValue;
                    return exType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CardRestrType
            if (typeof(T) == typeof(Enums.CardRestrType))
            {
                Enums.CardRestrType restrType = Enums.CardRestrType.TRANS_SUM;
                try
                {
                    restrType = (Enums.CardRestrType)intValue;
                    return restrType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CardRestrPeriodType
            else if (typeof(T) == typeof(Enums.CardRestrPeriodType))
            {
                Enums.CardRestrPeriodType restrPeriodType = Enums.CardRestrPeriodType.DAYLY;
                try
                {
                    restrPeriodType = (Enums.CardRestrPeriodType)intValue;
                    return restrPeriodType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CardRestrValueType
            else if (typeof(T) == typeof(Enums.CardRestrValueType))
            {
                Enums.CardRestrValueType restrValueType = Enums.CardRestrValueType.ABSOLUTE;
                try
                {
                    restrValueType = (Enums.CardRestrValueType)intValue;
                    return restrValueType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CardRestrValueActivationMode
            else if (typeof(T) == typeof(Enums.CardRestrValueActivationMode))
            {
                Enums.CardRestrValueActivationMode activationMode = Enums.CardRestrValueActivationMode.NOW;
                try
                {
                    activationMode = (Enums.CardRestrValueActivationMode)intValue;
                    return activationMode.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CardRestrPaymentType
            else if (typeof(T) == typeof(Enums.CardRestrPaymentType))
            {
                Enums.CardRestrPaymentType paymentType = Enums.CardRestrPaymentType.ALL;
                try
                {
                    paymentType = (Enums.CardRestrPaymentType)intValue;
                    return paymentType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CardRestrReadType
            else if (typeof(T) == typeof(Enums.CardRestrReadType))
            {
                Enums.CardRestrReadType cardReadType = Enums.CardRestrReadType.ALL;
                try
                {
                    cardReadType = (Enums.CardRestrReadType)intValue;
                    return cardReadType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CardRestrRuleType
            else if (typeof(T) == typeof(Enums.CardRestrRuleType))
            {
                Enums.CardRestrRuleType ruleType = Enums.CardRestrRuleType.NONE;
                try
                {
                    ruleType = (Enums.CardRestrRuleType)intValue;
                    return ruleType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CardRestrValueRelPeriodType
            else if (typeof(T) == typeof(Enums.CardRestrValueRelPeriodType))
            {
                Enums.CardRestrValueRelPeriodType valType = Enums.CardRestrValueRelPeriodType.NONE;
                try
                {
                    valType = (Enums.CardRestrValueRelPeriodType)intValue;
                    return valType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region ExchangeFormat
            else if (typeof(T) == typeof(Enums.ExchangeFormat))
            {
                Enums.ExchangeFormat exFormat = Enums.ExchangeFormat.XML_FILE;
                try
                {
                    exFormat = (Enums.ExchangeFormat)intValue;
                    return exFormat.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region ExchangeTransport
            else if (typeof(T) == typeof(Enums.ExchangeTransport))
            {
                Enums.ExchangeTransport exTransport = Enums.ExchangeTransport.EMAIL;
                try
                {
                    exTransport = (Enums.ExchangeTransport)intValue;
                    return exTransport.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region ArcType
            else if (typeof(T) == typeof(Enums.ArcType))
            {
                Enums.ArcType arcType = Enums.ArcType.NONE;
                try
                {
                    arcType = (Enums.ArcType)intValue;
                    return arcType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region VedColumn
            else if (typeof(T) == typeof(Enums.VedColumn))
            {
                Enums.VedColumn vedCol = Enums.VedColumn.COMM_NAME;
                try
                {
                    vedCol = (Enums.VedColumn)intValue;
                    return vedCol.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region ExchangeTaskState
            else if (typeof(T) == typeof(Enums.ExchangeTaskState))
            {
                Enums.ExchangeTaskState exchState = Enums.ExchangeTaskState.CREATED;
                try
                {
                    exchState = (Enums.ExchangeTaskState)intValue;
                    return exchState.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CrmGroup
            else if (typeof(T) == typeof(Enums.CrmGroup))
            {
                Enums.CrmGroup crmGroup = Enums.CrmGroup.BUCKET;
                try
                {
                    crmGroup = (Enums.CrmGroup)intValue;
                    return crmGroup.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CrmTaskOperation
            else if (typeof(T) == typeof(Enums.CrmTaskOperation))
            {
                Enums.CrmTaskOperation crmTaskOperation = Enums.CrmTaskOperation.TO_BUCKET;
                try
                {
                    crmTaskOperation = (Enums.CrmTaskOperation)intValue;
                    return crmTaskOperation.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CrmUserTaskRole
            else if (typeof(T) == typeof(Enums.CrmUserTaskRole))
            {
                Enums.CrmUserTaskRole crmUserTaskRole = Enums.CrmUserTaskRole.ALL;
                try
                {
                    crmUserTaskRole = (Enums.CrmUserTaskRole)intValue;
                    return crmUserTaskRole.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CabGridColumn_TaskList
            else if (typeof(T) == typeof(Enums.CabGridColumn_TaskList))
            {
                Enums.CabGridColumn_TaskList cabGridCol = Enums.CabGridColumn_TaskList.task_id;
                try
                {
                    cabGridCol = (Enums.CabGridColumn_TaskList)intValue;
                    return cabGridCol.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CvaSendStateEnum
            else if (typeof(T) == typeof(Enums.CvaSendStateEnum))
            {
                Enums.CvaSendStateEnum sendState = Enums.CvaSendStateEnum.NOT_SENT;
                try
                {
                    sendState = (Enums.CvaSendStateEnum)intValue;
                    return sendState.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region DeliveryState
            else if (typeof(T) == typeof(Enums.DeliveryState))
            {
                Enums.DeliveryState deliveryState = Enums.DeliveryState.READY;
                try
                {
                    deliveryState = (Enums.DeliveryState)intValue;
                    return deliveryState.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region CardUnitTypeEnum
            else if (typeof(T) == typeof(Enums.CardUnitTypeEnum))
            {
                Enums.CardUnitTypeEnum unitType = Enums.CardUnitTypeEnum.BONUS_PERCENT;
                try
                {
                    unitType = (Enums.CardUnitTypeEnum)intValue;
                    return unitType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            #region RequestStateEnum
            else if (typeof(T) == typeof(Enums.RequestStateEnum))
            {
                Enums.RequestStateEnum requestState = Enums.RequestStateEnum.NOT_CONFIRMED;
                try
                {
                    requestState = (Enums.RequestStateEnum)intValue;
                    return requestState.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            

            #region RequestTypeEnum
            else if (typeof(T) == typeof(Enums.RequestTypeEnum))
            {
                Enums.RequestTypeEnum requestType = Enums.RequestTypeEnum.RECORDS_ALL;
                try
                {
                    requestType = (Enums.RequestTypeEnum)intValue;
                    return requestType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            
            
            #region LogicType
            else if (typeof(T) == typeof(Enums.LogicType))
            {
                Enums.LogicType logicType = Enums.LogicType.PLUS;
                try
                {
                    logicType = (Enums.LogicType)intValue;
                    return logicType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            
            
            #region CardLimitType
            else if (typeof(T) == typeof(Enums.CardLimitType))
            {
                Enums.CardLimitType limitType = Enums.CardLimitType.BonusDaily;
                try
                {
                    limitType = (Enums.CardLimitType)intValue;
                    return limitType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            

            #region NotifyTransportTypeEnum
            else if (typeof(T) == typeof(Enums.NotifyTransportTypeEnum))
            {
                Enums.NotifyTransportTypeEnum notifyTransportType = Enums.NotifyTransportTypeEnum.ICQ;
                try
                {
                    notifyTransportType = (Enums.NotifyTransportTypeEnum)intValue;
                    return notifyTransportType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            

            #region CampaignStateEnum
            else if (typeof(T) == typeof(Enums.CampaignStateEnum))
            {
                Enums.CampaignStateEnum campaignStateEnum = Enums.CampaignStateEnum.ACTIVE;
                try
                {
                    campaignStateEnum = (Enums.CampaignStateEnum)intValue;
                    return campaignStateEnum.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            

            #region PrjClaimStateTypeEnum
            else if (typeof(T) == typeof(Enums.PrjClaimStateTypeEnum))
            {
                Enums.PrjClaimStateTypeEnum prjClaimStateTypeEnum = Enums.PrjClaimStateTypeEnum.UNDEFINED;
                try
                {
                    prjClaimStateTypeEnum = (Enums.PrjClaimStateTypeEnum)intValue;
                    return prjClaimStateTypeEnum.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            
            
            #region UndMatchStateEnum
            else if (typeof(T) == typeof(Enums.UndMatchStateEnum))
            {
                Enums.UndMatchStateEnum undMatchStateEnum = Enums.UndMatchStateEnum.NONE;
                try
                {
                    undMatchStateEnum = (Enums.UndMatchStateEnum)intValue;
                    return undMatchStateEnum.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            

            #region UndDocImportStateEnum
            else if (typeof(T) == typeof(Enums.UndDocImportStateEnum))
            {
                Enums.UndDocImportStateEnum undDocImportStateEnum = Enums.UndDocImportStateEnum.WAIT;
                try
                {
                    undDocImportStateEnum = (Enums.UndDocImportStateEnum)intValue;
                    return undDocImportStateEnum.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            
            
            #region WaTaskStateEnum
            else if (typeof(T) == typeof(Enums.WaTaskStateEnum))
            {
                Enums.WaTaskStateEnum waTaskStateEnum = Enums.WaTaskStateEnum.ISSUED_CLIENT;
                try
                {
                    waTaskStateEnum = (Enums.WaTaskStateEnum)intValue;
                    return waTaskStateEnum.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            
                        
            #region ProgrammParamType
            else if (typeof(T) == typeof(Enums.ProgrammParamType))
            {
                Enums.ProgrammParamType programmParamTypeEnum = Enums.ProgrammParamType.STRING_VAL;
                try
                {
                    programmParamTypeEnum = (Enums.ProgrammParamType)intValue;
                    return programmParamTypeEnum.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion
            
            #region PrjUserGroupPeriodTypeEnum
            else if (typeof(T) == typeof(Enums.PrjUserGroupPeriodTypeEnum))
            {
                Enums.PrjUserGroupPeriodTypeEnum prjUserGroupPeriodTypeEnum = Enums.PrjUserGroupPeriodTypeEnum.TODAY;
                try
                {
                    prjUserGroupPeriodTypeEnum = (Enums.PrjUserGroupPeriodTypeEnum)intValue;
                    return prjUserGroupPeriodTypeEnum.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion            

            return "[-]";
        }

        public static string EnumName<T>(this int intValue)
        {
            return ((int?)intValue).EnumName<T>();
        }

        public static string EnumName<T>(this long intValue)
        {
            return ((int?)intValue).EnumName<T>();
        }

        public static string EnumDataType<T>(this int? intValue)
        {
            #region MainObjectType
            if (typeof(T) == typeof(Enums.MainObjectType))
            {
                Enums.MainObjectType mainObj = Enums.MainObjectType.NONE;
                try
                {
                    mainObj = (Enums.MainObjectType)intValue;
                    return mainObj.GetAttribute<DataTypeAttribute>().CustomDataType;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            return "[-]";
        }

        public static string EnumDataType<T>(this int intValue)
        {
            return ((int?)intValue).EnumDataType<T>();
        }

        public static string EnumDataType<T>(this long intValue)
        {
            return ((int?)intValue).EnumDataType<T>();
        }

    }
}