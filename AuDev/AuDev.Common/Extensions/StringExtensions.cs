﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AuDev.Common.Extensions
{
    public static class StringExtensions
    {
        public static Dictionary<string, string> SexSpecificWords 
        { 
            get
            {
                if (sexSpecificWords == null)
                {
                    sexSpecificWords = new Dictionary<string, string>();
                    sexSpecificWords.Add("создал", "создала");
                    sexSpecificWords.Add("изменил", "изменила");
                    sexSpecificWords.Add("удалил", "удалила");
                    sexSpecificWords.Add("добавил", "добавила");
                    sexSpecificWords.Add("перенес", "перенесла");
                    sexSpecificWords.Add("поставил", "поставила");
                    sexSpecificWords.Add("убрал", "убрала");
                    sexSpecificWords.Add("восстановил", "восстановила");
                    sexSpecificWords.Add("принял", "приняла");
                    sexSpecificWords.Add("не принял", "не приняла");
                }
                return sexSpecificWords;
            }
        }
        private static Dictionary<string, string> sexSpecificWords;

        public static string Left(this string str, int length)
        {
            return str.Substring(0, Math.Min(length, str.Length));
        }

        public static string Right(this string str, int length)
        {
            if (length >= str.Length)
                return str;
            return str.Substring(str.Length - length);
        }

        public static string ExceptChars(this string str, IEnumerable<char> toExclude)
        {
            StringBuilder sb = new StringBuilder(str.Length);
            for (int i = 0; i < str.Length; i++)
            {
                char c = str[i];
                if (!toExclude.Contains(c))
                    sb.Append(c);
            }
            return sb.ToString();
        }

        public static string DoubleQouted(this string s)
        {
            return (String.IsNullOrEmpty(s) ? "\"\"" : "\"" + s + "\"");
        }

        public static string CutToLengthWithDots(this string val, int len)
        {
            string res = val;
            if (String.IsNullOrEmpty(res))
                return res;

            int val_len = res.Length;
            if (val_len <= len)
                return res;

            return res.Substring(0, len) + "...";
        }

        public static DateTime? ToDateTime(this string s)
        {            
            DateTime _date;
            return DateTime.TryParse(s, out _date) ? (DateTime?)_date : null;
        }

        public static DateTime? ToDateTimeStrict(this string s, string format)
        {
            DateTime _date;
            return DateTime.TryParseExact(s, format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out _date) ? (DateTime?)_date : null;
        }

        public static string ToSex(this string str, bool isMale)
        {
            if (String.IsNullOrEmpty(str))
                return str;
            string strParsed = str.Trim().ToLower();
            return ((!isMale) && (SexSpecificWords.ContainsKey(strParsed))) ? SexSpecificWords[strParsed] : str;
        }

        public static string RemoveLineEndings(this string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return value;         
            return value.Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty);
        }

        public static string RemoveWhitespace(this string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return value;
            return new string(value.ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }

        public static string TrimIfNotNull(this string value)
        {
            return value != null ? value.Trim() : "";
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
}