﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AuDev.Common.Extensions
{
    public static class ListExtensions
    {
        public static bool IsEmpty(this List<string> lst)
        {
            return ((lst == null) || (lst.Count <= 0));
        }

        public static bool IsEmpty(this List<string> lst, int element_index)
        {
            if (((lst == null) || (lst.Count <= 0)))
                return true;
            
            return String.IsNullOrEmpty(lst[element_index]);
        }

    }
}