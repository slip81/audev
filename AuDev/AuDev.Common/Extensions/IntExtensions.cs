﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AuDev.Common.Extensions
{
    public static class IntExtensions
    {
        public static string ToExactLength(this int val, int len, string symbol)
        {

            string res = val.ToString();

            int val_len = res.Length;
            if (val_len >= len)
                return res;
            
            while (val_len < len)
            {
                res = symbol + res;
                val_len++;
            }

            return res;
        }

        public static string ToHtmlTextLinkExactLength(this int? val, int len)
        {
            string res_begin = "<a href='/' onclick='return false;'>";
            string res_end = "</a>";
            string res = val.ToString();

            int val_len = res.Length;
            if (val_len >= len)
                return res_begin + res + res_end;

            res = res_begin + res + res_end;
            bool add_to_left = false;
            while (val_len < len)
            {
                if (add_to_left)
                {
                    res = "&nbsp;" + res;
                }
                else
                {
                    res = res + "&nbsp;";
                }
                add_to_left = !add_to_left;
                val_len++;
            }

            return res;
        }
    }
}