﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AuDev.Common.Util
{
    public class KendoFilterMain
    {
        public KendoFilterMain()
        {
            //
        }

        public List<KendoFilterItem> filters { get; set; }
        public string logic { get; set; }

    }

    public class KendoFilterItem
    {
        public KendoFilterItem()
        {
            //
        }

        public List<KendoFilterItemValue> filters { get; set; }
        public string logic { get; set; }

    }

    public class KendoFilterItemValue
    {
        public KendoFilterItemValue()
        {
            //
        }

        [JsonProperty("operator")]
        public string Operator { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonProperty("field")]
        public string Field { get; set; }

    }
}