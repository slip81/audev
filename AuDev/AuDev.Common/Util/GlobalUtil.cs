﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Hosting;
using System.Diagnostics;
using System.Text.RegularExpressions;
using AuDev.Common.Extensions;
using AuDev.Common.Network;
using System.Data.Entity.Validation;

namespace AuDev.Common.Util
{
    public static class GlobalUtil
    {

        #region From Discount

        public static decimal GetPercent_Value(decimal b, decimal a)
        {
            if (b == 0) return 0;
            return (decimal)((ToTwoDigitsAfterComma(a) * ToTwoDigitsAfterComma(b)) / 100M);
            //return (decimal)(b - ((a * b) / 100M));
        }

        public static decimal GetPercent_ResultWithPercent(decimal b, decimal a)
        {
            if (b == 0) return 0;           
            //return (decimal)((a * b) / 100M);
            return (decimal)(ToTwoDigitsAfterComma(b) - ((ToTwoDigitsAfterComma(a) * ToTwoDigitsAfterComma(b)) / 100M));
        }

        public static decimal GetPercent_Summa(decimal b, decimal a)
        {
            if (a == 0) return 0;
            if (b == 0) return 0;
            return (decimal)((ToTwoDigitsAfterComma(b) / ToTwoDigitsAfterComma(a)) * 100M);
        }

        public static decimal ToTwoDigitsAfterComma(decimal? val)
        {
            return !val.HasValue ? 0 : Math.Round((decimal)val, 2);
        }


        #region old
        /*
        public static bool ListContainsName(List<string> list, string name, string separator = null)
        {
            if ((list == null) || (list.Count <= 0) || (String.IsNullOrWhiteSpace(name)))
                return false;

            if (String.IsNullOrWhiteSpace(separator))
            {                
                return list.Where(ss => ss.Trim().ToLower().Equals(name.Trim().ToLower())).Any();
            }
            else
            {
                var nameParts = name.Split(separator.ToCharArray()).Where(ss => !String.IsNullOrWhiteSpace(ss)).ToList();
                for (var i = 0; i < nameParts.Count; i++)
                {
                    if (list.Where(ss => ss.Trim().ToLower().Equals(nameParts[i].Trim().ToLower())).Any())
                        return true;
                }
                return false;
            }
        }
        */
        #endregion

        /// <summary>
        /// Проверка, есть ли в списке наименований заданное
        /// </summary>
        /// <param name="list">список наименований с разделителем</param>
        /// <param name="name">заданное наименование</param>
        /// <param name="separator">разделитель списка наименований</param>
        /// <returns></returns>
        public static bool ListContainsName(string list, string name, string separator = null)
        {
            if ((String.IsNullOrWhiteSpace(list)) || (String.IsNullOrWhiteSpace(name)))
                return false;

            if (String.IsNullOrWhiteSpace(separator))
            {
                return list.Trim().ToLower().Equals(name.Trim().ToLower());
            }
            else
            {
                var listParts = list.Split(separator.ToCharArray()).Where(ss => !String.IsNullOrWhiteSpace(ss)).ToList();
                for (var i = 0; i < listParts.Count; i++)
                {
                    if (listParts[i].Trim().ToLower().Equals(name.Trim().ToLower()))
                        return true;
                }
                return false;
            }
        }

        public static string ExceptionInfo(Exception e)
        {
            if (e == null)
                return "";

            string res = e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                );

            if (e is DbEntityValidationException)
            {
                DbEntityValidationException eEnt = (DbEntityValidationException)e;
                if (eEnt.EntityValidationErrors != null)
                {
                    res += " [";
                    foreach (var validationErrors in eEnt.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            res += validationError.PropertyName + ": " + validationError.ErrorMessage + ";";
                        }
                    }
                    res += "]";
                }
            }

            return res;
        }

        public static string GetKassaId()
        {
            return "8E49231E-4F59-4ABF-9591-E8ABF4C11194";
        }

        public static bool IsKassa(string app_id)
        {
            return ((!String.IsNullOrEmpty(app_id)) && (GetKassaId().Equals(app_id.Trim())));
        }

        #endregion

        #region From Defect

        public static IEnumerable<T> GetEnumValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static string ReplaceRusLetters(string s)
        {
            var rusLetters = new[] { 'А', 'С', 'Е', 'Т', 'Н', 'О', 'Р', 'К', 'Х', 'В', 'М', 'У' };
            var engLetters = new[] { 'A', 'C', 'E', 'T', 'H', 'O', 'P', 'K', 'X', 'B', 'M', 'Y' };
            int cnt = 12;

            for (int i = 0; i < cnt; i++)
            {
                s = s.Replace(rusLetters[i], engLetters[i]);
            }

            return s;
        }

        public static string CompressString(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }

            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Convert.ToBase64String(gZipBuffer);
        }

        /// <summary>
        /// Decompresses the string.
        /// </summary>
        /// <param name="compressedText">The compressed text.</param>
        /// <returns></returns>
        public static string DecompressString(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        #endregion

        #region From Exchange

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        #endregion

        #region From Cabinet

        public static List<SimpleCombo> CreateSimpleCombo<T>()
        {
            List<SimpleCombo> list = new List<SimpleCombo>();
            var enumValues = GlobalUtil.GetEnumValues<T>();
            foreach (var enumValue in enumValues)
            {
                int intValue = Convert.ToInt32(enumValue);
                list.Add(new SimpleCombo()
                {
                    obj_id = intValue,
                    obj_name = intValue.EnumName<T>(),
                });
            }
            return list;
        }

        public static bool HasAllEmptyProperties(object obj)
        {
            if (obj == null)
                return true;

            var type = obj.GetType();
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var hasProperty = properties.Select(x => x.GetValue(obj, null))
                                        .Any(y => y != null && !String.IsNullOrWhiteSpace(y.ToString()));
            return !hasProperty;
        }

        public static bool HasAttribute(PropertyInfo prop, string attributeName)
        {
            // look for an attribute that takes one constructor argument
            foreach (CustomAttributeData attribData in prop.GetCustomAttributesData())
            {
                string typeName = attribData.Constructor.DeclaringType.Name;
                if (attribData.ConstructorArguments.Count == 0 &&
                                    (typeName == attributeName || typeName == attributeName + "Attribute"))
                {
                    return true;
                }
            }
            return false;
        }

        public static string DifffBetweenDates(DateTime? date1, DateTime? date2)
        {
            if ((!date1.HasValue) || (!date2.HasValue))
                return "";

            var days = ((TimeSpan)(((DateTime)date2).Subtract((DateTime)date1))).Days;
            var hours = ((TimeSpan)(((DateTime)date2).Subtract((DateTime)date1))).Hours;
            var minutes = ((TimeSpan)(((DateTime)date2).Subtract((DateTime)date1))).Minutes;

            var hours_and_minutes = hours > 0 ? (hours.ToString() + " ч " + minutes.ToString() + " мин") : (minutes.ToString() + " мин");
            return (days > 0 ? (days.ToString() + " д " + hours_and_minutes) : hours_and_minutes).Trim();
        }

        public static void WriteFileFromStream(Stream stream, string location)
        {
            using (FileStream fileToSave = new FileStream(location, FileMode.Create))
            {
                stream.CopyTo(fileToSave);
            }
        }

        public static string CheckCode(string version_name, string exe_name, string hash)
        {
            string infoStr = hash;
            try
            {
                string[] tokens = infoStr.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens[1] != version_name)
                {
                    //status = RegistrationStatus.InvalidVersion;
                    return String.Empty;
                }
                if (tokens[3] != exe_name)
                {
                    //status = RegistrationStatus.InvalidExeName;
                    return String.Empty;
                }
                string acc = String.Empty;
                if (tokens.Length > 4)
                    acc = tokens[4];

                string parameters = String.Format("{0} {1} {2} {3} {4}", tokens[0], tokens[1], tokens[3], acc, tokens[2]);
                string ExeName = "App_Data\\md5New.exe";
                var vers310 = new System.Version(3, 0, 0, 10);
                var curreVers = new System.Version(version_name);
                if (version_name == "3.0.0.10")
                    ExeName = "App_Data\\md5_3_10.exe";
                if (curreVers > vers310)
                    ExeName = "App_Data\\md5_3_10.exe";
                if (version_name == "2.9.5.13")
                    ExeName = "App_Data\\md5.exe";


                var startInfo = new ProcessStartInfo(HostingEnvironment.ApplicationPhysicalPath + ExeName,
                                                     parameters)
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true
                };
                Process p = Process.Start(startInfo);
                string output = p.StandardOutput.ReadToEnd().Substring(0, 23);
                p.WaitForExit();
                //status = RegistrationStatus.RightCode;
                return output;

            }
            catch (Exception)
            {
                //status = RegistrationStatus.InvalidCode;
                return String.Empty;
            }
        }

        public static Tuple<string, string, string, string> BuildStorageFileName(string file_name, int user_id, int version)
        {
            DateTime now = DateTime.Now;
            string fileName_orig = Path.GetFileName(file_name);                        
            string fileName_withoutExtention = Path.GetFileNameWithoutExtension(file_name);
            string fileName_extention = Path.GetExtension(file_name);
            
            /*
            string fileName_new = Path.ChangeExtension(fileName_withoutExtention + "_" + now.ToString("ddMMyyHHmmss")
                + user_id.ToString().PadLeft(4, '0')
                + version.ToString().PadLeft(2, '0'),
                fileName_extention);
            */
            string fileName_new = (fileName_withoutExtention + "_" + now.ToString("ddMMyyHHmmss")
                + user_id.ToString().PadLeft(4, '0')
                + version.ToString().PadLeft(2, '0')) + '.' + fileName_extention.TrimStart('.');

            return new Tuple<string, string, string, string>(fileName_orig, fileName_withoutExtention, fileName_extention.TrimStart('.'), fileName_new);
        }

        public static Tuple<string, string, string, string> BuildStorageFileName(string file_name, string file_name_without_extention, string file_name_extention,
            string file_name_date, string file_name_user, string file_name_version)
        {
            string fileName_orig = Path.GetFileName(file_name);
            /*
            string fileName_new = Path.ChangeExtension(file_name_without_extention + "_" + file_name_date
                + file_name_user.PadLeft(4, '0')
                + file_name_version.PadLeft(2, '0'),
                file_name_extention);
            */
            string fileName_new =(file_name_without_extention + "_" + file_name_date
                + file_name_user.PadLeft(4, '0')
                + file_name_version.PadLeft(2, '0')) + '.' + file_name_extention.TrimStart('.');

            return new Tuple<string, string, string, string>(fileName_orig, file_name_without_extention, file_name_extention.TrimStart('.'), fileName_new);
        }

        public static Tuple<string,string,string,string,string> ParseStorageFileName(string file_name)
        {
            // План по переводу ЛК на новый движок_131116115340001400.docx
            string fileName_extention = Path.GetExtension(file_name);
            string fileName_withoutExtention = Path.GetFileNameWithoutExtension(file_name);
            
            int separatorIndex = fileName_withoutExtention.LastIndexOf('_');
            string fileName_orig = fileName_withoutExtention.Substring(0, separatorIndex);
            string fileName_userData = fileName_withoutExtention.Substring(separatorIndex + 1);

            string fileName_date = fileName_userData.Left(12);
            string fileName_userAndVersion = fileName_userData.Right(6);
            string fileName_user = fileName_userAndVersion.Left(4).TrimStart('0');
            string fileName_version = fileName_userAndVersion.Right(2).TrimStart('0');
            fileName_version = String.IsNullOrEmpty(fileName_version) ? "0" : fileName_version;

            return new Tuple<string, string, string, string, string>(fileName_orig, fileName_extention.TrimStart('.'), fileName_date, fileName_user, fileName_version);
        }

        public static Tuple<bool, string, string> UploadFile(HttpPostedFileBase uploadingFile, string accessPath, string filePath, string fileName)
        {
            //if (0 == 1)
            if (!System.Diagnostics.Debugger.IsAttached)            
            {                
                NetworkCredential writeCredentials = new NetworkCredential("pavlov_m", "Qwe321asd", "APTEKAURAL");
                //NetworkCredential writeCredentials = new NetworkCredential("Михаил Павлов", "123456", "aptekaural");
                //NetworkCredential writeCredentials = new NetworkCredential("pavlov", "Pavloff99991", "tsb");
                using (new NetworkConnection(accessPath, writeCredentials))
                {
                    if (!System.IO.Directory.Exists(filePath))
                    {
                        System.IO.Directory.CreateDirectory(filePath);
                    }
                    uploadingFile.SaveAs(Path.Combine(filePath, fileName));
                }
            }
            else
            {
                if (!System.IO.Directory.Exists(filePath))
                {
                    System.IO.Directory.CreateDirectory(filePath);
                }
                uploadingFile.SaveAs(Path.Combine(filePath, fileName));
            }
            return new Tuple<bool, string, string>(true, filePath, fileName);
        }

        public static bool MoveFile(string accessPath, string fileSrcPath, string fileDestPath)
        {
            NetworkCredential writeCredentials = new NetworkCredential("pavlov_m", "Qwe321asd", "APTEKAURAL");
            //NetworkCredential writeCredentials = new NetworkCredential("Михаил Павлов", "123456", "aptekaural");
            //NetworkCredential writeCredentials = new NetworkCredential("pavlov", "Pavloff99991", "tsb");
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                using (new NetworkConnection(accessPath, writeCredentials))
                {
                    if (File.Exists(fileDestPath))
                        File.Delete(fileDestPath);
                    File.Move(fileSrcPath, fileDestPath);
                }
            }
            else
            {
                if (File.Exists(fileDestPath))
                    File.Delete(fileDestPath);
                File.Move(fileSrcPath, fileDestPath);
            }
            return true;
        }

        public static bool CopyFile(string accessPath, string fileSrcPath, string fileDestPath)
        {
            NetworkCredential writeCredentials = new NetworkCredential("pavlov_m", "Qwe321asd", "APTEKAURAL");
            //NetworkCredential writeCredentials = new NetworkCredential("Михаил Павлов", "123456", "aptekaural");
            //NetworkCredential writeCredentials = new NetworkCredential("pavlov", "Pavloff99991", "tsb");
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                using (new NetworkConnection(accessPath, writeCredentials))
                {
                    if (File.Exists(fileDestPath))
                        File.Delete(fileDestPath);
                    File.Copy(fileSrcPath, fileDestPath);
                }
            }
            else
            {
                if (File.Exists(fileDestPath))
                    File.Delete(fileDestPath);
                File.Copy(fileSrcPath, fileDestPath);
            }
            return true;
        }

        public static bool DeleteFile(string accessPath, string filePath)
        {
            NetworkCredential writeCredentials = new NetworkCredential("pavlov_m", "Qwe321asd", "APTEKAURAL");
            //NetworkCredential writeCredentials = new NetworkCredential("Михаил Павлов", "123456", "aptekaural");            
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                using (new NetworkConnection(accessPath, writeCredentials))
                {
                    if (File.Exists(filePath))
                        File.Delete(filePath);
                }
            }
            else
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
            }
            return true;
        }

        public static byte[] GetFile(string accessPath, string fileSrcPath)
        {
            NetworkCredential writeCredentials = new NetworkCredential("pavlov_m", "Qwe321asd", "APTEKAURAL");
            //NetworkCredential writeCredentials = new NetworkCredential("Михаил Павлов", "123456", "aptekaural");
            //NetworkCredential writeCredentials = new NetworkCredential("pavlov", "Pavloff99991", "tsb");
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                using (new NetworkConnection(accessPath, writeCredentials))
                {
                    return File.ReadAllBytes(fileSrcPath);
                }
            }
            else
            {
                return File.ReadAllBytes(fileSrcPath);
            }            
        }


        #endregion

        #region Stuff

        /// <summary>
        /// Отправка письма на почтовый ящик
        /// </summary>
        /// <param name="smtpServer">Имя SMTP-сервера</param>
        /// <param name="from">Адрес отправителя</param>
        /// <param name="password">пароль к почтовому ящику отправителя</param>
        /// <param name="mailto">Адрес получателя</param>
        /// <param name="caption">Тема письма</param>
        /// <param name="message">Сообщение</param>
        /// <param name="attachFile">Присоединенный файл</param>
        public static void SendMail(string smtpServer, string from, string password,
        string mailto, string caption, string message, string attachFile = null, bool throwException = false)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from);
                mail.To.Add(new MailAddress(mailto));
                mail.Subject = caption;
                mail.Body = message;
                if (!string.IsNullOrEmpty(attachFile))
                    mail.Attachments.Add(new Attachment(attachFile));
                SmtpClient client = new SmtpClient();
                client.Host = smtpServer;
                client.Port = 587;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(from.Split('@')[0], password);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
                mail.Dispose();
            }
            catch (Exception e)
            {
                if (throwException)
                {
                    throw new Exception("SendMail: " + GlobalUtil.ExceptionInfo(e));
                }                
            }
        }

        public static DateTime GetFirstDayLastMonth()
        {
            return new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
        }

        public static DateTime GetLastDayLastMonth()
        {
            return new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1);
        }

        public static string Linkify(string text)
        {
            Regex regx = new Regex(@"\b(((\S+)?)(@|mailto\:|www|(news|(ht|f)tp(s?))\://)\S+)\b", RegexOptions.IgnoreCase);
            text = text.Replace("&nbsp;", " ");
            MatchCollection matches = regx.Matches(text);

            foreach (Match match in matches)
            {
                if ((match.Value.StartsWith("http")) || (match.Value.StartsWith("www")))
                {
                    text = text.Replace(match.Value, "<a href='" + ((match.Value.StartsWith("www")) ? ("https://" + match.Value) : match.Value) + "' target='_blank'>" + match.Value + "</a>");
                }
            }

            return text;
        }

        public static DateTime? JavaScriptDateToDate(string date)
        {
            return String.IsNullOrEmpty(date) ? null : (DateTime?)DateTime.ParseExact(date.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
        }

        public static decimal RoundDown(decimal i, double decimalPlaces)
        {
            var power = Convert.ToDecimal(Math.Pow(10, decimalPlaces));
            return Math.Floor(i * power) / power;
        }


        #endregion
    }
}
