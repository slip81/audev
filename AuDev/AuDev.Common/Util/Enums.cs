﻿using System.ComponentModel.DataAnnotations;

namespace AuDev.Common.Util
{
    public static class Enums
    {
        #region Discount
        
        /*
        public enum BusinessRelTypeEnum
        {
            SELF = 1,
            PARENT_TO_CHILD = 2,
            CHILD_TO_PARENT = 3,
            PARTNER = 4,
        }
        */

        public enum CardUnitTypeEnum
        {            
            [Display(Name="Сумма бонусов")]
            BONUS_VALUE = 1,
            [Display(Name = "Процент скидки")]
            DISCOUNT_PERCENT = 2,
            [Display(Name = "Деньги")]
            MONEY = 3,
            [Display(Name = "Бонусный процент")]
            BONUS_PERCENT = 4,
            [Display(Name = "Сумма скидки")]
            DISCOUNT_VALUE = 5,
        }

        public enum CardSearchMode
        {
            BY_CARD_NUM_ONLY = 1,
            BY_CARD_NUM2_ONLY = 2,
            BY_CARD_NUM_THEN_CARD_NUM2 = 3,
            BY_CARD_NUM2_THEN_CARD_NUM = 4,
            BY_ALL_NUMS = 5,
        }

        public enum ProgrammParamType
        {
            [Display(Name = "Строка")]
            STRING_VAL = 1,
            [Display(Name = "Целое число")]
            INT_VAL = 2,
            [Display(Name = "Дробное число")]
            FLOAT_VAL = 3,
            [Display(Name = "Дата-время")]
            DATE_VAL = 4,
            [Display(Name = "Дата")]
            DATE_ONLY_VAL = 5,
            [Display(Name = "Время")]
            TIME_ONLY_VAL = 6,
        }

        public enum CardParamType
        {
            [Display(Name="Сумма бонусов")]
            BONUS_VALUE = 1,
            [Display(Name = "Процент скидки")]
            DISCOUNT_PERCENT = 2,
            [Display(Name = "Сумма на сертификате")]
            MONEY = 3,
            [Display(Name = "Бонусный процент")]
            BONUS_PERCENT = 4,
            [Display(Name = "Сумма скидки")]
            DISCOUNT_VALUE = 5,

            [Display(Name = "Накопленная сумма на карте")]
            SUMMA = 10,         // old_value 4
            [Display(Name = "Количество продаж по карте")]
            TRANS_COUNT = 11,   // old_value 5
            [Display(Name = "Дата сгорания накопленной суммы")]
            EXP_DATE = 12,      // old_value 6

            [Display(Name = "Статус карты")]
            CARD_STATUS = 13,

            [Display(Name = "Лимит по карте (в деньгах)")]
            CARD_MONEY_LIMIT = 14,
            [Display(Name = "Использованный лимит по карте (в деньгах)")]
            CARD_MONEY_LIMIT_USED = 15,

            [Display(Name = "Лимит по карте (в бонусах)")]
            CARD_BONUS_LIMIT = 16,
            [Display(Name = "Использованный лимит по карте (в бонусах)")]
            CARD_BONUS_LIMIT_USED = 17,

            [Display(Name = "Сумма неактивных бонусов")]
            BONUS_VALUE_INACTIVE = 18,
        }

        public enum TransParamType
        {
            [Display(Name = "Номер позиции в продаже")]
            POS_NUM = 1,
            [Display(Name = "Сумма позиции в продаже")]
            POS_SUMMA = 2,
            [Display(Name = "Сумма продажи")]
            SUMMA = 3,
            [Display(Name = "Дата-время продажи")]
            TIME = 4,
            [Display(Name = "Наименование позиции продажи")]
            POS_NAME = 5,
            [Display(Name = "Макс. процент скидки по позиции")]
            POS_MAX_DISCOUNT_PERCENT = 6,

            [Display(Name = "Стоимость позиции со скидкой")]
            POS_SUMMA_WITH_DISCOUNT = 7,
            [Display(Name = "Сумма скидки по позиции")]
            POS_SUMMA_DISCOUNT = 8,
            [Display(Name = "Процент скидки по позиции")]
            POS_PERCENT_DISCOUNT = 9,
            [Display(Name = "Сумма бонусов к зачислению по позиции")]
            POS_SUMMA_BONUS_FOR_CARD = 10,
            [Display(Name = "Процент бонусов к зачислению по позиции")]
            POS_PERCENT_BONUS_FOR_CARD = 11,
            [Display(Name = "Сумма бонусов к оплате по позиции")]
            POS_SUMMA_BONUS_FOR_PAY = 17,
            [Display(Name = "Процент бонусов к оплате по позиции")]
            POS_PERCENT_BONUS_FOR_PAY = 18,

            [Display(Name = "Сумма продажи со скидкой")]
            SUMMA_WITH_DISCOUNT = 12,
            [Display(Name = "Сумма скидки по продаже")]
            SUMMA_DISCOUNT = 13,
            [Display(Name = "Процент скидки по продаже")]
            PERCENT_DISCOUNT = 14,
            [Display(Name = "Сумма бонусов к зачислению по продаже")]
            SUMMA_BONUS_FOR_CARD = 15,
            [Display(Name = "Процент бонусов к зачислению по продаже")]
            PERCENT_BONUS_FOR_CARD = 16,
            [Display(Name = "Сумма бонусов к оплате по продаже")]
            SUMMA_BONUS_FOR_PAY = 19,
            [Display(Name = "Процент бонусов к оплате по продаже")]
            PERCENT_BONUS_FOR_PAY = 20,

            [Display(Name = "Макс. процент бонусов к зачислению по позиции")]
            POS_MAX_BONUS_FOR_CARD_PERCENT = 21,
            [Display(Name = "Макс. процент бонусов к оплате по позиции")]
            POS_MAX_BONUS_FOR_PAY_PERCENT = 22,
            [Display(Name = "Процент розничной наценки по позиции")]
            POS_PRICE_MARGIN_PERCENT = 23,

            [Display(Name = "Процент стоимости позиции от стоимости продажи")]
            POS_PERCENT_OF_SUMMA = 24,
            [Display(Name = "Количество позиций в продаже")]
            POS_COUNT = 25,
            [Display(Name = "Количество единиц товара в позиции продажи")]
            POS_UNIT_COUNT = 26,
            [Display(Name = "Позиция является ЖНВЛП")]
            POS_LIVE_PREP = 27,
            [Display(Name = "Позиция участвует в промо-акции")]
            POS_PROMO = 28,
            [Display(Name = "Процент НДС по позиции")]
            POS_TAX_PERCENT = 29,

            [Display(Name = "Карта выбрана сканером")]
            CARD_IS_SCANNED = 30,
            [Display(Name = "Карта выбрана карт-ридером")]
            CARD_IS_CARD_READERED = 31,

            [Display(Name = "Маржа по позиции")]
            POS_TAX_SUMMA = 32,
            [Display(Name = "Список позиций продажи")]
            POS_NAME_LIST = 33,
            [Display(Name = "Цена одной единицы позиции")]
            POS_UNIT_PRICE_TAX = 34,
            [Display(Name = "Комментарий по позиции")]
            POS_MESS = 35,
        }

        public enum ConditionType
        {
            [Display(Name = "Больше чем")]
            MORE_THAN = 1,
            [Display(Name = "Равно")]
            EQUALS = 2,
            [Display(Name = "Не равно")]
            NOT_EQUALS = 3,
            [Display(Name = "Больше или равно")]
            MORE_THAN_OR_EQUALS = 4,
            [Display(Name = "Есть в списке")]
            IN_LIST = 5,
            [Display(Name = "Кратно")]
            MULTIPLE = 6,
        }

        public enum LogicType
        {
            [Display(Name = "Сложить")]
            PLUS = 1,
            [Display(Name = "Вычесть")]
            MINUS = 2,
            [Display(Name = "Взять процент")]
            PERCENT = 3,
            [Display(Name = "Взять сумму за вычетом процента")]
            RESULT_WITH_PERCENT = 4,
            [Display(Name = "Умножить")]
            MULTIPLY = 5,
            [Display(Name = "Разделить")]
            DIVIDE = 6,
            [Display(Name = "Взять первый параметр")]
            TAKE_PARAM_1 = 7,
            [Display(Name = "Взять дату")]
            TAKE_DATE = 8,
            [Display(Name = "Взять время")]
            TAKE_TIME = 9,
            [Display(Name = "Взять год")]
            TAKE_YEAR = 10,
            [Display(Name = "Взять месяц")]
            TAKE_MONTH = 11,
            [Display(Name = "Взять день месяца")]
            TAKE_DAY_OF_MONTH = 12,
            [Display(Name = "Взять день недели")]
            TAKE_DAY_OF_WEEK = 13,
            [Display(Name = "Взять час")]
            TAKE_HOUR = 14,
            [Display(Name = "Взять минуту")]
            TAKE_MINUTE = 15,
            [Display(Name = "Взять процент от суммы")]
            PERCENT_SUMMA = 16,
            [Display(Name = "Целое от деления")]
            DIVISION_WHOLE = 17,
        }

        public enum CardTransactionType
        {
            CALC = 1,
            RETURN = 2,
            HAND_INPUT = 3,
            START_SALDO = 4,
        }

        public enum CardTransactionStatus
        {
            DONE = 0,
            ACTIVE = 1,
            CANCEL = 2,
        }

        public enum UserRole
        {
            ADMIN = 101,
            TESTER = 102,
            USER = 103,
            SUPERADMIN = 104,
        }
        /*
        public enum ReestrState
        {
            NONE = 0,
            IN_PROGRESS = 1,
            PROCESS_DONE = 2,
            PROCESS_ABORTED = 3,
        }
        */
        public enum CardSortField
        {
            DEFAULT = 0,
            CARD_ID = 1,
            DATE_BEG = 2,
            CARD_STATUS_ID = 3,
            REESTR_ID = 4,
            TRANS_COUNT = 5,
            CARD_NUM = 6,
            CARD_NUM2 = 7,
            TRANS_SUM = 8,
            CLIENT_ID = 9,
            CARD_TYPE_ID = 10,
        }
        
        public enum LogEventType
        {
            CARD_ADD = 1,
            CARD_UPDATE = 2,
            CARD_DEL = 3,
            CARD_IMPORT = 4,
            CARD_CARD_TYPE_EDIT = 5,
            CARD_CARD_STATUS_EDIT = 6,
            CARD_CARD_NOMINAL_EDIT = 7,
            BATCH_SUM_ADD = 8,
            BATCH_NOMINAL_ADD = 9,
            BATCH_STATUS_CHANGE = 10,
            BATCH_CARD_DEL = 11,
            BUSINESS_EDIT = 12,
            BUSINESS_ORG_EDIT = 13,
            BUSINESS_ORG_USER_EDIT = 14,
            BUSINESS_REL_EDIT = 15,
            CARD_TYPE_EDIT = 16,
            CARD_TYPE_PROGRAMM_EDIT = 17,
            CARD_TYPE_NOMINAL_ADD = 18,
            CARD_TYPE_UNIT_EDIT = 19,
            REESTR_EDIT = 20,
            REESTR_PROCESS = 21,
            REESTR_RULE_EDIT = 22,
            CLIENT_EDIT = 23,         
            CLIENT_CARD_EDIT = 24,
            
            SALE_COMMIT = 27,
            SALE_CANCEL = 28,
            SALE_REFUND = 29,
            SESSION_START = 30,
            SESSION_CLOSE = 31,
			
			CABINET = 41,
			SCHEDULER = 51,
            
            ERROR = 101,
        }

        public enum SaleRoundType
        {
            NONE = 0,
            TO_10_KOP = 1,
            TO_50_KOP = 2,
            TO_1_ROU = 3,
        }


        public enum CardStatusGroup
        {
            NOT_ACTIVE = 1,
            ACTIVE = 2,
        }

        public enum CardTransferNominalMode
        {
            REPLACE_ALL = 0,
            ADD_ALL = 1,
            NONE = 2,
        }

        public enum CardStatusEnum
        {
            NOT_ACTIVE = 1,
            ACTIVE = 2,
            TEMPORARY_INACTIVE = 3,
            LOST = 4,
            EXPIRED = 5,
            CLOSED = 6,
        }

        /*Тип ограничения*/
        public enum CardRestrType
        {
            NONE = 0,
            [Display(Name = "Ограничение на сумму покупок")]
            TRANS_SUM = 1,
            [Display(Name = "Ограничение на количество покупок")]
            TRANS_COUNT = 2,
            [Display(Name = "Ограничение на оплату бонусами")]
            BONUS_FOR_PAY_SUM = 3,
            [Display(Name = "Ограничение на зачисление бонусов")]
            BONUS_FOR_CARD_SUM = 4,
        }

        /*Тип периода ограничения*/
        public enum CardRestrPeriodType
        {
            [Display(Name = "Дневной лимит")]
            DAYLY = 1,
            [Display(Name = "Месячный лимит")]
            MONTHLY = 2,
            [Display(Name = "Годовой лимит")]
            YEARLY = 3,
        }

        /*Тип значения ограничения*/
        public enum CardRestrValueType
        {
            [Display(Name = "Явное значение")]
            ABSOLUTE = 1,
            [Display(Name = "Процент от значения предыдущего периода")]
            RELATIVE = 2,
        }

        /*Режим активации использованной части значений*/
        public enum CardRestrValueActivationMode
        {
            [Display(Name = "Сразу же")]
            NOW = 0,
            [Display(Name = "Через сутки")]
            AFTER_DAY = 1,
            [Display(Name = "Через месяц")]
            AFTER_MONTH = 2,
            [Display(Name = "Через год")]
            AFTER_YEAR = 3,
            [Display(Name = "На след. сутки")]
            NEXT_DAY = 4,
            [Display(Name = "На след. месяц")]
            NEXT_MONTH = 5,
            [Display(Name = "На след. год")]
            NEXT_YEAR = 6,
        }

        /*Допустимый тип оплаты*/
        public enum CardRestrPaymentType
        {
            [Display(Name = "Все")]
            ALL = 0,
            [Display(Name = "Наличные")]
            CASH = 1,
            [Display(Name = "Банковская карта")]
            BANK_CARD = 2,
        }

        /*Допустимый вид считывания карты*/
        public enum CardRestrReadType
        {
            [Display(Name = "Все")]
            ALL = 0,
            [Display(Name = "Ручной ввод")]
            HAND = 1,
            [Display(Name = "Сканер")]
            SCANNER = 2,
            [Display(Name = "Кард-ридер")]
            CARD_READER = 3,
        }

        /*Типы правил*/
        public enum CardRestrRuleType
        {
            [Display(Name = "Нет")]
            NONE = 0,
            [Display(Name = "Правило перехода на следующий период лимита")]
            TRANSITION_TO_NEXT_PERIOD = 1,
            [Display(Name = "Правило обработки остатка суммы завершившегося периода лимита")]
            LEFT_VALUE_OF_PREV_PERIOD = 2,
            [Display(Name = "Правило обработки использованной суммы завершившегося периода лимита")]
            SPENT_VALUE_OF_PREV_PERIOD = 3,
        }

        public enum CardRestrValueRelPeriodType
        {
            [Display(Name = "Нет")]
            NONE = 0,
            [Display(Name = "Дни")]
            DAYS = 1,
            [Display(Name = "Месяцы")]
            MONTHS = 2,
            [Display(Name = "Годы")]
            YEARS = 3,
        }

        public enum ProgrammParamGroupEnum
        {
            [Display(Name = "Нет")]
            NONE = 0,

            // scope_id = 1 (скидка, обычные товары)
            [Display(Name = "Порог скидки (сумма)")]
            BORDER_SUMM_DISC = 1,
            [Display(Name = "Порог скидки (процент)")]
            BORDER_PROC_DISC = 2,
            [Display(Name = "Постоянный процент скидки")]
            CONST_PROC_DISC = 3,
            [Display(Name = "Макс. процент скидки")]
            MAX_PROC_DISC = 4,
            [Display(Name = "Макс. % розн. наценки для скидки")]
            MAX_PROC_PRICE_MARGIN_DISC = 5,
            [Display(Name = "Мин. % розн. наценки для скидки")]
            MIN_PROC_PRICE_MARGIN_DISC = 6,

            // scope_id = 2 (бонусы, обычные товары)
            [Display(Name = "Порог бонусного процента (сумма)")]
            BORDER_SUMM_BONUS = 7,
            [Display(Name = "Порог бонусного процента (процент)")]
            BORDER_PROC_BONUS = 8,
            [Display(Name = "Постоянный бонусный процент")]
            CONST_PROC_BONUS = 9,
            [Display(Name = "Макс. бонусный процент")]
            MAX_PROC_BONUS = 10,
            [Display(Name = "Макс. сумма покупки для оплаты бонусами")]
            MAX_SALE_SUMM_FOR_PAY_BONUS = 11,
            [Display(Name = "Макс. сумма покупки для оплаты бонусами")]
            MAX_SALE_PROC_FOR_PAY_BONUS = 12,
   
            // scope_id = 3 (скидка, ЖНВЛП)
            [Display(Name = "Порог скидки (сумма)")]
            BORDER_SUMM_ZHV_DISC = 13,
            [Display(Name = "Порог скидки (процент)")]
            BORDER_PROC_ZHV_DISC = 14,
            [Display(Name = "Постоянный процент скидки")]
            CONST_PROC_ZHV_DISC = 15,
            [Display(Name = "Макс. процент скидки")]
            MAX_PROC_ZHV_DISC = 16,
            [Display(Name = "Макс. % розн. наценки для скидки")]
            MAX_PROC_PRICE_MARGIN_ZHV_DISC = 17,
            [Display(Name = "Мин. % розн. наценки для скидки")]
            MIN_PROC_PRICE_MARGIN_ZHV_DISC = 18,

            // scope_id = 4 (бонусы, ЖНВЛП)
            [Display(Name = "Порог бонусного процента (сумма)")]
            BORDER_SUMM_ZHV_BONUS = 19,
            [Display(Name = "Порог бонусного процента (процент)")]
            BORDER_PROC_ZHV_BONUS = 20,
            [Display(Name = "Постоянный бонусный процент")]
            CONST_PROC_ZHV_BONUS = 21,
            [Display(Name = "Макс. бонусный процент")]
            MAX_PROC_ZHV_BONUS = 22,
            [Display(Name = "Макс. сумма покупки для оплаты бонусами")]
            MAX_SALE_SUMM_FOR_PAY_ZHV_BONUS = 23,
            [Display(Name = "Макс. сумма покупки для оплаты бонусами")]
            MAX_SALE_PROC_FOR_PAY_ZHV_BONUS = 24,

        }

        public enum CampaignIssueRuleEnum
        {
            [Display(Name = "Создаются заранее")]
            BEFORE = 1,
            [Display(Name = "Выдаются при покупке, используются при следующей покупке")]
            IN_SALE_FOR_NEXT_SALE = 2,
            [Display(Name = "Выдаются и используются в одной покупке")]
            IN_SALE_FOR_THIS_SALE = 3,
        }

        public enum CampaignIssueConditionTypeEnum
        {
            [Display(Name = "Без ограничений")]
            NONE = 0,
            [Display(Name = "От суммы покупки")]
            FROM_SALE_SUM = 1,
            [Display(Name = "От количества товаров")]
            FROM_POS_COUNT = 2,
        }

        public enum CampaignStateEnum
        {
            [Display(Name = "Активна")]
            ACTIVE = 0,
            [Display(Name = "Подготовка")]
            PREPARING = 1,
            [Display(Name = "Ожидает активации")]
            READY = 2,
            [Display(Name = "Закончена")]
            FINISHED = 3,
            [Display(Name = "Ошибка")]
            ERROR = 4,
        }

        public enum ProgrammStepParamTypeEnum
        {
            [Display(Name = "Параметр карты")]
            CARD_PARAM = 1,
            [Display(Name = "Параметр продажи")]
            TRANS_PARAM = 2,
            [Display(Name = "Параметр алгоритма")]
            PROGRAMM_TYPE = 3,
            [Display(Name = "Параметр другого шага")]
            PREV_STEP_PARAM = 4, 
        }

        #endregion

        #region Defect

        public enum ErrCodeEnum
        {
            [Display(Name = "Непредвиденное исключение")]
            ERR_CODE_EXCEPTION = 100,
            [Display(Name = "Объект не найден")]
            ERR_CODE_NOT_FOUND = 101,
            [Display(Name = "Найден аналогичный объект")]
            ERR_CODE_DUPLICATE_OBJECT = 102,
            [Display(Name = "Атрибут не найден")]
            ERR_CODE_ATTR_NOT_FOUND = 103,
            [Display(Name = "Добавление запрещено")]
            ERR_CODE_ADD_DENIED = 104,
            [Display(Name = "Редактирование запрещено")]
            ERR_CODE_EDIT_DENIED = 105,
            [Display(Name = "Удаление запрещено")]
            ERR_CODE_DEL_DENIED = 106,
            [Display(Name = "Существует дочерний объект")]
            ERR_CODE_CHILD_OBJ_EXISTS = 107,
            [Display(Name = "Есть несколько строк")]
            ERR_CODE_MULTIPLE_RESULT = 108,
            [Display(Name = "Нет прав")]
            ERR_CODE_INSUFFICIENT_RIGHTS = 109,
            [Display(Name = "Пересечение дат")]
            ERR_CODE_DATE_INTERSECTION = 110,

            [Display(Name = "Нет карт")]
            ERR_CODE_REESTR_CARD_COUNT_ZERO = 130,
            [Display(Name = "Слишком много карт")]
            ERR_CODE_REESTR_CARD_COUNT_TOO_MUCH = 131,
            [Display(Name = "Ошибка при генерации номера карты")]
            ERR_CODE_REESTR_CARD_NUM_GEN_ERROR = 132,

            [Display(Name = "Такая карта уже есть")]
            ERR_CODE_IMPORT_CARD_EXISTS = 140,

            [Display(Name = "Сессия не найдена")]
            ERR_CODE_SESSION_NOT_FOUND = 201,
            [Display(Name = "Сессия закрыта")]
            ERR_CODE_SESSION_CLOSED = 202,

            [Display(Name = "Пользователь не авторизрван")]
            ERR_CODE_CAB_NOT_AUTHORIZED = 251,

            [Display(Name = "Лицензия не найдена")]
            ERR_CODE_LIC_NOT_FOUND = 301,
            [Display(Name = "Торговая точка не найдена")]
            ERR_CODE_LIC_SALES_NOT_FOUND = 302,
            [Display(Name = "Найдено несколько торговых точек")]
            ERR_CODE_LIC_SALES_MORE_THAN_ONE = 303,
            [Display(Name = "Рабочее место не найдено")]
            ERR_CODE_LIC_WORKPLACE_NOT_FOUND = 304,
            [Display(Name = "Найдено несколько рабочих мест")]
            ERR_CODE_LIC_WORKPLACE_MORE_THAN_ONE = 305,
            [Display(Name = "Логин не найден")]
            ERR_CODE_LIC_LOGIN_NOT_FOUND = 306,
            [Display(Name = "Несоответствие версии услуги и версии лицензии")]
            ERR_CODE_LIC_WRONG_VERSION_NUMBER = 307,

            [Display(Name = "Логин не найден")]
            ERR_CODE_LOGIN_NOT_FOUND = 401,
            [Display(Name = "Рабочее место не найдено")]
            ERR_CODE_WORKPLACE_NOT_FOUND = 402,
            [Display(Name = "Партнер не найден")]
            ERR_CODE_PARTNER_NOT_FOUND = 403,
            [Display(Name = "Регистрация для партнера не найдена")]
            ERR_CODE_REG_PARTNER_NOT_FOUND = 404,

            [Display(Name = "Не найден список брака")]
            ERR_CODE_DOWNLOAD_DEFFECT_NOT_FOUND = 501,

            [Display(Name = "Некорректный номер пакета")]
            ERR_CODE_ESN_UNCORRECT_BATCH_NUM = 601,
            [Display(Name = "Не найден пакет для подтверждения с заданным номером")]
            ERR_CODE_ESN_BATCH_FOR_CONFIRM_NOT_FOUND = 602,
            [Display(Name = "Не найдена активная цепочка пакетов")]
            ERR_CODE_ESN_ACTIVE_CHAIN_NOT_FOUND = 603,
            [Display(Name = "Есть активное задание на удаление сводного наличия")]
            ERR_CODE_ESN_ACTIVE_TASK_FOR_DEL_EXISTS = 604,
            [Display(Name = "Нарушена нумерация пакетов")]
            ERR_CODE_ESN_WRONG_BATCH_NUM = 605,
            [Display(Name = "Предыдущая цепочка пакетов не подтверждена")]
            ERR_CODE_ESN_PREVIOUS_CHAIN_NOT_CONFIRMED = 606,
            [Display(Name = "Сервис временно недоступен")]
            ERR_CODE_ESN_SERVICE_UNAVAILABLE = 607,


        }

        public enum DefectSourceEnum
        {
            HAND_INPUT = 1,
            SYNCHRONIZER = 2,
            ROSZDRAVNADZOR = 3,
        }

        public enum LogEsnType
        {
            ERROR = 101,

            DOWNLOAD_DEFECT = 2,
            MAKE_DEFECT_XML = 3,

            GET_EXCHANGE_XML = 4,
            SEND_EXCHANGE_DATA = 5,
            MAKE_EXCHANGE_FILE = 6,

            DOC_SEND = 7,
            DOC_RECEIVE = 8,
            DOC_CHANGE_STATE = 9,

            DOWNLOAD_VED = 10,

            UDPATE_EXCHANGE_DATA = 11,

            STOCK_UPLOAD = 12,
            STOCK_DOWNLOAD = 13,

            ASNA_SEND_REMAINS_FULL = 14,
            ASNA_SEND_REMAINS_CHANGED = 15,
            ASNA_RESET_SEND_REMAINS = 16,

            WA_CREATE_PLAN_TASK = 17,            
            WA_EXEC_TASK = 19,
            WA_START_WORKER = 20,
            WA_END_WORKER = 21,

            ASNA_SEND_ORDERS = 22,
            ASNA_GET_ORDERS = 23,
            ASNA_SEND_PREORDERS_FULL = 24,
            ASNA_SEND_PREORDERS_CHANGED = 25,
            ASNA_GET_PREORDERS = 26,
            ASNA_RESET_SEND_PREORDERS = 27,

            DOWNLOAD_MEDICAL = 28,
            MAKE_MEDICAL_XML = 29,
            
            ASNA_GET_REMAINS = 30,

            STOCK_DEL = 31,

            CABINET = 41,
            SCHEDULER = 51,
            LOGGER = 61,
            SENDER = 71,
            CVA = 81,
            RPTSVC = 91,
        }

        public enum LogScope
        {
            DISCOUNT = 1,
            CABINET = 2,
            DEFECT = 3,
            EXCHANGE_PARTNER = 4,
            EXCHANGE_DOC = 5,
            VED = 6,
            LOGGER = 7,
            SENDER = 8,
            CVA = 9,
            RPTSVC = 10,
            LICENSE = 11,
            STOCK = 12,
            ASNA = 13,
            WA = 14,
            MEDICAL = 15,
        }

        public enum RequestScope2
        {
            VED_REGION = (int)LogScope.VED + 10000,
        }

        #endregion

        #region Exchange

        public enum DataSourceEnum
        {
            MZ = 101,
            ASNA = 102,
        }

        public enum ExchangeItemEnum
        {
            [Display(Name = "Не определено")]
            None = 0,
            [Display(Name = "Остатки")]
            Ost = 1,
            [Display(Name = "Движения")]
            Move = 2,
            [Display(Name = "Заказ")]
            Que = 3,
            [Display(Name = "Товары ЦП")]
            Plan = 4,
            [Display(Name = "Товары по акциям")]
            Discount = 5,
            [Display(Name = "Товары ПР")]
            Recommend = 6,
            [Display(Name = "Справочник КА")]
            Client = 7,
            [Display(Name = "Справочник номенклатуры")]
            Prep = 8,
            [Display(Name = "Данные накопительным итогом")]
            Avg = 9,
        }

        public enum ExchangeFormatType
        {
            TextFile = 1,
            XML = 2,
        }

        public enum ExchangeTransportType
        {
            FTP = 1,
            HTTP = 2,
            Email = 3,
        }

        public enum ExchangeRegType
        {
            PARTNER = 1,
            DOC = 2,
        }

        public enum ExchangeDocState
        {
            SENDED = 0,
            RECEIVED = 1,
            LOADED = 2,
            NEED_RESEND = 3,
        }

        public enum ExchangeDirectionEnum
        {
            [Display(Name = "Передача от Клиента Партнеру")]
            FROM_CLIENT_TO_PARTNER = 0,
            [Display(Name = "Передача от Партнера Клиенту")]
            FROM_PARTNER_TO_CLIENT = 1,
        }

        public enum ExchangeTaskState
        {
            [Display(Name = "Выдано")]
            CREATED = 0,
            [Display(Name = "Получено")]
            RECEIVED = 1,
            [Display(Name = "Выполнено с ошибками")]
            DONE_ERROR = 2,
            [Display(Name = "Выполнено без ошибок")]
            DONE_SUCCESS = 3,
        }

        public enum ExchangeDocType
        {
            [Display(Name = "Формат накладных АУ")]
            AU_XML = 0,
            [Display(Name = "Формат Маркетолога")]
            MARKETOLOG = 1,
        }

        #endregion

        #region Cabinet

        public enum MainObjectType
        {
            [Display(Name = "-")]            
            NONE = 0,            
            [Display(Name = "Карта")]            
            CARD = 11,
            [Display(Name = "Доп номер карты")]            
            CARD_ADDITIONAL_NUM = 12,
            [Display(Name = "Тип карт")]            
            CARD_TYPE = 21,
            [Display(Name = "Номинал карты")]
            CARD_NOMINAL = 31,
            [Display(Name = "Организация")]
            BUSINESS = 41,
            [Display(Name = "Отделение организации")]
            BUSINESS_ORG = 51,
            [Display(Name = "Пользователь отделения")]
            BUSINESS_ORG_USER = 61,
            [Display(Name = "Алгоритм")]
            PROGRAMM = 71,
            [Display(Name = "Заявка на алгоритм")]
            PROGRAMM_ORDER = 72,
            [Display(Name = "Реестр")]
            REESTR = 81,
            [Display(Name = "Владелец карты")]            
            CLIENT = 91,
            [Display(Name = "Продажа")]
            CARD_TRANS = 101,
            [Display(Name = "Детали продажи")]
            CARD_TRANS_DETAIL = 111,
            [Display(Name = "Статус карт")]
            CARD_STATUS = 121,
            [Display(Name = "Группа типов карт")]
            CARD_TYPE_GROUP = 131,
            [Display(Name = "Отчет по дисконтам")]
            REPORT_DISCOUNT = 141,
            [Display(Name = "Параметр алгоритма")]
            PROGRAMM_PARAM = 151,
            [Display(Name = "Пакетная операция")]
            BATCH = 161,
            [Display(Name = "Ошибки пакетной операции")]
            BATCH_ERROR = 171,
            [Display(Name = "Импорт карт")]
            CARD_IMPORT = 181,
            [Display(Name = "Лимиты карт")]
            CARD_TYPE_lIMIT = 191,
            [Display(Name = "Статистика продаж")]
            SALE_SUMMARY = 201,
            [Display(Name = "Маркетинговые акции")]
            CAMPAIGN = 202,
            [Display(Name = "Акции в отделениях")]
            CAMPAIGN_BUSINESS_ORG = 203,
            [Display(Name = "Шаг алгоритма")]
            PROGRAMM_STEP = 204,
            [Display(Name = "Параметр шага алгоритма")]
            PROGRAMM_STEP_PARAM = 205,
            [Display(Name = "Условие шага алгоритма")]
            PROGRAMM_STEP_CONDITION = 206,
            [Display(Name = "Логика шага алгоритма")]
            PROGRAMM_STEP_LOGIC = 207,

            [Display(Name = "Вопрос")]
            STORY = 211,
            [Display(Name = "Цикл вопроса")]
            STORY_LC = 221,
            [Display(Name = "Статусы вопросов")]
            STORY_STATE = 231,


            [Display(Name = "Группы действий")]
            CAB_ACTION_GROUP = 241,
            [Display(Name = "Действия")]
            CAB_ACTION = 242,
            [Display(Name = "Роли")]
            CAB_ROLE = 243,
            [Display(Name = "Действия роли")]
            CAB_ROLE_ACTION = 244,
            [Display(Name = "Роли пользователей")]
            CAB_USER_ROLE = 245,
            [Display(Name = "Действия пользователей")]
            CAB_USER_ROLE_ACTION = 246,
            [Display(Name = "Пользователи ЛК")]
            CAB_USER = 253,
            [Display(Name = "Отделения для типов карт")]
            CARD_TYPE_BUSINESS_ORG = 254,
            [Display(Name = "Профиль пользователя")]
            CAB_USER_PROFILE = 255,
            [Display(Name = "Меню пользователя")]
            CAB_USER_MENU = 256,

            [Display(Name = "Клиент")]
            CAB_CLIENT = 301,
            [Display(Name = "Торговая точка")]
            CAB_SALES = 311,
            [Display(Name = "Лицензия")]
            CAB_LICENSE = 321,
            [Display(Name = "Услуга")]
            CAB_SERVICE = 322,
            [Display(Name = "Рабочее место")]
            CAB_WORKPLACE = 323,
            [Display(Name = "Заявка")]
            CAB_ORDER = 324,
            [Display(Name = "Услуга заявки")]
            CAB_ORDER_ITEM = 325,
            [Display(Name = "Версия")]
            CAB_VERSION = 326,
            [Display(Name = "Услуга клиента")]
            CAB_CLIENT_SERVICE = 327,
            [Display(Name = "Перерегистрация рабочего места")]
            CAB_WORKPLACE_REREG = 328,
            [Display(Name = "Незарегестрированная услуга клиента")]
            CAB_CLIENT_SERVICE_UNREG = 329,
            [Display(Name = "Параметры почтового сервиса")]
            CAB_SERVICE_SENDER = 330,
            [Display(Name = "Лог почтового сервиса")]
            CAB_SERVICE_LOG_SENDER = 331,
            [Display(Name = "Параметры сервиса ЦВА")]
            CAB_SERVICE_CVA = 332,
            [Display(Name = "Данные сервиса ЦВА")]
            CAB_SERVICE_CVA_DATA = 333,
            [Display(Name = "Логин клиента")]
            CAB_CLIENT_LOGIN = 334,
            [Display(Name = "Виджет")]
            CAB_WIDGET = 335,
            [Display(Name = "Виджет пользователя")]
            CAB_CLIENT_USER_WIDGET = 336,
            [Display(Name = "Параметры сервиса брака")]
            CAB_SERVICE_DEFECT = 337,
            [Display(Name = "Профиль клиента")]
            CAB_CLIENT_INFO = 338,
            [Display(Name = "Данные по услугам клиента")]
            CAB_CLIENT_SERVICE_SUMMARY = 339,
            [Display(Name = "Группы услуг")]
            CAB_SERVICE_GROUP = 340,
            [Display(Name = "Регистрация")]
            CAB_REG = 341,
            [Display(Name = "Шаблон рассылки")]
            CAB_DELIVERY_TEMPLATE = 342,
            [Display(Name = "Группы шаблонов рассылки")]
            CAB_DELIVERY_TEMPLATE_GROUP = 343,
            [Display(Name = "Рассылка")]
            CAB_DELIVERY = 344,
            [Display(Name = "Детали рассылки")]
            CAB_DELIVERY_DETAIL = 345,
            [Display(Name = "Параметры услуг клиента")]
            CAB_CLIENT_SERVICE_PARAM = 346,
            [Display(Name = "Папка")]
            STORAGE_FOLDER = 347,
            [Display(Name = "Файл")]
            STORAGE_FILE = 348,
            [Display(Name = "Версия файла")]
            STORAGE_FILE_VERSION = 349,
            [Display(Name = "Тип файла")]
            STORAGE_FILE_TYPE = 350,
            [Display(Name = "Статус файла")]
            STORAGE_FILE_STATE = 351,
            [Display(Name = "Лог хранилища")]
            STORAGE_LOG = 352,
            [Display(Name = "Параметры хранилища")]
            STORAGE_PARAM = 353,
            [Display(Name = "Группа файла")]
            STORAGE_FILE_TYPE_GROUP = 354,
            [Display(Name = "Внешняя ссылка")]
            STORAGE_LINK = 355,
            [Display(Name = "Корзина")]
            STORAGE_TRASH = 356,
            [Display(Name = "Набор услуг")]
            SERVICE_KIT = 357,
            [Display(Name = "Элемент набора услуг")]
            SERVICE_KIT_ITEM = 358,
            [Display(Name = "Справка ЛК")]
            CAB_HELP = 359,
            [Display(Name = "Уведомления")]
            CRM_NOTIFY = 360,
            [Display(Name = "Параметры уведомлений 1")]
            CRM_NOTIFY_PARAM_USER = 361,
            [Display(Name = "Параметры уведомлений 2")]
            CRM_NOTIFY_PARAM_ATTR = 362,
            [Display(Name = "Параметры уведомлений 3")]
            CRM_NOTIFY_PARAM_CONTENT = 363,
            [Display(Name = "Отчет по задачам 1")]
            CRM_REPORT1 = 364,
            [Display(Name = "Клиенты ЛК")]
            COMPARE_CLIENT_CAB = 365,
            [Display(Name = "Клиенты CRM")]
            COMPARE_CLIENT_CRM = 366,
            [Display(Name = "Файл для рассылки")]
            CAB_DELIVERY_TEMPLATE_FILE = 367,
            [Display(Name = "Событие")]
            PLANNER_EVENT = 368,
            [Display(Name = "Приоритет")]
            PLANNER_EVENT_PRIORITY = 369,
            [Display(Name = "Запись")]
            PLANNER_NOTE = 370,
            [Display(Name = "Статус")]
            PLANNER_EVENT_STATE = 371,
            [Display(Name = "Софт для обновления")]
            UPDATE_SOFT = 372,
            [Display(Name = "Файл софта")]
            UPDATE_SOFT_ITEM = 373,
            [Display(Name = "Версия софта")]
            UPDATE_SOFT_VERSION = 374,
            [Display(Name = "Пакет обновления")]
            UPDATE_BATCH = 375,
            [Display(Name = "Состав пакета")]
            UPDATE_BATCH_ITEM = 376,
            [Display(Name = "Проект")]
            PRJ = 377,
            [Display(Name = "Задача")]
            PRJ_CLAIM = 378,
            [Display(Name = "Статус проекта")]
            PRJ_STATE_TYPE = 379,
            [Display(Name = "Задание")]
            PRJ_TASK = 380,
            [Display(Name = "Набор типов работ")]
            PRJ_WORK_TYPE_SET = 381,
            [Display(Name = "Работа")]
            PRJ_WORK = 382,
            [Display(Name = "Тип работы")]
            PRJ_WORK_TYPE = 383,
            [Display(Name = "Статус задачи")]
            PRJ_CLAIM_STATE_TYPE = 384,
            [Display(Name = "Статус задания")]
            PRJ_TASK_STATE_TYPE = 385,
            [Display(Name = "Статус работы")]
            PRJ_WORK_STATE_TYPE = 386,
            [Display(Name = "Комментарий")]
            PRJ_MESS = 387,
            [Display(Name = "Журнал задач")]
            PRJ_LOG = 388,
            [Display(Name = "Конфигурация задач")]
            PRJ_USER_SETTINGS = 389,
            [Display(Name = "План работ")]
            PRJ_PLAN = 390,
            [Display(Name = "Параметры задач по умолчанию")]
            PRJ_CLAIM_DEFAULT = 391,
            [Display(Name = "Параметры работ по умолчанию")]
            PRJ_WORK_DEFAULT = 392,
            [Display(Name = "Параметры проектов по умолчанию")]
            PRJ_DEFAULT = 393,
            [Display(Name = "Файл задачи")]
            PRJ_FILE = 394,
            [Display(Name = "Параметры сводного наличия")]
            CAB_SERVICE_STOCK = 395,
            [Display(Name = "Сводное наличие")]
            STOCK = 396,
            [Display(Name = "Отправка наличия")]
            STOCK_BATCH = 397,
            [Display(Name = "Загрузка наличия")]
            STOCK_SALES_DOWNLOAD = 398,
            [Display(Name = "Группа софта")]
            UPDATE_SOFT_GROUP= 399,
            [Display(Name = "Напоминания")]
            PRJ_NOTIFY_TYPE = 400,
            [Display(Name = "Группы авторизации")]
            AUTH_GROUP = 401,
            [Display(Name = "Секции авторизации")]
            AUTH_SECTION = 402,
            [Display(Name = "Разделы авторизации")]
            AUTH_SECTION_PART = 403,
            [Display(Name = "Роли авторизации")]
            AUTH_ROLE = 404,
            [Display(Name = "Роли пользователей")]
            AUTH_USER_ROLE = 405,
            [Display(Name = "Права пользователей")]
            AUTH_USER_PERM = 406,
            [Display(Name = "Права ролей")]
            AUTH_ROLE_PERM = 407,
            [Display(Name = "Группы ролей")]
            AUTH_ROLE_GROUP = 408,
            [Display(Name = "Участники")]
            PRJ_EVENT_PARTY = 409,
            [Display(Name = "Уведомление")]
            PRJ_NOTIFY = 410,
            [Display(Name = "Пользователь АСНА")]
            ASNA = 411,
            [Display(Name = "WA: задание")]
            WA_TASK = 412,
            [Display(Name = "WA: тип запроса")]
            WA_REQUEST_TYPE = 413,
            [Display(Name = "АСНА: тип интервала")]
            ASNA_SCHEDULE_INTERVAL_TYPE = 414,
            [Display(Name = "АСНА: тип времени")]
            ASNA_SCHEDULE_TIME_TYPE = 415,
            [Display(Name = "АСНА: журнал")]
            ASNA_USER_LOG = 416,
            [Display(Name = "АСНА: справочник связок")]
            ASNA_LINK = 417,
            [Display(Name = "WA: этап задания")]
            WA_TASK_LC = 418,
            [Display(Name = "АСНА: остатки")]
            ASNA_STOCK_ROW = 419,
            [Display(Name = "АСНА: цепочки")]
            ASNA_STOCK_CHAIN = 420,
            [Display(Name = "АСНА: актуальные остатки")]
            ASNA_STOCK_ROW_ACTUAL = 421,
            [Display(Name = "АСНА: тип статуса заказа")]
            ASNA_ORDER_STATE_TYPE = 422,
            [Display(Name = "АСНА: заказ")]
            ASNA_ORDER = 423,
            [Display(Name = "АСНА: строка заказа")]
            ASNA_ORDER_ROW = 424,
            [Display(Name = "АСНА: статус заказа")]
            ASNA_ORDER_STATE = 425,
            [Display(Name = "АСНА: статус строки заказа")]
            ASNA_ORDER_ROW_STATE = 426,
            [Display(Name = "Тип группы")]
            PRJ_SYS_GROUP_TYPE = 427,
            [Display(Name = "Группа")]
            PRJ_USER_GROUP = 428,
            [Display(Name = "Тип сводки")]
            PRJ_BRIEF_TYPE = 429,
            [Display(Name = "Тип интервала")]
            PRJ_BRIEF_INTERVAL_TYPE = 430,
            [Display(Name = "Запись")]
            PRJ_NOTE = 431,
            [Display(Name = "Элемент группы")]
            PRJ_USER_GROUP_ITEM = 432,
            [Display(Name = "Параметры сервиса ЕСН")]
            CAB_SERVICE_ESN2 = 433,
            [Display(Name = "Версия")]
            PRJ_PROJECT_VERSION = 434,
            [Display(Name = "Фильтр задач")]
            PRJ_CLAIM_FILTER = 435,


            [Display(Name = "Брак")]
            DEFECT = 501,
            [Display(Name = "Журнал скачивания брака")]
            DEFECT_REQUEST = 502,
            [Display(Name = "Лог скачивания брака")]
            DEFECT_LOG = 503,

            [Display(Name = "Журнал обмена")]
            EXCHANGE_LOG = 601,
            [Display(Name = "Пользователи")]
            EXCHANGE_USER = 602,
            [Display(Name = "Регистрации")]
            EXCHANGE_REG = 603,
            [Display(Name = "Регистрации для обмена с партнерами")]
            EXCHANGE_REG_PARTNER = 604,
            [Display(Name = "Клиенты")]
            EXCHANGE_CLIENT = 605,
            [Display(Name = "Партнеры")]
            EXCHANGE_PARTNER = 606,
            [Display(Name = "Данные обмена с партнерами")]
            EXCHANGE_DATA = 608,
            [Display(Name = "Регистрации для обмена накладными")]
            EXCHANGE_REG_DOC = 609,
            [Display(Name = "Торговые точки для обмена накладными")]
            EXCHANGE_REG_DOC_SALES = 610,
            [Display(Name = "Накладные из обмена накладными")]
            EXCHANGE_DOC = 611,
            [Display(Name = "Данные накладных")]
            EXCHANGE_DOC_DATA = 612,
            [Display(Name = "Монитор обмена")]
            EXCHANGE_MONITOR = 613,
            [Display(Name = "Настройки файлов обмена")]
            EXCHANGE_REG_PARTNER_ITEM = 614,
            [Display(Name = "Лог монитора обмена")]
            EXCHANGE_MONITOR_LOG = 615,
            [Display(Name = "Задача на обмен")]
            EXCHANGE_TASK = 616,
            [Display(Name = "Тип задачи на обмен")]
            EXCHANGE_TASK_TYPE = 617,
            [Display(Name = "Журнал обмена")]
            EXCHANGE_FILE_LOG = 618,
            [Display(Name = "Настройка узлов")]
            EXCHANGE_FILE_NODE = 619,
            [Display(Name = "Настройка файлов узлов")]
            EXCHANGE_FILE_NODE_ITEM = 620,
            [Display(Name = "Файлы обмена партнера")]
            EXCHANGE_PARTNER_ITEM = 621,

            [Display(Name = "Источник ЖВ")]
            VED_SOURCE = 701,
            [Display(Name = "Источник ЖВ в регионах")]
            VED_SOURCE_REGION = 702,
            [Display(Name = "Шаблон импорта ЖВ")]
            VED_TEMPLATE = 703,
            [Display(Name = "Колонки шаблона импорта ЖВ")]
            VED_TEMPLATE_COLUMN = 704,
            [Display(Name = "Госреестр ЖВ")]
            VED_REESTR = 705,
            [Display(Name = "Строки госреестра ЖВ")]
            VED_REESTR_ITEM = 706,
            [Display(Name = "Исключенные строки госреестра ЖВ")]
            VED_REESTR_ITEM_OUT = 707,
            [Display(Name = "Региональный реестр ЖВ")]
            VED_REESTR_REGION = 708,
            [Display(Name = "Строки регионального реестра ЖВ")]
            VED_REESTR_REGION_ITEM = 709,
            [Display(Name = "Исключенные строки регионального реестра ЖВ")]
            VED_REESTR_REGION_ITEM_OUT = 710,
            [Display(Name = "Несопоставленные строки регионального реестра ЖВ")]
            VED_REESTR_REGION_ITEM_ERROR = 711,
            [Display(Name = "Запросы на скачивание ЖНВЛП")]
            VED_REQUEST = 712,

            [Display(Name = "Задача")]
            CRM_TASK = 801,
            [Display(Name = "Подзадача")]
            CRM_SUBTASK = 802,
            [Display(Name = "Комментарий")]
            CRM_TASK_NOTE = 803,
            [Display(Name = "Файл задачи")]
            CRM_TASK_FILE = 804,
            [Display(Name = "Фильтр грида")]
            CRM_TASK_USER_FILTER = 805,
            [Display(Name = "Лог задач")]
            CRM_LOG = 806,
            [Display(Name = "Контроль задач")]
            CRM_USER_TASK_FOLLOW = 807,
            [Display(Name = "Операции по группам задач")]
            CRM_TASK_GROUP_OPERATION = 808,
            [Display(Name = "Группы задач")]
            CRM_GROUP= 809,
            [Display(Name = "Тип связи задач")]
            CRM_TASK_REL_TYPE = 810,
            [Display(Name = "Связь задач")]
            CRM_TASK_REL = 811,
            [Display(Name = "Календарь")]
            CRM_SCHEDULER = 812,
            [Display(Name = "Клиент")]
            CRM_CLIENT = 813,
            [Display(Name = "Программа")]
            CRM_PROJECT = 814,
            [Display(Name = "Участник программы")]
            CRM_PROJECT_USER = 815,
            [Display(Name = "Приоритет")]
            CRM_PRIORITY = 816,
            [Display(Name = "Модуль")]
            CRM_MODULE = 817,
            [Display(Name = "Версия модуля")]
            CRM_MODULE_VERSION = 818,
            [Display(Name = "Раздел модуля")]
            CRM_MODULE_PART = 819,
            [Display(Name = "Роль в задачах")]
            CRM_USER_ROLE = 820,
            [Display(Name = "Статус Выполнено для роли")]
            CRM_USER_ROLE_STATE_DONE = 821,
            [Display(Name = "Статус")]
            CRM_STATE = 822,
            [Display(Name = "Проект")]
            CRM_BATCH = 823,
            [Display(Name = "Фильтр задач")]
            CRM_TASK_FILTER = 824,
            [Display(Name = "Версия проекта")]
            CRM_PROJECT_VERSION = 825,

            [Display(Name = "Грид")]
            CAB_GRID = 901,
            [Display(Name = "Колонка грида")]
            CAB_GRID_COLUMN = 902,
            [Display(Name = "Настройка колонки")]
            CAB_GRID_COLUMN_USER_SETTINGS = 903,
            [Display(Name = "Настройка грида")]
            CAB_GRID_USER_SETTINGS = 904,

        }

        public enum ReportType
        {
            [Display(Name = "Общий отчет по картам")]
            CARD_INFO = 1,
            [Display(Name = "Количество чеков за период")]
            CHECK_COUNT = 2,
            [Display(Name = "Неиспользуемые карты")]
            UNUSED_CARDS = 3,
            [Display(Name = "Сводный отчет по скидкам")]
            SVOD_DSCOUNT = 4,
            [Display(Name = "Количество чеков за день больше одного")]
            CHECK_COUNT2 = 5,
            [Display(Name = "Первые продажи по картам")]
            FIRST_SALE = 6,
            [Display(Name = "АСНА: отчет по дисконтам")]
            ASNA_DISCOUNT = 7,
        }

        public enum CardTypeGroup
        {
            [Display(Name = "Дисконтная карта")]
            DISCOUNT = 1,
            [Display(Name = "Бонусная карта")]
            BONUS = 2,
            [Display(Name = "Дисконтно-бонусная карта")]
            DISCOUNT_BONUS = 3,
            [Display(Name = "Сертификат")]
            CERT = 4,
        }

        public enum BatchType
        {
            [Display(Name = "Пакетные операции")]
            BatchOperations = 0,
            [Display(Name = "Добавление карт")]
            BatchAdd = 1,
            [Display(Name = "Импорт карт")]
            BatchImport = 2,
        }

        public enum BatchState
        {
            [Display(Name = "-")]
            None = 0,
            [Display(Name = "Выполняется")]
            Running = 1,
            [Display(Name = "Завершена")]
            Finished = 2,
        }

        public enum BatchOperationsMode
        {
            [Display(Name = "Добавить")]
            Add = 0,
            [Display(Name = "Заменить")]
            Replace = 1,
        }

        public enum BatchCardListMode
        {
            [Display(Name = "На текущей странице")]
            CurrentPage = 0,
            [Display(Name = "На всех страницах")]
            AllPages = 1,
            [Display(Name = "Все существующие")]
            AllCards = 2,
            [Display(Name = "Только выделенные")]
            SelectedCards = 3,
        }

        public enum CardLimitType
        {
            [Display(Name = "-")]
            None = 0,
            [Display(Name = "Месячный лимит продаж")]
            MoneyMonthly = 1,
            [Display(Name = "Дневной лимит бонусов")]
            BonusDaily = 2,
            [Display(Name = "Годовой лимит продаж")]
            MoneyYearly = 3,
        }

        public enum StoryStateEnum
        {
            [Display(Name = "В очереди")]
            WAITING = 1,
            [Display(Name = "Принято")]
            ACCEPTED = 2,
        }

        public enum RequestTypeEnum
        {
            [Display(Name="Только новое")]
            RECORDS_NEW = 0,
            [Display(Name = "Все строки")]
            RECORDS_ALL = 1,
        }

        public enum RequestStateEnum
        {
            [Display(Name = "Не подтверждено")]
            NOT_CONFIRMED = 0,
            [Display(Name = "Подтверждено")]
            CONFIRMED = 1,
        }

        public enum ExchangeFormat
        {
            //[Display(Name = "Текстовый файл")]
            //TEXT_FILE = 1,
            [Display(Name = "XML")]
            XML_FILE = 2,
        }

        public enum ExchangeTransport
        {
            [Display(Name = "FTP")]
            FTP = 1,
            //[Display(Name = "HTTP")]
            //HTTP = 2,
            [Display(Name = "Email")]
            EMAIL = 3,
        }

        public enum ArcType
        {
            [Display(Name = "Нет")]
            NONE = 0,
            [Display(Name = "ZIP")]
            ZIP = 1,
            [Display(Name = "RAR")]
            RAR = 2,
            [Display(Name = "7Z")]
            SevenZ = 3,
        }

        public enum VedColumn
        {
            [Display(Name = "МНН")]
            MNN = 1,
            [Display(Name = "Торг. наименование")]
            COMM_NAME = 2,
            [Display(Name = "Упаковка")]
            PACK_NAME = 3,
            [Display(Name = "Изготовитель")]
            PRODUCER = 4,
            [Display(Name = "Кол-во в упаковке")]
            PACK_COUNT = 5,
            [Display(Name = "Предельная цена")]
            LIMIT_PRICE = 6,
            [Display(Name = "Цена указана для первич. упаковки")]
            PRICE_FOR_INIT_PACK = 7,
            [Display(Name = "№ РУ")]
            REG_NUM = 8,
            [Display(Name = "Дата и номер регистрации")]
            PRICE_REG_DATE = 9,
            [Display(Name = "EAN13")]
            EAN13 = 10,
            [Display(Name = "Предельная оптовая надбавка")]
            LIMIT_OPT_INCR = 11,
            [Display(Name = "Предельная розничная надбавка")]
            LIMIT_ROZN_INCR = 12,
            [Display(Name = "Предельная розничная цена")]
            LIMIT_ROZN_PRICE = 13,
            [Display(Name = "Предельная розничная цена с НДС")]
            LIMIT_ROZN_PRICE_WITH_NDS = 14,
            [Display(Name = "Причина исключения")]
            OUT_REAZON = 15,
            [Display(Name = "Дата исключения")]
            OUT_DATE = 16,
        }

        public enum CrmGroup
        {
            [Display(Name = "Все")]
            ALL = -1,
            [Display(Name = "Неразобранные")]
            BUCKET = 0,
            [Display(Name = "Обработанные")]
            PREPARED = 1,
            [Display(Name = "К утверждению")]
            FIXED = 2,
            [Display(Name = "В работе")]
            APPROVED = 3,
            [Display(Name = "Закрытые")]
            CLOSED = 4,
            [Display(Name = "В версии")]
            IN_VERSION = 5,
            [Display(Name = "Проекты")]
            PROJECT = 6,
        }

        public enum CrmTaskOperation
        {
            [Display(Name = "Назад в неразобранные")]
            TO_BUCKET = 0,
            [Display(Name = "В обработанные")]
            TO_PREPARED = 1,
            [Display(Name = "К утверждению")]
            TO_FIXED = 2,
            [Display(Name = "В работу")]
            TO_APPROVED = 3,
            [Display(Name = "Закрыть")]
            TO_CLOSED = 4,
            [Display(Name = "В версию")]
            TO_VERSION = 5,
            [Display(Name = "В контроль")]
            TO_FOLLOW = 11,
            [Display(Name = "Из контроля")]
            TO_UNFOLLOW = 12,
            [Display(Name = "Архивировать")]
            TO_ARCHIVE = 13,
            [Display(Name = "Вернуть из архива")]
            FROM_ARCHIVE = 14,
            [Display(Name = "В проект")]
            TO_BATCH = 15,
        }

        public enum CrmUserTaskRole
        {
            [Display(Name = "Все")]
            ALL = 0,
            [Display(Name = "Я - исполнитель")]
            MY_EXEC = 1,
            [Display(Name = "Я - ответственный")]
            MY_ASSIGNED = 2,
            [Display(Name = "На контроле")]
            MY_FOLLOW = 3,
            [Display(Name = "Я - участник")]
            MY_PROJECT = 4,
        }

        public enum CabGrid
        {
            [Display(Name = "TaskList")]
            TaskList = 1,
            [Display(Name = "ClientList")]
            ClientList = 2,
            [Display(Name = "PrjList")]
            PrjList = 3,
            [Display(Name = "PrjClaimList")]
            PrjClaimList = 4,
            [Display(Name = "PrjTaskList")]
            PrjTaskList = 5,
            [Display(Name = "PrjWorkList")]
            PrjWorkList = 6,
            [Display(Name = "PrjBrief")]
            PrjBrief = 7,
        }

        public enum CabGridColumn_TaskList
        {
            [Display(Name = "task_id")]
            task_id = 1,
            [Display(Name = "crt_date")]
            crt_date = 2,
            [Display(Name = "task_num")]
            task_num = 3,
            [Display(Name = "task_name")]
            task_name = 4,
            [Display(Name = "state_name")]
            state_name = 5,
            [Display(Name = "client_name")]
            client_name = 6,
            [Display(Name = "exec_user_name")]
            exec_user_name = 7,
            [Display(Name = "module_name")]
            module_name = 8,
            [Display(Name = "module_version_name")]
            module_version_name = 10,
            [Display(Name = "owner_user_name")]
            owner_user_name = 11,
            [Display(Name = "priority_name")]
            priority_name = 12,
            [Display(Name = "project_name")]
            project_name = 13,
            [Display(Name = "required_date")]
            required_date = 14,
            [Display(Name = "repair_date")]
            repair_date = 15,
            [Display(Name = "repair_version_name")]
            repair_version_name = 16,
            [Display(Name = "point")]
            point = 17,
            [Display(Name = "assigned_user_name")]
            assigned_user_name = 18,
            [Display(Name = "is_fin_name")]
            is_fin_name = 19,
            [Display(Name = "fin_cost")]
            fin_cost = 20,
            [Display(Name = "upd_date")]
            upd_date = 21,
            [Display(Name = "task_text")]
            task_text = 38,
            [Display(Name = "subtasks_text")]
            subtasks_text = 39,
            [Display(Name = "notes_text")]
            notes_text = 40,
            [Display(Name = "batch_name")]
            batch_name = 44,
            [Display(Name = "cost_plan")]
            cost_plan = 45,
            [Display(Name = "cost_fact")]
            cost_fact = 46,

            // системные значения, нет в БД
            /*
            [Display(Name = "Группа")]
            SYS_group_id = 10001,
            [Display(Name = "Календарь")]
            SYS_is_event = 10002,
            [Display(Name = "Модерация")]
            SYS_moderator_user_id = 10003,
            [Display(Name = "Комментарии")]
            SYS_comments = 10004,
            [Display(Name = "Подзадачи")]
            SYS_subtasks = 10005,
            */
        }

        public enum CabGridColumn_ClientList
        {
            [Display(Name = "id")]
            id = 22,
            [Display(Name = "client_name")]
            client_name = 23,
            [Display(Name = "phone")]
            phone = 24,
            [Display(Name = "site")]
            site = 25,
            [Display(Name = "contact_person")]
            contact_person = 26,
            [Display(Name = "is_phis_str")]
            is_phis_str = 27,
            [Display(Name = "inn")]
            inn = 28,
            [Display(Name = "city_name")]
            city_name = 29,
            [Display(Name = "region_name")]
            region_name = 30,
            [Display(Name = "legal_address")]
            legal_address = 31,
            [Display(Name = "absolute_address")]
            absolute_address = 32,
            [Display(Name = "reg_date")]
            reg_date = 33,
            [Display(Name = "last_activity_date")]
            last_activity_date = 34,
            [Display(Name = "created_by")]
            created_by = 35,
            [Display(Name = "is_activated_str")]
            is_activated_str = 36,
            [Display(Name = "sales_count")]
            sales_count = 37,
            [Display(Name = "kpp")]
            kpp = 41,
            [Display(Name = "state_name")]
            state_name = 42,
            [Display(Name = "sm_id")]
            sm_id = 43,
        }

        public enum NotifyScope
        {
            [Display(Name = "Задачи")]
            CRM_TASK = 1,
            [Display(Name = "HelpDesk")]
            HELP_DESCK = 2,
        }

        public enum CabUserRole
        {
            [Display(Name = "Разработчик")]
            DEVELOPER = 1,
            [Display(Name = "Тестировщик")]
            TESTER = 2,
            [Display(Name = "Сервис")]
            SERVICE = 3,
            [Display(Name = "Руководитель")]
            MANAGER = 4,
        }

        public enum ClientServiceEnum
        {
            [Display(Name = "NONE")]
            NONE = 0,
            [Display(Name = "Брак")]
            DEFECT = 34,
            [Display(Name = "Госреестр")]
            GRLS = 43,
            [Display(Name = "Обмен с партнерами")]
            EXCHANGE = 52,
            [Display(Name = "Дисконтные карты")]
            DISCOUNT = 56,
            [Display(Name = "Брак на почту")]
            SENDER_DEFECT = 57,
            [Display(Name = "Госреестр на почту")]
            SENDER_GRLS = 58,
            [Display(Name = "Отправка данных на ЦеныВАптеках.РФ")]
            CVA = 59,
            [Display(Name = "Сервис отчетности")]
            REPORT_SVC = 60,
            [Display(Name = "ЕСН 2.0")]
            UND = 64,
            [Display(Name = "Сводное наличие")]
            STOCK = 65,
            [Display(Name = "АСНА 2.0")]
            ASNA = 66,
        }

        public enum ScheduleTypeEnum
        {
            [Display(Name = "NONE")]
            NONE = 0,
            [Display(Name = "Интервал в днях")]
            INTERVAL_DAILY = 1,
            [Display(Name = "Недельное расписание")]
            SCHEDULE_WEEKLY = 2,
            [Display(Name = "Месячное раписание")]
            SCHEDULE_MONTHLY = 3,
        }

        public enum CvaSendStateEnum
        {            
            [Display(Name = "Не отправлен")]
            NOT_SENT = 1,
            [Display(Name = "Отправляется")]
            SENDING = 2,
            [Display(Name = "Ошибка при отправке")]
            SENT_ERROR = 3,
            [Display(Name = "Отправлен")]
            SENT = 4,
        }

        public enum CabWidgetEnum
        {
            [Display(Name = "NONE")]
            NONE = 0,
            [Display(Name = "Дисконтные карты")]
            DISCOUNT = 1,
            [Display(Name = "Брак")]
            DEFECT = 2,
            [Display(Name = "Обмен с партнерами")]
            EXCHANGE = 3,
            [Display(Name = "ЦеныВАптеках.РФ")]
            CVA = 4,
            [Display(Name = "Брак на почту")]
            SEND_DEFECT = 5,
            [Display(Name = "Госреестр на почту")]
            SEND_GRLS = 6,
        }

        public enum ClientServiceStateEnum
        {
            [Display(Name = "Отстутствует")]
            NONE = -1,
            [Display(Name = "Установлена")]
            INSTALLED = 0,
            [Display(Name = "Заказана")]
            ORDERED = 1,
        }

        public enum DeliveryState
        {
            [Display(Name = "Готова")]
            READY = 0,
            [Display(Name = "Выполняется")]
            IN_PROGRESS = 1,
            [Display(Name = "Выполнена")]
            DONE = 2,
            [Display(Name = "Ошибка")]
            ERROR = 3,
        }

        public enum AuthGroupEnum
        {
            [Display(Name = "Сотрудники")]
            PERS = 1,
            [Display(Name = "Клиенты")]
            CLIENTS = 2,
        }

        public enum AuthPartEnum
        {
            [Display(Name = "Не указан")]
            NONE = 0,
            [Display(Name = "")]
            DISCOUNT_CARD = 1,
            [Display(Name = "")]
            DISCOUNT_BATCH = 2,
            [Display(Name = "")]
            DISCOUNT_CARD_TYPE = 3,
            [Display(Name = "")]
            DISCOUNT_PROGRAMM = 4,
            [Display(Name = "")]
            DISCOUNT_CARD_HOLDER = 5,
            [Display(Name = "")]
            DISCOUNT_CARD_TRANS = 6,
            [Display(Name = "")]
            DISCOUNT_REPORT = 7,
            [Display(Name = "")]
            DISCOUNT_BUSINESS = 8,
            [Display(Name = "")]
            DISCOUNT_CARD_IMPORT = 9,
            [Display(Name = "")]
            EXCHANGE_LOG = 10,
            [Display(Name = "")]
            EXCHANGE_MONITOR = 11,
            [Display(Name = "")]
            EXCHANGE_CLIENT = 12,
            [Display(Name = "")]
            EXCHANGE_PARTNER = 13,
            [Display(Name = "")]
            DEFECT_LIST = 14,
            [Display(Name = "")]
            DEFECT_LOG = 15,
            [Display(Name = "")]
            SPRAV_SERVICE = 16,
            [Display(Name = "")]
            PRJ = 17,
            [Display(Name = "")]
            PRJ_PLAN = 19,
            [Display(Name = "")]
            PRJ_SPRAV = 21,
            [Display(Name = "")]
            CVA = 23,
            [Display(Name = "")]
            CLIENT = 24,
            [Display(Name = "")]
            REG = 25,
            [Display(Name = "")]
            DELIVERY = 26,
            [Display(Name = "")]
            HELP_DESK = 27,
            [Display(Name = "")]
            ADM_ROLE = 28,
            [Display(Name = "")]
            ADM_USER = 29,
            [Display(Name = "")]
            ADM_LOG = 30,
            [Display(Name = "")]
            STORAGE_FOLDER = 31,
            [Display(Name = "")]
            STORAGE_FILE = 32,
            [Display(Name = "")]
            STORAGE_LOG = 33,
            [Display(Name = "")]
            STORAGE_PARAM = 34,
            [Display(Name = "")]
            PLANNER = 35,
            [Display(Name = "")]
            UPDATE_SOFT = 36,
            [Display(Name = "")]
            UPDATE_BATCH = 37,
            [Display(Name = "")]
            STOCK = 38,
            [Display(Name = "")]
            STOCK_LOG = 39,
            [Display(Name = "")]
            ASNA_USER = 40,
            [Display(Name = "")]
            ASNA_LINK = 41,
            [Display(Name = "")]
            PROGRAMM_BUILDER = 42,
        }

        public enum StorageEnum
        {
            [Display(Name = "Хранилище")]
            MAIN = 1,
        }

        public enum StorageFileStateEnum
        {
            [Display(Name = "Доступен")]
            ENABLED = 1,
            [Display(Name = "Загружается")]
            UPLOADING = 2,
            [Display(Name = "Блокирован")]
            BLOCKED = 3,
            [Display(Name = "Недоступен")]
            DISABLED = 4,
        }

        public enum StorageItemEnum
        {
            [Display(Name = "Файл")]
            FILE = 1,
            [Display(Name = "Внешняя ссылка")]
            LINK = 2,
            [Display(Name = "Папка")]
            FOLDER = 3,
        }

        public enum CabHelpEnum
        {
            [Display(Name = "Нет")]
            NONE = 0,
            [Display(Name = "ЛК")]
            CAB = 1,
            [Display(Name = "Структура ЛК")]
            CAB_STRUCT = 2,
            [Display(Name = "Разделы ЛК")]
            CAB_SECTION = 3,
            [Display(Name = "Главная")]
            MAIN = 4,
            [Display(Name = "Услуги")]
            SERV = 5,
            [Display(Name = "Клиенты")]
            CLIENT = 6,
            [Display(Name = "Сервис")]
            SERVICE = 7,
            [Display(Name = "Админ")]
            ADMIN = 8,
            [Display(Name = "Мои страницы")]
            MY = 9,
            [Display(Name = "Меню пользователя")]
            USER_MENU = 10,
            [Display(Name = "Дисконтные карты")]
            DISCOUNT = 11,
            [Display(Name = "Обмен с партнерами")]
            EXCHANGE = 12,
            [Display(Name = "Брак")]
            DEFECT = 13,
            [Display(Name = "ЦВА")]
            CVA = 14,
            [Display(Name = "Клиенты")]
            CLIENT_LIST = 15,
            [Display(Name = "Заявки на активацию")]
            REG = 16,
            [Display(Name = "Справочники")]
            SPRAV = 17,
            [Display(Name = "Рассылка")]
            DELIVERY = 18,
            [Display(Name = "HelpDesk")]
            HELP_DESK = 19,
            [Display(Name = "Задачи")]
            CRM_TASK = 20,
            [Display(Name = "Хранилище")]
            STORAGE = 21,
            [Display(Name = "Журнал событий")]
            CAB_LOG = 22,
            [Display(Name = "Полномочия")]
            AUTH = 23,
            [Display(Name = "Настройка")]
            MY_SETTINGS = 24,
            [Display(Name = "Профиль")]
            USER_PROFILE = 25,
            [Display(Name = "Сменить пароль")]
            USER_CHANGE_PWD = 26,
            [Display(Name = "Выход")]
            USER_LOGOUT = 27,
        }

        public enum NotifyTransportTypeEnum
        {
            [Display(Name = "ICQ")]
            ICQ = 0,
            [Display(Name = "Email")]
            EMAIL = 1,
        }

        public enum PlannerEventStateEnum
        {
            [Display(Name = "Активно")]
            ACTIVE = 0,
            [Display(Name = "Выполнено")]
            DONE = 1,
            [Display(Name = "Отменено")]
            CANCELED = 2,
        }

        public enum PlannerEventPriorityEnum
        {
            [Display(Name = "Обычный")]
            LEVEL0 = 0,
            [Display(Name = "Повышенный")]
            LEVEL1 = 1,
            [Display(Name = "Высокий")]
            LEVEL2 = 2,
            [Display(Name = "Максимальный")]
            LEVEL3 = 3,
        }

        public enum CrmEventGroupEnum
        {
            [Display(Name = "Обычные")]
            NORMAL = 1,
            [Display(Name = "Важные")]
            IMPORTANT = 2,
            [Display(Name = "Срочные")]
            URGENT = 3,
        }

        public enum PlannerEventPublicFilterModeEnum
        {
            [Display(Name = "Все")]
            ALL = 0,
            [Display(Name = "Свои")]
            MY = 1,
        }

        public enum PrjStateTypeEnum
        {
            [Display(Name = "Подготовка")]
            PREPARE = 1,
            [Display(Name = "Активен")]
            ACTIVE = 2,
            [Display(Name = "Завершен")]
            DONE = 3,
        }

        public enum PrjClaimStateTypeEnum
        {
            [Display(Name = "Неразобрано")]
            UNDEFINED = 1,
            [Display(Name = "Активно")]
            ACTIVE = 2,
            [Display(Name = "Выполнено")]
            DONE = 3,
            [Display(Name = "Отменено")]
            CANCELED = 4,
            [Display(Name = "Принято")]
            VERIFIED = 5,
        }

        public enum UndImportFileTypeEnum
        {
            [Display(Name = "EXCEL")]
            EXCEL = 1,
            [Display(Name = "XML")]
            XML = 2,
        }

        public enum UndMatchStateEnum
        {
            [Display(Name = "Не привязано")]
            NONE = 0,
            [Display(Name = "Новое")]
            NEW = 1,
            [Display(Name = "Синоним")]
            SYN = 2,
        }

        public enum UndDocImportLogTypeEnum
        {
            [Display(Name = "Добавление в эталонные")]
            NEW = 1,
            [Display(Name = "Добавление в синонимы")]
            SYN = 2,
            [Display(Name = "Удаление сопоставления")]
            REMOVE = 3,
        }

        public enum UndDocImportStateEnum
        {
            [Display(Name = "В очереди")]
            WAIT = 0,
            [Display(Name = "Загрузка файла")]
            UPLOAD = 1,
            [Display(Name = "Сопоставление строк")]
            MATCH = 2,
            [Display(Name = "Готов")]
            READY = 3,
            [Display(Name = "Полностью сопоставлен")]
            COMPLETE = 4,
        }

        public enum UndDocStateEnum
        {
            [Display(Name = "Загружен")]
            UPLOADED = 1,
            [Display(Name = "В очереди")]
            WAITING = 2,
            [Display(Name = "Обрабатывается")]
            PROCESSING = 3,
            [Display(Name = "Обработан")]
            READY = 4,
        }

        public enum UndDocTypeEnum
        {
            [Display(Name = "Справочник")]
            SPRAV = 1,
            [Display(Name = "Накладная")]
            NAKL = 2,
            [Display(Name = "Справочник поставщика")]
            SUPPLIER_SPRAV = 3,
        }
        
        public enum UndStockRowStateEnum
        {
            [Display(Name = "Строка добавлена в наличие")]
            ADDED = 0,
            [Display(Name = "Строка удалена из наличия")]
            REMOVED = 1,
            [Display(Name = "Строка изменена в наличии")]
            CHANGED = 2,
        }

        public enum UpdateSoftGroupEnum
        {
            [Display(Name = "Модули АУ")]
            AU = 1,
            [Display(Name = "Модули НАП")]
            NAP = 2,
        }

        public enum WaEnum
        {
            [Display(Name = "auesnapi")]
            AUESNAPI = 1,         
        }

        public enum WaRequestTypeGroup1Enum
        {
            [Display(Name = "ЕСН")]
            UND = 1,
            [Display(Name = "АСНА")]
            ASNA = 2,
            [Display(Name = "СН")]
            STOCK = 3,
        }

        public enum WaRequestTypeGroup2Enum
        {
            [Display(Name = "Отправка в АСНА")]
            ASNA_SendTo = 1,
            [Display(Name = "Получение из АСНА")]
            ASNA_GetFrom = 2,
        }

        public enum WaRequestTypeEnum
        {
            [Display(Name = "Получение эталонного справочника")]
            UND_GetEtalonSprav = 1,
            [Display(Name = "АСНА: получение информации об аптеке")]
            ASNA_GetStoreInfo = 2,
            [Display(Name = "АСНА: получение справочника связок номенклатуры")]
            ASNA_GetGoodsLinks = 3,
            [Display(Name = "АСНА: получение информации об остатках аптеки")]
            ASNA_GetStocks = 4,
            [Display(Name = "АСНА: передача полных остатков аптеки")]
            ASNA_SendStocksFull = 5,
            [Display(Name = "АСНА: передача изменений по остаткам аптеки")]
            ASNA_SendStocksChanges = 6,
            [Display(Name = "АСНА: удаление информации по остаткам аптеки")]
            ASNA_DelStocks = 7,
            [Display(Name = "АСНА: передача полного сводного прайс-листа")]
            ASNA_SendConsPriceFull = 8,
            [Display(Name = "АСНА: передача изменений в сводном прайс-листе")]
            ASNA_SendConsPriceChanges = 9,
            [Display(Name = "CH: удаление сводного наличия")]
            STOCK_DelStock = 10,
        }

        public enum WaRequestStateEnum
        {
            [Display(Name = "В очереди")]
            WAITING = 1,
            [Display(Name = "Обрабатывается")]
            HANDLING = 2,
            [Display(Name = "Выполнен успешно")]
            SUCCESS = 3,
            [Display(Name = "Выполнен с ошибками")]
            ERROR = 4,
        }

        public enum WaTaskStateEnum
        {
            [Display(Name = "Выдано клиенту")]
            ISSUED_CLIENT = 1,
            [Display(Name = "Выдано серверу")]
            ISSUED_SERVER = 2,
            [Display(Name = "Выполняется на клиенте")]
            HANDLING_CLIENT = 3,
            [Display(Name = "Выполняется на сервере")]
            HANDLING_SERVER = 4,
            [Display(Name = "Выполнено клиентом успешно")]
            SUCCESS_CLIENT = 5,
            [Display(Name = "Выполнено клиентом с ошибками")]
            ERROR_CLIENT = 6,
            [Display(Name = "Выполнено сервером успешно")]
            SUCCESS_SERVER = 7,
            [Display(Name = "Выполнено сервером с ошибками")]
            ERROR_SERVER = 8,
            [Display(Name = "Отменено клиентом")]
            CANCELED_CLIENT = 9,
            [Display(Name = "Отменено сервером")]
            CANCELED_SERVER = 10,
        }

        public enum AsnaScheduleTypeEnum
        {
            [Display(Name = "Отправка полных остатков")]
            SEND_FULL_REMAINS = 1,
            [Display(Name = "Отправка изменений остатков")]
            SEND_CHANGED_REMAINS = 2,
        }

        public enum TaxSystemTypeEnum
        {
            [Display(Name = "ОСНО")]
            OSNO = 1,
            [Display(Name = "ЕНВД")]
            ENVD = 2,
            [Display(Name = "УСН")]
            USN = 3,
        }

        public enum PriceLimitTypeEnum
        {
            [Display(Name = "От 0 до 50 вкл.")]
            LESS_OR_EQUALS_50 = 1,
            [Display(Name = "От 50 до 500 вкл.")]
            GREATER_50_LESS_OR_EQUALS_500 = 2,
            [Display(Name = "От 500")]
            GREATER_500 = 3,
        }

        public enum PrjBriefEnum
        {
            [Display(Name = "Мониторинг задач")]
            DEV_CLAIM_MONITORING = 1,
            [Display(Name = "Мониторинг работ")]
            DEV_WORK_MONITORING = 2,
            [Display(Name = "Мониторинг заданий")]
            DEV_TASK_MONITORING = 3,
        }

        public enum PrjBriefIntervalTypeEnum
        {
            [Display(Name = "За 2 месяца")]
            TWO_MONTHS = 1,
            [Display(Name = "За 1 месяц")]
            ONE_MONTH = 2,
            [Display(Name = "За 1 неделю")]
            ONE_WEEK = 3,
            [Display(Name = "За 2 дня")]
            TWO_DAYS = 4,
        }

        public enum PrjUserGroupItemTypeEnum
        {
            [Display(Name = "Задача")]
            CLAIM = 1,
            [Display(Name = "Событие")]
            EVENT = 2,
            [Display(Name = "Запись")]
            NOTE = 3,
            [Display(Name = "Задание")]
            TASK = 4,
        }

        public enum PrjBriefStateTypeEnum
        {
            [Display(Name = "1. Новые")]
            NEW = 1,
            [Display(Name = "2. Активные")]
            ACTIVE = 2,
            [Display(Name = "3. Неразобранные")]
            UNHANDLED = 3,
            [Display(Name = "4. Завершенные")]
            COMPLETED = 4,
        }

        public enum PrjBriefDateCutTypeEnum
        {
            [Display(Name = "По дате изменения")]
            UPD_DATE = 1,
            [Display(Name = "По дате создания")]
            CRT_DATE = 2,
        }

        public enum PrjPageTypeEnum
        {
            [Display(Name = "-")]
            NONE = 0,
            [Display(Name = "prj")]
            PRJ = 1,
            [Display(Name = "claim")]
            CLAIM = 2,
            [Display(Name = "task")]
            TASK = 3,
            [Display(Name = "work")]
            WORK = 4,
            [Display(Name = "plan")]
            PLAN = 5,
            [Display(Name = "news")]
            NEWS = 6,
            [Display(Name = "log")]
            LOG = 7,
            [Display(Name = "brief")]
            BRIEF = 8,
            [Display(Name = "notebook")]
            NOTEBOOK = 9,
            [Display(Name = "sprav")]
            SPRAV = 10,
            [Display(Name = "settings")]
            SETTINGS = 11,
        }

        public enum PrjUserSettingsSaveModeEnum
        {
            [Display(Name = "Создать новую")]
            NEW_SETTING = 1,
            [Display(Name = "Перезаписать текущую")]
            REWRITE_CURRENT = 2,
            [Display(Name = "Перезаписать существующую")]
            REWRITE_OTHER = 3,
        }

        public enum PrjTaskUserStateTypeEnum
        {
            [Display(Name = "Передано в работу")]
            SENT_TO_WORK = 1,
            [Display(Name = "Принято в работу")]
            RECEIVED_TO_WORK = 2,
            [Display(Name = "Выполнено")]
            COMPLETED = 3,
            [Display(Name = "Отменено")]
            CANCELED = 4,
        }

        public enum PrjTaskActionEnum
        {
            [Display(Name = "NONE")]
            NONE = 0,
            [Display(Name = "Добавить задание")]
            ADD_TASK = 1,
            [Display(Name = "Передать в работу")]
            SEND_TO_WORK = 2,
            [Display(Name = "Принять в работу")]
            RECEIVE_TO_WORK = 3,
            [Display(Name = "Выполнить и передать дальше")]
            COMPLETE_AND_FORWARD = 4,
            [Display(Name = "Выполнить и вернуть")]
            COMPLETE_AND_RETURN = 5,
            [Display(Name = "Выполнить и завершить")]
            COMPLETE_AND_CLOSE = 6,
            [Display(Name = "Отменить")]
            CANCEL = 7,
        }

        public enum PrjWorkStateTypeEnum
        {
            [Display(Name = "Активно")]
            ACTIVE = 1,
            [Display(Name = "Выполнено")]
            DONE = 2,
            [Display(Name = "Отменено")]
            CANCELED = 3,
            [Display(Name = "Возвращено")]
            RETURNED = 4,
        }

        public enum PrjWorkTypeEnum
        {
            [Display(Name = "Рассмотрение")]
            REVIEW = 1,
            [Display(Name = "Проектирование")]
            DESIGN = 2,
            [Display(Name = "Программирование")]
            PROGRAMM = 3,
            [Display(Name = "Тестирование")]
            TEST = 4,
            [Display(Name = "Документирование")]
            DOC = 5,
            [Display(Name = "Консультирование")]
            CONSULT = 6,
        }

        public enum PrjUserGroupFixedKindEnum
        {
            NONE = 0,
            [Display(Name = "Принятые")]
            RECEIVED = 1,
            [Display(Name = "Переданные")]
            SENT = 2,
            [Display(Name = "Выполненные")]
            DONE = 3,
            [Display(Name = "Отмененнные")]
            CANCELED = 4,
        }

        public enum PrjClaimFieldTypeEnum
        {
            NONE = 0,
            [Display(Name = "Ссылка")]
            REF = 1,
            [Display(Name = "Строка")]
            STRING = 2,
            [Display(Name = "Целое")]
            INTEGER = 3,
            [Display(Name = "Дата")]
            DATE = 4,
            [Display(Name = "Да-нет")]
            BOOLEAN = 5,
            [Display(Name = "Дробное")]
            NUMERIC = 6,
        }

        public enum PrjClaimFilterOpTypeEnum
        {
            NONE = 0,
            [Display(Name = "равно")]
            EQ = 1,
            [Display(Name = "не равно")]
            NEQ = 2,
            [Display(Name = "больше")]
            GT = 3,
            [Display(Name = "больше равно")]
            GTE = 4,
            [Display(Name = "меньше")]
            LT = 5,
            [Display(Name = "меньше равно")]
            LTE = 6,
            [Display(Name = "в диапазоне")]
            IN = 7,
            [Display(Name = "вне диапазона")]
            NIN = 8,
            [Display(Name = "содержит")]
            CONTAINS = 9,
            [Display(Name = "не содержит")]
            NOT_CONTAINS = 10,
        }

        public enum PrjVersionTypeEnum
        {
            [Display(Name = "Заявка")]
            APPLY = 0,
            [Display(Name = "Старые")]
            OLD = 1,
            [Display(Name = "Выпущенная")]
            RELEASED = 2,
            [Display(Name = "Текущая")]
            CURRENT = 3,
            [Display(Name = "Следующая")]
            NEXT = 4,
            [Display(Name = "Будущие")]
            PLANNED = 5,
        }

        public enum PrjUserGroupPeriodTypeEnum
        {
            [Display(Name = "сегодня")]
            TODAY = 1,
            [Display(Name = "сегодня и вчера")]
            TODAY_AND_YESTERDAY = 2,
            [Display(Name = "3 дня")]
            THREE_DAYS = 3,
            [Display(Name = "7 дней")]
            SEVEN_DAYS = 4,
        }

        #endregion
    }
}
