﻿using System;
using System.Threading;
using System.Threading.Tasks;


namespace CabinetSchedulerClient
{
    class Program
    {
        static void Main(string[] args)
        {                        
            try
            {
                // на продуктиве
                CabinetServ.CabinetServiceClient serv = new CabinetServ.CabinetServiceClient("BasicHttpBinding_ICabinetService");
                serv.StorageTrashClear("iguana");
                serv.ChangeCampaignState("iguana");
                //serv.UpdateSaleSummary("iguana");
                serv.CardStuffRefresh("iguana");

                /*
                int dayIndex = (int)DateTime.Now.DayOfWeek;
                if ((dayIndex == 2) || (dayIndex == 5)) // вторник или пятница
                {
                    serv.DelOldData("iguana");
                    serv.VacuumAnalyze("iguana");
                }
                */
                serv.DelOldData("iguana");
                serv.VacuumAnalyze("iguana");

                //Console.WriteLine("Done...");
                //Console.ReadKey();
            }
            catch (Exception ex)
            {
                //
                //Console.WriteLine(ex.Message);
                //Console.ReadKey();
            }
        }
    }

}
