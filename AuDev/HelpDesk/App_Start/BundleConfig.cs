﻿using System.Web;
using System.Web.Optimization;
using CabinetHelpDesk.Util;

namespace CabinetHelpDesk
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            string kendoVersion = HelpDeskUtil.GetKendoFolder();
            string jsBundleName = HelpDeskUtil.GetJsBundleName();
            string cssBundleName = HelpDeskUtil.GetCssBundleName();

            bundles.Add(new ScriptBundle(jsBundleName).Include(
                     "~/Scripts/jquery-{version}.js",
                     "~/Scripts/modernizr-*",
                     //"~/Scripts/kendo/" + kendoVersion + "/jszip.min.js",
                     "~/Scripts/kendo/" + kendoVersion + "/kendo.all.min.js",
                     "~/Scripts/kendo/" + kendoVersion + "/kendo.web.min.js",
                     "~/Scripts/kendo/" + kendoVersion + "/kendo.timezones.min.js",
                     "~/Scripts/kendo/" + kendoVersion + "/kendo.aspnetmvc.min.js",
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/respond.js",                     
                     "~/Scripts/cultures/kendo.culture.ru-RU.min.js",
                     "~/Scripts/Site-{version}.js"
                      ));

            bundles.Add(new StyleBundle(cssBundleName).Include(
                      "~/Content/bootstrap.css",
                      "~/Content/kendo/" + kendoVersion + "/kendo.common-bootstrap.min.css",
                      "~/Content/kendo/" + kendoVersion + "/kendo.bootstrap.min.css",
                      "~/Content/kendo/" + kendoVersion + "/kendo.dataviz.min.css",
                      "~/Content/kendo/" + kendoVersion + "/kendo.dataviz.bootstrap.min.css",
                      "~/Content/Site-{version}.css"
                      ));
            /*
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            */
        }
    }
}
