﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetHelpDesk.Models
{


    public class StoryLcViewModel : BaseViewModel        
    {
        public StoryLcViewModel()
            : base(Enums.MainObjectType.STORY_LC)
        {
            //
        }

        public StoryLcViewModel(story_lc item)
            : base(Enums.MainObjectType.STORY_LC)
        {
            lc_id = item.lc_id;
            story_id = item.story_id;
            state_id = item.state_id;
            date_beg = item.date_beg;
            date_end = item.date_end;
            crt_date = item.crt_date;
            upd_date = item.upd_date;
            crt_user = item.crt_user;
            upd_user = item.upd_user;
        }

        [Key()]
        public long lc_id { get; set; }
        public long story_id { get; set; }
        public long state_id { get; set; }
        public Nullable<System.DateTime> date_beg { get; set; }
        public Nullable<System.DateTime> date_end { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string crt_user { get; set; }
        public string upd_user { get; set; }
    }
}