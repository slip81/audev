﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace CabinetHelpDesk.Models
{
    public interface IHelpDeskService<T>
        where T : BaseViewModel
    {
        IQueryable<T> GetList();
        IQueryable<T> GetList_byParent(long parent_id);
        IQueryable<T> GetList_bySearch(string search1, string search2);
        T GetItem(long id);
        T Insert(T item, ModelStateDictionary modelState);
        T Update(T item, ModelStateDictionary modelState);
        bool Delete(T item, ModelStateDictionary modelState);
    }
}
