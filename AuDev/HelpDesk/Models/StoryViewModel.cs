﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetHelpDesk.Models
{

    public class StoryViewModel : BaseViewModel        
    {
        public StoryViewModel()
            : base(Enums.MainObjectType.STORY)
        {
            //
        }

        public StoryViewModel(story item)
            : base(Enums.MainObjectType.STORY)
        {
            story_id = item.story_id;
            story_text = item.story_text;
            story_subject = item.story_subject;
            org_name = item.org_name;
            org_address = item.org_address;
            person_name = item.person_name;
            person_phone = item.person_phone;
            person_email = item.person_email;
            person_id = item.person_id;
            person_ip = item.person_ip;
            init_state_id = item.init_state_id;
            curr_state_id = item.curr_state_id;
            is_answered = item.is_answered;
            resp_user = item.resp_user;
            resp_user_id = item.resp_user_id;
            crm_num = item.crm_num;
            crm_id = item.crm_id;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
            upd_user = item.upd_user;
            upd_date = item.upd_date;
            version_name = item.version_name;
        }
        
        [Key()]
        public long story_id { get; set; }
        [Required(ErrorMessage="Не задан текст вопроса")]
        [DataType(DataType.MultilineText)]
        public string story_text { get; set; }
        public string story_subject { get; set; }
        [Required(ErrorMessage = "Не задана организация")]
        public string org_name { get; set; }
        [Required(ErrorMessage = "Не задан адрес")]
        public string org_address { get; set; }
        [Required(ErrorMessage = "Не задано ФИО")]
        public string person_name { get; set; }
        [Required(ErrorMessage = "Не задан телефон")]
        [RegularExpression("\\(\\d{3}\\)\\s\\d{3}-\\d{4}", ErrorMessage = "Неверно задан телефон")]
        public string person_phone { get; set; }
        [Required(ErrorMessage = "Не задана почта")]
        [DataType(DataType.EmailAddress)]
        public string person_email { get; set; }
        public Nullable<long> person_id { get; set; }
        public string person_ip { get; set; }
        public long init_state_id { get; set; }
        public long curr_state_id { get; set; }
        public bool is_answered { get; set; }
        public string resp_user { get; set; }
        public Nullable<long> resp_user_id { get; set; }
        public Nullable<int> crm_num { get; set; }
        public Nullable<int> crm_id { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }        
        public string upd_user { get; set; }        
        //[RegularExpression(@"\d{1}\.\d{1}\.\d{1}\.\d{1}", ErrorMessage = "Неверно задан номер версии")]        
        public string version_name { get; set; }
    }
}