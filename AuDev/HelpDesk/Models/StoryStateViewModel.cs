﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetHelpDesk.Models
{


    //     <link href="@Url.Content("~/Content/kendo/2015.2.624/kendo.moonlight.min.css")" rel="stylesheet" type="text/css" />
    //     <link href="@Url.Content("~/Content/kendo/2015.2.624/kendo.dataviz.moonlight.min.css")" rel="stylesheet" type="text/css" />

    public class StoryStateViewModel : BaseViewModel        
    {
        public StoryStateViewModel()
            : base(Enums.MainObjectType.STORY_STATE)
        {
            //
        }

        public StoryStateViewModel(story_state item)
            : base(Enums.MainObjectType.STORY_STATE)
        {
            state_id = item.state_id;
            state_name = item.state_name;
            is_answered = item.is_answered;
        }

        [Key()]
        public long state_id { get; set; }
        public string state_name { get; set; }
        public bool is_answered { get; set; }
    }
}