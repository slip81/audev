﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Net.Mail;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetHelpDesk.Models
{
    public class StoryService : BaseService
    {

        protected override BaseViewModel xInsert(BaseViewModel item, ModelStateDictionary modelState)
        {
            Task insertStory = new Task(() =>
            {
                xxInsert(item, modelState);
            }
            );
            insertStory.Start();

            return new StoryViewModel() { ErrMess = "", };
        }

        private  BaseViewModel xxInsert(BaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StoryViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                //return null;
                return new BaseViewModel() { ErrMess = "Некорректные параметры" };
            }

            StoryViewModel storyAdd = (StoryViewModel)item;
            if (storyAdd == null)
            {
                modelState.AddModelError("", "Не задан вопрос");
                //return null;
                return new BaseViewModel() { ErrMess = "Не задан вопрос" };
            }

            DateTime now = DateTime.Now;

            using (var context = new AuMainDb())
            {
                story story = new story();
                story.story_text = storyAdd.story_text;
                story.story_subject = storyAdd.story_subject;
                story.org_name = storyAdd.org_name;
                story.org_address = storyAdd.org_address;
                story.person_name = storyAdd.person_name;
                story.person_phone = storyAdd.person_phone;
                story.person_email = storyAdd.person_email;
                story.person_id = storyAdd.person_id;
                story.person_ip = storyAdd.person_ip;
                story.version_name = storyAdd.version_name;

                story_state curr_state = context.story_state.Where(ss => ss.state_id == (int)Enums.StoryStateEnum.WAITING).FirstOrDefault();
                story.init_state_id = curr_state.state_id;
                story.curr_state_id = curr_state.state_id;
                story.is_answered = curr_state.is_answered;

                story.resp_user = null;
                story.resp_user_id = null;
                story.crm_num = null;
                story.crm_id = null;

                story.crt_date = now;
                story.crt_user = storyAdd.crt_user;
                story.upd_user = null;
                story.upd_date = null;

                context.story.Add(story);

                story_lc story_lc = new story_lc();
                story_lc.story = story;
                story_lc.state_id = curr_state.state_id;
                story_lc.date_beg = story.crt_date;
                story_lc.date_end = null;
                story_lc.crt_date = story.crt_date;
                story_lc.crt_user = story.crt_user;
                story_lc.upd_date = null;
                story_lc.upd_user = null;

                context.story_lc.Add(story_lc);
                
                string person_email = storyAdd.person_email;
                string mail_body = "";
                mail_body += "Наименование организации: " + storyAdd.org_name;
                mail_body += System.Environment.NewLine;
                mail_body += "Адрес: " + storyAdd.org_address;
                mail_body += System.Environment.NewLine;
                mail_body += "Контактное лицо: " + storyAdd.person_name;
                mail_body += System.Environment.NewLine;
                mail_body += "Контактный телефон: " + storyAdd.person_phone;
                mail_body += System.Environment.NewLine;
                mail_body += "Электронная почта: " + person_email;
                mail_body += System.Environment.NewLine;
                mail_body += "Вопрос: ";
                mail_body += System.Environment.NewLine;
                mail_body += storyAdd.story_text;
                mail_body += System.Environment.NewLine;

                // send ICQ message
                notify notify = null;
                var user_list = context.cab_user.Where(ss => ss.crm_user_role_id == (int)Enums.CabUserRole.SERVICE).ToList();
                if ((user_list != null) && (user_list.Count > 0))
                {
                    foreach (var user in user_list)
                    {
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = "Новый вопрос в HelpDesk";
                        notify.message += System.Environment.NewLine;
                        notify.message += mail_body;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.HELP_DESCK;
                        notify.sent_date = null;                        
                        notify.target_user_id = user.user_id;
                        context.notify.Add(notify);
                    }
                }
                
                context.SaveChanges();

                // !!!
                // send email
                if (!(System.Diagnostics.Debugger.IsAttached))
                //if (1==1)
                {
                    //CabinetHelpDesk.Util.HelpDeskUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", "support@aptekaural.ru", "Новое сообщение в HelpDesk", mail_body, null);
                    GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", "support@aptekaural.ru", "Новое сообщение в HelpDesk", mail_body, null);

                    //
                    if (!String.IsNullOrEmpty(person_email))
                    {
                        //System.Threading.Thread.Sleep(30000);
                        //CabinetHelpDesk.Util.HelpDeskUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", person_email, "Заявка принята", "Добрый день! Ваша заявка принята службой технической поддержки.", null);
                        GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", person_email, "Заявка принята", "Добрый день! Ваша заявка принята службой технической поддержки.", null);
                    }
                }
                
                return new StoryViewModel(story);
            }
        }

    }
}