﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetHelpDesk.Models
{
    public class BaseService : IDisposable, IHelpDeskService<BaseViewModel>
    {        
        protected AuMainDb dbContext;
        protected BaseViewModel modelBeforeChanges;

        public BaseService()
        {
            dbContext = new AuMainDb();
        }

        public void Dispose()
        {
            dbContext.Dispose();
        }

        public BaseViewModel GetItem(long id)
        {
            try
            {
                return xGetItem(id);
            }
            catch (Exception ex)
            {
                // log
                return null;
            }
        }

        public IQueryable<BaseViewModel> GetList()
        {
            try
            {
                return xGetList();
            }
            catch (Exception ex)
            {
                // log
                return null;
            }
        }

        public IQueryable<BaseViewModel> GetList_byParent(long parent_id)
        {
            try
            {
                return xGetList_byParent(parent_id);
            }
            catch (Exception ex)
            {
                // log
                return null;
            }
        }

        public IQueryable<BaseViewModel> GetList_bySearch(string search1, string search2)
        {
            try
            {
                return xGetList_bySearch(search1, search2);
            }
            catch (Exception ex)
            {
                // log
                return null;
            }
        }

        public BaseViewModel Insert(BaseViewModel item, ModelStateDictionary modelState)
        {
            modelBeforeChanges = null;
            try
            {                
                var res = xInsert(item, modelState);
                //ToLog("Добавление", (long)Enums.LogEventType.CABINET, new LogObject(res != null ? res : item));
                return res;
            }
            catch (Exception ex)
            {                
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                //ToLog("Ошибка при добавлении", (long)Enums.LogEventType.CABINET, new LogObject(item));
                //return null;
                return new BaseViewModel() { ErrMess = GlobalUtil.ExceptionInfo(ex) };
            }
        }

        public BaseViewModel Update(BaseViewModel item, ModelStateDictionary modelState)
        {
            modelBeforeChanges = null;
            try
            {                
                var res = xUpdate(item, modelState);
                //ToLog("Изменение", (long)Enums.LogEventType.CABINET, modelBeforeChanges == null ? null : new LogObject(modelBeforeChanges), new LogObject(res != null ? res : item));
                return res;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                //ToLog("Ошибка при изменении", (long)Enums.LogEventType.CABINET, modelBeforeChanges == null ? null : new LogObject(modelBeforeChanges), new LogObject(item));
                //return null;
                return new BaseViewModel() { ErrMess = GlobalUtil.ExceptionInfo(ex) };
            }
        }

        public bool Delete(BaseViewModel item, ModelStateDictionary modelState)
        {            
            try
            {
                var res = xDelete(item, modelState);
                //if (res)
                    //ToLog("Удаление", (long)Enums.LogEventType.CABINET, new LogObject(item));
                return res;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                //ToLog("Ошибка при удалении", (long)Enums.LogEventType.CABINET, new LogObject(item));
                return false;
            }
        }



        protected virtual BaseViewModel xGetItem(long id)
        {
            throw new NotImplementedException();
        }

        protected virtual IQueryable<BaseViewModel> xGetList()
        {
            throw new NotImplementedException();
        }

        protected virtual IQueryable<BaseViewModel> xGetList_byParent(long parent_id)
        {

            throw new NotImplementedException();
        }

        protected virtual IQueryable<BaseViewModel> xGetList_bySearch(string search1, string search2)
        {
            throw new NotImplementedException();
        }

        protected virtual BaseViewModel xInsert(BaseViewModel item, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }

        protected virtual BaseViewModel xUpdate(BaseViewModel item, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }

        protected virtual bool xDelete(BaseViewModel item, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }
        
    }

}
