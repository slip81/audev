﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetHelpDesk.Models;

namespace CabinetHelpDesk.Controllers
{
    public class BaseController : Controller
    {
        #region Service

        private List<BaseService> ServiceList;

        protected T GetService<T>()
            where T : BaseService, new()
        {
            var res = ServiceList.Where(ss => ss.GetType()== typeof(T)).Cast<T>().FirstOrDefault();
            if (res == null)
            {
                res = new T();
                ServiceList.Add(res);
            }
            return res;
        }                

        #endregion

        #region Constructor

        public BaseController()
            : base()
        {
            ServiceList = new List<BaseService>();
        }

        #endregion

        #region Dispose

        protected override void Dispose(bool disposing)
        {
            if (ServiceList != null)
            {
                foreach (var item in ServiceList)
                {
                    if (item is IDisposable)
                        ((IDisposable)item).Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #endregion
    }
}
