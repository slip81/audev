﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetHelpDesk.Models;
using AuDev.Common.Util;

using BotDetect.Web;
using BotDetect.Web.UI.Mvc;

namespace CabinetHelpDesk.Controllers
{
    public class HomeController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            HttpCookie aurit_hd = Request.Cookies["aurit_hd"];
            string org_name = "";
            string org_address = "";
            string person_name = "";
            string person_phone = "";
            string person_email = "";
            string version_name = "";

            if (aurit_hd != null)
            {
                org_name = aurit_hd.Values["org_name"];
                org_address = aurit_hd.Values["org_address"];
                person_name = aurit_hd.Values["person_name"];
                person_phone = aurit_hd.Values["person_phone"];
                person_email = aurit_hd.Values["person_email"];
                version_name = aurit_hd.Values["version_name"];
            }
            ViewBag.org_name = org_name;
            ViewBag.org_address = org_address;
            ViewBag.person_name = person_name;
            ViewBag.person_phone = person_phone;
            ViewBag.person_email = person_email;
            ViewBag.version_name = version_name;
            // !!!
            //return View(new StoryViewModel() { person_phone = person_phone, person_email = person_email, });
            return View(new StoryViewModel() { person_phone = person_phone, person_email = person_email, version_name = version_name,});
        }

        [HttpPost]
        //[CaptchaValidation("txtCaptcha", "SendVerifCaptcha", "Captcha: Неверный код подтверждения")]
        public ActionResult SendStory(StoryViewModel storyAdd)
        {            
            string Mess = "";
            string ip = this.HttpContext.Request.UserHostAddress;
            try
            {                
                if (ModelState.IsValid)
                {
                    HttpCookie aurit_hd = Request.Cookies["aurit_hd"];
                    if (aurit_hd == null)
                        aurit_hd = new HttpCookie("aurit_hd");
                    aurit_hd.Expires = DateTime.Today.AddMonths(12);
                    aurit_hd.Values["org_name"] = storyAdd.org_name;
                    aurit_hd.Values["org_address"] = storyAdd.org_address;
                    aurit_hd.Values["person_name"] = storyAdd.person_name;
                    aurit_hd.Values["person_phone"] = storyAdd.person_phone;
                    aurit_hd.Values["person_email"] = storyAdd.person_email;
                    aurit_hd.Values["version_name"] = storyAdd.version_name;                    
                    Response.SetCookie(aurit_hd);

                    storyAdd.person_ip = ip;
                    var res = GetService<StoryService>().Insert(storyAdd, ModelState);
                    Mess = res.ErrMess;
                    if (!String.IsNullOrEmpty(Mess))
                    {
                        Mess = "Ошибка: " + Mess;
                        return Json(Mess, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        Mess = "Ваш вопрос отправлен. Спасибо !";
                        return Json(Mess, JsonRequestBehavior.AllowGet);
                    }
                    //return Json(new[] { storyAdd }, JsonRequestBehavior.AllowGet);
                    //return View(res == null ? storyAdd : res);
                }
                else
                {
                    Mess = "Неверный код подтверждения";
                    return Json(Mess, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Mess = GlobalUtil.ExceptionInfo(ex);
                return Json(Mess, JsonRequestBehavior.AllowGet);
                //return Json(new[] { Mess }, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Home/CheckCaptcha/
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None, NoStore = true)]
        public JsonResult CheckCaptcha(string captchaId, string instanceId, string userInput)
        {
            bool ajaxValidationResult =
              CaptchaControl.AjaxValidate(captchaId, userInput, instanceId);

            return Json(ajaxValidationResult, JsonRequestBehavior.AllowGet);
        }
    }
}