$(function () {
    $("form").kendoValidator();
    //
    $(document).keypress(function (e) {
        if (e.which == 13 && e.target.tagName != 'TEXTAREA') {
            return false;
        }
    });
    //
    $('#btnSend').click(function (e) {
        var sendResultWindow = $("<div />").kendoWindow({
            actions: [],
            title: "�������� �������...",
            resizable: false,
            modal: true,
            width: "400px",
            height: "100px"
        });

        var validator = $("form").kendoValidator().data("kendoValidator");

        // get client-side Captcha object instance
        var captchaObj = $("#txtCaptcha").get(0).Captcha;

        // gather data required for Captcha validation
        var params = {}
        params.CaptchaId = captchaObj.Id;
        params.InstanceId = captchaObj.InstanceId;
        params.UserInput = $("#txtCaptcha").val();

        e.preventDefault();

        if (validator.validate()) {

            // make asynchronous Captcha validation request
            $.getJSON('Home/CheckCaptcha', params, function (result) {
                if (true === result) {
                    //alert('Check passed');
                    //$("#btnSend").kendoButton().data("kendoButton").enable(false);
                    $('#btnSend').addClass('disabled');

                    sendResultWindow.data("kendoWindow")
                        .content($("#send-story-window").html())
                        .center().open();
                    sendResultWindow.data("kendoWindow").title("�������� �������...");
                    sendResultWindow.find(".return-to-send").prop('disabled', true);
                    sendResultWindow.find(".return-to-send")
                            .click(function () {
                                //$("#btnSend").kendoButton().data("kendoButton").enable(true);
                                $('#btnSend').removeClass('disabled');
                                //$("story_text").val(' ');
                                sendResultWindow.data("kendoWindow").close();
                                captchaObj.ReloadImage();
                            })
                         .end();

                    var sendData = getData();
                    $.ajax({
                        url: 'Home/SendStory',
                        data: JSON.stringify(sendData),
                        type: "POST",
                        contentType: 'application/json; charset=windows-1251',
                        success: function (response) {
                            //alert('������ ���������');                    
                            sendResultWindow.data("kendoWindow").title(response);
                            sendResultWindow.find(".return-to-send").prop('disabled', false);
                        },
                        error: function (response) {
                            //$("#btnSend").kendoButton().data("kendoButton").enable(true);
                            //alert('������ ��� ������� ��������� ������');
                            sendResultWindow.data("kendoWindow").title(response);
                            sendResultWindow.find(".return-to-send").prop('disabled', false);
                        }
                    });
                } else {
                    //alert('�������� ��� �������������');
                    // always change Captcha code if validation fails
                    //captchaObj.ReloadImage();
                    sendResultWindow.data("kendoWindow")
                        .content($("#captcha-validate-window").html())
                        .center().open();
                    sendResultWindow.data("kendoWindow").title("�������� ��� �������������");
                    sendResultWindow.find(".return-to-send").prop('disabled', false);
                    sendResultWindow.find(".return-to-send")
                            .click(function () {
                                //$("#btnSend").kendoButton().data("kendoButton").enable(true);
                                $('#btnSend').removeClass('disabled');
                                sendResultWindow.data("kendoWindow").close();
                                captchaObj.ReloadImage();
                            })
                         .end();

                }
            });
        }
        else {
            //alert('�� �������� ���������');
        };
    });
    //
    $('#btnClear').click(function (e) {
        //alert('btnClear clicked !');        
        eraseCookieFromAllPaths('aurit_hd');
        //
        $("#org_name").val('');
        $("#org_address").val('');
        $("#person_name").val('');
        $("#person_phone").val('');
        $("#person_email").val('');
        $("#version_name").val('');
        $("#story_text").val('');        
    });
});

function getData() {

    var org_name_val = $("#org_name").val();
    var org_address_val = $("#org_address").val();
    var person_name_val = $("#person_name").val();
    var person_phone_val = $("#person_phone").val();
    var person_email_val = $("#person_email").val();
    var version_name_val = $("#version_name").val();
    var story_text_val = $("#story_text").val();

    return {
        org_name: org_name_val, org_address: org_address_val, person_name: person_name_val,
        person_phone: person_phone_val, person_email: person_email_val, version_name: version_name_val, story_text: story_text_val
    }
};

function eraseCookieFromAllPaths(name) {
    // This function will attempt to remove a cookie from all paths.
    var pathBits = location.pathname.split('/');
    var pathCurrent = ' path=';

    // do a simple pathless delete first.
    document.cookie = name + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT;';

    for (var i = 0; i < pathBits.length; i++) {
        pathCurrent += ((pathCurrent.substr(-1) != '/') ? '/' : '') + pathBits[i];
        document.cookie = name + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT;' + pathCurrent + ';';
    }
};