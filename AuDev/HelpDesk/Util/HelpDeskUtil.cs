﻿using System;
using System.Collections.Generic;

namespace CabinetHelpDesk.Util
{
    public static class HelpDeskUtil
    {

        public static string GetKendoFolder()
        {
            return "2015.2.624";
        }

        public static string GetJsBundleName()
        {
            return "~/Scripts/kendo/" + GetKendoFolder() + "/js";
        }

        public static string GetCssBundleName()
        {
            return "~/Content/kendo/" + GetKendoFolder() + "/css";
        }
    }
}