﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using auesn.codegen.Model;
using auesn.util;
using auesn.dbmodel;

namespace auesn.codegen
{
    public partial class CodeGen
    {
        //private const string pathToSrc = @"..\..\..\db\Controllers\DbController_gen.cs";

        public CodeGen()
        {
            setupKnownTypes();
        }

        public void Start()
        {
            consoleEnd("Нажмите для генерации кода в файл: " + System.Configuration.ConfigurationManager.AppSettings["pathToSrc"]);

            try
            {
                generate();
                consoleEnd("Генерация завершена");
            }
            catch (Exception ex)
            {
                consoleEnd("Ошибка: " + GlobalUtil.ExceptionInfo(ex));
            }
        }

        private void generate()
        {
            using (var dbContext = new AuEsnDb())
            {
                List<MainTable> mainTableList = dbContext.Database.SqlQuery<MainTable>("select * from adm.main_table order by main_table_name").ToList();
                if ((mainTableList == null) || (mainTableList.Count <= 0))
                    throw new InvalidOperationException("Нет строк в main_table");

                CodeCompileUnit servUnit = new CodeCompileUnit();

                var servEmptyNamespace = new CodeNamespace();

                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System.IO"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System.Linq"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System.Net"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System.Net.Http"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System.Net.Http.Headers"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System.Security.Claims"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System.Text"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("System.Web.Http"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("Newtonsoft.Json"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("auesn.rest.Service"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("auesn.rest.Model"));
                servEmptyNamespace.Imports.Add(new CodeNamespaceImport("auesn.util"));

                CodeTypeDeclaration servClass = new CodeTypeDeclaration()
                {
                    Name = "DbController",
                    IsClass = true,
                    IsPartial = true,
                };
                servClass.BaseTypes.Add("BaseController");

                CodeMemberField tableServiceField = createPrivateProperty("tableService", "ITableService");
                servClass.Members.Add(tableServiceField);

                CodeConstructor dbControllerConstr = new CodeConstructor();
                dbControllerConstr.Attributes = MemberAttributes.Public;
                dbControllerConstr.Parameters.Add(new CodeParameterDeclarationExpression("ITableService", "_tableService"));
                dbControllerConstr.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "tableService = _tableService;"));
                servClass.Members.Add(dbControllerConstr);

                CodeMemberMethod method = null;
                string tableName = "";
                string viewName = "";
                string modelName = "";
                string tableUuid = "";
                string tableId = "";
                bool viewExists = false;
                foreach (var mainTable in mainTableList)
                {
                    tableName = mainTable.main_table_name;
                    viewName = String.IsNullOrWhiteSpace(mainTable.view_name) ? tableName : mainTable.view_name.Trim();
                    modelName = convertToCamelCase(tableName);
                    tableUuid = "model." + tableName + "_uuid";
                    tableId = tableName + "_id";
                    viewExists = !String.IsNullOrWhiteSpace(mainTable.view_name);

                    if (mainTable.allow_select)
                    {
                        string sql = "SELECT column_name"
                            + " FROM information_schema.columns"
                            + " WHERE table_schema = 'main'"
                            + " AND table_name = '" + tableName + "'"
                            + " AND column_name like '%id'"
                            + " AND column_name not like '%uuid'"
                            //+ " AND column_name != '" + tableId + "'"
                            + " ORDER BY column_name";
                        List<string> idColumnNameList = dbContext.Database.SqlQuery<string>(sql).ToList();                        

                        method = createPublicMethod(tableName, "HttpResponseMessage");
                        method.CustomAttributes.Add(
                            new CodeAttributeDeclaration()
                            {
                                Name = "Route",
                                Arguments =
                                {
                                    new CodeAttributeArgument(new CodeSnippetExpression("\"" + tableName + "\""))
                                },
                            }
                        );
                        method.CustomAttributes.Add(
                            new CodeAttributeDeclaration()
                            {
                                Name = "HttpGet",
                            }
                        );

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "try"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "{"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "var queryParams = this.Request.GetQueryStrings();"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "IEnumerable<string> preferHeaders;"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "string preferHeader = string.Empty;"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "if (Request.Headers.TryGetValues(" + "\"" + "Prefer" + "\"" + ", out preferHeaders))"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "preferHeader = preferHeaders.FirstOrDefault();"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "}"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "preferHeader = String.IsNullOrWhiteSpace(preferHeader) ? string.Empty : preferHeader.Trim().ToLower().TrimEnd(';');"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "if (preferHeader == " + "\"" + "count=true" + "\"" + ")"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "var result = tableService.GetRowCount<auesn.rest.Model." + modelName + ">(" + "\"" + viewName + "\"" + ", queryParams);"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "return new HttpResponseMessage()"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "StatusCode = HttpStatusCode.OK,"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "Content = new StringContent("));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "result.ToString(),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "Encoding.GetEncoding(" + "\"" + "windows-1251" + "\"" + "),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "AuEsnConst.MEDIA_TYPE_TEXT),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "};"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "}"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "else"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "var result = tableService.GetRows<auesn.rest.Model." + modelName + ">(" + "\"" + viewName + "\"" + ", queryParams);"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "var allfileds = ((queryParams.ContainsKey(" + "\"" + "allfields" + "\"" + ")) && (queryParams[" + "\"" + "allfields" + "\"" + "] == " + "\"" + "true" + "\"" + "));"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "var jsonResolver = getJsonResolver<auesn.rest.Model." + modelName + ">(allfileds);"));

                        if ((idColumnNameList != null) && (idColumnNameList.Count > 0))
                        {
                            method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "if (!allfileds)"));
                            method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "{"));
                            foreach (var idColumnName in idColumnNameList)
                            {
                                method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "jsonResolver.Ignore(typeof(auesn.rest.Model." + modelName + "), " + "\"" + idColumnName + "\"" + ");"));
                            }
                            method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "}"));                            
                        }
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "var jsonSettings = new JsonSerializerSettings() { ContractResolver = jsonResolver };"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "return new HttpResponseMessage()"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "StatusCode = HttpStatusCode.OK,"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "Content = new StringContent("));

                        if ((idColumnNameList != null) && (idColumnNameList.Count > 0))
                        {
                            method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "JsonConvert.SerializeObject(result, jsonSettings),"));
                        }
                        else
                        {
                            method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "JsonConvert.SerializeObject(result),"));
                        }

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "Encoding.GetEncoding(" + "\"" + "windows-1251" + "\"" + "),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "AuEsnConst.MEDIA_TYPE_JSON),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "};"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "}"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "}"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "catch (Exception ex)"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "{"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "return new HttpResponseMessage()"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "StatusCode = HttpStatusCode.InternalServerError,"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "Content = new StringContent("));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "GlobalUtil.ExceptionInfo(ex),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "Encoding.GetEncoding(" + "\"" + "windows-1251" + "\"" + "),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "AuEsnConst.MEDIA_TYPE_TEXT),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "};"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "}"));

                        method.StartDirectives.Add(new CodeRegionDirective(CodeRegionMode.Start, mainTable.main_table_name + " (GET)"));
                        method.EndDirectives.Add(new CodeRegionDirective(CodeRegionMode.End, ""));

                        servClass.Members.Add(method);
                    }

                    if (mainTable.allow_insert)
                    {
                        method = createPublicMethod(tableName + "_post", "HttpResponseMessage");
                        method.Parameters.Add(new CodeParameterDeclarationExpression("auesn.rest.Model." + modelName, "model"));
                        method.CustomAttributes.Add(
                            new CodeAttributeDeclaration()
                            {
                                Name = "Route",
                                Arguments =
                                {
                                    new CodeAttributeArgument(new CodeSnippetExpression("\"" + tableName + "\""))
                                },
                            }
                        );
                        method.CustomAttributes.Add(
                            new CodeAttributeDeclaration()
                            {
                                Name = "HttpPost",
                            }
                        );

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "try"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "{"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "var res = tableService.InsertOrUpdate<auesn.rest.Model." + modelName + ">(" + "\"" + tableName + "\"" + ", " + tableUuid + ", model);"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "if ((res == null) || (res.HasError))"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "return new HttpResponseMessage()"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "StatusCode = HttpStatusCode.InternalServerError,"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "Content = new StringContent("));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "(res != null && res.HasError) ? res.ErrMess : " + "\"" + "Ошибка при добавлении / изменении" + "\"" + ","));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "Encoding.GetEncoding(" + "\"" + "windows-1251" + "\"" + "),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "AuEsnConst.MEDIA_TYPE_TEXT),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "};"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "}"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "HttpResponseMessage result = new HttpResponseMessage()"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "StatusCode = HttpStatusCode.OK,"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "};"));
                        //method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "result.Headers.Add(" + "\"" + "Location" + "\"" + ", res.Uuid);"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "result.Headers.Add(" + "\"" + "x-uuid" + "\"" + ", res.Uuid);"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "return result;"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "}"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "catch (Exception ex)"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "{"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "return new HttpResponseMessage()"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "StatusCode = HttpStatusCode.InternalServerError,"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "Content = new StringContent("));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "GlobalUtil.ExceptionInfo(ex),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "Encoding.GetEncoding(" + "\"" + "windows-1251" + "\"" + "),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "AuEsnConst.MEDIA_TYPE_TEXT),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "};"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "}"));

                        method.StartDirectives.Add(new CodeRegionDirective(CodeRegionMode.Start, mainTable.main_table_name + " (POST)"));
                        method.EndDirectives.Add(new CodeRegionDirective(CodeRegionMode.End, ""));

                        servClass.Members.Add(method);
                    }

                    if (mainTable.allow_delete)
                    {
                        method = createPublicMethod(tableName + "_del", "HttpResponseMessage");                        
                        method.CustomAttributes.Add(
                            new CodeAttributeDeclaration()
                            {
                                Name = "Route",
                                Arguments =
                                {
                                    new CodeAttributeArgument(new CodeSnippetExpression("\"" + tableName + "\""))
                                },
                            }
                        );
                        method.CustomAttributes.Add(
                            new CodeAttributeDeclaration()
                            {
                                Name = "HttpDelete",
                            }
                        );

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "try"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "{"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "var queryParams = this.Request.GetQueryStrings();"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "var res = tableService.Delete<auesn.rest.Model." + modelName + ">(" + "\"" + tableName + "\"" + (viewExists ? (", " + "\"" + viewName + "\"") : ", null") + ", queryParams);"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "if (!res)"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "return new HttpResponseMessage()"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "StatusCode = HttpStatusCode.InternalServerError,"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "Content = new StringContent("));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "\"" + "Ошибка при удалении" + "\"" + ","));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "Encoding.GetEncoding(" + "\"" + "windows-1251" + "\"" + "),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(28) + "AuEsnConst.MEDIA_TYPE_TEXT),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "};"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "}"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "return new HttpResponseMessage()"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "StatusCode = HttpStatusCode.OK,"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "};"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "}"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "catch (Exception ex)"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "{"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "return new HttpResponseMessage()"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "{"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "StatusCode = HttpStatusCode.InternalServerError,"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(20) + "Content = new StringContent("));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "GlobalUtil.ExceptionInfo(ex),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "Encoding.GetEncoding(" + "\"" + "windows-1251" + "\"" + "),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(24) + "AuEsnConst.MEDIA_TYPE_TEXT),"));
                        method.Statements.Add(new CodeSnippetStatement(getSpaces(16) + "};"));

                        method.Statements.Add(new CodeSnippetStatement(getSpaces(12) + "}"));

                        method.StartDirectives.Add(new CodeRegionDirective(CodeRegionMode.Start, mainTable.main_table_name + " (DELETE)"));
                        method.EndDirectives.Add(new CodeRegionDirective(CodeRegionMode.End, ""));

                        servClass.Members.Add(method);
                    }
                }
                

                CodeNamespace servNamespace = new CodeNamespace("auesn.rest.Controllers");            
                servNamespace.Types.Add(servClass);

                servUnit.Namespaces.Add(servEmptyNamespace);
                servUnit.Namespaces.Add(servNamespace);

                using (var outFile = File.Open(System.Configuration.ConfigurationManager.AppSettings["pathToSrc"], FileMode.Create))
                using (var fileWriter = new StreamWriter(outFile))
                using (var indentedTextWriter = new IndentedTextWriter(fileWriter, "    "))
                {
                    // Generate source code using the code provider.
                    var provider = new Microsoft.CSharp.CSharpCodeProvider();
                    provider.GenerateCodeFromCompileUnit(servUnit,
                        indentedTextWriter,
                        new CodeGeneratorOptions() { BracingStyle = "C" });
                }
            }
        }

    }
}
