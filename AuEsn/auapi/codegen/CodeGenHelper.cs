﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using auesn.codegen.Model;
using auesn.util;

namespace auesn.codegen
{
    public partial class CodeGen
    {
        private void consoleWrite(string mess)
        {
            if (!String.IsNullOrWhiteSpace(mess))
                Console.WriteLine(mess);
        }

        private void consoleEnd(string mess = null)
        {
            if (!String.IsNullOrWhiteSpace(mess))
                Console.WriteLine(mess);
            Console.ReadKey();
        }

        private string getSpaces(int cnt)
        {
            return new String(' ', cnt);
        }

        private string convertToCamelCase(string str)
        {
            return System.Globalization.CultureInfo.InvariantCulture.TextInfo.ToTitleCase(breakOnUpperCase(str).ToLowerInvariant().Trim()).Replace("_", "").Replace("-", "").Replace(".", "");
        }

        private string breakOnUpperCase(string str)
        {
            // переводит DocumentAction -> Document_Action
            // надо упростить

            List<char> chars = new List<char>();

            int ind = 0;
            char new_char = ' ';
            foreach (var upc in str.ToArray())
            {
                new_char = upc;
                if (Char.IsUpper(upc))
                {
                    if (str.IndexOf(upc) != 0)
                    {
                        chars.Insert(ind, '_');
                        ind++;
                    }
                }

                chars.Insert(ind, new_char);
                ind++;
            }

            return new string(chars.ToArray());
        }

        private string firstCharacterToLower(string str)
        {
            if (String.IsNullOrEmpty(str) || Char.IsLower(str, 0))
                return str;

            return Char.ToLowerInvariant(str[0]) + str.Substring(1);
        }

        private CodeMemberField createPrivateProperty(string name, string typeName)
        {
            return new CodeMemberField()
            {
                Name = name,
                Attributes = MemberAttributes.Private,
                Type = createType(typeName),
            }
            ;
        }

        private CodeMemberProperty createPublicProperty(string name, string typeName)
        {
            return new CodeMemberProperty()
            {
                Name = name,
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                Type = createType(typeName),
            }
            ;
        }

        private CodeMemberMethod createPrivateMethod(string name, string typeName)
        {
            return new CodeMemberMethod()
            {
                Name = name,
                Attributes = MemberAttributes.Private,
                ReturnType = createType(typeName),
            }
            ;
        }

        private CodeMemberMethod createPublicMethod(string name, string typeName)
        {
            return new CodeMemberMethod()
            {
                Name = name,
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                ReturnType = createType(typeName),
            }
            ;
        }

        private CodeMemberMethod createFinalMethod(string name, string typeName)
        {
            return new CodeMemberMethod()
            {
                Name = name,
                Attributes = MemberAttributes.Final,
                ReturnType = createType(typeName),
            }
            ;
        }

        private CodeMemberMethod createPublicStaticMethod(string name, string typeName)
        {
            return new CodeMemberMethod()
            {
                Name = name,
                Attributes = MemberAttributes.Public | MemberAttributes.Final | MemberAttributes.Static,
                ReturnType = createType(typeName),
            }
            ;
        }

        private Dictionary<string, Type> _knownTypes = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);

        private void setupKnownTypes()
        {
            _knownTypes["int"] = typeof(int);
            _knownTypes["double"] = typeof(double);
            _knownTypes["bool"] = typeof(bool);
            _knownTypes["string"] = typeof(string);
            _knownTypes["float"] = typeof(float);
            _knownTypes["decimal"] = typeof(decimal);
            _knownTypes["long"] = typeof(long);
            _knownTypes["float"] = typeof(float);
            _knownTypes["short"] = typeof(short);
            _knownTypes["void"] = typeof(void);
        }

        private Type getShortcutType(string name)
        {
            Type toReturn = null;
            _knownTypes.TryGetValue(name, out toReturn);
            return toReturn; //returns null if not found
        }

        private CodeTypeReference createType(string name)
        {
            Type tp1 = getShortcutType(name);
            if (tp1 != null)
                return new CodeTypeReference(tp1);
            else
                return new CodeTypeReference(name);
        }
    }
}