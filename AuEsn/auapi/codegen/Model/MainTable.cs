﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace auesn.codegen.Model
{
    public class MainTable
    {
        public MainTable()
        {
            //auesn.codegen.Db.main_table
        }

        public long main_table_id { get; set; }
        public string main_table_name { get; set; }
        public bool allow_select { get; set; }
        public bool allow_insert { get; set; }
        public bool allow_update { get; set; }
        public bool allow_delete { get; set; }
        public bool is_logged { get; set; }
        public string view_name { get; set; }
    }
}
