﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using auesn.dbmodel;
using auesn.util;

namespace auesn.handler.austock
{
    // обязательно должен называться именно так
    public class Worker
    {
        // нужен атрибут для этого метода
        public bool DoWork(Guid docUuid)
        {
            // doc_type = 2

            try
            {
                return doWork(docUuid);                
            }
            catch (Exception ex)
            {
                DbLogger.Log(GlobalUtil.ExceptionInfo(ex));
                return false;
            }            
        }

        private bool doWork(Guid docUuid)
        {
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string userName = "worker";

            using (var context = new AuEsnDb())
            {
                Action<auesn.dbmodel.doc, Enums.DocStateEnum, auesn.dbmodel.doc_client_to, bool, string> setDocState = (item, state, cl, isSuccess, mess) =>
                {
                    doc_period doc_period = new doc_period();
                    doc_period.doc_id = item.doc_id;
                    doc_period.doc_state_id = (int)state;
                    doc_period.crt_date = now;
                    doc_period.crt_user = userName;
                    doc_period.upd_date = now;
                    doc_period.upd_user = userName;
                    doc_period.is_deleted = false;
                    context.doc_period.Add(doc_period);

                    item.doc_state_id = (int)state;
                    item.upd_date = now;
                    item.upd_user = userName;

                    if (cl == null)
                    {
                        List<doc_client_to> doc_client_to_list = context.doc_client_to.Where(ss => ss.doc_id == item.doc_id && !ss.is_deleted).ToList();
                        if ((doc_client_to_list != null) && (doc_client_to_list.Count > 0))
                        {
                            foreach (var doc_client_to in doc_client_to_list)
                            {
                                doc_client_to.doc_state_id = (int)state;
                                doc_client_to.is_success = isSuccess;
                                if (!String.IsNullOrWhiteSpace(mess))
                                    doc_client_to.mess = mess;
                                doc_client_to.upd_date = now;
                                doc_client_to.upd_user = userName;
                            }
                        }
                    }
                    else
                    {
                        cl.doc_state_id = (int)state;
                        cl.is_success = isSuccess;
                        if (!String.IsNullOrWhiteSpace(mess))
                            cl.mess = mess;
                        cl.upd_date = now;
                        cl.upd_user = userName;
                    }

                    context.SaveChanges();
                };

                doc doc = context.doc.Where(ss => ss.doc_uuid == docUuid && !ss.is_deleted && ss.doc_state_id == (int)Enums.DocStateEnum.WEB_PROCESS).FirstOrDefault();
                if (doc == null)
                {                    
                    return abortWork(null, "Не найден документ с кодом " + docUuid.ToString());
                }

                setDocState(doc, Enums.DocStateEnum.ESN_QUEUE, null, true, null);

                // !!!
                return completeWork(docUuid, "Документ передан в сервис ЕСН");
            }
        }

        private bool completeWork(Guid docUuid, string mess = null)
        {
            if (!String.IsNullOrWhiteSpace(mess))
                DbLogger.Log(mess);

            return true;
        }

        private bool abortWork(Guid? docUuid, string mess = null)
        {
            if (!String.IsNullOrWhiteSpace(mess))
                DbLogger.Log(mess);

            return true;
        }
    }
}
