﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace auesn.msgsvc
{
    public static class Logger
    {
        public static void Log(string mess)
        {
            StreamWriter sw = null;
            DateTime now = DateTime.Now;
            string thismonth_str = now.ToString("yyyyMM");
            string now_str = now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\msgsvclog_" + thismonth_str + ".txt", true);
                sw.WriteLine("[" + now_str + "]: " + (String.IsNullOrEmpty(mess) ? "-" : mess));
                sw.Flush();
                sw.Close();
            }
            catch
            {
                //
            }
        }

        public static void Log(Exception ex)
        {
            if (ex != null)
                Log(ExceptionInfo(ex));
        }

        public static string ExceptionInfo(Exception e)
        {
            if (e == null)
                return "";

            return e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                );
        }
    }
}
