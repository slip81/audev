﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using auesn.util;
using auesn.dbmodel;

namespace auesn.msgsvc
{
    public partial class MainService : ServiceBase
    {

        /*
            c:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe "c:\Release\auesn\msgsvc\msgsvc.exe"
            c:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe /u "c:\Release\auesn\msgsvc\msgsvc.exe"
        */

        private Timer timer1 = null;
        private int? lastNotifyId = null;

        private string intervalinseconds = "";
        private string createworkerurl = "";
        private bool svclogEnabled = false;
        private bool apilogEnabled = false;

        private int intervalinseconds_int = 150; //  2.5 min
        private int intervalinmiliseconds = 150000; //  2.5 min

        public MainService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            lastNotifyId = null;
            //
            intervalinseconds = ConfigurationManager.AppSettings["intervalinseconds"];
            createworkerurl = ConfigurationManager.AppSettings["createworkerurl"];
            svclogEnabled = ConfigurationManager.AppSettings["svclog"] == "1";
            apilogEnabled = ConfigurationManager.AppSettings["apilog"] == "1";
            //
            bool parseOk = int.TryParse(intervalinseconds, out intervalinseconds_int);
            if (!parseOk)
                intervalinmiliseconds = 150000; //  2.5 min
            else
                intervalinmiliseconds = intervalinseconds_int * 1000;
            //
            timer1 = new Timer();
            //timer1.Interval = 60000; // 60 sec = 1 min
            //timer1.Interval = 900000; // 60 sec * 15 = 900000 msec = 15 min
            //timer1.Interval = 120000; //  2 min
            //timer1.Interval = 150000; //  2.5 min
            timer1.Interval = intervalinmiliseconds;            
            timer1.Elapsed += new ElapsedEventHandler(timer1_Tick);
            timer1.Enabled = true;
            if (svclogEnabled)
            {
                string logMessage = "MainService started [intervalinmiliseconds=" 
                    + intervalinmiliseconds.ToString() 
                    + ","
                    + "createworkerurl="
                    + createworkerurl
                    + ","
                    + "svclogEnabled="
                    + (svclogEnabled ? "1" : "0")
                    + ","
                    + "apilogEnabled="
                    + (apilogEnabled ? "1" : "0")
                    + "]"
                    ;
                Logger.Log(logMessage);
            }
        }

        protected override void OnStop()
        {
            lastNotifyId = null;
            //
            timer1.Enabled = false;
            if (svclogEnabled)
                Logger.Log("MainService stopped");                        
        }

        private void doLog(string mess)
        {
            if (svclogEnabled)
                Logger.Log(mess);
        }

        private void doLog(Exception ex)
        {
            if (svclogEnabled)
                Logger.Log(ex);
        }

        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            //Call_SendMailAndNotify();
            //Call_CreateWorker();
            Call_ProcessWaitingDocs();
        }

        private void Call_ProcessWaitingDocs()
        {
            try
            {
                using (var context = new AuEsnDb())
                {
                    DateTime now = DateTime.Now;
                    DateTime today = DateTime.Today;
                    string userName = "msgsvc";
                    string errMess = "";

                    int cnt_all = 0;
                    int cnt_processed = 0;
                    int cnt_ok = 0;
                    int cnt_err = 0;

                    Action<auesn.dbmodel.doc, Enums.DocStateEnum, auesn.dbmodel.doc_client_to, bool, string> setDocState = (item, state, cl, isSuccess, mess) =>
                    {
                        doc_period doc_period = new doc_period();
                        doc_period.doc_period_uuid = Guid.NewGuid();
                        doc_period.doc_id = item.doc_id;
                        doc_period.doc_state_id = (int)state;
                        doc_period.crt_date = now;
                        doc_period.crt_user = userName;
                        doc_period.upd_date = now;
                        doc_period.upd_user = userName;
                        doc_period.is_deleted = false;
                        context.doc_period.Add(doc_period);

                        item.doc_state_id = (int)state;                        
                        item.upd_date = now;
                        item.upd_user = userName;

                        if (cl == null)
                        {
                            List<doc_client_to> doc_client_to_list = context.doc_client_to.Where(ss => ss.doc_id == item.doc_id && !ss.is_deleted).ToList();
                            if ((doc_client_to_list != null) && (doc_client_to_list.Count > 0))
                            {
                                foreach (var doc_client_to in doc_client_to_list)
                                {
                                    doc_client_to.doc_state_id = (int)state;
                                    doc_client_to.is_success = isSuccess;
                                    if (!String.IsNullOrWhiteSpace(mess))
                                        doc_client_to.mess = mess;
                                    doc_client_to.upd_date = now;
                                    doc_client_to.upd_user = userName;
                                }
                            }
                        }
                        else
                        {
                            cl.doc_state_id = (int)state;
                            cl.is_success = isSuccess;
                            if (!String.IsNullOrWhiteSpace(mess))
                                cl.mess = mess;
                            cl.upd_date = now;
                            cl.upd_user = userName;
                        }
                        
                        context.SaveChanges();
                    };


                    // все документы в очереди
                    var doc_list = context.doc.Where(ss => ss.doc_state_id == (int)Enums.DocStateEnum.SENDER_QUEUE && !ss.is_deleted).ToList();
                    if ((doc_list == null) || (doc_list.Count <= 0))
                    {
                        doLog("Нет документов для обработки");
                        return;
                    }
                    cnt_all = doc_list.Count;

                    // первые 5 документов в очереди
                    foreach (var doc_list_item in doc_list.OrderBy(ss => ss.crt_date).Take(5).ToList())
                    {
                        cnt_processed = cnt_processed + 1;
                        doc_type doc_type = context.doc_type.Where(ss => ss.doc_type_id == doc_list_item.doc_type_id).FirstOrDefault();
                        //switch (doc_list_item.doc_type_id)
                        switch (doc_type.doc_type_uuid.ToString())
                        {                        
                            case "3b0cc3b8-e086-11e7-9d2e-631d1fb7905e":
                                cnt_ok = cnt_ok + 1;
                                setDocState(doc_list_item, Enums.DocStateEnum.WEB_PROCESS, null, true, null);
                                //

                                // !!!
                                /*
                                austock.Worker worker_austock = new austock.Worker();
                                worker_austock.DoWork(doc_list_item.doc_uuid);
                                */

                                // Остатки товаров
                                Worker worker_austock = new Worker();
                                worker_austock.DoWork(doc_list_item.doc_uuid);
                                break;
                            case "4144803c-ff61-11e7-f888-0050569cc756":
                                cnt_ok = cnt_ok + 1;
                                setDocState(doc_list_item, Enums.DocStateEnum.WEB_PROCESS, null, true, null);

                                // Сводный заказ (CSV)
                                Worker2 worker_sz = new Worker2();
                                worker_sz.DoWork(doc_list_item.doc_uuid);
                                break;
                            default:
                                cnt_err = cnt_err + 1;
                                errMess = "Не найдена обработка для данного документа";
                                setDocState(doc_list_item, Enums.DocStateEnum.TARGET_QUEUE, null, false, errMess);
                                //
                                doLog("[doc_uuid:" + doc_list_item.doc_uuid.ToString());
                                doLog(errMess);
                                break;
                        }
                    }
                    
                    doLog("Документов в очереди: " + cnt_all.ToString() + ", обработано: " + cnt_processed.ToString() + ", успешно: " + cnt_ok.ToString() + ", с ошибками: " + cnt_err.ToString());
                }
            }
            catch (Exception ex)
            {
                doLog(ex);
            }
        }

        private void Call_SendMailAndNotify()
        {
            /*
            ExchangeServ.ExchangeServiceClient servMail = new ExchangeServ.ExchangeServiceClient();
            CabinetServ.CabinetServiceClient servNotify = new CabinetServ.CabinetServiceClient();
            try
            {
                if (lastNotifyId.GetValueOrDefault(0) <= 0)
                {
                    var res1 = servNotify.GetLastNotify_Email("iguana");
                    if (res1.error != null)
                    {
                        Logger.Log(res1.error.Message);
                    }
                    else
                    {
                        lastNotifyId = res1.notify_id;
                    }
                }
                if (lastNotifyId.GetValueOrDefault(0) <= 0)
                {
                    Logger.Log("Не найдено последнее неотправленное уведомление");
                    return;
                }

                var res2 = servNotify.GetNotifyList_Email("iguana", (int)lastNotifyId);
                if (res2.error != null)
                {
                    Logger.Log(res2.error.Message);
                    return;
                }
                if ((res2.Notify_list == null) || (res2.Notify_list.Count <= 0))
                {
                    Logger.Log("Не найдены уведомления для отправки");
                    return;
                }

                string topic = "";
                string mess = "";
                int cnt = 0;
                string prev_email = "";
                string curr_email = "";
                List<int> notifyIdList = new List<int>();
                foreach (var notify in res2.Notify_list.OrderBy(ss => ss.email).ThenBy(ss => ss.notify_id))
                {
                    curr_email = notify.email;
                    if ((prev_email != curr_email) && !(String.IsNullOrEmpty(prev_email)))
                    {
                        var res3 = servMail.SendMail(mess, topic, prev_email, "cab.aptekaural@gmail.com");
                        if (res3.error != null)
                        {
                            Logger.Log(res3.error.Message);
                        }
                        else
                        {
                            Logger.Log("Письмо отправлено на " + prev_email + ": " + topic);
                        }

                        mess = "";
                        cnt = 0;
                        topic = "";
                    }

                    lastNotifyId = lastNotifyId < notify.notify_id ? notify.notify_id : lastNotifyId;
                    notifyIdList.Add(notify.notify_id);
                    mess += notify.message;
                    mess += System.Environment.NewLine;
                    cnt++;
                    topic = "Уведомление об изменениях в задачах ЛК [" + cnt.ToString() + "]";
                    prev_email = notify.email;
                }

                if ((!(String.IsNullOrEmpty(mess))) && (!(String.IsNullOrEmpty(prev_email))))
                {
                    var res4 = servMail.SendMail(mess, topic, prev_email, "cab.aptekaural@gmail.com");
                    if (res4.error != null)
                    {
                        Logger.Log(res4.error.Message);
                    }
                    else
                    {
                        Logger.Log("Письмо отправлено на " + prev_email + ": " + topic);
                    }

                    mess = "";
                    cnt = 0;
                    topic = "";
                }

                if (notifyIdList.Count > 0)
                {
                    var res5 = servNotify.UpdateNotifyList_Email("iguana", string.Join(",", notifyIdList));
                    if (res5.error != null)
                    {
                        Logger.Log(res5.error.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            */
        }

        private void Call_CreateWorker()
        {
            /*
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    bool isResponseOk = false;
                    System.Net.HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;

                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri(createworkerurl),
                        Method = HttpMethod.Post,
                    };

                    request.Headers.Add("Cache-Control", "no-cache");
                    request.Content = new StringContent("{\"pwd\":\"iguana\",\"log_enabled\":\"" + (apilogEnabled ? "1" : "0") + "\"}",
                                        Encoding.UTF8,
                                        "application/json");

                    var task = httpClient.SendAsync(request)
                        .ContinueWith((taskwithmsg) =>
                        {
                            var response = taskwithmsg.Result;
                            isResponseOk = response.IsSuccessStatusCode;
                            if (!isResponseOk)
                            {
                                responseStatusCode = response.StatusCode;
                                if (svclogEnabled)
                                    Logger.Log("Запрос вернул ошибочный статус: " + responseStatusCode.ToString());
                                return;
                            }
                            if (svclogEnabled)
                                Logger.Log("Запрос выполнен: " + responseStatusCode.ToString());
                        });
                    task.Wait();
                }
            }
            catch (Exception ex)
            {
                if (svclogEnabled)
                    Logger.Log(ex);
            }
            */
        }
    }
}
