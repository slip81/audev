﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using auesn.util;
using auesn.dbmodel;

namespace auesn.msgsvc
{
    // обязательно должен называться именно так
    public class Worker2
    {
        // нужен атрибут для этого метода
        public bool DoWork(Guid docUuid)
        {
            // doc_type = 2

            try
            {
                return doWork(docUuid);                
            }
            catch (Exception ex)
            {                
                DbLogger.Log(GlobalUtil.ExceptionInfo(ex));
                // это нужно если на верхнем уровне тоже есть обработка исключений
                throw ex;
            }            
        }

        private bool doWork(Guid docUuid)
        {
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string userName = "worker";

            using (var context = new AuEsnDb())
            {
                // !!!
                // дублируется с методом в MainService.cs
                Action<auesn.dbmodel.doc, Enums.DocStateEnum, auesn.dbmodel.doc_client_to, bool, string> setDocState = (item, state, cl, isSuccess, mess) =>
                {
                    doc_period doc_period = new doc_period();
                    doc_period.doc_period_uuid = Guid.NewGuid();
                    doc_period.doc_id = item.doc_id;
                    doc_period.doc_state_id = (int)state;
                    doc_period.crt_date = now;
                    doc_period.crt_user = userName;
                    doc_period.upd_date = now;
                    doc_period.upd_user = userName;
                    doc_period.is_deleted = false;
                    context.doc_period.Add(doc_period);

                    item.doc_state_id = (int)state;
                    item.upd_date = now;
                    item.upd_user = userName;

                    if (cl == null)
                    {
                        List<doc_client_to> doc_client_to_list = context.doc_client_to.Where(ss => ss.doc_id == item.doc_id && !ss.is_deleted).ToList();
                        if ((doc_client_to_list != null) && (doc_client_to_list.Count > 0))
                        {
                            foreach (var doc_client_to in doc_client_to_list)
                            {
                                doc_client_to.doc_state_id = (int)state;
                                doc_client_to.is_success = isSuccess;
                                if (!String.IsNullOrWhiteSpace(mess))
                                    doc_client_to.mess = mess;
                                doc_client_to.upd_date = now;
                                doc_client_to.upd_user = userName;
                            }
                        }
                    }
                    else
                    {
                        cl.doc_state_id = (int)state;
                        cl.is_success = isSuccess;
                        if (!String.IsNullOrWhiteSpace(mess))
                            cl.mess = mess;
                        cl.upd_date = now;
                        cl.upd_user = userName;
                    }

                    context.SaveChanges();
                };

                doc doc = context.doc.Where(ss => ss.doc_uuid == docUuid && !ss.is_deleted && ss.doc_state_id == (int)Enums.DocStateEnum.WEB_PROCESS).FirstOrDefault();
                if (doc == null)
                {                    
                    return abortWork(null, "Не найден документ с кодом " + docUuid.ToString());
                }
                client clientFrom = context.client.Where(ss => ss.client_id == doc.client_from_id && !ss.is_deleted).FirstOrDefault();
                if (clientFrom == null)
                {
                    return abortWork(null, "Не найден клиент-отправитель для документа с кодом " + docUuid.ToString());
                }


                var filePath = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["docpath"], clientFrom.client_uuid.ToString(), docUuid.ToString());
                string docContentType = doc.content_type;

                string contentType_def = AuEsnConst.MEDIA_TYPE_XML;
                if (String.IsNullOrWhiteSpace(docContentType))
                    docContentType = contentType_def;

                bool docHandled = false;
                List<string> innList = new List<string>();
                IEnumerable<DocType6> sourceDoc = GlobalUtil.GetDocContent_AsEnumerable<DocType6>(filePath, docContentType);
                if ((sourceDoc != null) && (sourceDoc.Count() > 0))
                {                    
                    foreach (var tovPos in sourceDoc)
                    {
                        string tovCode = tovPos.КодСЗИ.Trim().ToLower();
                        string tovBarcode = String.IsNullOrWhiteSpace(tovPos.Штрихкод) ? "" : tovPos.Штрихкод.Trim().ToLower();
                        /*
                        long esnGoodId = (from t1 in context.good
                                          from t2 in context.client_good
                                          where t1.good_id == t2.good_id
                                          && t2.client_id == clientFrom.client_id
                                          && t2.client_code.Trim().ToLower().Equals(tovCode.Trim().ToLower())
                                          && t1.barcode.Trim().ToLower().Equals(tovBarcode)
                                          && !t1.is_deleted
                                          && !t2.is_deleted
                                          select t1.good_id)
                                          .FirstOrDefault();
                        if (esnGoodId > 0)
                            tovPos.КодТовараЕСН = esnGoodId.ToString();
                        */
                        Guid esnGoodUuid = (from t1 in context.good
                                          from t2 in context.client_good
                                          where t1.good_id == t2.good_id
                                          && t2.client_id == clientFrom.client_id
                                          && t2.client_code.Trim().ToLower().Equals(tovCode.Trim().ToLower())
                                          && t1.barcode.Trim().ToLower().Equals(tovBarcode)
                                          && !t1.is_deleted
                                          && !t2.is_deleted
                                          select t1.good_uuid)
                                          .FirstOrDefault();
                        string esnGoodUuidString = esnGoodUuid == null ? null : esnGoodUuid.ToString();
                        if (!String.IsNullOrWhiteSpace(esnGoodUuidString))
                            tovPos.КодТовараЕСН = esnGoodUuidString;

                        if (String.IsNullOrWhiteSpace(tovPos.МНН))
                            continue;

                        if (innList.Where(ss => ss.Trim().ToLower().Equals(tovPos.МНН.Trim().ToLower())).Any())
                            continue;
                        innList.Add(tovPos.МНН.Trim().ToLower());
                    }

                    // !!!
                    foreach (var inn in innList)
                    {
                        long mnnId = (from t1 in context.inn_source
                                      where t1.client_source_id == clientFrom.client_id
                                      && !t1.is_deleted
                                      && t1.inn_source_name.Trim().ToLower().Equals(inn.Trim().ToLower())
                                      select t1.inn_source_id)
                                      .FirstOrDefault();
                        if (mnnId > 0)
                        {
                            foreach (var tovPos in sourceDoc.Where(ss => ss.МНН.Trim().ToLower().Equals(inn.Trim().ToLower())))
                            {
                                tovPos.КодМННЕСН = mnnId.ToString();
                            }
                        }
                    }                    

                    var tovUnhandledCnt = sourceDoc.Where(ss => String.IsNullOrWhiteSpace(ss.КодТовараЕСН)).Count();
                    if (tovUnhandledCnt <= 0)
                        docHandled = true;
                }

                //setDocState(doc, docHandled ? Enums.DocStateEnum.TARGET_QUEUE : Enums.DocStateEnum.ESN_QUEUE, null, true, null);
                setDocState(doc, Enums.DocStateEnum.ESN_QUEUE, null, true, null);

                // !!!
                return completeWork(docUuid, "Документ передан в сервис ЕСН");
            }
        }

        private bool completeWork(Guid docUuid, string mess = null)
        {            
            if (!String.IsNullOrWhiteSpace(mess))
                DbLogger.Log(mess);            

            return true;
        }

        private bool abortWork(Guid? docUuid, string mess = null)
        {
            if (!String.IsNullOrWhiteSpace(mess))
                DbLogger.Log(mess);
            
            return true;
        }
    }


}
