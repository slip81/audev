﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.doc.Models;
using auesn.doc.Entities;

namespace auesn.doc
{
    public static class DocStore
    {
        public static IEnumerable<DocModel> GetDocs(int client_to_id)
        {
            using (var context = new AuEsnDb())
            {
                var result = context.doc.Where(ss => ss.client_to_id == client_to_id && !ss.is_deleted)
                    .OrderByDescending(ss => ss.crt_date)
                    .Select(ss => new DocModel()
                    {
                        uuid = ss.doc_uuid,
                        doc_num = ss.doc_num,
                        doc_name = ss.doc_name,
                        doc_date = ss.doc_date,
                    })
                    .ToList();
                return result;
            }
        }

        public static DocModel GetDoc(int doc_id)
        {
            using (var context = new AuEsnDb())
            {
                var result = context.doc.Where(ss => ss.doc_id == doc_id && !ss.is_deleted)                    
                    .Select(ss => new DocModel()
                    {
                        uuid = ss.doc_uuid,
                        doc_num = ss.doc_num,
                        doc_name = ss.doc_name,
                        doc_date = ss.doc_date,
                    })
                    .FirstOrDefault();
                return result;
            }
        }


        public static DocModel GetDoc(Guid doc_uuid)
        {
            using (var context = new AuEsnDb())
            {
                var result = context.doc.Where(ss => ss.doc_uuid == doc_uuid && !ss.is_deleted)                    
                    .Select(ss => new DocModel()
                    {
                        uuid = ss.doc_uuid,
                        doc_num = ss.doc_num,
                        doc_name = ss.doc_name,
                        doc_date = ss.doc_date,
                    })
                    .FirstOrDefault();
                return result;
            }
            
        }

        public static bool DelDoc(Guid doc_uuid)
        {
            using (var context = new AuEsnDb())
            {
                var doc = context.doc.Where(ss => ss.doc_uuid == doc_uuid && !ss.is_deleted)
                    .FirstOrDefault();
                if (doc == null)
                    return true;

                doc.is_deleted = true;
                doc.del_date = DateTime.Now;
                doc.del_user = "api";
                context.SaveChanges();

                return true;
            }

        }
    }
}