﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace auesn.api.Service
{
    public class GetGoodInfoParam
    {
        public GetGoodInfoParam()
        {
            //
        }

        public int client_id { get; set; }
        public string acceptContentType { get; set; }
        public bool shortForm { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string barcode { get; set; }
        public string codes { get; set; }
        public string barcodes { get; set; }        
    }
}