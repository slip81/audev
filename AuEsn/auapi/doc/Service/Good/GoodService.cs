﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using auesn.dbmodel;
using auesn.api.Model;
using auesn.util;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Newtonsoft.Json;
using CsvHelper;
using System.Globalization;
using System.IO.Compression;

namespace auesn.api.Service
{
    public class GoodService : BaseService, IGoodService
    {        
        public GoodService(IMapper _mapper) 
            : base(_mapper)
        {
            //
        }

        #region GetGoodInfo

        public string GetGoodInfo(GetGoodInfoParam getGoodInfoParam)            
        {
            if (getGoodInfoParam == null)
                return null;

            int client_id = getGoodInfoParam.client_id;
            string acceptContentType = getGoodInfoParam.acceptContentType;
            string barcode = getGoodInfoParam.barcode;
            string barcodes = getGoodInfoParam.barcodes;
            string code = getGoodInfoParam.code;
            string name = getGoodInfoParam.name;
            string codes = getGoodInfoParam.codes;
            bool shortForm = getGoodInfoParam.shortForm;

            List<GoodInfo> goodInfo = null;
            if ((!String.IsNullOrWhiteSpace(barcode)) || (!String.IsNullOrWhiteSpace(barcodes)))
            {
                goodInfo = GetGoodInfo_fromVwGoodInfo(barcode, barcodes, shortForm);
            }
            else
            {
                goodInfo = GetGoodInfo_fromVwClientGoodInfo(client_id, code, name, codes, shortForm);
            }

            if (goodInfo == null)
                return null;

            var resFormatted = GlobalUtil.GetContentFormatted<List<GoodInfo>>(goodInfo, acceptContentType);
            return !resFormatted.Item1 ? resFormatted.Item2 : null;
        }

        private List<GoodInfo> GetGoodInfo_fromVwClientGoodInfo(int client_id, string code, string name, string codes, bool shortForm)
        {
            if ((String.IsNullOrWhiteSpace(code)) && (String.IsNullOrWhiteSpace(name)) && (String.IsNullOrWhiteSpace(codes)))
                return null;

            var query = dbContext.vw_client_good_info.Where(ss => ss.client_id == client_id);            
            
            if (!String.IsNullOrWhiteSpace(code))
                query = query.Where(ss => ss.client_code.Trim().ToLower().Equals(code.Trim().ToLower()));
            if (!String.IsNullOrWhiteSpace(name))
                query = query.Where(ss => ss.client_name.Trim().ToLower().Equals(name.Trim().ToLower()));
            if (!String.IsNullOrWhiteSpace(codes))
            {
                var codesSplitted = codes.Split(',').Select(ss => ss.Trim().ToLower()).ToList();
                if ((codesSplitted != null) && (codesSplitted.Count > 0))
                {
                    query = query.Where(ss => codesSplitted.Contains(ss.client_code.Trim().ToLower()));
                }
            }                        

            var result = query.OrderByDescending(ss => ss.client_good_id)            
                .Select(ss => new GoodInfo()
                {

                    barcode = ss.barcode,
                    productName = ss.product_name,
                    mfrName = ss.mfr_name,
                    countryName = ss.country_name,
                    productTypeName = ss.product_type_name,
                    innName = ss.inn_name,
                    atcName = ss.atc_name,
                    atcCode = ss.atc_code,
                    dosageName = ss.dosage_name,
                    formName = ss.form_name,
                    formShortName = ss.form_short_name,
                    formGroupName = ss.form_group_name,
                    pharmTherGroupName = ss.pharm_ther_group_name,
                    okpd2Name = ss.okpd2_name,
                    okpd2Code = ss.okpd2_code,
                    brandName = ss.brand_name,
                    recipeTypeName = ss.recipe_type_name,
                    registryNum = ss.registry_num,
                    registryDate = ss.registry_date,
                    baaTypeName = ss.baa_type_name,
                    metanameName = ss.metaname_name,
                    tradeName = ss.trade_name_name,
                    storageCondition = shortForm ? ss.storage_condition_short : ss.storage_condition,
                    indication = shortForm ? ss.indication_short : ss.indication,
                    contraindication = shortForm ? ss.contraindication_short : ss.contraindication,
                    storagePeriod = ss.storage_period,
                    applicAndDose = ss.applic_and_dose,
                })
                .ToList();

            return result;
        }

        private List<GoodInfo> GetGoodInfo_fromVwGoodInfo(string barcode, string barcodes, bool shortForm)
        {
            var query = dbContext.vw_good_info.Where(ss => 1 == 1);

            if (!String.IsNullOrWhiteSpace(barcode))
                query = query.Where(ss => ss.barcode.Trim().ToLower().Equals(barcode.Trim().ToLower()));

            if (!String.IsNullOrWhiteSpace(barcodes))
            {
                var barcodesSplitted = barcodes.Split(',').Select(ss => ss.Trim().ToLower()).ToList();
                if ((barcodesSplitted != null) && (barcodesSplitted.Count > 0))
                {
                    query = from t1 in query
                            from t2 in barcodesSplitted
                            where t1.barcode.Trim().ToLower().Equals(t2)
                            select t1;
                }
            }
            
            var result = query
                .Select(ss => new GoodInfo()
                {
                    barcode = ss.barcode,
                    productName = ss.product_name,
                    mfrName = ss.mfr_name,
                    countryName = ss.country_name,
                    productTypeName = ss.product_type_name,
                    innName = ss.inn_name,
                    atcName = ss.atc_name,
                    atcCode = ss.atc_code,
                    dosageName = ss.dosage_name,
                    formName = ss.form_name,
                    formShortName = ss.form_short_name,
                    formGroupName = ss.form_group_name,
                    pharmTherGroupName = ss.pharm_ther_group_name,
                    okpd2Name = ss.okpd2_name,
                    okpd2Code = ss.okpd2_code,
                    brandName = ss.brand_name,
                    recipeTypeName = ss.recipe_type_name,
                    registryNum = ss.registry_num,
                    registryDate = ss.registry_date,
                    baaTypeName = ss.baa_type_name,
                    metanameName = ss.metaname_name,
                    tradeName = ss.trade_name_name,
                    storageCondition = shortForm ? ss.storage_condition_short : ss.storage_condition,
                    indication = shortForm ? ss.indication_short : ss.indication,
                    contraindication = shortForm ? ss.contraindication_short : ss.contraindication,
                    storagePeriod = ss.storage_period,
                    applicAndDose = ss.applic_and_dose,
                })
                .ToList();

            return result;
        }

        #endregion

        #region GetGoodAnalogs

        public string GetGoodAnalogs(GetGoodAnalogsParam getGoodAnalogsParam)
        {
            if (getGoodAnalogsParam == null)
                return null;

            int? client_id = getGoodAnalogsParam.client_id;
            string acceptContentType = getGoodAnalogsParam.acceptContentType;

            List<GoodAnalog> goodAnalogs = new List<GoodAnalog>();

            var query = dbContext.vw_good_info.Where(ss => 1 == 1 /*ss.is_perfect == true*/);
            bool queryConditionExists = false;
            foreach (var item in getGoodAnalogsParam.items.Where(ss => ((ss.mandatory == "1") || (String.IsNullOrWhiteSpace(ss.mandatory))) && (!String.IsNullOrWhiteSpace(ss.type)) && (!String.IsNullOrWhiteSpace(ss.value))))
            {
                Enums.GetGoodAnalogParamItemTypeEnum itemType = Enums.GetGoodAnalogParamItemTypeEnum.EAN13;
                if (!Enum.TryParse(item.type.ToUpper(), out itemType))
                    continue;
                switch (itemType)
                {
                    case Enums.GetGoodAnalogParamItemTypeEnum.EAN13:
                        //query = query.Where(ss => ss.barcode.Trim().ToLower().Equals(item.value.Trim().ToLower()));
                        var goodWithEan = dbContext.vw_good_info.Where(ss => ss.barcode.Trim().ToLower().Equals(item.value.Trim().ToLower())).FirstOrDefault();
                        if (goodWithEan != null)
                        {
                            query = query.Where(ss => ss.inn_name.Trim().ToLower().Equals(goodWithEan.inn_name.Trim().ToLower()));
                        }
                        else
                        {
                            query = query.Where(ss => 1 == 0);
                        }
                        queryConditionExists = true;
                        break;
                    case Enums.GetGoodAnalogParamItemTypeEnum.MNN:
                        //query = query.Where(ss => ss.inn_name.Trim().ToLower().Equals(item.value.Trim().ToLower()));
                        query = query.Where(ss => ss.inn_name.Trim().ToLower().Contains(item.value.Trim().ToLower()));
                        queryConditionExists = true;
                        break;
                    case Enums.GetGoodAnalogParamItemTypeEnum.TN:
                        //query = query.Where(ss => ss.trade_name_name.Trim().ToLower().Equals(item.value.Trim().ToLower()));
                        query = query.Where(ss => ss.trade_name_name.Trim().ToLower().Contains(item.value.Trim().ToLower()));
                        queryConditionExists = true;
                        break;
                    default:
                        break;
                }
            }
            if (queryConditionExists)
            {
                goodAnalogs = query.Select(ss => new GoodAnalog()
                {
                    barcode = ss.barcode,
                    innName = ss.inn_name,
                    tradeName = ss.trade_name_name,
                    mfrName = ss.mfr_name,

                }).Distinct().ToList();
            }

            /*
            // !!
            // test
            goodAnalogs.Add(new GoodAnalog()
            {
                barcode = "123",
                tradeName = "Наименование 1",
                innName = "МНН 1",
                mfrName = "Пр-ль 1",
            }
            );
            */
            if ((goodAnalogs == null) || (goodAnalogs.Count <= 0))
                return null;

            var resFormatted = GlobalUtil.GetContentFormatted<List<GoodAnalog>>(goodAnalogs, acceptContentType);
            return !resFormatted.Item1 ? resFormatted.Item2 : null;
        }

        #endregion

        #region GetGoodEtalon

        public byte[] GetGoodEtalon(GetGoodEtalonParam getGoodEtalonParam)
        {
            if (getGoodEtalonParam == null)
                return null;

            int client_id = getGoodEtalonParam.client_id;

            var client = dbContext.client.Where(ss => ss.client_id == client_id).FirstOrDefault();
            if (client == null)
                return null;

            bool get_full = getGoodEtalonParam.get_full;

            DateTime etalon_download_date = new DateTime(2000, 1, 1);
            if (client.etalon_download_date.HasValue)
                etalon_download_date = (DateTime)client.etalon_download_date;

            var query = dbContext.vw_good_info.Where(ss => ss.is_perfect == true 
                    && ss.is_deleted != true                    
            );
            if (!get_full)
                query = query.Where(ss => ss.upd_date > etalon_download_date);

            // !!!
            // query = query.OrderByDescending(ss => ss.good_uuid).Take(10);

            List<GoodEtalon> queryList = query
                .Select(ss => new GoodEtalon()
                {
                    barcode = ss.barcode,
                    productName = ss.product_name,
                    mfrName = ss.mfr_name,
                    countryName = ss.country_name,
                    productTypeName = ss.product_type_name,
                    innName = ss.inn_name,
                    atcName = ss.atc_name,
                    atcCode = ss.atc_code,
                    dosageName = ss.dosage_name,
                    formName = ss.form_name,
                    formShortName = ss.form_short_name,
                    formGroupName = ss.form_group_name,
                    pharmTherGroupName = ss.pharm_ther_group_name,
                    okpd2Name = ss.okpd2_name,
                    okpd2Code = ss.okpd2_code,
                    brandName = ss.brand_name,
                    recipeTypeName = ss.recipe_type_name,
                    registryNum = ss.registry_num,
                    registryDate = ss.registry_date,
                    baaTypeName = ss.baa_type_name,
                    metanameName = ss.metaname_name,
                    tradeName = ss.trade_name_name,
                    storageCondition = ss.storage_condition,
                    indication = ss.indication,
                    contraindication = ss.contraindication,
                    storagePeriod = ss.storage_period,
                    applicAndDose = ss.applic_and_dose,
                    goodGuid = ss.good_uuid.ToString(),
                    productGuid = ss.product_uuid.ToString(),
                })
                .ToList();

            DateTime etalon_donwload_date_new = DateTime.Now;
            client.etalon_download_date = etalon_donwload_date_new;

            if (queryList.Count <= 0)
            {
                dbContext.SaveChanges();
                return null;
            }

            Encoding currEncoding = Encoding.GetEncoding("windows-1251");

            string filePath = @"c:\Release\auesn\api\etalon\" + client.client_uuid.ToString();
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            string fileName = @"etalon_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".csv";            
            string filePathAndName = Path.Combine(filePath, fileName);
            if (File.Exists(filePathAndName))
                File.Delete(filePathAndName);

            using (TextWriter streamWriter = new StreamWriter(filePathAndName, false, currEncoding))
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.Configuration.CultureInfo = CultureInfo.CurrentCulture;

                csvWriter.WriteRecords<GoodEtalon>(queryList);                

                dbContext.SaveChanges();                
            }

            using (var ms = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    var entry = zipArchive.CreateEntry(fileName);

                    using (var fileStream = new FileStream(filePathAndName, FileMode.Open))
                    using (var entryStream = entry.Open())
                    {
                        fileStream.CopyTo(entryStream);
                    }

                }
                ms.Position = 0;
                return ms.ToArray();
            }

            // return File.ReadAllBytes(filePath);

            // !!!
            // test
            // var res = File.ReadAllBytes(@"c:\Pavlov\exp_discount_card_excel.zip");

            // return res;            
        }

        #endregion

        #region GetGoodSourceCodes

        public byte[] GetGoodSourceCodes(GetGoodSourceCodesParam getGoodSourceCodesParam)
        {
            if (getGoodSourceCodesParam == null)
                return null;

            int client_id = getGoodSourceCodesParam.client_id;
            string source_client_uuid = getGoodSourceCodesParam.source_client_uuid;

            var client = dbContext.client.Where(ss => ss.client_id == client_id).FirstOrDefault();
            if (client == null)
                return null;

            Guid clientUuid = Guid.Empty;
            if (!Guid.TryParse(source_client_uuid, out clientUuid))
                clientUuid = Guid.Empty;

            if (clientUuid == Guid.Empty)
                return null;

            var sourceClient = dbContext.client.Where(ss => ss.client_uuid == clientUuid).FirstOrDefault();
            if (sourceClient == null)
                return null;

            var query = dbContext.vw_good_info.Where(ss => ss.is_perfect == true
                    && ss.is_deleted != true
            );

            // !!!
            // query = query.OrderByDescending(ss => ss.good_uuid).Take(10);

            //List<GoodEtalon> queryList = query
            List<GoodSourceCodes> queryList = (from ss in dbContext.vw_good_info
                                         from t2 in dbContext.good
                                         from t3 in dbContext.client_good
                                         where ss.good_uuid == t2.good_uuid
                                         && t2.good_id == t3.good_id
                                         && t3.client_id == sourceClient.client_id
                                         && ss.is_perfect == true
                                         && ss.is_deleted != true
                                         select new GoodSourceCodes()
                                         {
                                             barcode = ss.barcode,
                                             productName = ss.product_name,
                                             mfrName = ss.mfr_name,
                                             countryName = ss.country_name,
                                             productTypeName = ss.product_type_name,
                                             innName = ss.inn_name,
                                             atcName = ss.atc_name,
                                             atcCode = ss.atc_code,
                                             dosageName = ss.dosage_name,
                                             formName = ss.form_name,
                                             formShortName = ss.form_short_name,
                                             formGroupName = ss.form_group_name,
                                             pharmTherGroupName = ss.pharm_ther_group_name,
                                             okpd2Name = ss.okpd2_name,
                                             okpd2Code = ss.okpd2_code,
                                             brandName = ss.brand_name,
                                             recipeTypeName = ss.recipe_type_name,
                                             registryNum = ss.registry_num,
                                             registryDate = ss.registry_date,
                                             baaTypeName = ss.baa_type_name,
                                             metanameName = ss.metaname_name,
                                             tradeName = ss.trade_name_name,
                                             storageCondition = ss.storage_condition,
                                             indication = ss.indication,
                                             contraindication = ss.contraindication,
                                             storagePeriod = ss.storage_period,
                                             applicAndDose = ss.applic_and_dose,
                                             goodGuid = ss.good_uuid.ToString(),
                                             productGuid = ss.product_uuid.ToString(),
                                             goodSourceCode = t3.client_code,
                                         })
                .ToList();

            if (queryList.Count <= 0)
            {
                dbContext.SaveChanges();
                return null;
            }

            Encoding currEncoding = Encoding.GetEncoding("windows-1251");

            // !!!
            string filePath = @"c:\Release\auesn\api\sourcecodes\" + client.client_uuid.ToString();
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            string fileName = @"sourcecodes_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".csv";
            string filePathAndName = Path.Combine(filePath, fileName);
            if (File.Exists(filePathAndName))
                File.Delete(filePathAndName);

            using (TextWriter streamWriter = new StreamWriter(filePathAndName, false, currEncoding))
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.Configuration.CultureInfo = CultureInfo.CurrentCulture;

                csvWriter.WriteRecords<GoodSourceCodes>(queryList);

                dbContext.SaveChanges();
            }

            using (var ms = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    var entry = zipArchive.CreateEntry(fileName);

                    using (var fileStream = new FileStream(filePathAndName, FileMode.Open))
                    using (var entryStream = entry.Open())
                    {
                        fileStream.CopyTo(entryStream);
                    }

                }
                ms.Position = 0;
                return ms.ToArray();
            }

            // return File.ReadAllBytes(filePath);

            // !!!
            // test
            // var res = File.ReadAllBytes(@"c:\Pavlov\exp_discount_card_excel.zip");

            // return res;            
        }

        #endregion
    }
}