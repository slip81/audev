﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.api.Model;

namespace auesn.api.Service
{
    public interface IGoodService
    {
        string GetGoodInfo(GetGoodInfoParam getGoodInfoParam);
        string GetGoodAnalogs(GetGoodAnalogsParam getGoodAnalogsParam);
        byte[] GetGoodEtalon(GetGoodEtalonParam getGoodEtalonParam);
        byte[] GetGoodSourceCodes(GetGoodSourceCodesParam getGoodSourceCodesParam);
    }
}