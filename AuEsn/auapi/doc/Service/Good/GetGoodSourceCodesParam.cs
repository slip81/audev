﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using auesn.util;

namespace auesn.api.Service
{
    public class GetGoodSourceCodesParam
    {
        public GetGoodSourceCodesParam()
        {
            //
        }

        public int client_id { get; set; }
        public string source_client_uuid { get; set; }
    }
}