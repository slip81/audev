﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using auesn.util;

namespace auesn.api.Service
{
    public class GetGoodEtalonParam
    {
        public GetGoodEtalonParam()
        {
            //
        }

        public int client_id { get; set; }
        public bool get_full { get; set; }
    }
}