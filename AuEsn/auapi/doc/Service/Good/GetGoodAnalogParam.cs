﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using auesn.util;

namespace auesn.api.Service
{
    public class GetGoodAnalogsParam
    {
        public GetGoodAnalogsParam()
        {
            //
        }
        [JsonIgnore()]
        public int? client_id { get; set; }
        [JsonIgnore()]
        public string acceptContentType { get; set; }
        public List<GetGoodAnalogsParamItem> items { get; set; }
    }

    public class GetGoodAnalogsParamItem
    {
        public GetGoodAnalogsParamItem()
        {
            //
        }

        public string type { get; set; }
        public string value { get; set; }
        public string mandatory { get; set; }
    }
}