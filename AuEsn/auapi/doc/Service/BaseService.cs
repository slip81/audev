﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.dbmodel;

namespace auesn.api.Service
{
    public class BaseService : IDisposable
    {
        protected AuEsnDb dbContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = new AuEsnDb();
                }
                return _dbContext;
            }
        }
        private AuEsnDb _dbContext;

        protected IMapper mapper;

        public BaseService(IMapper _mapper)
        {
            //mapper = Mapper.Instance;
            mapper = _mapper;
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {                
                _dbContext.Dispose();
            }
        }

    }
}