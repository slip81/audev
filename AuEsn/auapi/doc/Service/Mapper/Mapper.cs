﻿using AutoMapper;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.api.Model;
using auesn.dbmodel;

namespace auesn.api.Service
{
    public class AutoMapperModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToMethod(AutoMapper).InSingletonScope();
        }

        private IMapper AutoMapper(Ninject.Activation.IContext context)
        {
            Mapper.Initialize(config =>
            {                                
                config.ConstructServicesUsing(type => context.Kernel.GetService(type));

                config.CreateMap<auesn.dbmodel.doc, Doc>();
            });

            return Mapper.Instance;
        }
    }
}