﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace auesn.api.Service
{
    public class BaseServiceResult
    {        
        public BaseServiceResult()             
        {
            //
        }
       
        public bool HasError { get; set; }
        public string ErrMess { get; set; }
    }
}