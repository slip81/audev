﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using auesn.dbmodel;
using auesn.api.Model;
using auesn.util;
using AutoMapper;
using System.Transactions;
using System.Net;
using System.Net.Http;
using System.Text;
using CsvHelper;

namespace auesn.api.Service
{
    public class GoodSzService : BaseService, IGoodSzService
    {        
        public GoodSzService(IMapper _mapper) 
            : base(_mapper)
        {
            //
        }

        #region GetGoodSzFromSite

        public BaseServiceResult GetGoodSzFromSite()
        {            
            try
            {                
                string uriParam = "0";

                good_sz_log good_sz_log = null;                

                good_sz_log goodSzLogLast = null;
                bool is_get_full = false;
                goodSzLogLast = dbContext.good_sz_log.Where(ss => ss.is_success).OrderByDescending(ss => ss.good_sz_log_id).FirstOrDefault();
                uriParam = ((goodSzLogLast != null) && (goodSzLogLast.download_date.HasValue))
                    ? ((DateTime) goodSzLogLast.download_date).AddDays(1).ToString("yyyyMMdd")
                    : "0";
                is_get_full = uriParam == "0";

                string url = "http://api.2227778.net:1234/v1/basetovs/new/" + uriParam;

                GoodSz goodSz = null;
                bool isResponseOk = false;
                HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
                string responseErrMess = null;
                using (HttpClient httpClient = new HttpClient())
                {
                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri(url),
                        Method = HttpMethod.Get,
                    };

                    var task = httpClient.SendAsync(request)
                        .ContinueWith((taskwithmsg) =>
                        {
                            var response = taskwithmsg.Result;
                            isResponseOk = response.IsSuccessStatusCode;
                            if (!isResponseOk)
                            {
                                responseStatusCode = response.StatusCode;
                                //response.Headers.ToString();
                                responseErrMess = response.Content.ReadAsStringAsync().Result;
                                return;
                            }

                            var jsonTask = response.Content.ReadAsAsync<GoodSz>();
                            jsonTask.Wait();
                            goodSz = jsonTask.Result;
                        });
                    task.Wait();
                }
                
                if (!isResponseOk)
                {
                    good_sz_log = new good_sz_log();
                    good_sz_log.good_sz_log_uuid = Guid.NewGuid();
                    good_sz_log.download_date = DateTime.Today;
                    good_sz_log.cnt = 0;
                    good_sz_log.mess = responseErrMess;
                    good_sz_log.is_success = false;
                    good_sz_log.crt_date = DateTime.Now;
                    good_sz_log.upd_date = DateTime.Now;
                    good_sz_log.upd_user = "downloader";
                    dbContext.good_sz_log.Add(good_sz_log);
                    dbContext.SaveChanges();
                    //
                    return new BaseServiceResult() {ErrMess = responseErrMess, HasError = true,};
                }

                if ((goodSz == null) || (goodSz.error != "SUCCESS") || (goodSz.data == null) ||
                    (goodSz.data.basetovs == null) || (goodSz.data.basetovs.Count <= 0))
                {
                    good_sz_log = new good_sz_log();
                    good_sz_log.good_sz_log_uuid = Guid.NewGuid();
                    good_sz_log.download_date = DateTime.Today;
                    good_sz_log.cnt = 0;
                    good_sz_log.mess = "Нет новых строк [" + url + "]";
                    good_sz_log.is_success = true;
                    good_sz_log.crt_date = DateTime.Now;
                    good_sz_log.upd_date = DateTime.Now;
                    good_sz_log.upd_user = "downloader";
                    dbContext.good_sz_log.Add(good_sz_log);
                    dbContext.SaveChanges();
                    return new BaseServiceResult() {ErrMess = null, HasError = false,};
                }

                if (is_get_full)
                {
                    dbContext.good_sz.RemoveRange(dbContext.good_sz);
                    dbContext.SaveChanges();
                }

                List<GoodSzBasetov> existingList = new List<GoodSzBasetov>();
                int cntNew = 0;
                int cntExisting = 0;
                AuEsnDb transactionContext = null;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
                {
                    transactionContext = null;
                    try
                    {
                        transactionContext = new AuEsnDb();
                        transactionContext.Configuration.AutoDetectChangesEnabled = false;

                        cntNew = 0;
                        foreach (var item in goodSz.data.basetovs)
                        {
                            var existing = transactionContext.good_sz.Where(ss => !ss.is_deleted && ss.tov_id == item.tov_id).FirstOrDefault();
                            if (existing != null)
                            {
                                existingList.Add(item);
                                continue;
                            }

                            cntNew = cntNew + 1;

                            good_sz good_sz = new good_sz();
                            good_sz.good_sz_uuid = Guid.NewGuid();
                            good_sz.tov_id = item.tov_id;
                            good_sz.kat_id = item.kat_id;
                            good_sz.kat = item.kat;
                            good_sz.izg_id = item.izg_id;
                            good_sz.izg = item.izg;
                            good_sz.mnn_id = item.mnn_id;
                            good_sz.mnn = item.mnn;
                            good_sz.gr_id = item.gr_id;
                            good_sz.gr = item.gr;
                            good_sz.gvls = item.pku;
                            good_sz.pku3m = item.pku3m;
                            good_sz.assort = item.assort;
                            good_sz.modify_date = item.modify_date;
                            good_sz.eans_list = item.eans_list;
                            good_sz.crt_date = DateTime.Now;
                            good_sz.upd_date = DateTime.Now;
                            good_sz.upd_user = "downloader";

                            transactionContext = DbContextUtil.AddToContext<good_sz>(transactionContext, good_sz, cntNew, 100, true);
                        }

                        transactionContext.SaveChanges();
                    }
                    finally
                    {
                        if (transactionContext != null)
                            transactionContext.Dispose();
                    }

                    scope.Complete();
                }

                if ((existingList != null) && (existingList.Count > 0))
                {
                    foreach (var item in existingList)
                    {
                        var good_sz_existing = dbContext.good_sz.Where(ss => !ss.is_deleted && ss.tov_id == item.tov_id).FirstOrDefault();
                        if (good_sz_existing == null)
                            continue;

                        cntExisting = cntExisting + 1;

                        good_sz_existing.kat_id = item.kat_id;
                        good_sz_existing.kat = item.kat;
                        good_sz_existing.izg_id = item.izg_id;
                        good_sz_existing.izg = item.izg;
                        good_sz_existing.mnn_id = item.mnn_id;
                        good_sz_existing.mnn = item.mnn;
                        good_sz_existing.gr_id = item.gr_id;
                        good_sz_existing.gr = item.gr;
                        good_sz_existing.gvls = item.pku;
                        good_sz_existing.pku3m = item.pku3m;
                        good_sz_existing.assort = item.assort;
                        good_sz_existing.modify_date = item.modify_date;
                        good_sz_existing.eans_list = item.eans_list;
                        good_sz_existing.upd_date = DateTime.Now;
                        good_sz_existing.upd_user = "downloader";
                    }

                    dbContext.SaveChanges();
                }

                good_sz_log = new good_sz_log();
                good_sz_log.good_sz_log_uuid = Guid.NewGuid();
                good_sz_log.download_date = DateTime.Today;
                good_sz_log.cnt = cntNew;
                good_sz_log.mess = "Новых строк: " + cntNew.ToString() + ", обновлено строк: " + cntExisting.ToString() + " [" + url + "]";
                good_sz_log.is_success = true;
                good_sz_log.crt_date = DateTime.Now;
                good_sz_log.upd_date = DateTime.Now;
                good_sz_log.upd_user = "downloader";
                dbContext.good_sz_log.Add(good_sz_log);
                dbContext.SaveChanges();

                return new BaseServiceResult() {ErrMess = null, HasError = false,};
            }
            catch (Exception ex)
            {
                good_sz_log good_sz_log = new good_sz_log();
                good_sz_log.good_sz_log_uuid = Guid.NewGuid();
                good_sz_log.download_date = DateTime.Today;
                good_sz_log.cnt = 0;
                good_sz_log.mess = GlobalUtil.ExceptionInfo(ex);
                good_sz_log.is_success = false;
                good_sz_log.crt_date = DateTime.Now;
                good_sz_log.upd_date = DateTime.Now;
                good_sz_log.upd_user = "downloader";
                dbContext.good_sz_log.Add(good_sz_log);
                dbContext.SaveChanges();
                //
                return new BaseServiceResult() { ErrMess = GlobalUtil.ExceptionInfo(ex), HasError = true, };
            }
        }

        #endregion

        #region GetGoodSz

        public byte[] GetGoodSz(GetGoodEtalonParam getGoodSzParam)
        {
            if (getGoodSzParam == null)
                return null;

            int client_id = getGoodSzParam.client_id;

            var client = dbContext.client.Where(ss => ss.client_id == client_id).FirstOrDefault();
            if (client == null)
                return null;

            bool get_full = getGoodSzParam.get_full;

            DateTime sz_download_date = new DateTime(2000, 1, 1);
            if (client.sz_download_date.HasValue)
                sz_download_date = (DateTime)client.sz_download_date;

            var query = dbContext.good_sz.Where(ss => ss.is_deleted != true);
            if (!get_full)
                query = query.Where(ss => ss.upd_date > sz_download_date);

            List<GoodSzBasetov> queryList = query
                .Select(ss => new GoodSzBasetov()
                {
                    tov_id = ss.tov_id,
                    gr = ss.gr,
                    gr_id = ss.gr_id,
                    pku = ss.pku,
                    mnn = ss.mnn,
                    izg = ss.izg,
                    pku3m = ss.pku3m,
                    assort = ss.assort,
                    mnn_id = ss.mnn_id,
                    izg_id = ss.izg_id,
                    kat = ss.kat,
                    kat_id = ss.kat_id,
                    gvls = ss.gvls,
                    modify = ss.modify_date.HasValue ? ((DateTime)ss.modify_date).ToString("dd.MM.yy") : null,
                    eans = String.IsNullOrWhiteSpace(ss.eans_list) ? null : ss.eans_list.Split(',').Select(tt => tt).ToList(),
                })
                .ToList();

            DateTime sz_donwload_date_new = DateTime.Now;
            client.sz_download_date = sz_donwload_date_new;

            if (queryList.Count <= 0)
            {
                dbContext.SaveChanges();
                return null;
            }

            Encoding currEncoding = Encoding.GetEncoding("windows-1251");

            string filePath = @"c:\Release\auesn\api\sz\" + client.client_uuid.ToString();
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            string fileName = @"sz_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".csv";
            string filePathAndName = Path.Combine(filePath, fileName);
            if (File.Exists(filePathAndName))
                File.Delete(filePathAndName);

            using (TextWriter streamWriter = new StreamWriter(filePathAndName, false, currEncoding))
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.Configuration.CultureInfo = CultureInfo.CurrentCulture;
                csvWriter.Configuration.RegisterClassMap<GoodSzBasetovClassMap>();

                csvWriter.WriteRecords<GoodSzBasetov>(queryList);

                dbContext.SaveChanges();
            }

            using (var ms = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    var entry = zipArchive.CreateEntry(fileName);

                    using (var fileStream = new FileStream(filePathAndName, FileMode.Open))
                    using (var entryStream = entry.Open())
                    {
                        fileStream.CopyTo(entryStream);
                    }

                }
                ms.Position = 0;
                return ms.ToArray();
            }

            // return File.ReadAllBytes(filePath);

            // !!!
            // test
            // var res = File.ReadAllBytes(@"c:\Pavlov\exp_discount_card_excel.zip");

            // return res;            
        }

        #endregion
    }
}