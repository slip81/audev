﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.api.Model;

namespace auesn.api.Service
{
    public interface IGoodSzService
    {
        BaseServiceResult GetGoodSzFromSite();
        byte[] GetGoodSz(GetGoodEtalonParam getGoodSzParam);
    }
}