﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace auesn.api.Service
{
    public class SendDocResult : BaseServiceResult
    {        
        public SendDocResult()             
        {
            //
        }

        public string Uuid { get; set; }
    }
}