﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace auesn.api.Service
{
    public class GetDocResult : BaseServiceResult
    {        
        public GetDocResult()             
        {
            //
        }

        public string Body { get; set; }
        public int? BodyLength { get; set; }
        public Guid? Uuid { get; set; }
        public DateTime? CrtDate { get; set; }
        public Guid? DocTypeUuid { get; set; }
        public string DocTypeName { get; set; }
        public int ClientFromId { get; set; }
        public Guid? ClientFromUuid { get; set; }
        public string ClientFromName { get; set; }
        public int? MsgCnt { get; set; }
        public string DocContentType { get; set; }
        public bool IsNeedReply { get; set; }
    }
}