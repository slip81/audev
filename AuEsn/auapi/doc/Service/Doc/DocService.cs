﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using auesn.dbmodel;
using auesn.api.Model;
using auesn.util;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Newtonsoft.Json;

namespace auesn.api.Service
{
    public class DocService : BaseService, IDocService
    {        
        public DocService(IMapper _mapper) 
            : base(_mapper)
        {
            //
        }

        #region SendDoc

        //public SendDocResult SendDoc(Client user, string docTypeString, string docNumString, string targetStringList, string docUuid, string docBody)
        public SendDocResult SendDoc(SendDocParam sendDocParam)
        {
            if ((sendDocParam == null) || (sendDocParam.client == null))
                return new SendDocResult() { HasError = true, ErrMess = "Не заданы параметры" };

            Client user = sendDocParam.client;
            return user.client_type_id == AuEsnConst.CLIENT_TYPE_ESN ? sendDoc_esn(sendDocParam) : sendDoc_client(sendDocParam);
        }

        private SendDocResult sendDoc_esn(SendDocParam sendDocParam)
        {
            Client user = sendDocParam.client;
            string docUuid = sendDocParam.docUuid;

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string userName = user.client_name;

            Guid documentUuid = Guid.Empty;
            if (!Guid.TryParse(docUuid, out documentUuid))
                return sendDoc_client(sendDocParam);

            var doc = dbContext.doc
                .Where(ss => ss.doc_uuid == documentUuid && ss.doc_state_id == (int)Enums.DocStateEnum.ESN_PROCESS && !ss.is_deleted && !ss.is_canceled)
                .FirstOrDefault();
            if (doc == null)
                return sendDoc_client(sendDocParam);

            List<doc_client_to> doc_client_to_list = dbContext.doc_client_to.Where(ss => ss.doc_id == doc.doc_id && ss.doc_state_id == (int)Enums.DocStateEnum.ESN_PROCESS && !ss.is_deleted).ToList();
            if ((doc_client_to_list != null) && (doc_client_to_list.Count > 0))
            {
                foreach (var doc_client_to in doc_client_to_list)
                {
                    doc_client_to.doc_state_id = (int)Enums.DocStateEnum.TARGET_QUEUE;
                    doc_client_to.upd_user = userName;
                }
            }

            doc_period doc_period = new doc_period();
            doc_period.doc_period_uuid = Guid.NewGuid();
            doc_period.doc_id = doc.doc_id;
            doc_period.doc_state_id = (int)Enums.DocStateEnum.TARGET_QUEUE;
            doc_period.crt_date = now;
            doc_period.crt_user = user.client_name;
            doc_period.upd_date = now;
            doc_period.upd_user = user.client_name;
            doc_period.is_deleted = false;
            dbContext.doc_period.Add(doc_period);
            
            doc.doc_state_id = (int)Enums.DocStateEnum.TARGET_QUEUE;
            doc.upd_user = user.client_name;

            dbContext.SaveChanges();

            return new SendDocResult()
            {
                Uuid = docUuid,
            };
        }

        private SendDocResult sendDoc_client(SendDocParam sendDocParam)
        {
            Client user = sendDocParam.client;
            string targetStringList = sendDocParam.targetUuidList;
            string docNumString = sendDocParam.docNum;
            string docContentTypeString = sendDocParam.docContentType;
            string docTypeString = sendDocParam.docTypeUuid;
            string docUuid = sendDocParam.docUuid;
            string docBody = sendDocParam.docBody;

            if (String.IsNullOrWhiteSpace(docBody))
                return new SendDocResult() { HasError = true, ErrMess = "Пустое тело документа" };

            Guid docTypeUuid = Guid.Empty;
            if (!Guid.TryParse(docTypeString, out docTypeUuid))
                return new SendDocResult() { HasError = true, ErrMess = "Некорректный формат x-document-type" };

            Guid targetClientUuid = Guid.Empty;
            bool targetClientSpecified = true;
            bool isNeedReply = true;
            if (String.IsNullOrWhiteSpace(targetStringList))    // документ отправлен всем доступным получателям
            {
                targetClientSpecified = false;
            }
            else if ((targetStringList == "0") || (targetStringList == "1"))  // документ отправлен в сервис ЕСН (доступный для данного клиента) (0 - ответ не нужен, 1 - ответ нужен)
            {
                /*
                client esnClient = (from t1 in dbContext.client
                                    from t2 in dbContext.client_rel
                                    from t3 in dbContext.client
                                    where t1.client_id == t2.client1_id
                                    && t2.client2_id == t3.client_id
                                    && t1.client_id == user.client_id
                                    && t3.client_type_id == AuEsnConst.CLIENT_TYPE_ESN
                                    && !t1.is_deleted
                                    && !t2.is_deleted
                                    && !t3.is_deleted
                                    select t3)
                                       .FirstOrDefault();
                */
                client esnClient = (from t3 in dbContext.client
                                    where t3.client_type_id == AuEsnConst.CLIENT_TYPE_ESN
                                    && !t3.is_deleted
                                    select t3)
                                    .FirstOrDefault();
                if (esnClient == null)
                    return new SendDocResult() { HasError = true, ErrMess = "Не найден системный получатель c типом " + AuEsnConst.CLIENT_TYPE_ESN.ToString() };
                
                targetClientUuid = user.client_uuid;       // конечный получатель - сам отправитель

                isNeedReply = targetStringList == "1";
            }
            else                                                // документ отправлен конкретному получателю
            {
                if (!Guid.TryParse(targetStringList, out targetClientUuid))
                    return new SendDocResult() { HasError = true, ErrMess = "Некорректный формат x-recipient" };
            }

            Guid documentUuid = Guid.Empty;
            bool documentUuidSpecified = true;
            if (String.IsNullOrWhiteSpace(docUuid))
            {
                documentUuidSpecified = false;
            }
            else
            {
                if (!Guid.TryParse(docUuid, out documentUuid))
                    return new SendDocResult() { HasError = true, ErrMess = "Некорректный формат x-document-uuid" };
            }

            doc_type doc_type = dbContext.doc_type.Where(ss => ss.doc_type_uuid == docTypeUuid && !ss.is_deleted).FirstOrDefault();
            if (doc_type == null)
                return new SendDocResult() { HasError = true, ErrMess = "Не найден тип документа " + docTypeUuid.ToString() };

            client client = null;
            List<client> client_list = null;
            if (targetClientSpecified)
            {
                client = dbContext.client.Where(ss => ss.client_uuid == targetClientUuid && !ss.is_deleted).FirstOrDefault();
                if (client == null)
                    return new SendDocResult() { HasError = true, ErrMess = "Не найден клиент-получатель " + targetClientUuid.ToString() };
            }
            else
            {
                client_list = (from t1 in dbContext.client
                               from t2 in dbContext.client_rel
                               where t1.client_id == t2.client2_id
                               && t2.client1_id == user.client_id
                               && t1.client_id != t2.client1_id
                               && t1.client_type_id != AuEsnConst.CLIENT_TYPE_ESN
                               && !t1.is_deleted
                               && !t2.is_deleted
                               select t1)
                              .ToList();
                if ((client_list == null) || (client_list.Count <= 0))
                    return new SendDocResult() { HasError = true, ErrMess = "Не найдены доступные клиенты-получатели для данного пользователя" };
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string userName = user.client_name;

            if (documentUuidSpecified)
            {
                auesn.dbmodel.doc documentExisting = dbContext.doc.Where(ss => ss.doc_uuid == documentUuid && !ss.is_deleted).FirstOrDefault();
                if (documentExisting != null)
                    return new SendDocResult() { HasError = true, ErrMess = "Уже есть документ с кодом " + documentUuid.ToString() };
            }

            auesn.dbmodel.doc document = new auesn.dbmodel.doc();
            document.doc_uuid = documentUuidSpecified ? documentUuid : Guid.NewGuid();
            document.doc_num = docNumString;
            document.doc_name = String.IsNullOrWhiteSpace(docNumString) ? ("Док от " + user.client_name + "/" + now.ToString("dd.MM.yyyy HH:mm:ss")) : docNumString;
            document.doc_date = DateTime.Today;
            document.doc_type_id = doc_type.doc_type_id;
            document.doc_state_id = (int)Enums.DocStateEnum.SENDER_QUEUE;
            document.client_from_id = user.client_id;
            document.client_to_id = targetClientSpecified ? (int?)client.client_id : null;
            document.content_type = docContentTypeString;
            document.is_need_reply = isNeedReply;
            document.crt_date = now;
            document.crt_user = userName;
            document.upd_date = now;
            document.upd_user = userName;
            document.is_deleted = false;
            dbContext.doc.Add(document);

            auesn.dbmodel.doc_period doc_period = new auesn.dbmodel.doc_period();
            doc_period.doc_period_uuid = Guid.NewGuid();
            doc_period.doc = document;
            doc_period.doc_state_id = (int)Enums.DocStateEnum.SENDER_QUEUE;
            doc_period.crt_date = now;
            doc_period.crt_user = userName;
            doc_period.upd_date = now;
            doc_period.upd_user = userName;
            doc_period.is_deleted = false;
            dbContext.doc_period.Add(doc_period);

            auesn.dbmodel.doc_client_to doc_client_to = null;
            if (targetClientSpecified)
            {
                doc_client_to = new auesn.dbmodel.doc_client_to();
                doc_client_to.doc_client_to_uuid = Guid.NewGuid();
                doc_client_to.doc = document;
                doc_client_to.client_id = client.client_id;
                doc_client_to.doc_state_id = (int)Enums.DocStateEnum.SENDER_QUEUE;
                doc_client_to.crt_date = now;
                doc_client_to.crt_user = userName;
                doc_client_to.upd_date = now;
                doc_client_to.upd_user = userName;
                doc_client_to.is_deleted = false;
                dbContext.doc_client_to.Add(doc_client_to);
            }
            else
            {
                foreach (var client_list_item in client_list)
                {
                    doc_client_to = new auesn.dbmodel.doc_client_to();
                    doc_client_to.doc_client_to_uuid = Guid.NewGuid();
                    doc_client_to.doc = document;
                    doc_client_to.client_id = client_list_item.client_id;
                    doc_client_to.doc_state_id = (int)Enums.DocStateEnum.SENDER_QUEUE;
                    doc_client_to.crt_date = now;
                    doc_client_to.crt_user = userName;
                    doc_client_to.upd_date = now;
                    doc_client_to.upd_user = userName;
                    doc_client_to.is_deleted = false;
                    dbContext.doc_client_to.Add(doc_client_to);
                }
            }

            dbContext.SaveChanges();

            var path = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["docpath"], user.client_uuid.ToString());
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            File.WriteAllText(Path.Combine(path, document.doc_uuid.ToString()), docBody, Encoding.GetEncoding("windows-1251"));

            return new SendDocResult()
            {
                Uuid = document.doc_uuid.ToString(),
            };
        }

        #endregion

        #region GetLastDoc

        public GetDocResult GetLastDoc(Client user, string acceptContentType)
        {
            return user.client_type_id == AuEsnConst.CLIENT_TYPE_ESN ? getLastDoc_esn(user, acceptContentType) : getLastDoc_client(user, acceptContentType);
        }                

        private GetDocResult getLastDoc_esn(Client user, string acceptContentType)
        {
            GetDocResult result = new GetDocResult();

            var msgCnt = (from t1 in dbContext.doc
                          //from t2 in dbContext.doc_client_to
                          from t3 in dbContext.client
                          from t4 in dbContext.doc_type
                          where 1 == 1
                          //&& t1.doc_id == t2.doc_id
                          //&& t2.client_id == user.client_id
                          && t1.doc_state_id == (int)Enums.DocStateEnum.ESN_QUEUE
                          && t1.client_from_id == t3.client_id
                          && t1.doc_type_id == t4.doc_type_id
                          && !t1.is_deleted
                          //&& !t2.is_deleted
                          && !t3.is_deleted
                          && !t4.is_deleted
                          && !t1.is_canceled
                          select t1
                        )
                        .Distinct()
                        .Count();

            result = (from t1 in dbContext.doc
                      //from t2 in dbContext.doc_client_to
                      from t3 in dbContext.client
                      from t4 in dbContext.doc_type
                      where 1 == 1
                      //&& t1.doc_id == t2.doc_id
                      //&& t2.client_id == user.client_id
                      && t1.doc_state_id == (int)Enums.DocStateEnum.ESN_QUEUE
                      && t1.client_from_id == t3.client_id
                      && t1.doc_type_id == t4.doc_type_id
                      && !t1.is_deleted
                      //&& !t2.is_deleted
                      && !t3.is_deleted
                      && !t4.is_deleted
                      && !t1.is_canceled
                      orderby t1.crt_date
                      select new GetDocResult()
                      {
                          Uuid = t1.doc_uuid,
                          CrtDate = t1.crt_date,
                          DocTypeUuid = t4.doc_type_uuid,
                          DocTypeName = t4.doc_type_name,
                          ClientFromId = t1.client_from_id,
                          ClientFromUuid = t3.client_uuid,
                          ClientFromName = t3.client_name,
                          DocContentType = t1.content_type,
                          IsNeedReply = t1.is_need_reply,
                      })
                      .FirstOrDefault();

            if (result == null)
            {
                //return result;
                return new GetDocResult() { HasError = true, ErrMess = "getLastDoc_esn: result == null" };
            }

            var filePath = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["docpath"], result.ClientFromUuid.ToString(), result.Uuid.ToString());

            string contentType_def = AuEsnConst.MEDIA_TYPE_XML;
            if (String.IsNullOrWhiteSpace(result.DocContentType))
                result.DocContentType = contentType_def;

            // !!!
            // если в content_type строка вида text/xml;charset=utf-8
            result.DocContentType = result.DocContentType.Split(';')[0];
            // если в content_type строка вида text/xml,application/json
            result.DocContentType = result.DocContentType.Split(',')[0];

            string acceptContentType_def = AuEsnConst.MEDIA_TYPE_JSON;
            if (String.IsNullOrWhiteSpace(acceptContentType))
                acceptContentType = acceptContentType_def;

            switch (result.DocTypeUuid.ToString())
            {
                case "3b0cc3b8-e086-11e7-9d2e-631d1fb7905e":
                    Doc2 sourceDoc = GlobalUtil.GetDocContent<Doc2>(filePath, result.DocContentType);
                    // !!!
                    // дублируется с методом в Worker.cs
                    if ((sourceDoc != null) && (sourceDoc.ТоварныеПозиции != null) && (sourceDoc.ТоварныеПозиции.ТоварнаяПозиция != null))
                    {
                        foreach (var tovPos in sourceDoc.ТоварныеПозиции.ТоварнаяПозиция.Where(ss => String.IsNullOrWhiteSpace(ss.КодТовараЕСН) && ss.Товар != null))
                        {
                            //string tovCode = tovPos.Товар.Код.Trim().ToLower() + "_" + tovPos.Изготовитель.Код.Trim().ToLower();
                            string tovCode = tovPos.Товар.Код.Trim().ToLower();
                            string tovBarcode = String.IsNullOrWhiteSpace(tovPos.ЕАН13) ? "" : tovPos.ЕАН13.Trim().ToLower();
                            /*
                            long esnGoodId = (from t1 in dbContext.good
                                              from t2 in dbContext.client_good
                                              where t1.good_id == t2.good_id
                                              && t2.client_id == result.ClientFromId
                                              && t2.client_code.Trim().ToLower().Equals(tovCode.Trim().ToLower())
                                              && t1.barcode.Trim().ToLower().Equals(tovBarcode)
                                              select t1.good_id)
                                              .FirstOrDefault();
                            if (esnGoodId > 0)
                                tovPos.КодТовараЕСН = esnGoodId.ToString();
                            */
                            Guid esnGoodUuid = (from t1 in dbContext.good
                                              from t2 in dbContext.client_good
                                              where t1.good_id == t2.good_id
                                              && t2.client_id == result.ClientFromId
                                              && t2.client_code.Trim().ToLower().Equals(tovCode.Trim().ToLower())
                                              && t1.barcode.Trim().ToLower().Equals(tovBarcode)
                                              select t1.good_uuid)
                                              .FirstOrDefault();
                            string esnGoodUuidString = esnGoodUuid == null ? null : esnGoodUuid.ToString();
                            if (!String.IsNullOrWhiteSpace(esnGoodUuidString))
                                tovPos.КодТовараЕСН = esnGoodUuidString;
                        }
                    }

                    var getDocContentFormattedResult = GlobalUtil.GetContentFormatted<Doc2>(sourceDoc, result.DocContentType, acceptContentType);
                    if (getDocContentFormattedResult.Item1)
                        return new GetDocResult() { HasError = true, ErrMess = String.IsNullOrWhiteSpace(getDocContentFormattedResult.Item2) ? "Ошибка при получении отформатированного содержимого документа" : getDocContentFormattedResult.Item2 };
                    result.Body = getDocContentFormattedResult.Item2;
                    break;
                case "4144803c-ff61-11e7-f888-0050569cc756":
                    IEnumerable<DocType6> sourceDoc6 = GlobalUtil.GetDocContent_AsEnumerable<DocType6>(filePath, result.DocContentType);
                    
                    // !!!
                    // todo

                    var getDocContentFormattedFromEnumerableResult = GlobalUtil.GetContentFormatted_FromEnumerable<DocType6>(sourceDoc6, result.DocContentType, acceptContentType);
                    if (getDocContentFormattedFromEnumerableResult.Item1)
                        return new GetDocResult() { HasError = true, ErrMess = String.IsNullOrWhiteSpace(getDocContentFormattedFromEnumerableResult.Item2) ? "Ошибка при получении отформатированного содержимого документа" : getDocContentFormattedFromEnumerableResult.Item2 };
                    result.Body = getDocContentFormattedFromEnumerableResult.Item2;
                    break;
                default:
                    return new GetDocResult() { HasError = true, ErrMess = "Неподдерживаемый тип документа: " + result.DocTypeUuid.ToString() };
            }           

            result.BodyLength = result.Body != null ? (int?)result.Body.Length : null;
            result.MsgCnt = msgCnt;

            return result;
        }

        private GetDocResult getLastDoc_client(Client user, string acceptContentType)
        {
            GetDocResult result = new GetDocResult();            

            Func<int, int> getMsgCnt = (ss) => 
            {
                return (from t1 in dbContext.doc
                        from t2 in dbContext.doc_client_to
                        from t3 in dbContext.client
                        from t4 in dbContext.doc_type
                        where t1.doc_id == t2.doc_id
                        && t2.client_id == user.client_id
                        && t2.doc_state_id == ss
                        && t1.client_from_id == t3.client_id
                        && t1.doc_type_id == t4.doc_type_id
                        && !t1.is_deleted
                        && !t2.is_deleted
                        && !t3.is_deleted
                        && !t4.is_deleted
                        && !t1.is_canceled
                        select t1
                        )
                        .Distinct()
                        .Count();
            };

            int docStateId = (int)Enums.DocStateEnum.TARGET_QUEUE;
            int msgCnt = getMsgCnt(docStateId);

            // если нет полностью обработанных доков - берем те, которые на данный момент в обработке
            if (msgCnt <= 0)
            {
                docStateId = (int)Enums.DocStateEnum.ESN_PROCESS;
                msgCnt = getMsgCnt(docStateId);
            }

            result = (from t1 in dbContext.doc
                      from t2 in dbContext.doc_client_to
                      from t3 in dbContext.client
                      from t4 in dbContext.doc_type
                      where t1.doc_id == t2.doc_id
                      && t2.client_id == user.client_id
                      && t2.doc_state_id == docStateId
                      && t1.client_from_id == t3.client_id
                      && t1.doc_type_id == t4.doc_type_id
                      && !t1.is_deleted
                      && !t2.is_deleted
                      && !t3.is_deleted
                      && !t4.is_deleted
                      && !t1.is_canceled
                      orderby t1.crt_date
                      select new GetDocResult()
                      {
                          Uuid = t1.doc_uuid,
                          CrtDate = t1.crt_date,
                          DocTypeUuid = t4.doc_type_uuid,
                          DocTypeName = t4.doc_type_name,
                          ClientFromId = t1.client_from_id,
                          ClientFromUuid = t3.client_uuid,
                          ClientFromName = t3.client_name,
                          DocContentType = t1.content_type,
                          IsNeedReply = false,
                      })
                      .FirstOrDefault();

            if (result == null)
            {
                //return result;
                return new GetDocResult() { HasError = true, ErrMess = "getLastDoc_client: result == null" };
            }


            var filePath = Path.Combine(System.Configuration.ConfigurationManager.AppSettings["docpath"], user.client_uuid.ToString(), result.Uuid.ToString());

            string contentType_def = AuEsnConst.MEDIA_TYPE_XML;
            if (String.IsNullOrWhiteSpace(result.DocContentType))
                result.DocContentType = contentType_def;

            // !!!
            // если в content_type строка вида text/xml;charset=utf-8
            result.DocContentType = result.DocContentType.Split(';')[0];
            // если в content_type строка вида text/xml,application/json
            result.DocContentType = result.DocContentType.Split(',')[0];

            string acceptContentType_def = AuEsnConst.MEDIA_TYPE_JSON;
            if (String.IsNullOrWhiteSpace(acceptContentType))
                acceptContentType = acceptContentType_def;

            switch (result.DocTypeUuid.ToString())
            {
                case "3b0cc3b8-e086-11e7-9d2e-631d1fb7905e":
                    Doc2 sourceDoc = GlobalUtil.GetDocContent<Doc2>(filePath, result.DocContentType);
                    // !!!
                    // дублируется с методом в Worker.cs
                    if ((sourceDoc != null) && (sourceDoc.ТоварныеПозиции != null) && (sourceDoc.ТоварныеПозиции.ТоварнаяПозиция != null))
                    {
                        foreach (var tovPos in sourceDoc.ТоварныеПозиции.ТоварнаяПозиция.Where(ss => String.IsNullOrWhiteSpace(ss.КодТовараЕСН) && ss.Товар != null))
                        {                            
                            string tovCode = tovPos.Товар.Код.Trim().ToLower();
                            string tovBarcode = String.IsNullOrWhiteSpace(tovPos.ЕАН13) ? "" : tovPos.ЕАН13.Trim().ToLower();

                            Guid esnGoodUuid = (from t1 in dbContext.good
                                              from t2 in dbContext.client_good
                                              where t1.good_id == t2.good_id
                                              && t2.client_id == result.ClientFromId
                                              && t2.client_code.Trim().ToLower().Equals(tovCode.Trim().ToLower())
                                              && t1.barcode.Trim().ToLower().Equals(tovBarcode)
                                              select t1.good_uuid)
                                              .FirstOrDefault();
                            string esnGoodUuidString = esnGoodUuid == null ? null : esnGoodUuid.ToString();
                            if (!String.IsNullOrWhiteSpace(esnGoodUuidString))
                                tovPos.КодТовараЕСН = esnGoodUuidString;
                        }

                        // если взяли док, который на данный момент в обработке - возвращаем только состыкованные строки
                        sourceDoc.ТоварныеПозиции.ТоварнаяПозиция = sourceDoc.ТоварныеПозиции.ТоварнаяПозиция.Where(ss => !String.IsNullOrWhiteSpace(ss.КодТовараЕСН)).ToList();
                    }

                    var getDocContentFormattedResult = GlobalUtil.GetContentFormatted<Doc2>(sourceDoc, result.DocContentType, acceptContentType);
                    if (getDocContentFormattedResult.Item1)
                        return new GetDocResult() { HasError = true, ErrMess = String.IsNullOrWhiteSpace(getDocContentFormattedResult.Item2) ? "Ошибка при получении отформатированного содержимого документа" : getDocContentFormattedResult.Item2 };
                    result.Body = getDocContentFormattedResult.Item2;
                    break;
                case "4144803c-ff61-11e7-f888-0050569cc756":
                    IEnumerable<DocType6> sourceDoc6 = GlobalUtil.GetDocContent_AsEnumerable<DocType6>(filePath, result.DocContentType);

                    // !!!
                    // todo

                    var getDocContentFormattedFromEnumerableResult = GlobalUtil.GetContentFormatted_FromEnumerable<DocType6>(sourceDoc6, result.DocContentType, acceptContentType);
                    if (getDocContentFormattedFromEnumerableResult.Item1)
                        return new GetDocResult() { HasError = true, ErrMess = String.IsNullOrWhiteSpace(getDocContentFormattedFromEnumerableResult.Item2) ? "Ошибка при получении отформатированного содержимого документа" : getDocContentFormattedFromEnumerableResult.Item2 };
                    result.Body = getDocContentFormattedFromEnumerableResult.Item2;
                    break;
                default:
                    return new GetDocResult() { HasError = true, ErrMess = "Неподдерживаемый тип документа: " + result.DocTypeUuid.ToString() };
            }

            result.BodyLength = result.Body != null ? (int?)result.Body.Length : null;
            result.MsgCnt = msgCnt;

            return result;
        }

        #endregion

        #region SendDocReceived
        public BaseServiceResult SendDocReceived(Client user, Guid docUuid)
        {
            return user.client_type_id == AuEsnConst.CLIENT_TYPE_ESN ? sendDocReceived_esn(user, docUuid) : sendDocReceived_client(user, docUuid);
        }

        private BaseServiceResult sendDocReceived_esn(Client user, Guid docUuid)
        {
            DateTime now = DateTime.Now;

            var doc = dbContext.doc.Where(ss => ss.doc_uuid == docUuid && ss.doc_state_id == (int)Enums.DocStateEnum.ESN_QUEUE && !ss.is_deleted).FirstOrDefault();
            if (doc == null)
            {
                return new BaseServiceResult()
                {
                    HasError = true,
                    ErrMess = "Не найден документ с кодом " + docUuid.ToString(),
                };
            }

            List<doc_client_to> doc_client_to_list = dbContext.doc_client_to.Where(ss => ss.doc_id == doc.doc_id && ss.doc_state_id == (int)Enums.DocStateEnum.ESN_QUEUE && !ss.is_deleted).ToList();
            if ((doc_client_to_list != null) && (doc_client_to_list.Count > 0))
            {
                foreach (var doc_client_to in doc_client_to_list)
                {
                    doc_client_to.doc_state_id = (int)Enums.DocStateEnum.ESN_PROCESS;
                    doc_client_to.upd_user = user.client_name;
                }
            }

            doc_period doc_period = new doc_period();
            doc_period.doc_period_uuid = Guid.NewGuid();
            doc_period.doc_id = doc.doc_id;
            doc_period.doc_state_id = (int)Enums.DocStateEnum.ESN_PROCESS;
            doc_period.crt_date = now;
            doc_period.crt_user = user.client_name;
            doc_period.upd_date = now;
            doc_period.upd_user = user.client_name;
            doc_period.is_deleted = false;
            dbContext.doc_period.Add(doc_period);

            //document.is_deleted = true;
            doc.doc_state_id = (int)Enums.DocStateEnum.ESN_PROCESS;
            doc.upd_user = user.client_name;

            dbContext.SaveChanges();

            return new BaseServiceResult();
        }

        private BaseServiceResult sendDocReceived_client(Client user, Guid docUuid)
        {
            DateTime now = DateTime.Now;

            var cnt = (from t1 in dbContext.doc_client_to
                       from t2 in dbContext.doc
                       where t1.doc_id == t2.doc_id
                       && t2.doc_uuid == docUuid
                       //&& t1.client_id == client.client_id
                       && t1.doc_state_id == (int)Enums.DocStateEnum.TARGET_QUEUE
                       && !t1.is_deleted
                       && !t2.is_deleted
                       && !t2.is_canceled
                       select t1)
                      .Distinct()
                      .Count();

            if (cnt <= 0)
            {
                return new BaseServiceResult()
                {
                    HasError = true,
                    ErrMess = "Не найден документ с кодом " + docUuid.ToString(),
                };
            }

            doc_client_to doc_client_to = (from t1 in dbContext.doc_client_to
                                           from t2 in dbContext.doc
                                           where t1.doc_id == t2.doc_id
                                           && t2.doc_uuid == docUuid
                                           && t1.client_id == user.client_id
                                           && t1.doc_state_id == (int)Enums.DocStateEnum.TARGET_QUEUE
                                           && !t1.is_deleted
                                           && !t2.is_deleted
                                           && !t2.is_canceled
                                           select t1)
                                          .FirstOrDefault();

            if (doc_client_to == null)
            {
                return new BaseServiceResult()
                {
                    HasError = true,
                    ErrMess = "Не найден документ с кодом " + docUuid.ToString(),
                };
            }

            //doc_client_to.is_deleted = true;
            doc_client_to.doc_state_id = (int)Enums.DocStateEnum.TARGET_COMPLETE;
            doc_client_to.upd_user = user.client_name;

            if (cnt == 1)
            {
                var document = dbContext.doc.Where(ss => ss.doc_id == doc_client_to.doc_id && !ss.is_deleted).FirstOrDefault();
                if (document != null)
                {
                    doc_period doc_period = new doc_period();
                    doc_period.doc_period_uuid = Guid.NewGuid();
                    doc_period.doc_id = document.doc_id;
                    doc_period.doc_state_id = (int)Enums.DocStateEnum.TARGET_COMPLETE;
                    doc_period.crt_date = now;
                    doc_period.crt_user = user.client_name;
                    doc_period.upd_date = now;
                    doc_period.upd_user = user.client_name;
                    doc_period.is_deleted = false;
                    dbContext.doc_period.Add(doc_period);

                    //document.is_deleted = true;
                    document.doc_state_id = (int)Enums.DocStateEnum.TARGET_COMPLETE;
                    document.upd_user = user.client_name;
                }
            }

            dbContext.SaveChanges();

            return new BaseServiceResult();
        }

        #endregion
    }
}