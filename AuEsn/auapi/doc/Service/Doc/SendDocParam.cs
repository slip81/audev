﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.api.Model;

namespace auesn.api.Service
{
    public class SendDocParam
    {        
        public SendDocParam()             
        {
            //
        }

        public Client client { get; set; }
        public string targetUuidList { get; set; }        
        public string docNum { get; set; }
        public string docContentType { get; set; }
        public string docTypeUuid { get; set; }
        public string docUuid { get; set; }
        public string docBody { get; set; }
    }
}