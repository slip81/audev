﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.api.Model;

namespace auesn.api.Service
{
    public interface IDocService
    {        
        SendDocResult SendDoc(SendDocParam sendDocParam);
        GetDocResult GetLastDoc(Client client, string acceptContentType);
        BaseServiceResult SendDocReceived(Client client, Guid docUuid);
    }
}