﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.api.Model;

namespace auesn.api.Service
{
    public interface IClientService
    {        
        Client GetClient(int cab_client_id, int cab_sales_id);
        string GetRecipients(int client_id, string acceptContentType);
    }
}