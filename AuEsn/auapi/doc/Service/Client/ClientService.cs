﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.dbmodel;
using auesn.api.Model;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using auesn.util;

namespace auesn.api.Service
{
    public class ClientService : BaseService, IClientService
    {
        public ClientService(IMapper _mapper)
            : base(_mapper)
        {
            //
        }

        public Client GetClient(int cab_client_id, int cab_sales_id)
        {
            var query = dbContext.client.Where(ss => ss.cab_client_code == cab_client_id);
            if (cab_sales_id > 0)
                query = query.Where(ss => ss.cab_sales_code == cab_sales_id);

            return query
                .ProjectTo<Client>()
                .FirstOrDefault();
        }

        public string GetRecipients(int client_id, string acceptContentType)
        {
            List<Recipient> res = (from t1 in dbContext.client_rel
                                   from t2 in dbContext.client
                                   where t1.client1_id == client_id
                                   && t1.client2_id == t2.client_id
                                   && t2.client_type_id != AuEsnConst.CLIENT_TYPE_ESN
                                   select new Recipient()
                                   {
                                       uuid = t2.client_uuid,
                                       name = t2.client_name,
                                   })
                                   .ToList();
            if ((res == null) || (res.Count <= 0))
                return null;

            var resFormatted = GlobalUtil.GetContentFormatted<List<Recipient>>(res, acceptContentType);
            return !resFormatted.Item1 ? resFormatted.Item2 : null;
        }
    }
}