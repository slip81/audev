﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.api.Model
{    
    public class GoodSourceCodes : GoodEtalon
    {
        public GoodSourceCodes()
        {            
            //
        }

        /*
        public string barcode { get; set; }
        public string productName { get; set; }
        public string mfrName { get; set; }
        public string countryName { get; set; }
        public string productTypeName { get; set; }
        public string innName { get; set; }
        public string atcName { get; set; }
        public string atcCode { get; set; }
        public string dosageName { get; set; }
        public string formName { get; set; }
        public string formShortName { get; set; }
        public string formGroupName { get; set; }
        public string pharmTherGroupName { get; set; }
        public string okpd2Name { get; set; }
        public string okpd2Code { get; set; }
        public string brandName { get; set; }
        public string recipeTypeName { get; set; }
        public string registryNum { get; set; }
        public Nullable<System.DateTime> registryDate { get; set; }
        public string baaTypeName { get; set; }
        public string metanameName { get; set; }
        public string tradeName { get; set; }        
        public string storageCondition { get; set; }
        public string indication { get; set; }
        public string contraindication { get; set; }
        public string storagePeriod { get; set; }
        public string applicAndDose { get; set; }
        public string goodGuid { get; set; }
        public string productGuid { get; set; }
        */
        public string goodSourceCode { get; set; }
    }
}