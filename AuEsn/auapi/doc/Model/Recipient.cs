﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.api.Model
{    
    public class Recipient
    {
        public Recipient()
        {
            //
        }
        
        public System.Guid uuid { get; set; }
        public string name { get; set; }
    }
}