﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.api.Model
{    
    public class GoodAnalog
    {
        public GoodAnalog()
        {            
            //auesn.dbmodel.vw_good_info
        }

        public string tradeName { get; set; }
        public string barcode { get; set; }
        public string mfrName { get; set; }
        public string innName { get; set; }
    }
}