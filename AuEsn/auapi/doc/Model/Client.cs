﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.api.Model
{    
    public class Client
    {
        public Client()
        {
            //auesn.dbmodel.client
        }

        public int client_id { get; set; }
        public System.Guid client_uuid { get; set; }
        public string client_name { get; set; }
        public Nullable<int> parent_id { get; set; }
        public string labeling_code { get; set; }
        public System.DateTime crt_date { get; set; }
        public string crt_user { get; set; }
        public System.DateTime upd_date { get; set; }
        public string upd_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
        public Nullable<int> client_type_id { get; set; }
        public Nullable<int> cab_client_code { get; set; }
        public Nullable<int> cab_sales_code { get; set; }

    }
}