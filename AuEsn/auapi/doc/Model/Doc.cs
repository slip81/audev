﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.api.Model
{    
    public class Doc
    {
        public Doc()
        {            
            //auesn.dbmodel.doc
        }

        public long doc_id { get; set; }
        public System.Guid doc_uuid { get; set; }
        public string doc_num { get; set; }
        public string doc_name { get; set; }
        public Nullable<System.DateTime> doc_date { get; set; }
        public int doc_type_id { get; set; }
        public int doc_state_id { get; set; }
        public string doc_path { get; set; }
        public int client_from_id { get; set; }
        public Nullable<int> client_to_id { get; set; }
        public System.DateTime crt_date { get; set; }
        public string crt_user { get; set; }
        public System.DateTime upd_date { get; set; }
        public string upd_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
        public string content_type { get; set; }
        public bool is_need_reply { get; set; }
        public bool is_canceled { get; set; }
        public Nullable<System.DateTime> cancel_date { get; set; }
        public string cancel_user { get; set; }
    }
}