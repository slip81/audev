﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using CsvHelper;
using CsvHelper.Configuration;

namespace auesn.api.Model
{

    public class GoodSz
    {
        public GoodSz()
        {
            //
        }

        public string error { get; set; }
        public string description { get; set; }
        public GoodSzData data { get; set; }
    }

    public class GoodSzData
    {
        public List<GoodSzBasetov> basetovs { get; set; }
    }
    public class GoodSzBasetov
    {
        public int? tov_id { get; set; }
        public int? kat_id { get; set; }
        public string kat { get; set; }
        public int? izg_id { get; set; }
        public string izg { get; set; }
        public int? mnn_id { get; set; }
        public string mnn { get; set; }
        public int? gr_id { get; set; }
        public string gr { get; set; }
        public int? gvls { get; set; }
        public int? pku { get; set; }
        public int? pku3m { get; set; }
        public int? assort { get; set; }
        public string modify { get; set; }
        public List<string> eans { get; set; }
        [JsonIgnore()]
        public DateTime? modify_date
        {
            get
            {
                return String.IsNullOrWhiteSpace(modify)
                    ? null
                    : (DateTime?)DateTime.ParseExact(modify, "dd.MM.yy", CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
        }
        [JsonIgnore()]
        public string eans_list
        {
            get { return ((eans == null) || (eans.Count <= 0)) ? null : string.Join<string>(",", eans); }
        }
    }

    public sealed class GoodSzBasetovClassMap : ClassMap<GoodSzBasetov>
    {
        public GoodSzBasetovClassMap()
        {
            AutoMap();
            Map(m => m.eans).Ignore();
            Map(m => m.modify).Ignore();
        }
    }
}