﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using auesn.util;
using System.Security.Claims;
using auesn.api.Service;

namespace auesn.api.Controllers
{
    public class BaseController : ApiController
    {

        protected IDocService docService;
        protected IClientService clientService;
        protected IGoodService goodService;
        protected IGoodSzService goodSzService;


        protected AuEsnUser CurrentUser
        {
            get
            {
                if (currentUser == null)
                {
                    currentUser = new AuEsnUser(User.Identity as ClaimsIdentity);
                }
                return currentUser;
            }            
        }
        private AuEsnUser currentUser;

        public BaseController()
        {
            //
        }
        
    }
}
