﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using auesn.api.Service;
using auesn.util;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Linq;

namespace auesn.api.Controllers
{
    [Authorize]
    [RoutePrefix("recipients")]
    public partial class RecipientsController : BaseController
    {
        private const string acceptHeader_def = AuEsnConst.MEDIA_TYPE_JSON;
        private const string acceptCharsetHeader_def = AuEsnConst.CHARSET_UTF8;

        public RecipientsController(IClientService _clientService)
        {            
            clientService = _clientService;            
        }

        #region GetRecipients
        [Route("")]
        [HttpGet()]
        public HttpResponseMessage GetRecipients()
        {
            try
            {
                var client = clientService.GetClient(CurrentUser.CabClientId, CurrentUser.CabSalesId);
                if (client == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найден клиент с кодом " + CurrentUser.CabClientId.ToString(),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                IEnumerable<string> acceptHeaders;
                string acceptHeader = string.Empty;
                if (Request.Headers.TryGetValues("Accept", out acceptHeaders))
                {
                    acceptHeader = acceptHeaders.FirstOrDefault();
                }
                if (String.IsNullOrWhiteSpace(acceptHeader))
                    acceptHeader = acceptHeader_def;
                acceptHeader = acceptHeader.Trim().ToLower().TrimEnd(';');

                switch (acceptHeader)
                {
                    case AuEsnConst.MEDIA_TYPE_JSON:
                    case AuEsnConst.MEDIA_TYPE_TEXT:
                    case AuEsnConst.MEDIA_TYPE_CSV:
                    case AuEsnConst.MEDIA_TYPE_XML:
                        break;
                    default:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Неподдерживаемый тип заголовка Accept [" + acceptHeader + "]",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                };

                var recipients = clientService.GetRecipients(client.client_id, acceptHeader);
                if (String.IsNullOrWhiteSpace(recipients))
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                    };

                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                switch (acceptHeader)
                {

                    case AuEsnConst.MEDIA_TYPE_JSON:
                    case AuEsnConst.MEDIA_TYPE_XML:
                    case AuEsnConst.MEDIA_TYPE_TEXT:
                        result.Content = new StringContent(
                            recipients,
                            Encoding.GetEncoding("windows-1251"),
                            acceptHeader);
                        break;
                    case AuEsnConst.MEDIA_TYPE_CSV:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Преобразвание в формат " + acceptHeader + " пока не реализовано",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                    default:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Неподдерживаемый тип заголовка Accept [" + acceptHeader + "]",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                };

                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion        
        
    }
}
