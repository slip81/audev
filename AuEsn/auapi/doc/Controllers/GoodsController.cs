﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using auesn.api.Service;
using auesn.util;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Linq;

namespace auesn.api.Controllers
{
    [Authorize]
    [RoutePrefix("goods")]
    public partial class GoodsController : BaseController
    {
        private const string acceptHeader_def = AuEsnConst.MEDIA_TYPE_JSON;
        private const string acceptCharsetHeader_def = AuEsnConst.CHARSET_UTF8;

        public GoodsController(IClientService _clientService, IGoodService _goodService, IGoodSzService _goodSzService)
        {            
            clientService = _clientService;
            goodService = _goodService;
            goodSzService = _goodSzService;
        }        

        #region GetGoodInfo
        [Route("")]
        [HttpGet()]
        public HttpResponseMessage GetGoodInfo()
        {
            try
            {
                var queryParams = this.Request.GetQueryStrings();

                string code = queryParams.ContainsKey("code") ? queryParams["code"] : null;
                string name = queryParams.ContainsKey("name") ? queryParams["name"] : null;
                string barcode = queryParams.ContainsKey("barcode") ? queryParams["barcode"] : null;
                string codes = queryParams.ContainsKey("codes") ? queryParams["codes"] : null;
                string barcodes = queryParams.ContainsKey("barcodes") ? queryParams["barcodes"] : null;
                string shortFormStr = queryParams.ContainsKey("short") ? queryParams["short"] : null;
                bool shortForm = !String.IsNullOrWhiteSpace(shortFormStr) && shortFormStr.Trim().ToLower().Equals("true");

                var client = clientService.GetClient(CurrentUser.CabClientId, CurrentUser.CabSalesId);
                if (client == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найден клиент с кодом " + CurrentUser.CabClientId.ToString(),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                IEnumerable<string> acceptHeaders;
                string acceptHeader = string.Empty;
                if (Request.Headers.TryGetValues("Accept", out acceptHeaders))
                {
                    acceptHeader = acceptHeaders.FirstOrDefault();
                }
                if (String.IsNullOrWhiteSpace(acceptHeader))
                    acceptHeader = acceptHeader_def;
                acceptHeader = acceptHeader.Trim().ToLower().TrimEnd(';');

                switch (acceptHeader)
                {
                    case AuEsnConst.MEDIA_TYPE_JSON:
                    case AuEsnConst.MEDIA_TYPE_TEXT:
                    case AuEsnConst.MEDIA_TYPE_CSV:
                    case AuEsnConst.MEDIA_TYPE_XML:
                        break;
                    default:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Неподдерживаемый тип заголовка Accept [" + acceptHeader + "]",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                };

                GetGoodInfoParam getGoodInfoParam = new GetGoodInfoParam()
                {
                    client_id = client.client_id,
                    acceptContentType = acceptHeader,
                    shortForm = shortForm,
                    code = code,
                    name = name,
                    barcode = barcode,
                    codes = codes,
                    barcodes = barcodes,
                };
                var goodInfo = goodService.GetGoodInfo(getGoodInfoParam);
                if (String.IsNullOrWhiteSpace(goodInfo))
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                    };

                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                switch (acceptHeader)
                {

                    case AuEsnConst.MEDIA_TYPE_JSON:
                    case AuEsnConst.MEDIA_TYPE_XML:
                    case AuEsnConst.MEDIA_TYPE_TEXT:
                        result.Content = new StringContent(
                            goodInfo,
                            Encoding.GetEncoding("windows-1251"),
                            acceptHeader);
                        break;
                    case AuEsnConst.MEDIA_TYPE_CSV:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Преобразвание в формат " + acceptHeader + " пока не реализовано",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                    default:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Неподдерживаемый тип заголовка Accept [" + acceptHeader + "]",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                };

                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion        

        #region GetGoodAnalogs
        [Route("analogs")]
        [HttpPost()]
        public HttpResponseMessage GetGoodAnalogs([FromBody]GetGoodAnalogsParam getGoodAnalogsParam)
        {
            try
            {
                if ((getGoodAnalogsParam == null) || (getGoodAnalogsParam.items == null) || (getGoodAnalogsParam.items.Count <= 0))
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                        "Не заданы входные параметры",
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                var client = clientService.GetClient(CurrentUser.CabClientId, CurrentUser.CabSalesId);
                if (client == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найден клиент с кодом " + CurrentUser.CabClientId.ToString(),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }
                getGoodAnalogsParam.client_id = client.client_id;

                IEnumerable<string> acceptHeaders;
                string acceptHeader = string.Empty;
                if (Request.Headers.TryGetValues("Accept", out acceptHeaders))
                {
                    acceptHeader = acceptHeaders.FirstOrDefault();
                }
                if (String.IsNullOrWhiteSpace(acceptHeader))
                    acceptHeader = acceptHeader_def;
                acceptHeader = acceptHeader.Trim().ToLower().TrimEnd(';');

                switch (acceptHeader)
                {
                    case AuEsnConst.MEDIA_TYPE_JSON:
                    case AuEsnConst.MEDIA_TYPE_TEXT:
                    case AuEsnConst.MEDIA_TYPE_CSV:
                    case AuEsnConst.MEDIA_TYPE_XML:
                        break;
                    default:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Неподдерживаемый тип заголовка Accept [" + acceptHeader + "]",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                };
                getGoodAnalogsParam.acceptContentType = acceptHeader;

                var goodAnalogs = goodService.GetGoodAnalogs(getGoodAnalogsParam);
                if (String.IsNullOrWhiteSpace(goodAnalogs))
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                    };

                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                switch (acceptHeader)
                {
                    case AuEsnConst.MEDIA_TYPE_JSON:
                    case AuEsnConst.MEDIA_TYPE_XML:
                    case AuEsnConst.MEDIA_TYPE_TEXT:
                        result.Content = new StringContent(
                            goodAnalogs,
                            Encoding.GetEncoding("windows-1251"),
                            acceptHeader);
                        break;
                    case AuEsnConst.MEDIA_TYPE_CSV:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Преобразвание в формат " + acceptHeader + " пока не реализовано",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                    default:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Неподдерживаемый тип заголовка Accept [" + acceptHeader + "]",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                };

                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion        

        #region GetGoodEtalon
        [Route("etalon")]
        [HttpGet()]
        //[AllowAnonymous()]
        public HttpResponseMessage GetGoodEtalon()
        {
            try
            {                
                var client = clientService.GetClient(CurrentUser.CabClientId, CurrentUser.CabSalesId);
                if (client == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найден клиент с кодом " + CurrentUser.CabClientId.ToString(),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                IEnumerable<string> getFullEtalonHeaders;
                string getFullEtalonHeader = string.Empty;
                if (Request.Headers.TryGetValues("x-full-etalon", out getFullEtalonHeaders))
                {
                    getFullEtalonHeader = getFullEtalonHeaders.FirstOrDefault();
                }
                bool getFullEtalon = (!String.IsNullOrWhiteSpace(getFullEtalonHeader)) && (getFullEtalonHeader.Trim().ToLower().Equals("1"));

                GetGoodEtalonParam getGoodEtalonParam = new GetGoodEtalonParam()
                {
                    client_id = client.client_id,
                    get_full = getFullEtalon,
                };


                // !!!
                // test
                /*
                GetGoodEtalonParam getGoodEtalonParam = new GetGoodEtalonParam()
                {
                    client_id = 194,
                };
                */

                var goodEtalon = goodService.GetGoodEtalon(getGoodEtalonParam);
                if ((goodEtalon == null) || (goodEtalon.Length <= 0))
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                    };

                var responseBytes = new MemoryStream(goodEtalon.ToArray());
                var response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StreamContent(responseBytes)
                };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                return response;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion        

        #region GetGoodSourceCodes
        [Route("sourcecodes")]
        [HttpGet()]
        //[AllowAnonymous()]
        public HttpResponseMessage GetGoodSourceCodes()
        {
            try
            {
                var client = clientService.GetClient(CurrentUser.CabClientId, CurrentUser.CabSalesId);
                if (client == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найден клиент с кодом " + CurrentUser.CabClientId.ToString(),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                IEnumerable<string> getHeaders;
                string getHeader = string.Empty;
                if (Request.Headers.TryGetValues("x-source-client", out getHeaders))
                {
                    getHeader = getHeaders.FirstOrDefault();
                }
                string sourceClientUuid = getHeader;

                GetGoodSourceCodesParam getGoodSourceCodesParam = new GetGoodSourceCodesParam()
                {
                    client_id = client.client_id,
                    source_client_uuid = sourceClientUuid,
                };


                // !!!
                // test
                /*
                GetGoodEtalonParam getGoodEtalonParam = new GetGoodEtalonParam()
                {
                    client_id = 194,
                };
                */

                var goodSourceCodes = goodService.GetGoodSourceCodes(getGoodSourceCodesParam);
                if ((goodSourceCodes == null) || (goodSourceCodes.Length <= 0))
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                    };

                var responseBytes = new MemoryStream(goodSourceCodes.ToArray());
                var response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StreamContent(responseBytes)
                };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                return response;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion        

        #region GetGoodSzFromSite
        [Route("GetGoodSzFromSite")]
        [HttpGet()]
        [AllowAnonymous()]
        public HttpResponseMessage GetGoodSzFromSite()
        {
            try
            {
                var getGoodSzFromSiteResult = goodSzService.GetGoodSzFromSite();
                if ((getGoodSzFromSiteResult == null) || (getGoodSzFromSiteResult.HasError))
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.InternalServerError,
                        Content = new StringContent(
                            getGoodSzFromSiteResult.ErrMess,
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };

                var response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,                    
                };
                return response;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion        

        #region GetGoodSz
        [Route("sz")]
        [HttpGet()]        
        public HttpResponseMessage GetGoodSz()
        {
            try
            {
                var client = clientService.GetClient(CurrentUser.CabClientId, CurrentUser.CabSalesId);
                if (client == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найден клиент с кодом " + CurrentUser.CabClientId.ToString(),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                IEnumerable<string> getFullEtalonHeaders;
                string getFullEtalonHeader = string.Empty;
                if (Request.Headers.TryGetValues("x-full-etalon", out getFullEtalonHeaders))
                {
                    getFullEtalonHeader = getFullEtalonHeaders.FirstOrDefault();
                }
                bool getFullEtalon = (!String.IsNullOrWhiteSpace(getFullEtalonHeader)) && (getFullEtalonHeader.Trim().ToLower().Equals("1"));

                GetGoodEtalonParam getGoodSzParam = new GetGoodEtalonParam()
                {
                    client_id = client.client_id,
                    get_full = getFullEtalon,
                };

                var goodSz = goodSzService.GetGoodSz(getGoodSzParam);
                if ((goodSz == null) || (goodSz.Length <= 0))
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                    };

                var responseBytes = new MemoryStream(goodSz.ToArray());
                var response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StreamContent(responseBytes)
                };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                return response;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion        
    }
}
