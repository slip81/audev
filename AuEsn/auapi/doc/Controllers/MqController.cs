﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using auesn.api.Service;
using auesn.util;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Linq;

namespace auesn.api.Controllers
{    
    [Authorize]
    [RoutePrefix("mq")]
    public partial class MqController : BaseController
    {
        private const string acceptHeader_def = AuEsnConst.MEDIA_TYPE_JSON;
        private const string acceptCharsetHeader_def = AuEsnConst.CHARSET_UTF8;

        public MqController(IDocService _docService, IClientService _clientService)
        {
            docService = _docService;
            clientService = _clientService;
        }

        #region GetLastDoc
        [Route("")]
        [HttpGet()]
        public HttpResponseMessage GetLastDoc()
        {
            try
            {
                var client = clientService.GetClient(CurrentUser.CabClientId, CurrentUser.CabSalesId);
                if (client == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найден клиент с кодом " + CurrentUser.CabClientId.ToString(),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                IEnumerable<string> acceptHeaders;
                string acceptHeader = string.Empty;                
                if (Request.Headers.TryGetValues("Accept", out acceptHeaders))
                {
                    acceptHeader = acceptHeaders.FirstOrDefault();
                }
                if (String.IsNullOrWhiteSpace(acceptHeader))
                    acceptHeader = acceptHeader_def;
                acceptHeader = acceptHeader.Trim().ToLower().TrimEnd(';');

                IEnumerable<string> acceptCharsetHeaders;
                string acceptCharsetHeader = string.Empty;
                if (Request.Headers.TryGetValues("Accept-Charset", out acceptCharsetHeaders))
                {
                    acceptCharsetHeader = acceptCharsetHeaders.FirstOrDefault();
                }
                if (String.IsNullOrWhiteSpace(acceptCharsetHeader))
                    acceptCharsetHeader = acceptCharsetHeader_def;
                acceptCharsetHeader = acceptCharsetHeader.Trim().ToLower().TrimEnd(';');

                switch (acceptHeader)
                {

                    case AuEsnConst.MEDIA_TYPE_JSON:
                    case AuEsnConst.MEDIA_TYPE_TEXT:
                    case AuEsnConst.MEDIA_TYPE_CSV:
                    case AuEsnConst.MEDIA_TYPE_XML:
                        break;
                    default:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Неподдерживаемый тип заголовка Accept [" + acceptHeader + "]",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                };

                var getDocResult = docService.GetLastDoc(client, acceptHeader);
                if (getDocResult == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "getDocResult == null",
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                if (getDocResult.HasError)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        Content = new StringContent(
                            String.IsNullOrWhiteSpace(getDocResult.ErrMess) ? "Ошибка при вызове метода GetLastDoc" : ("Ошибка при вызове метода GetLastDoc: " + getDocResult.ErrMess),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                if (String.IsNullOrWhiteSpace(getDocResult.Body))
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найдено тело документа",
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }


                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                switch (acceptHeader)
                {

                    case AuEsnConst.MEDIA_TYPE_JSON:
                    case AuEsnConst.MEDIA_TYPE_XML:
                    case AuEsnConst.MEDIA_TYPE_TEXT:
                    case AuEsnConst.MEDIA_TYPE_CSV:
                        result.Content = new StringContent(
                            getDocResult.Body,
                            Encoding.GetEncoding("windows-1251"),
                            acceptHeader);
                        break;                    
                    default:
                        return new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Content = new StringContent(
                                "Неподдерживаемый тип заголовка Accept [" + acceptHeader + "]",
                                Encoding.GetEncoding("windows-1251"),
                                AuEsnConst.MEDIA_TYPE_TEXT),
                        };
                };

                //result.Content.Headers.Add("Content-Type", acceptHeader);
                //result.Content.Headers.Add("Content-Length", getDocResult.BodyLength.HasValue ? getDocResult.BodyLength.ToString() : "0");
                result.Headers.Add("x-document-uuid", getDocResult.Uuid.HasValue ? getDocResult.Uuid.ToString() : "");
                result.Headers.Add("x-document-date", getDocResult.CrtDate.HasValue ? getDocResult.CrtDate.ToString() : "");
                result.Headers.Add("x-document-type", getDocResult.DocTypeUuid.HasValue ? getDocResult.DocTypeUuid.ToString() : "");
                result.Headers.Add("x-type-description", getDocResult.DocTypeName);
                result.Headers.Add("x-sender-uuid", getDocResult.ClientFromUuid.HasValue ? getDocResult.ClientFromUuid.ToString() : "");
                result.Headers.Add("x-sender", getDocResult.ClientFromName);
                result.Headers.Add("x-messages-counter", getDocResult.MsgCnt.HasValue ? getDocResult.MsgCnt.ToString() : "");
                result.Headers.Add("x-reply-expected", getDocResult.IsNeedReply ? "true" : "false");

                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        "Непредвиденное исключение: " + GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion

        #region SendDocReceived
        [Route("")]
        [HttpPost()]
        public HttpResponseMessage SendDocReceived()
        {
            try
            {
                var client = clientService.GetClient(CurrentUser.CabClientId, CurrentUser.CabSalesId);
                if (client == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найден клиент с кодом " + CurrentUser.CabClientId.ToString(),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }


                IEnumerable<string> documentUuidHeaders;
                string documentUuidHeader = string.Empty;
                if (Request.Headers.TryGetValues("x-document-uuid", out documentUuidHeaders))
                {
                    documentUuidHeader = documentUuidHeaders.FirstOrDefault();
                }
                if (String.IsNullOrWhiteSpace(documentUuidHeader))
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не задан код документа",
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }
                documentUuidHeader = documentUuidHeader.Trim().ToLower().TrimEnd(';');
                Guid documentUuid = Guid.Empty;
                if (!Guid.TryParse(documentUuidHeader, out documentUuid))
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Некорректный формат кода документа: " + documentUuidHeader,
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }                


                var res = docService.SendDocReceived(client, documentUuid);
                if (res.HasError)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        Content = new StringContent(
                            res.ErrMess,
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK,
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion
    }
}

