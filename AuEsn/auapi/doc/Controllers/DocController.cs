﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using auesn.api.Service;
using auesn.util;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Linq;

namespace auesn.api.Controllers
{    
    [Authorize]
    [RoutePrefix("doc")]
    public partial class DocController : BaseController
    {
        private const string acceptHeader_def = AuEsnConst.MEDIA_TYPE_TEXT;
        private const string contentTypeHeader_def = AuEsnConst.MEDIA_TYPE_JSON;

        public DocController(IDocService _docService, IClientService _clientService)
        {
            docService = _docService;
            clientService = _clientService;
        }

        #region SendDoc
        [Route("")]
        [HttpPost()]
        public HttpResponseMessage SendDoc()
        {
            try
            {
                var client = clientService.GetClient(CurrentUser.CabClientId, CurrentUser.CabSalesId);
                if (client == null)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не найден клиент с кодом " + CurrentUser.CabClientId.ToString(),
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                HttpContent requestContent = Request.Content;                
                string body = requestContent.ReadAsStringAsync().Result;

                IEnumerable<string> contentTypeHeaders;
                string contentTypeHeader = string.Empty;                
                if (Request.Content.Headers.TryGetValues("Content-Type", out contentTypeHeaders))
                {
                    contentTypeHeader = contentTypeHeaders.FirstOrDefault();
                }
                if (String.IsNullOrWhiteSpace(contentTypeHeader))
                    contentTypeHeader = contentTypeHeader_def;
                contentTypeHeader = contentTypeHeader.Trim().ToLower().TrimEnd(';');

                IEnumerable<string> documentTypeHeaders;
                string documentTypeHeader = string.Empty;
                if (Request.Headers.TryGetValues("x-document-type", out documentTypeHeaders))
                {
                    documentTypeHeader = documentTypeHeaders.FirstOrDefault();
                }
                if (String.IsNullOrWhiteSpace(documentTypeHeader))
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.NoContent,
                        Content = new StringContent(
                            "Не задан тип документа",
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }
                documentTypeHeader = documentTypeHeader.Trim().ToLower().TrimEnd(';');

                IEnumerable<string> documentNumHeaders;
                string documentNumHeader = string.Empty;
                if (Request.Headers.TryGetValues("x-document-num", out documentNumHeaders))
                {
                    documentNumHeader = documentNumHeaders.FirstOrDefault();
                }
                documentNumHeader = documentNumHeader == null ? documentNumHeader : documentNumHeader.Trim().ToLower().TrimEnd(';');

                IEnumerable<string> recipientHeaders;
                string recipientHeader = string.Empty;
                if (Request.Headers.TryGetValues("x-recipient", out recipientHeaders))
                {
                    recipientHeader = recipientHeaders.FirstOrDefault();
                }
                recipientHeader = recipientHeader == null ? recipientHeader : recipientHeader.Trim().ToLower().TrimEnd(';');

                IEnumerable<string> documentUuidHeaders;
                string documentUuidHeader = string.Empty;
                if (Request.Headers.TryGetValues("x-document-uuid", out documentUuidHeaders))
                {
                    documentUuidHeader = documentUuidHeaders.FirstOrDefault();
                }
                documentUuidHeader = documentUuidHeader == null ? documentUuidHeader : documentUuidHeader.Trim().ToLower().TrimEnd(';');

                string bodyFormatted = body;
                // потом сделать обратно красивое форматирование
                /*
                if (!String.IsNullOrWhiteSpace(bodyFormatted))
                {
                    switch (contentTypeHeader)
                    {
                        case AuEsnConst.MEDIA_TYPE_JSON:
                            bodyFormatted = GlobalUtil.FormatAsJSON(body);
                            break;
                        case AuEsnConst.MEDIA_TYPE_XML:
                            bodyFormatted = GlobalUtil.FormatAsXML(body);
                            break;
                        default:
                            break;
                    }
                }
                */

                SendDocParam sendDocParam = new SendDocParam()
                {
                    client = client,
                    targetUuidList = recipientHeader,
                    docNum = documentNumHeader,
                    docContentType = contentTypeHeader,
                    docTypeUuid = documentTypeHeader,                    
                    docUuid = documentUuidHeader,
                    docBody = bodyFormatted,
                };

                //var res = docService.SendDoc(client, documentTypeHeader, documentNumHeader, recipientHeader, documentUuidHeader, bodyFormatted);
                var res = docService.SendDoc(sendDocParam);
                if (res.HasError)
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        Content = new StringContent(
                            res.ErrMess,
                            Encoding.GetEncoding("windows-1251"),
                            AuEsnConst.MEDIA_TYPE_TEXT),
                    };
                }

                HttpResponseMessage result = new HttpResponseMessage();
                result.Headers.Add("x-document-uuid", res.Uuid);
                //result.Content = new StringContent("");                
                result.StatusCode = HttpStatusCode.OK;
                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(
                        GlobalUtil.ExceptionInfo(ex),
                        Encoding.GetEncoding("windows-1251"),
                        AuEsnConst.MEDIA_TYPE_TEXT),
                };
            }
        }
        #endregion

    }
}

