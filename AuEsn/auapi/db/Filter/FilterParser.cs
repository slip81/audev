﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace auesn.rest.Service
{
    public static class FilterParser
    {
        public static string OperationToSql(string op)
        {
            switch (op)
            {
                case "eq":
                    return "=";
                case "gt":
                    return ">";
                case "gte":
                    return ">=";
                case "lt":
                    return "<";
                case "lte":
                    return "<=";
                case "neq":
                    return "!=";
                case "like":
                    return "ilike";
                case "in":
                    return "in";
                case "is":
                    return "is";
                case "nis":
                    return "is not";
                default:
                    throw new NotSupportedException("FilterParser: операция не поддерживается [" + op + "]");
            }            
        }
    }
}