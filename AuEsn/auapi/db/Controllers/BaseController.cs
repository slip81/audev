﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using auesn.util;
using System.Security.Claims;

namespace auesn.rest.Controllers
{
    public class BaseController : ApiController
    {
        protected AuEsnUser CurrentUser
        {
            get
            {
                if (currentUser == null)
                {
                    currentUser = new AuEsnUser(User.Identity as ClaimsIdentity);
                }
                return currentUser;
            }            
        }
        private AuEsnUser currentUser;

        public BaseController()
        {
            //
        }
        
    }
}
