﻿using System.Web.Http;


namespace auesn.rest.Controllers
{
    [RoutePrefix("db")]
    public partial class DbController : BaseController
    {
        private IgnorableSerializerContractResolver getJsonResolver<T>(bool allfields)
        {
            var jsonResolver = new IgnorableSerializerContractResolver();
            if (!allfields)
            {
                jsonResolver.Ignore(typeof(T), "crt_date");
                jsonResolver.Ignore(typeof(T), "crt_user");
                jsonResolver.Ignore(typeof(T), "upd_date");
                jsonResolver.Ignore(typeof(T), "upd_user");
                jsonResolver.Ignore(typeof(T), "is_deleted");
                jsonResolver.Ignore(typeof(T), "del_date");
                jsonResolver.Ignore(typeof(T), "del_user");
            }
            
            if (typeof(T) == typeof(Model.Client))
            {
                jsonResolver.Ignore(typeof(T), "client_rel");
            }

            return jsonResolver;
        }
    }
}
