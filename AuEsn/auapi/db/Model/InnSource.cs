using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{   
    public partial class InnSource
    {        
        public InnSource()
        {
            //auesn.dbmodel.inn_source
        }

        public long inn_source_id { get; set; }
        public Nullable<System.Guid> inn_source_uuid { get; set; }
        public string inn_source_name { get; set; }
        public Nullable<int> client_source_id { get; set; }
        [MappedDbField("client_source_id", "client")]
        public string client_source_uuid { get; set; }
        public Nullable<int> inn_id { get; set; }
        [MappedDbField("inn_id", "inn")]
        public string inn_uuid { get; set; }

        [ChildObject()]
        public IEnumerable<InnSourceIngredient> inn_source_ingredient { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
