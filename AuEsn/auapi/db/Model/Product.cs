using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class Product
    {        
        public Product()
        {
            //auesn.dbmodel.product
        }
    
        public long product_id { get; set; }
        public Nullable<System.Guid> product_uuid { get; set; }
        public string product_name { get; set; }
        public int product_type_id { get; set; }
        [MappedDbField("product_type_id", "product_type")]
        public string product_type_uuid { get; set; }
        public Nullable<long> atc_id { get; set; }
        [MappedDbField("atc_id", "atc")]
        public string atc_uuid { get; set; }
        public Nullable<int> dosage_id { get; set; }
        [MappedDbField("dosage_id", "dosage")]
        public string dosage_uuid { get; set; }
        public Nullable<int> form_id { get; set; }
        [MappedDbField("form_id", "form")]
        public string form_uuid { get; set; }
        public Nullable<int> pharm_ther_group_id { get; set; }
        [MappedDbField("pharm_ther_group_id", "pharm_ther_group")]
        public string pharm_ther_group_uuid { get; set; }
        public Nullable<int> okpd2_id { get; set; }
        [MappedDbField("okpd2_id", "okpd2")]
        public string okpd2_uuid { get; set; }
        public Nullable<long> brand_id { get; set; }
        [MappedDbField("brand_id", "brand")]
        public string brand_uuid { get; set; }
        public Nullable<int> recipe_type_id { get; set; }
        [MappedDbField("recipe_type_id", "recipe_type")]
        public string recipe_type_uuid { get; set; }
        public Nullable<long> registry_id { get; set; }
        [MappedDbField("registry_id", "registry")]
        public string registry_uuid { get; set; }
        public Nullable<int> baa_type_id { get; set; }
        [MappedDbField("baa_type_id", "baa_type")]
        public string baa_type_uuid { get; set; }
        public Nullable<long> metaname_id { get; set; }
        [MappedDbField("metaname_id", "metaname")]
        public string metaname_uuid { get; set; }
        public string storage_period { get; set; }
        public string storage_condition { get; set; }
        public Nullable<bool> is_vital { get; set; }
        public Nullable<bool> is_drug2 { get; set; }
        public Nullable<bool> is_drug3 { get; set; }
        public Nullable<bool> is_precursor { get; set; }
        public Nullable<bool> is_alcohol { get; set; }
        public Nullable<bool> is_imm_bio { get; set; }
        public Nullable<bool> is_mandatory { get; set; }
        public Nullable<bool> is_defect_exists { get; set; }
        public string indication { get; set; }
        public string contraindication { get; set; }
        public string drug_interaction { get; set; }
        public Nullable<long> trade_name_id { get; set; }
        [MappedDbField("trade_name_id", "trade_name")]
        public string trade_name_uuid { get; set; }
        public string storage_condition_short { get; set; }
        public string indication_short { get; set; }
        public string contraindication_short { get; set; }
        public string applic_and_dose { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
