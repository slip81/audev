using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class Substance
    {        
        public Substance()
        {
            //auesn.db.Db.substance
        }

        public long substance_id { get; set; }
        public Nullable<System.Guid> substance_uuid { get; set; }
        public string substance_name { get; set; }
        public string substance_trade_name { get; set; }
        public string storage_period { get; set; }
        public string storage_condition { get; set; }
        public string doc_name { get; set; }
        public Nullable<bool> is_drug { get; set; }
        public Nullable<long> mfr_id { get; set; }
        [MappedDbField("mfr_id", "mfr")]
        public string mfr_uuid { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
