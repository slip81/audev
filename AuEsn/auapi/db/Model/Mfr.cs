using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{   
    public partial class Mfr
    {        
        public Mfr()
        {
            //auesn.db.Db.mfr
        }        
        public long mfr_id { get; set; }
        public Nullable<System.Guid> mfr_uuid { get; set; }
        public string mfr_name { get; set; }
        public Nullable<int> country_id { get; set; }
        [MappedDbField("country_id", "country")]
        public string country_uuid { get; set; }
        public string labeling_code { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
