using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class ClientGood
    {
        public ClientGood()
        {
            //
        }

        public long client_good_id { get; set; }
        public Nullable<System.Guid> client_good_uuid { get; set; }
        public int client_id { get; set; }
        [MappedDbField("client_id", "client")]
        public string client_uuid { get; set; }
        public long good_id { get; set; }
        [MappedDbField("good_id", "good")]
        public string good_uuid { get; set; }
        public string client_code { get; set; }
        public string client_name { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
