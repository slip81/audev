using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class ProductPharmGroup
    {
        public ProductPharmGroup()
        {
            //
        }

        public long product_pharm_group_id { get; set; }
        public Nullable<System.Guid> product_pharm_group_uuid { get; set; }
        public long product_id { get; set; }
        [MappedDbField("product_id", "product")]
        public string product_uuid { get; set; }
        public int pharm_group_id { get; set; }
        [MappedDbField("pharm_group_id", "pharm_group")]
        public string pharm_group_uuid { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
