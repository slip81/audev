using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class Doc
    {        
        public Doc()
        {
            //auesn.dbmodel.doc
        }
    
        public long doc_id { get; set; }
        public Nullable<System.Guid> doc_uuid { get; set; }
        public string doc_num { get; set; }
        public string doc_name { get; set; }
        public Nullable<System.DateTime> doc_date { get; set; }
        public int doc_type_id { get; set; }
        [MappedDbField("doc_type_id", "doc_type")]
        public string doc_type_uuid { get; set; }
        public int doc_state_id { get; set; }
        [MappedDbField("doc_state_id", "doc_state")]
        public string doc_state_uuid { get; set; }
        public string doc_path { get; set; }
        public int client_from_id { get; set; }
        [MappedDbField("client_from_id", "client")]
        public string client_from_uuid { get; set; }
        public Nullable<int> client_to_id { get; set; }
        [MappedDbField("client_to_id", "client")]
        public string client_to_uuid { get; set; }
        public string content_type { get; set; }
        public bool is_need_reply { get; set; }
        public bool is_canceled { get; set; }
        public Nullable<System.DateTime> cancel_date { get; set; }
        public string cancel_user { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
