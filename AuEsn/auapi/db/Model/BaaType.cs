using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{

    public partial class BaaType
    {        
        public BaaType()
        {
            //auesn.db.Db.baa_type
        }
    
        public int baa_type_id { get; set; }
        public Nullable<System.Guid> baa_type_uuid { get; set; }
        public string baa_type_name { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }

    }
}
