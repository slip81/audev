using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
   
    public partial class Consumer
    {        
        public Consumer()
        {
            //auesn.db.Db.consumer
        }

        public int consumer_id { get; set; }
        public Nullable<System.Guid> consumer_uuid { get; set; }
        public string consumer_name { get; set; }
        
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
