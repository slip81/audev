using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class ClientService
    {
        public ClientService()
        {
            //
        }
            
        public int client_service_id { get; set; }
        public Nullable<System.Guid> client_service_uuid { get; set; }
        public int client_id { get; set; }
        [MappedDbField("client_id", "client")]
        public string client_uuid { get; set; }
        public int service_id { get; set; }
        [MappedDbField("service_id", "service")]
        public string service_uuid { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
