using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{ 
    public partial class Mkb10
    {  
        public Mkb10()
        {
            //auesn.dbmodel.mkb10
        }

        public int mkb10_id { get; set; }
        public Nullable<System.Guid> mkb10_uuid { get; set; }
        public string mkb10_name { get; set; }
        public string mkb10_code { get; set; }
        public Nullable<int> mkb10_class_id { get; set; }
        [MappedDbField("mkb10_class_id", "mkb10_class")]
        public string mkb10_class_uuid { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
