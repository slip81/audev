using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{   
    public partial class Supplier
    {        
        public Supplier()
        {
            //auesn.db.Db.supplier
        }

        public int supplier_id { get; set; }
        public Nullable<System.Guid> supplier_uuid { get; set; }
        public string supplier_name { get; set; }
        public string labeling_code { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
