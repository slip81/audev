using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class GoodDefect
    {
        public GoodDefect()
        {
            //
        }

        public long good_defect_id { get; set; }
        public Nullable<System.Guid> good_defect_uuid { get; set; }
        public long good_id { get; set; }
        [MappedDbField("good_id", "good")]
        public string good_uuid { get; set; }
        public string defect_series { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
