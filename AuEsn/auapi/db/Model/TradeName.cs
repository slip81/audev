using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{    
    public partial class TradeName
    {        
        public TradeName()
        {
            //auesn.db.Db.trade_name
        }

        public long trade_name_id { get; set; }
        public Nullable<System.Guid> trade_name_uuid { get; set; }
        public string trade_name_name { get; set; }
        public Nullable<long> inn_source_id { get; set; }
        [MappedDbField("inn_source_id", "inn_source")]
        public string inn_source_uuid { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
