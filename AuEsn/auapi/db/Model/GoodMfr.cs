using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class GoodMfr
    {
        public GoodMfr()
        {
            //
        }
            
        public long good_mfr_id { get; set; }
        public Nullable<System.Guid> good_mfr_uuid { get; set; }
        public long good_id { get; set; }
        [MappedDbField("good_id", "good")]
        public string good_uuid { get; set; }
        public long mfr_id { get; set; }
        [MappedDbField("mfr_id", "mfr")]
        public string mfr_uuid { get; set; }
        public Nullable<int> mfr_role_id { get; set; }
        [MappedDbField("mfr_role_id", "mfr_role")]
        public string mfr_role_uuid { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
