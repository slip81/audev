using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{    
    public partial class SupplierGood
    {
        public SupplierGood()
        {

        }

        public int supplier_good_id { get; set; }
        public Nullable<System.Guid> supplier_good_uuid { get; set; }
        public int supplier_id { get; set; }
        [MappedDbField("supplier_id", "supplier")]
        public string supplier_uuid { get; set; }
        public long good_id { get; set; }
        [MappedDbField("good_id", "good")]
        public string good_uuid { get; set; }
        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> quantity { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
