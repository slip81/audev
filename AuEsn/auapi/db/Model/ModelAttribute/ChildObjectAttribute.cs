using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ChildObjectAttribute : Attribute
    {
        public ChildObjectAttribute() 
        {
            //        
        }
    }
}
