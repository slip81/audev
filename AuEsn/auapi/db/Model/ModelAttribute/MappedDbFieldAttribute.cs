using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class MappedDbFieldAttribute : Attribute
    {
        public string KeyField { get; private set; }
        public string RefTable { get; private set; }        

        public MappedDbFieldAttribute(string keyField, string refTable) 
        {
            this.KeyField = keyField;
            this.RefTable = refTable;            
        }
    }
}
