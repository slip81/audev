using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{ 
    public partial class ClientRel
    {  
        public ClientRel()
        {
            //auesn.db.Db.client_rel
        }

        public int client_rel_id { get; set; }
        public Nullable<System.Guid> client_rel_uuid { get; set; }
        public int client1_id { get; set; }
        [MappedDbField("client1_id", "client")]
        public string client1_uuid { get; set; }
        public int client2_id { get; set; }
        [MappedDbField("client2_id", "client")]
        public string client2_uuid { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
