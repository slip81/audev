using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{ 
    public partial class Client
    {  
        public Client()
        {
            //auesn.db.Db.client
        }

        public int client_id { get; set; }
        public Nullable<System.Guid> client_uuid { get; set; }
        public string client_name { get; set; }
        public Nullable<int> parent_id { get; set; }
        [MappedDbField("parent_id", "client")]
        public string parent_uuid { get; set; }
        public string labeling_code { get; set; }
        public Nullable<int> client_type_id { get; set; }
        [MappedDbField("client_type_id", "client_type")]
        public string client_type_uuid { get; set; }
        public Nullable<int> cab_client_code { get; set; }
        public Nullable<int> cab_sales_code { get; set; }

        //[JsonIgnore()]
        [ChildObject()]
        public IEnumerable<ClientRel> client_rel { get; set; }

        [ChildObject()]
        public IEnumerable<ClientDocType> client_doc_type { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
