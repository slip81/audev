using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class Good
    {
        public Good()
        {
            //auesn.dbmodel.good
        }
    
        public long good_id { get; set; }
        public Nullable<System.Guid> good_uuid { get; set; }
        public long product_id { get; set; }
        [MappedDbField("product_id", "product")]
        public string product_uuid { get; set; }
        public long mfr_id { get; set; }
        [MappedDbField("mfr_id", "mfr")]
        public string mfr_uuid { get; set; }
        public string barcode { get; set; }
        public string gtin { get; set; }
        public bool is_perfect { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
