using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class MfrAlias
    {
        public MfrAlias()
        {
            //
        }

        public long mfr_alias_id { get; set; }
        public Nullable<System.Guid> mfr_alias_uuid { get; set; }
        public long mfr_id { get; set; }
        [MappedDbField("mfr_id", "mfr")]
        public string mfr_uuid { get; set; }
        public string mfr_alias_name { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
