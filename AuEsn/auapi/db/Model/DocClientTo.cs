using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class DocClientTo
    {        
        public DocClientTo()
        {
            //auesn.db.Db.doc_client_to
        }

        public long doc_client_to_id { get; set; }
        public Nullable<System.Guid> doc_client_to_uuid { get; set; }
        public long doc_id { get; set; }
        [MappedDbField("doc_id", "doc")]
        public string doc_uuid { get; set; }
        public int client_id { get; set; }
        [MappedDbField("client_id", "client")]
        public string client_uuid { get; set; }
        public int doc_state_id { get; set; }
        [MappedDbField("doc_state_id", "doc_state")]
        public string doc_state_uuid { get; set; }
        public bool is_success { get; set; }
        public string mess { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
