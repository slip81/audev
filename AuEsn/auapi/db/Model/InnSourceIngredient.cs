using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{    
    public partial class InnSourceIngredient
    {
        public InnSourceIngredient()
        {
            //auesn.dbmodel.inn_source_ingredient
        }

        public long inn_source_ingredient_id { get; set; }
        public Nullable<System.Guid> inn_source_ingredient_uuid { get; set; }
        public long inn_source_id { get; set; }
        [MappedDbField("inn_source_id", "inn_source")]
        public string inn_source_uuid { get; set; }
        public long ingredient_id { get; set; }
        [MappedDbField("ingredient_id", "ingredient")]
        public string ingredient_uuid { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
