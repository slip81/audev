using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class Okpd2
    {        
        public Okpd2()
        {
            //auesn.db.Db.okpd2
        }

        public int okpd2_id { get; set; }
        public Nullable<System.Guid> okpd2_uuid { get; set; }
        public string okpd2_name { get; set; }
        public string okpd2_code { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
