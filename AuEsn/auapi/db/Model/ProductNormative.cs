using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{   
    public partial class ProductNormative
    {
        public ProductNormative()
        {
            //
        }
            
        public long product_normative_id { get; set; }
        public Nullable<System.Guid> product_normative_uuid { get; set; }
        public long product_id { get; set; }
        [MappedDbField("product_id", "product")]
        public string product_uuid { get; set; }
        public string doc_num { get; set; }
        public Nullable<int> doc_year { get; set; }
        public string doc_change_num { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
