using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class Registry
    {        
        public Registry()
        {
            //
        }
    
        public long registry_id { get; set; }
        public Nullable<System.Guid> registry_uuid { get; set; }
        public string registry_num { get; set; }
        public System.DateTime registry_date { get; set; }
        public Nullable<System.DateTime> renewal_date { get; set; }
        public Nullable<long> mfr_id { get; set; }
        [MappedDbField("mfr_id", "mfr")]
        public string mfr_uuid { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
