using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{    
    public partial class Service
    {        
        public Service()
        {
            //
        }
    
        public int service_id { get; set; }
        public Nullable<System.Guid> service_uuid { get; set; }
        public string service_name { get; set; }
        public string service_descr { get; set; }
        public int doc_type_id { get; set; }
        [MappedDbField("doc_type_id", "doc_type")]
        public string doc_type_uuid { get; set; }
        public string service_logic { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
