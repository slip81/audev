using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{
    public partial class VwGoodInfo
    {
        public VwGoodInfo()
        {
            //auesn.dbmodel.vw_good_info
        }

        public System.Guid good_uuid { get; set; }
        public string barcode { get; set; }
        public string gtin { get; set; }
        public Nullable<bool> is_perfect { get; set; }
        public System.DateTime crt_date { get; set; }
        public string crt_user { get; set; }
        public System.DateTime upd_date { get; set; }
        public string upd_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
        public System.Guid product_uuid { get; set; }
        public string product_name { get; set; }
        public string storage_period { get; set; }
        public string storage_condition { get; set; }
        public Nullable<bool> is_vital { get; set; }
        public Nullable<bool> is_drug2 { get; set; }
        public Nullable<bool> is_drug3 { get; set; }
        public Nullable<bool> is_precursor { get; set; }
        public Nullable<bool> is_alcohol { get; set; }
        public Nullable<bool> is_imm_bio { get; set; }
        public Nullable<bool> is_mandatory { get; set; }
        public Nullable<bool> is_defect_exists { get; set; }
        public string indication { get; set; }
        public string contraindication { get; set; }
        public string drug_interaction { get; set; }
        public string storage_condition_short { get; set; }
        public string indication_short { get; set; }
        public string contraindication_short { get; set; }
        public string applic_and_dose { get; set; }
        public Nullable<System.Guid> mfr_uuid { get; set; }
        public string mfr_name { get; set; }
        public Nullable<System.Guid> country_uuid { get; set; }
        public string country_name { get; set; }
        public Nullable<System.Guid> product_type_uuid { get; set; }
        public string product_type_name { get; set; }
        public Nullable<System.Guid> atc_uuid { get; set; }
        public string atc_name { get; set; }
        public string atc_code { get; set; }
        public Nullable<System.Guid> dosage_uuid { get; set; }
        public string dosage_name { get; set; }
        public Nullable<System.Guid> form_uuid { get; set; }
        public string form_name { get; set; }
        public string form_short_name { get; set; }
        public string form_group_name { get; set; }
        public Nullable<System.Guid> pharm_ther_group_uuid { get; set; }
        public string pharm_ther_group_name { get; set; }
        public Nullable<System.Guid> okpd2_uuid { get; set; }
        public string okpd2_name { get; set; }
        public string okpd2_code { get; set; }
        public Nullable<System.Guid> brand_uuid { get; set; }
        public string brand_name { get; set; }
        public Nullable<System.Guid> recipe_type_uuid { get; set; }
        public string recipe_type_name { get; set; }
        public Nullable<System.Guid> registry_uuid { get; set; }
        public string registry_num { get; set; }
        public Nullable<System.DateTime> registry_date { get; set; }
        public Nullable<System.Guid> baa_type_uuid { get; set; }
        public string baa_type_name { get; set; }
        public Nullable<System.Guid> metaname_uuid { get; set; }
        public string metaname_name { get; set; }
        public Nullable<System.Guid> trade_name_uuid { get; set; }
        public string trade_name_name { get; set; }
        public Nullable<System.Guid> inn_uuid { get; set; }
        public string inn_name { get; set; }
    }
}
