using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace auesn.rest.Model
{   
    public partial class Country
    {        
        public Country()
        {
            //auesn.db.Db.country
        }

        public int country_id { get; set; }
        public Nullable<System.Guid> country_uuid { get; set; }
        public string country_name { get; set; }
        public string short_name { get; set; }
        public string aplpha2 { get; set; }
        public string aplpha3 { get; set; }
        public string country_code { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
