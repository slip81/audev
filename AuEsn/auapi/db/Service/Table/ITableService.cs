﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using auesn.rest.Model;

namespace auesn.rest.Service
{
    public interface ITableService
    {
        IEnumerable<T> GetRows<T>(string table, Dictionary<string,string> filter);
        long GetRowCount<T>(string table, Dictionary<string, string> filter);
        PostResult InsertOrUpdate<T>(string table, Nullable<System.Guid> uuid, T model);
        bool Delete<T>(string table, string view, Dictionary<string, string> filter);        
    }
}