﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Newtonsoft.Json;
using auesn.rest.Model;
using auesn.util;
using auesn.dbmodel;

namespace auesn.rest.Service
{
    public class TableService : BaseService, ITableService
    {
        //private const int ROWS_LIMIT_MAX = 500000;
        private int ROWS_LIMIT_MAX;

        public TableService(IMapper _mapper)
            : base(_mapper)
        {
            if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings["getlimit"], out ROWS_LIMIT_MAX))
                ROWS_LIMIT_MAX = 500000;            
        }

        #region GetRows

        public IEnumerable<T> GetRows<T>(string table, Dictionary<string, string> filter)
        {
            string sql = "select t1.* from main." + table + " t1 where 1=1";

            sql += getSqlForFilter<T>(table, filter);
            using (var dbContext = new AuEsnDb())
            {
                return dbContext.Database.SqlQuery<T>(sql).ToList();
            }
        }

        #endregion

        #region GetRowCount
        public long GetRowCount<T>(string table, Dictionary<string, string> filter)
        {
            string sql_count1 = "select count(x1.*) from (";
            string sql_count2 = ") x1";

            string sql = "select t1.* from main." + table + " t1 where 1=1";

            sql += getSqlForFilter<T>(table, filter, true);

            sql = sql_count1 + sql + sql_count2;

            using (var dbContext = new AuEsnDb())
            {
                return dbContext.Database.SqlQuery<long>(sql).FirstOrDefault();
            }
        }
        #endregion

        #region InsertOrUpdate

        public PostResult InsertOrUpdate<T>(string table, Nullable<System.Guid> uuid, T model)
        {
            PrepareSqlResult prepareSqlResult = getSqlForInsertOrUpdate<T>(table, uuid, model);
            if ((prepareSqlResult == null) || (prepareSqlResult.HasError))
                return ServiceResult.ErrorPostResult(((prepareSqlResult != null) && (!String.IsNullOrWhiteSpace(prepareSqlResult.ErrMess))) ? prepareSqlResult.ErrMess : "Ошибка формирования SQL-запроса");

            bool isUpdate = prepareSqlResult.IsUpdate;
            string sql = prepareSqlResult.Sql;
            List<string> sqlBefore = prepareSqlResult.SqlBefore;
            List<string> sqlAfter = prepareSqlResult.SqlAfter;
            string sqlAfter_ParentIdQuery = prepareSqlResult.SqlAfter_ParentIdQuery;

            string result = "";
            using (var dbContext = new AuEsnDb())
            {
                dbContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
                try
                {
                    if ((sqlBefore != null) && (sqlBefore.Count > 0))
                    {
                        foreach (var sqlBeforeItem in sqlBefore)
                        {
                            dbContext.Database.ExecuteSqlCommand(sqlBeforeItem);
                        }
                    }

                    result = dbContext.Database.SqlQuery<string>(sql).FirstOrDefault();
                    if (String.IsNullOrWhiteSpace(result))
                        return ServiceResult.ErrorPostResult(isUpdate ? (table + ": не найдена запись с кодом " + uuid) : (table + ": добавление не выполнено"));

                    if ((sqlAfter != null) && (sqlAfter.Count > 0))
                    {
                        string paramStringValue = null;
                        if (!String.IsNullOrWhiteSpace(sqlAfter_ParentIdQuery))
                        {
                            paramStringValue = dbContext.Database.SqlQuery<string>(sqlAfter_ParentIdQuery).FirstOrDefault();
                        }

                        foreach (var sqlAfterItem in sqlAfter)
                        {
                            if (!String.IsNullOrWhiteSpace(paramStringValue))
                                dbContext.Database.ExecuteSqlCommand(sqlAfterItem, int.Parse(paramStringValue));
                            else
                                dbContext.Database.ExecuteSqlCommand(sqlAfterItem);
                        }
                    }

                    if (dbContext.Database.CurrentTransaction != null)
                        dbContext.Database.CurrentTransaction.Commit();
                }
                catch (Exception ex)
                {
                    if (dbContext.Database.CurrentTransaction != null)
                        dbContext.Database.CurrentTransaction.Rollback();
                    throw ex;
                }
            }

            return ServiceResult.OkPostResult(result);
        }

        private PrepareSqlResult getSqlForInsertOrUpdate<T>(string table, Nullable<System.Guid> uuid, T model)
        {
            PrepareSqlResult result = new PrepareSqlResult();

            string sql = "";
            string expr = "";
            string propName = "";
            string propValue = "";

            bool isUpdate = false;
            bool first = true;
            bool ok = false;
            List<string> fieldNames_insert = new List<string>();
            List<string> fieldValues_insert = new List<string>();

            string pk_name = table + "_id";
            string bk_name = table + "_uuid";
            List<string> excludedFields_update = new List<string>() { pk_name, bk_name };
            List<string> excludedFields_insert = new List<string>() { pk_name };

            if (uuid == Guid.Empty)
                uuid = null;

            if (!uuid.HasValue)
            {
                isUpdate = false;
            }
            else
            {
                using (var dbContext = new AuEsnDb())
                {
                    var existingItem = dbContext.Database.SqlQuery<int>("select count(" + pk_name + ") from main." + table + " where " + bk_name + " = '" + uuid + "'").FirstOrDefault();
                    if (existingItem > 0)
                        isUpdate = true;
                }
            }

            var propListFull = model.GetType()
                .GetProperties(BindingFlags.Instance |
                                BindingFlags.Public)
                .Where(p => !p.GetIndexParameters().Any())
                .Where(p => p.CanRead && p.CanWrite /*&& !p.IsPropertyACollection()*/)
                .ToList();

            var propWithAttrList = propListFull.Where(ss => ss.CustomAttributes.Where(tt => tt.AttributeType == typeof(MappedDbFieldAttribute)).Any()).ToList();
            if ((propWithAttrList != null) && (propWithAttrList.Count > 0))
            {
                foreach (var propWithAttr in propWithAttrList)
                {
                    var propDbFieldAttr = propWithAttr.CustomAttributes.Where(ss => ss.AttributeType == typeof(MappedDbFieldAttribute)).FirstOrDefault();
                    excludedFields_update.Add((string)propDbFieldAttr.ConstructorArguments[0].Value);
                    excludedFields_insert.Add((string)propDbFieldAttr.ConstructorArguments[0].Value);
                }
            }

            var propList = propListFull
                                .Where(p => ((isUpdate) && (!excludedFields_update.Contains(p.Name)))
                                ||
                                ((!isUpdate) && (!excludedFields_insert.Contains(p.Name)))
                                )
                                .ToList();

            foreach (var prop in propList)
            {
                var value = prop.GetValue(model, null);
                if (value == null)
                    continue;

                if (prop.CustomAttributes.Where(ss => ss.AttributeType == typeof(ChildObjectAttribute)).Any())
                {                    
                    if (typeof(T) == typeof(auesn.rest.Model.Client))
                    {
                        #region
                        if (prop.Name == "client_rel")
                        {                            
                            List<ClientRel> clientRelList = (List<ClientRel>)value;
                            if ((clientRelList != null) && (clientRelList.Count > 0))
                            {
                                foreach (var clientRel in clientRelList)
                                {
                                    var sqlAdditional = getSqlForInsertOrUpdate<auesn.rest.Model.ClientRel>("client_rel", null, clientRel);
                                    if ((sqlAdditional == null) || (sqlAdditional.HasError))
                                        return ServiceResult.ErrorPrepareSqlResult(((sqlAdditional != null) && (!String.IsNullOrWhiteSpace(sqlAdditional.ErrMess))) ? sqlAdditional.ErrMess : "Ошибка формирования SQL-запроса");
                                    if (!String.IsNullOrWhiteSpace(sqlAdditional.Sql))
                                    {
                                        if (result.SqlAfter == null)
                                            result.SqlAfter = new List<string>();
                                        result.SqlAfter.Add(sqlAdditional.Sql);
                                        result.SqlAfter_ParentIdQuery = sqlAdditional.SqlAfter_ParentIdQuery;
                                    }
                                }
                            }

                            if (result.SqlBefore == null)
                                result.SqlBefore = new List<string>();
                            result.SqlBefore.Add("delete from main.client_rel t1 using main.client t2 where t1.client1_id = t2.client_id and t2.client_uuid = '" + uuid + "';");
                        }
                        else if (prop.Name == "client_doc_type")
                        {                            
                            List<ClientDocType> clientDocTypeList = (List<ClientDocType>)value;
                            if ((clientDocTypeList != null) && (clientDocTypeList.Count > 0))
                            {
                                foreach (var clientDocType in clientDocTypeList)
                                {
                                    var sqlAdditional = getSqlForInsertOrUpdate<auesn.rest.Model.ClientDocType>("client_doc_type", null, clientDocType);
                                    if ((sqlAdditional == null) || (sqlAdditional.HasError))
                                        return ServiceResult.ErrorPrepareSqlResult(((sqlAdditional != null) && (!String.IsNullOrWhiteSpace(sqlAdditional.ErrMess))) ? sqlAdditional.ErrMess : "Ошибка формирования SQL-запроса");
                                    if (!String.IsNullOrWhiteSpace(sqlAdditional.Sql))
                                    {
                                        if (result.SqlAfter == null)
                                            result.SqlAfter = new List<string>();
                                        result.SqlAfter.Add(sqlAdditional.Sql);
                                        result.SqlAfter_ParentIdQuery = sqlAdditional.SqlAfter_ParentIdQuery;
                                    }
                                }
                            }

                            if (result.SqlBefore == null)
                                result.SqlBefore = new List<string>();
                            result.SqlBefore.Add("delete from main.client_doc_type t1 using main.client t2 where t1.client_id = t2.client_id and t2.client_uuid = '" + uuid + "';");
                        }
                        #endregion
                    } else if (typeof(T) == typeof(auesn.rest.Model.InnSource))
                    {
                        #region
                        if (prop.Name == "inn_source_ingredient")
                        {                            
                            List<InnSourceIngredient> innSourceIngredientList = (List<InnSourceIngredient>)value;
                            if ((innSourceIngredientList != null) && (innSourceIngredientList.Count > 0))
                            {
                                foreach (var innSourceIngredient in innSourceIngredientList)
                                {
                                    var sqlAdditional = getSqlForInsertOrUpdate<auesn.rest.Model.InnSourceIngredient>("inn_source_ingredient", null, innSourceIngredient);
                                    if ((sqlAdditional == null) || (sqlAdditional.HasError))
                                        return ServiceResult.ErrorPrepareSqlResult(((sqlAdditional != null) && (!String.IsNullOrWhiteSpace(sqlAdditional.ErrMess))) ? sqlAdditional.ErrMess : "Ошибка формирования SQL-запроса");
                                    if (!String.IsNullOrWhiteSpace(sqlAdditional.Sql))
                                    {
                                        if (result.SqlAfter == null)
                                            result.SqlAfter = new List<string>();
                                        result.SqlAfter.Add(sqlAdditional.Sql);
                                        result.SqlAfter_ParentIdQuery = sqlAdditional.SqlAfter_ParentIdQuery;
                                    }
                                }
                            }

                            if (result.SqlBefore == null)
                                result.SqlBefore = new List<string>();
                            result.SqlBefore.Add("delete from main.inn_source_ingredient t1 using main.inn_source t2 where t1.inn_source_id = t2.inn_source_id and t2.inn_source_uuid = '" + uuid + "';");
                        }
                        #endregion
                    }
                    continue;
                }

                propName = prop.Name;
                propValue = prop.IsNeedQuotes() ? ("'" + value.ToString() + "'") : value.ToString();

                var propDbFieldAttr = prop.CustomAttributes.Where(ss => ss.AttributeType == typeof(MappedDbFieldAttribute)).FirstOrDefault();
                if (propDbFieldAttr != null)
                {
                    string propDbFieldAttr_keyField = (string)propDbFieldAttr.ConstructorArguments[0].Value;
                    string propDbFieldAttr_refTable = (string)propDbFieldAttr.ConstructorArguments[1].Value;

                    string refTable_pk = propDbFieldAttr_refTable + "_id";
                    string refTable_bk = propDbFieldAttr_refTable + "_uuid";

                    var refTable_bkProp = propListFull.Where(ss => ss.Name == propName).FirstOrDefault();
                    if (refTable_bkProp == null)
                        return ServiceResult.ErrorPrepareSqlResult("Не найдена связь между таблицами " + table + " и " + propDbFieldAttr_refTable + " по полю " + propName);
                    string refTable_bkValue = refTable_bkProp.GetValue(model, null).ToString();

                    propName = propDbFieldAttr_keyField;

                    using (var dbContext = new AuEsnDb())
                    {
                        string refSql = "select " + refTable_pk + "::character varying from main." + propDbFieldAttr_refTable + " where " + refTable_bk + " = '" + refTable_bkValue + "';";
                        string propDbFieldAttr_keyFieldValue = dbContext.Database.SqlQuery<string>(refSql).FirstOrDefault();
                        if (String.IsNullOrWhiteSpace(propDbFieldAttr_keyFieldValue))
                        {
                            propValue = "@p0";
                            result.SqlAfter_ParentIdQuery = refSql;
                        }
                        else
                        {
                            propValue = propDbFieldAttr_keyFieldValue;
                        }
                    }
                }

                if (isUpdate)
                {
                    if (!first)
                        expr += " ,";
                    expr += " " + propName + " = " + propValue;
                }
                else
                {
                    fieldNames_insert.Add(propName);
                    fieldValues_insert.Add(propValue);
                }

                ok = true;
                first = false;
            }

            if (!ok)
                return ServiceResult.ErrorPrepareSqlResult("Пустой SQL-запрос");

            sql = "";
            if (isUpdate)
            {
                sql = "update main." + table + " set";
                sql += expr;
                sql += " where " + bk_name + " = '" + uuid + "'";
            }
            else
            {
                sql = "insert into main." + table + " (";

                first = true;
                for (int i = 0; i < fieldNames_insert.Count; i = i + 1)
                {
                    if (!first)
                        sql += " ,";
                    sql += fieldNames_insert[i];
                    first = false;
                }

                sql += " ) values (";

                first = true;
                for (int i = 0; i < fieldValues_insert.Count; i = i + 1)
                {
                    if (!first)
                        sql += " ,";
                    sql += fieldValues_insert[i];
                    first = false;
                }

                sql += " )";
            }

            sql += " returning " + bk_name;

            result.Sql = sql;
            result.IsUpdate = isUpdate;
            return result;
        }

        #endregion

        #region Delete

        public bool Delete<T>(string table, string view, Dictionary<string, string> filter)
        {
            string sql = "";
            
            if (String.IsNullOrWhiteSpace(view))
            {
                sql = "delete from main." + table + " t1 where 1=1";
            }
            else
            {
                string pk_name = table + "_id";
                sql = "delete from main." + table + " t0 using main." + view + " t1 where 1=1 and t0." + pk_name + " = t1." + pk_name;
            }

            if ((filter == null) || (filter.Count <= 0))
                throw new NotSupportedException("Не задан параметр filter");

            sql += getSqlForFilter<T>(table, filter, false, true);

            using (var dbContext = new AuEsnDb())
            {
                dbContext.Database.ExecuteSqlCommand(sql);
            }

            return true;
        }

        #endregion

        #region getSqlForFilter

        private string getSqlForFilter<T>(string table, Dictionary<string, string> filter, bool forRowCount = false, bool forDelete = false)
        {
            if ((filter == null) || (filter.Count <= 0))
            //return "";
            {
                if (filter == null)
                    filter = new Dictionary<string, string>();
                if (!forRowCount)
                    filter.Add("limit", ROWS_LIMIT_MAX.ToString());
            }
                

            string sql = "";
            string expr = "";
            string fieldName = "";
            string[] opString = null;
            string[] opString_full = null;
            string opName = "";
            string opValue = "";
            bool first = true;

            expr = "";
            fieldName = "";
            opString = null;
            opString_full = null;
            opName = "";
            opValue = "";
            first = true;
   
            var filterItem_and = filter.Where(ss => ss.Key == "and").FirstOrDefault();
            var filterItem_or = filter.Where(ss => ss.Key == "or").FirstOrDefault();

            if ((filterItem_and.Key == "and") && (!String.IsNullOrWhiteSpace(filterItem_and.Value)))
            {
                #region 
                opString_full = filterItem_and.Value.TrimStart('(').TrimEnd(')').Split(',').ToArray();

                foreach (var opString_full_item in opString_full)
                {
                    opString = opString_full_item.Split('.').ToArray();

                    fieldName = opString[0];
                    opName = opString[1];
                    opValue = typeof(T).IsNeedQuotes(fieldName) ? ("'" + opString[2] + "'") : opString[2];

                    expr = " and (t1." + fieldName + " " + FilterParser.OperationToSql(opName) + " " + opValue + ")";
                    sql += expr;

                    first = false;
                }
                #endregion
            }
            if ((filterItem_or.Key == "or") && (!String.IsNullOrWhiteSpace(filterItem_or.Value)))
            {
                #region
                opString_full = filterItem_or.Value.TrimStart('(').TrimEnd(')').Split(',').ToArray();

                sql += " and (";
                foreach (var opString_full_item in opString_full)
                {
                    opString = opString_full_item.Split('.').ToArray();

                    fieldName = opString[0];
                    opName = opString[1];
                    opValue = typeof(T).IsNeedQuotes(fieldName) ? ("'" + opString[2] + "'") : opString[2];

                    expr = "";
                    if (!first)
                        expr += " or";
                    expr += " (t1." + fieldName + " " + FilterParser.OperationToSql(opName) + " " + opValue + ")";
                    sql += expr;

                    first = false;
                }
                sql += ")";
                #endregion
            }
            else foreach (var filterItem in filter.Where(ss => typeof(T).HasProperty(ss.Key)))
                {
                    #region
                    fieldName = filterItem.Key;
                    opString = filterItem.Value.Split('.').ToArray();
                    opName = opString[0];
                    opValue = typeof(T).IsNeedQuotes(fieldName) ? ("'" + opString[1] + "'") : opString[1];

                    expr = " and (t1." + fieldName + " " + FilterParser.OperationToSql(opName) + " " + opValue + ")";
                    sql += expr;

                    first = false;
                    #endregion
                }

            if (!forDelete)
            {
                #region is_deleted
                bool isDeletedSpecified = filter.Where(ss => ss.Key == "is_deleted").Any();
                if (!isDeletedSpecified)
                    sql += " and (t1.is_deleted = false)";
                #endregion

                #region order, orderdesc
                var filterItem_order = filter.Where(ss => ((ss.Key == "order") || (ss.Key == "orderdesc"))).FirstOrDefault();
                if ((filterItem_order.Key == "order") && (!String.IsNullOrWhiteSpace(filterItem_order.Value)))
                {
                    sql += " order by " + filterItem_order.Value;
                }
                else if ((filterItem_order.Key == "orderdesc") && (!String.IsNullOrWhiteSpace(filterItem_order.Value)))
                {
                    sql += " order by " + filterItem_order.Value + " desc";
                }
                #endregion

                #region limit
                bool limitSpecified = false;
                int limit = ROWS_LIMIT_MAX;
                var filterItem_limit = filter.Where(ss => ss.Key == "limit").FirstOrDefault();
                if ((filterItem_limit.Key == "limit") && (!String.IsNullOrWhiteSpace(filterItem_limit.Value)))
                {
                    limitSpecified = true;
                    if (!(int.TryParse(filterItem_limit.Value, out limit)))
                    {
                        limit = ROWS_LIMIT_MAX;
                        limitSpecified = false;
                    }
                }
                if (limit > ROWS_LIMIT_MAX)
                    limit = ROWS_LIMIT_MAX;

                sql += " limit " + (((!limitSpecified) && (forRowCount)) ? "all" : limit.ToString());
                #endregion

                #region offset
                var filterItem_offset = filter.Where(ss => ss.Key == "offset").FirstOrDefault();
                if (!String.IsNullOrWhiteSpace(filterItem_offset.Value))
                {
                    sql += " offset " + filterItem_offset.Value;
                }
                #endregion
            }
            return sql;
        }

        #endregion

    }
}