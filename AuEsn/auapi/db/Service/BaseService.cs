﻿using AutoMapper;
using System;
using auesn.dbmodel;

namespace auesn.rest.Service
{
    public class BaseService : IDisposable
    {
        /*
        protected AuEsnDb dbContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = new AuEsnDb();
                }
                return _dbContext;
            }
        }
        private AuEsnDb _dbContext;
        */

        protected IMapper mapper;

        public BaseService()
        {
            //
        }

        public BaseService(IMapper _mapper)
        {            
            mapper = _mapper;
        }

        public void Dispose()
        {
            /*
            if (_dbContext != null)
            {                
                _dbContext.Dispose();
            }
            */
        }

    }
}