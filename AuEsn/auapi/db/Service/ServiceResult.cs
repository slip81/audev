﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace auesn.rest.Service
{
    public static class ServiceResult
    {
        public static PostResult OkPostResult(string uuid = null)
        {
            return new PostResult() { Uuid = uuid, ErrMess = null, HasError = false, };
        }

        public static PostResult ErrorPostResult(string errMess = null)
        {
            return new PostResult() { Uuid = null, ErrMess = errMess, HasError = true, };
        }

        public static PrepareSqlResult OkPrepareSqlResult(string sql)
        {
            return new PrepareSqlResult() { Sql = sql, ErrMess = null, HasError = false, };
        }

        public static PrepareSqlResult ErrorPrepareSqlResult(string errMess = null)
        {
            return new PrepareSqlResult() { Sql = null, ErrMess = errMess, HasError = true, };
        }
    }
}