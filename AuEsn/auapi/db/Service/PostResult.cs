﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace auesn.rest.Service
{
    public class PostResult
    {

        public PostResult()
        {
            Uuid = null;
            HasError = false;
            ErrMess = null;
        }

        public string Uuid { get; set; }
        public bool HasError { get; set; }
        public string ErrMess { get; set; }

    }

}