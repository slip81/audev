﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace auesn.rest.Service
{
    public class PrepareSqlResult
    {

        public PrepareSqlResult()
        {
            Sql = null;
            IsUpdate = false;
            SqlBefore = null;
            SqlAfter = null;
            SqlAfter_ParentIdQuery = null;
            HasError = false;
            ErrMess = null;
        }
                
        public string Sql { get; set; }
        public bool IsUpdate { get; set; }
        public List<string> SqlBefore { get; set; }
        public List<string> SqlAfter { get; set; }
        public string SqlAfter_ParentIdQuery { get; set; }
        public bool HasError { get; set; }
        public string ErrMess { get; set; }

    }
}