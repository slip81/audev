﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace auesn.util
{
    public class AuEsnUser
    {        
        public AuEsnUser(ClaimsIdentity identity)
        {            
            if (identity == null)
                throw new NullReferenceException("Не задан параметр identity");

            CabClientId = identity.Claims == null ? 0 : identity.Claims.Where(ss => ss.Type == "cci").Select(ss => int.Parse(ss.Value)).FirstOrDefault();
            CabSalesId = identity.Claims == null ? 0 : identity.Claims.Where(ss => ss.Type == "csi").Select(ss => int.Parse(ss.Value)).FirstOrDefault();
        }        

        public int CabClientId { get; private set; }
        public int CabSalesId { get; private set; }

    }
}
