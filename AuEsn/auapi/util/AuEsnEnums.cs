﻿using System.ComponentModel.DataAnnotations;

namespace auesn.util
{
    public static class Enums
    {
        public enum DocStateEnum
        {
            [Display(Name = "В очереди отправителя")]
            SENDER_QUEUE = 1,
            [Display(Name = "Обрабатывается веб-сервисом")]
            WEB_PROCESS = 2,
            [Display(Name = "В очереди сервиса ЕСН")]
            ESN_QUEUE = 3,
            [Display(Name = "Обрабатывается сервисом ЕСН")]
            ESN_PROCESS = 4,
            [Display(Name = "В очереди получателя")]
            TARGET_QUEUE = 5,
            [Display(Name = "Досьавлен получателю")]
            TARGET_COMPLETE = 6,
        }

        public enum GetGoodAnalogParamItemTypeEnum
        {
            [Display(Name = "EAN13")]
            EAN13 = 1,
            [Display(Name = "Торговое наименование")]
            TN = 2,
            [Display(Name = "МНН")]
            MNN = 3,
        }

    }
}
