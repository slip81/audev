﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

// http://xmltocsharp.azurewebsites.net/

namespace auesn.util
{
    [XmlRoot(ElementName = "ЗаголовокДокумента")]
    public class Doc2FullDocHeader
    {
        [XmlElement(ElementName = "ТипДок")]
        public string ТипДок { get; set; }
        [XmlElement(ElementName = "НомерДок")]
        public string НомерДок { get; set; }
        [XmlElement(ElementName = "ДатаДок")]
        public string ДатаДок { get; set; }
        [XmlElement(ElementName = "Подразделение")]
        public string Подразделение { get; set; }
        [XmlElement(ElementName = "ОписаниеДокумента")]
        public string ОписаниеДокумента { get; set; }
    }

    [XmlRoot(ElementName = "Товар")]
    public class Doc2FullTov
    {
        [XmlAttribute(AttributeName = "Код")]
        public string Код { get; set; }
        [XmlAttribute(AttributeName = "КодСвязи")]
        public string КодСвязи { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Изготовитель")]
    public class Doc2FullTovMfr
    {
        [XmlAttribute(AttributeName = "Код")]
        public string Код { get; set; }
        [XmlAttribute(AttributeName = "КодСвязи")]
        public string КодСвязи { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Количество")]
    public class Doc2FullTovCnt
    {
        [XmlAttribute(AttributeName = "Распак")]
        public string Распак { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Серия")]
    public class Doc2FullSerie
    {
        [XmlElement(ElementName = "СерияТовара")]
        public string СерияТовара { get; set; }
        [XmlElement(ElementName = "НомерСертиф")]
        public string НомерСертиф { get; set; }
        [XmlElement(ElementName = "ОрганСертиф")]
        public string ОрганСертиф { get; set; }
        [XmlElement(ElementName = "ДатаВыдачиСертиф")]
        public string ДатаВыдачиСертиф { get; set; }
        [XmlElement(ElementName = "СрокДействияСертиф")]
        public string СрокДействияСертиф { get; set; }
        [XmlElement(ElementName = "СрокГодностиТовара")]
        public string СрокГодностиТовара { get; set; }
        [XmlElement(ElementName = "РегНомер")]
        public string РегНомер { get; set; }
        [XmlElement(ElementName = "РегДатаСертиф")]
        public string РегДатаСертиф { get; set; }
        [XmlElement(ElementName = "РегОрганСертиф")]
        public string РегОрганСертиф { get; set; }
    }

    [XmlRoot(ElementName = "Серии")]
    public class Doc2FullSeries
    {
        [XmlElement(ElementName = "Серия")]
        public Doc2FullSerie Серия { get; set; }
    }

    [XmlRoot(ElementName = "Фармгруппа")]
    public class Doc2FullFarmGroup
    {
        [XmlElement(ElementName = "Идентификатор")]
        public string Идентификатор { get; set; }
        [XmlElement(ElementName = "КодГруппы")]
        public string КодГруппы { get; set; }
        [XmlElement(ElementName = "Наименование")]
        public string Наименование { get; set; }
        [XmlElement(ElementName = "Примечание")]
        public string Примечание { get; set; }
    }

    [XmlRoot(ElementName = "ТоварнаяПозиция")]
    public class Doc2FullTovPosition
    {
        [XmlElement(ElementName = "Товар")]
        public Doc2FullTov Товар { get; set; }
        [XmlElement(ElementName = "Изготовитель")]
        public Doc2FullTovMfr Изготовитель { get; set; }
        [XmlElement(ElementName = "СтранаИзготовителя")]
        public string СтранаИзготовителя { get; set; }
        [XmlElement(ElementName = "Количество")]
        public Doc2FullTovCnt Количество { get; set; }
        [XmlElement(ElementName = "Артикул")]
        public string Артикул { get; set; }
        [XmlElement(ElementName = "ПервичныйАртикул")]
        public string ПервичныйАртикул { get; set; }
        [XmlElement(ElementName = "БазовыйАртикул")]
        public string БазовыйАртикул { get; set; }
        [XmlElement(ElementName = "ЦенаИзг")]
        public string ЦенаИзг { get; set; }
        [XmlElement(ElementName = "ЦенаГР")]
        public string ЦенаГР { get; set; }
        [XmlElement(ElementName = "НаценОпт")]
        public string НаценОпт { get; set; }
        [XmlElement(ElementName = "ЦенаОпт")]
        public string ЦенаОпт { get; set; }
        [XmlElement(ElementName = "СуммаОпт")]
        public string СуммаОпт { get; set; }
        [XmlElement(ElementName = "СтавкаНДС")]
        public string СтавкаНДС { get; set; }
        [XmlElement(ElementName = "ЦенаОптВклНДС")]
        public string ЦенаОптВклНДС { get; set; }
        [XmlElement(ElementName = "СуммаОптВклНДС")]
        public string СуммаОптВклНДС { get; set; }
        [XmlElement(ElementName = "СуммаНДС")]
        public string СуммаНДС { get; set; }
        [XmlElement(ElementName = "НаценРозн")]
        public string НаценРозн { get; set; }
        [XmlElement(ElementName = "ЦенаРознБезНалогов")]
        public string ЦенаРознБезНалогов { get; set; }
        [XmlElement(ElementName = "СуммаРознБезНалогов")]
        public string СуммаРознБезНалогов { get; set; }
        [XmlElement(ElementName = "ЖНВЛП")]
        public string ЖНВЛП { get; set; }
        [XmlElement(ElementName = "ЦенаРозн")]
        public string ЦенаРозн { get; set; }
        [XmlElement(ElementName = "СуммаРозн")]
        public string СуммаРозн { get; set; }
        [XmlElement(ElementName = "НаценПрайс1")]
        public string НаценПрайс1 { get; set; }
        [XmlElement(ElementName = "ЦенаПрайс1")]
        public string ЦенаПрайс1 { get; set; }
        [XmlElement(ElementName = "СуммаПрайс1")]
        public string СуммаПрайс1 { get; set; }
        [XmlElement(ElementName = "НаценПрайс2")]
        public string НаценПрайс2 { get; set; }
        [XmlElement(ElementName = "ЦенаПрайс2")]
        public string ЦенаПрайс2 { get; set; }
        [XmlElement(ElementName = "СуммаПрайс2")]
        public string СуммаПрайс2 { get; set; }
        [XmlElement(ElementName = "НаценПрайс3")]
        public string НаценПрайс3 { get; set; }
        [XmlElement(ElementName = "ЦенаПрайс3")]
        public string ЦенаПрайс3 { get; set; }
        [XmlElement(ElementName = "СуммаПрайс3")]
        public string СуммаПрайс3 { get; set; }
        [XmlElement(ElementName = "ГТД")]
        public string ГТД { get; set; }
        [XmlElement(ElementName = "Поставщик")]
        public string Поставщик { get; set; }
        [XmlElement(ElementName = "НомерДокПост")]
        public string НомерДокПост { get; set; }
        [XmlElement(ElementName = "ДатаДокПост")]
        public string ДатаДокПост { get; set; }
        [XmlElement(ElementName = "НомерРеестра")]
        public string НомерРеестра { get; set; }
        [XmlElement(ElementName = "Сильнодействующий")]
        public string Сильнодействующий { get; set; }
        [XmlElement(ElementName = "Рецептурный")]
        public string Рецептурный { get; set; }
        [XmlElement(ElementName = "ДатаРеестра")]
        public string ДатаРеестра { get; set; }
        [XmlElement(ElementName = "ЕАН13")]
        public string ЕАН13 { get; set; }
        [XmlElement(ElementName = "Штрих-код")]
        public string Штрихкод { get; set; }
        [XmlElement(ElementName = "КодСвязи")]
        public string КодСвязи { get; set; }
        [XmlElement(ElementName = "ВидВалюты")]
        public string ВидВалюты { get; set; }
        [XmlElement(ElementName = "ЦенаВВалюте")]
        public string ЦенаВВалюте { get; set; }
        [XmlElement(ElementName = "ПредДопРознЦена")]
        public string ПредДопРознЦена { get; set; }
        [XmlElement(ElementName = "Код1")]
        public string Код1 { get; set; }
        [XmlElement(ElementName = "Код2")]
        public string Код2 { get; set; }
        [XmlElement(ElementName = "Код3")]
        public string Код3 { get; set; }
        [XmlElement(ElementName = "ЕдИзм")]
        public string ЕдИзм { get; set; }
        [XmlElement(ElementName = "Серии")]
        public Doc2FullSeries Серии { get; set; }
        [XmlElement(ElementName = "Фармгруппа")]
        public Doc2FullFarmGroup Фармгруппа { get; set; }
        [XmlElement(ElementName = "Синонимы")]
        public string Синонимы { get; set; }
        [XmlAttribute(AttributeName = "Номер")]
        public string Номер { get; set; }

        // !!!
        [XmlElement(ElementName = "КодТовараЕСН", IsNullable = true)]
        public string КодТовараЕСН { get; set; }
    }

    [XmlRoot(ElementName = "ТоварныеПозиции")]
    public class Doc2FullTovPositions
    {
        [XmlElement(ElementName = "ТоварнаяПозиция")]
        public List<Doc2FullTovPosition> ТоварнаяПозиция { get; set; }
        [XmlAttribute(AttributeName = "ВсегоПозиций")]
        public string ВсегоПозиций { get; set; }
    }

    [XmlRoot(ElementName = "Документ")]
    public class Doc2Full : DocTypeBase
    {
        public Doc2Full()
        {
            //
        }

        [XmlElement(ElementName = "ЗаголовокДокумента")]
        public Doc2FullDocHeader ЗаголовокДокумента { get; set; }
        [XmlElement(ElementName = "ТоварныеПозиции")]
        public Doc2FullTovPositions ТоварныеПозиции { get; set; }
        [XmlAttribute(AttributeName = "Идентификатор")]
        public string Идентификатор { get; set; }
        [XmlAttribute(AttributeName = "ТипДок")]
        public string ТипДок { get; set; }
    }
}