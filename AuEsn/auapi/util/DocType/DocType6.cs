﻿using LINQtoCSV;
using System;

namespace auesn.util
{
    public class DocType6 : DocTypeBase
    {
        public DocType6()
        {
            //
        }

        [CsvColumn(Name = "Код СЗИ", FieldIndex = 1)]
        public string КодСЗИ { get; set; }

        [CsvColumn(Name = "Код СЗ", FieldIndex = 2)]
        public string КодСЗ { get; set; }

        [CsvColumn(Name = "Наименование", FieldIndex = 3)]
        public string Наименование { get; set; }

        [CsvColumn(Name = "Код Изг", FieldIndex = 4)]
        public string КодИзг { get; set; }

        [CsvColumn(Name = "Изготовитель", FieldIndex = 5)]
        public string Изготовитель { get; set; }

        [CsvColumn(Name = "Группа товаров", FieldIndex = 6)]
        public string ГруппаТоваров { get; set; }

        [CsvColumn(Name = "ЖНВЛП", FieldIndex = 7)]
        public string ЖНВЛП { get; set; }

        [CsvColumn(Name = "ПКУ", FieldIndex = 8)]
        public string ПКУ { get; set; }

        [CsvColumn(Name = "3 мес.", FieldIndex = 9)]
        public string ТриМес { get; set; }

        [CsvColumn(Name = "Код МНН", FieldIndex = 10)]
        public string КодМНН { get; set; }

        [CsvColumn(Name = "МНН", FieldIndex = 11)]
        public string МНН { get; set; }

        [CsvColumn(Name = "Создан", FieldIndex = 12)]
        public string Создан { get; set; }

        [CsvColumn(Name = "Штрихкод", FieldIndex = 13)]
        public string Штрихкод { get; set; }

        
        public string КодТовараЕСН { get; set; }

        public string КодМННЕСН { get; set; }

    }

    public class DocType6Inn
    {
        public DocType6Inn()
        {

        }
            
        public string inn_name { get; set; }
        public string inn_id { get; set; }
    }
}