﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

// http://xmltocsharp.azurewebsites.net/

namespace auesn.util
{
    [XmlRoot(ElementName = "ЗаголовокДокумента")]
    public class Doc2DocHeader
    {
        [XmlElement(ElementName = "ТипДок")]
        public string ТипДок { get; set; }
        [XmlElement(ElementName = "НомерДок")]
        public string НомерДок { get; set; }
        [XmlElement(ElementName = "ДатаДок")]
        public string ДатаДок { get; set; }
        [XmlElement(ElementName = "Подразделение")]
        public string Подразделение { get; set; }
        [XmlElement(ElementName = "ОписаниеДокумента")]
        public string ОписаниеДокумента { get; set; }
    }

    [XmlRoot(ElementName = "Товар")]
    public class Doc2Tov
    {
        [XmlAttribute(AttributeName = "Код")]
        public string Код { get; set; }
        [XmlAttribute(AttributeName = "КодСвязи")]
        public string КодСвязи { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Изготовитель")]
    public class Doc2TovMfr
    {
        [XmlAttribute(AttributeName = "Код")]
        public string Код { get; set; }
        [XmlAttribute(AttributeName = "КодСвязи")]
        public string КодСвязи { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Количество")]
    public class Doc2TovCnt
    {
        [XmlAttribute(AttributeName = "Распак")]
        public string Распак { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ТоварнаяПозиция")]
    public class Doc2TovPosition
    {
        [XmlElement(ElementName = "Товар")]
        public Doc2Tov Товар { get; set; }
        [XmlElement(ElementName = "Изготовитель")]
        public Doc2TovMfr Изготовитель { get; set; }
        [XmlElement(ElementName = "СтранаИзготовителя")]
        public string СтранаИзготовителя { get; set; }
        [XmlElement(ElementName = "Количество")]
        public Doc2TovCnt Количество { get; set; }
        [XmlElement(ElementName = "Артикул")]
        public string Артикул { get; set; }
        [XmlElement(ElementName = "Поставщик")]
        public string Поставщик { get; set; }
        [XmlElement(ElementName = "ЕАН13")]
        public string ЕАН13 { get; set; }
        [XmlElement(ElementName = "Штрих-код")]
        public string Штрихкод { get; set; }

        // !!!
        [XmlElement(ElementName = "КодТовараЕСН", IsNullable = true)]
        public string КодТовараЕСН { get; set; }
    }

    [XmlRoot(ElementName = "ТоварныеПозиции")]
    public class Doc2TovPositions
    {
        [XmlElement(ElementName = "ТоварнаяПозиция")]
        public List<Doc2TovPosition> ТоварнаяПозиция { get; set; }
        [XmlAttribute(AttributeName = "ВсегоПозиций")]
        public string ВсегоПозиций { get; set; }
    }

    [XmlRoot(ElementName = "Документ")]
    public class Doc2 : DocTypeBase
    {
        public Doc2()
        {
            //
        }

        [XmlElement(ElementName = "ЗаголовокДокумента")]
        public Doc2DocHeader ЗаголовокДокумента { get; set; }
        [XmlElement(ElementName = "ТоварныеПозиции")]
        public Doc2TovPositions ТоварныеПозиции { get; set; }
        [XmlAttribute(AttributeName = "Идентификатор")]
        public string Идентификатор { get; set; }
        [XmlAttribute(AttributeName = "ТипДок")]
        public string ТипДок { get; set; }
    }
}
