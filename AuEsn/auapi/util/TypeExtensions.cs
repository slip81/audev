﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;

namespace auesn.util
{
    public static class TypeExtensions
    {
        public static bool HasProperty(this Type obj, string propertyName)
        {
            return obj.GetProperty(propertyName) != null;
        }

        public static bool IsNeedQuotes(this Type obj, string propertyName)
        {
            var propType = obj.GetProperty(propertyName).PropertyType;
            return (
                (propType == typeof(string))
                || (propType == typeof(DateTime))
                || (propType == typeof(Nullable<DateTime>))
                || (propType == typeof(System.Guid))
                || (propType == typeof(Nullable<System.Guid>))
                )
                ;
        }

        public static bool IsNeedQuotes(this PropertyInfo obj)
        {            
            return (
                (obj.PropertyType == typeof(string)) 
                || (obj.PropertyType == typeof(DateTime))
                || (obj.PropertyType == typeof(Nullable<DateTime>))
                || (obj.PropertyType == typeof(System.Guid))
                || (obj.PropertyType == typeof(Nullable<System.Guid>))
                )
                ;
        }

        public static bool IsPropertyEnumerable(this PropertyInfo property)
        {
            //return property.PropertyType.GetInterface(typeof(IEnumerable<>).FullName) != null;
            return typeof(IEnumerable).IsAssignableFrom(property.PropertyType);
            //return property.GetType().GetInterfaces().Any(t => t.IsGenericType && t.GetGenericTypeDefinition() == typeof(IEnumerable<>));
        }
    }
}
