﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace auesn.util
{
    public static class AuEsnConst
    {
        // content-type
        public const string MEDIA_TYPE_TEXT = "text/html";
        public const string MEDIA_TYPE_BINARY = "application/octet-stream";
        public const string MEDIA_TYPE_CSV = "text/csv";
        public const string MEDIA_TYPE_JSON = "application/json";
        public const string MEDIA_TYPE_XML = "text/xml";
        public const string MEDIA_TYPE_EXCEL = "application/vnd.ms-excel";

        // encoding
        public const string CHARSET_UTF8 = "utf-8";
        public const string CHARSET_WINDOWS1251 = "windows-1251";

        // тип рабочего места для установки консоли ЕСН
        public const int WORKPLACE_TYPE_ESN = 1;

        // тип клиента "Консоль ЕСН"
        public const int CLIENT_TYPE_ESN = 1;
    }
}
