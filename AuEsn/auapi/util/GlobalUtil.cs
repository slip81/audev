﻿using LINQtoCSV;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace auesn.util
{
    public static class GlobalUtil
    {
        public static string ExceptionInfo(Exception e)
        {
            if (e == null)
                return "";

            string res = e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                );

            /*
            if (e is DbEntityValidationException)
            {
                DbEntityValidationException eEnt = (DbEntityValidationException)e;
                if (eEnt.EntityValidationErrors != null)
                {
                    res += " [";
                    foreach (var validationErrors in eEnt.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            res += validationError.PropertyName + ": " + validationError.ErrorMessage + ";";
                        }
                    }
                    res += "]";
                }
            }
            */

            return res;
        }

        public static string FormatAsJSON(string text)
        {
            return JValue.Parse(text).ToString(Newtonsoft.Json.Formatting.Indented);
        }

        public static string FormatAsXML(string text)
        {
            string Result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(text);

                writer.Formatting = System.Xml.Formatting.Indented;

                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();

                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;

                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);

                // Extract the text from the StreamReader.
                string FormattedXML = sReader.ReadToEnd();

                Result = FormattedXML;
            }
            catch (XmlException)
            {
            }

            mStream.Close();
            writer.Close();

            return Result;
        }

        public static T GetDocContent<T>(string docFilePath, string docContentType)
            where T : DocTypeBase, new()
        {
            T result = null;
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            string docFileBody = null;
            switch (docContentType)
            {
                case AuEsnConst.MEDIA_TYPE_XML:
                    StreamReader reader = new StreamReader(docFilePath, Encoding.GetEncoding("windows-1251"));
                    result = (T)xmlSerializer.Deserialize(reader);
                    reader.Close();
                    break;
                case AuEsnConst.MEDIA_TYPE_JSON:
                    docFileBody = File.ReadAllText(docFilePath, Encoding.GetEncoding("windows-1251"));
                    result = JsonConvert.DeserializeObject<T>(docFileBody);
                    break;
                case AuEsnConst.MEDIA_TYPE_CSV:
                    docFileBody = File.ReadAllText(docFilePath, Encoding.GetEncoding("windows-1251"));
                    CsvFileDescription inputFileDescription = new CsvFileDescription
                    {
                        SeparatorChar = ';',
                        FirstLineHasColumnNames = true,
                        TextEncoding = Encoding.GetEncoding("windows-1251"),
                    };
                    CsvContext cc = new CsvContext();
                    var res = cc.Read<T>("products.csv", inputFileDescription);
                    break;
                default:
                    return null;
            }
            return result;
        }

        public static IEnumerable<T> GetDocContent_AsEnumerable<T>(string docFilePath, string docContentType)
            where T : DocTypeBase, new()
        {
            IEnumerable<T> result = null;
            switch (docContentType)
            {
                case AuEsnConst.MEDIA_TYPE_CSV:                    
                    CsvFileDescription inputFileDescription = new CsvFileDescription
                    {
                        SeparatorChar = ';',
                        FirstLineHasColumnNames = true,
                        TextEncoding = Encoding.GetEncoding("windows-1251"),
                    };
                    CsvContext cc = new CsvContext();
                    result = cc.Read<T>(docFilePath, inputFileDescription);
                    break;
                default:
                    return null;
            }
            return result;
        }

        //public static Tuple<bool, string> GetDocContentFormatted<T>(DocTypeBase sourceDoc, string docContentType, string acceptContentType)
        //where T : DocTypeBase
        public static Tuple<bool, string> GetContentFormatted<T>(T contentSource, string contentType, string acceptContentType)
        {
            if (contentSource == null)
                return Tuple.Create<bool, string>(true, "Нет содержимого");

            string body = null;
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            switch (contentType)
            {
                case AuEsnConst.MEDIA_TYPE_XML:
                case AuEsnConst.MEDIA_TYPE_JSON:                
                    switch (acceptContentType)
                    {
                        case AuEsnConst.MEDIA_TYPE_XML:
                            // в XML                            
                            using (var sww = new StringWriter())
                            {
                                using (XmlWriter writer = XmlWriter.Create(sww))
                                {
                                    xmlSerializer.Serialize(writer, contentSource, ns);
                                    body = sww.ToString();
                                }
                            }
                            break;
                        case AuEsnConst.MEDIA_TYPE_JSON:
                            // в JSON
                            body = JsonConvert.SerializeObject(contentSource);
                            break;
                        case AuEsnConst.MEDIA_TYPE_TEXT:
                            // в text (т.е. оставляем в исходном варианте)
                            switch (contentType)
                            {
                                case AuEsnConst.MEDIA_TYPE_XML:
                                    using (var sww = new StringWriter())
                                    {
                                        using (XmlWriter writer = XmlWriter.Create(sww))
                                        {
                                            xmlSerializer.Serialize(writer, contentSource, ns);
                                            body = sww.ToString();
                                        }
                                    }
                                    break;
                                case AuEsnConst.MEDIA_TYPE_JSON:
                                    body = JsonConvert.SerializeObject(contentSource);
                                    break;
                            }
                            break;
                        default:
                            return Tuple.Create<bool, string>(true, "Неподдерживаемый тип заголовка Accept: " + acceptContentType);
                    }
                    break;
                default:
                    return Tuple.Create<bool, string>(true, "Неподдерживаемый тип содержимого: " + contentType);
            }

            return Tuple.Create<bool, string>(false, body);
        }

        public static Tuple<bool, string> GetContentFormatted_FromEnumerable<T>(IEnumerable<T> contentSource, string contentType, string acceptContentType)
        {
            if (contentSource == null)
                return Tuple.Create<bool, string>(true, "Нет содержимого");

            string body = null;
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            //
            CsvFileDescription outputFileDescription = null;
            CsvContext cc = null;
            TextWriter stringWriter = null;
            switch (contentType)
            {
                case AuEsnConst.MEDIA_TYPE_CSV:
                    switch (acceptContentType)
                    {
                        case AuEsnConst.MEDIA_TYPE_XML:
                            // в XML                            
                            using (var sww = new StringWriter())
                            {
                                using (XmlWriter writer = XmlWriter.Create(sww))
                                {
                                    xmlSerializer.Serialize(writer, contentSource, ns);
                                    body = sww.ToString();
                                }
                            }
                            break;
                        case AuEsnConst.MEDIA_TYPE_JSON:
                            // в JSON
                            body = JsonConvert.SerializeObject(contentSource);
                            break;
                        case AuEsnConst.MEDIA_TYPE_CSV:
                            outputFileDescription = new CsvFileDescription
                            {
                                SeparatorChar = ';',
                                FirstLineHasColumnNames = true,
                                TextEncoding = Encoding.GetEncoding("windows-1251"),
                            };
                            cc = new CsvContext();
                            stringWriter = new StringWriter();
                            cc.Write<T>(contentSource, stringWriter, outputFileDescription);
                            body = stringWriter.ToString();
                            break;
                        case AuEsnConst.MEDIA_TYPE_TEXT:
                            // в text (т.е. оставляем в исходном варианте)
                            switch (contentType)
                            {
                                case AuEsnConst.MEDIA_TYPE_XML:
                                    using (var sww = new StringWriter())
                                    {
                                        using (XmlWriter writer = XmlWriter.Create(sww))
                                        {
                                            xmlSerializer.Serialize(writer, contentSource, ns);
                                            body = sww.ToString();
                                        }
                                    }
                                    break;
                                case AuEsnConst.MEDIA_TYPE_JSON:
                                    body = JsonConvert.SerializeObject(contentSource);
                                    break;
                                case AuEsnConst.MEDIA_TYPE_CSV:
                                    outputFileDescription = new CsvFileDescription
                                    {
                                        SeparatorChar = ';',
                                        FirstLineHasColumnNames = true,
                                        TextEncoding = Encoding.GetEncoding("windows-1251"),
                                    };
                                    cc = new CsvContext();
                                    stringWriter = new StringWriter();
                                    cc.Write<T>(contentSource, stringWriter, outputFileDescription);
                                    body = stringWriter.ToString();
                                    break;
                            }
                            break;
                        default:
                            return Tuple.Create<bool, string>(true, "Неподдерживаемый тип заголовка Accept: " + acceptContentType);
                    }
                    break;
                default:
                    return Tuple.Create<bool, string>(true, "Неподдерживаемый тип содержимого: " + contentType);
            }

            return Tuple.Create<bool, string>(false, body);
        }

        public static Tuple<bool, string> GetContentFormatted<T>(T contentSource, string acceptContentType)
        {
            return GetContentFormatted<T>(contentSource, AuEsnConst.MEDIA_TYPE_JSON, acceptContentType);
        }
    }
}
