﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using auesn.util;

namespace resttest
{
    public partial class Form1 : Form
    {
        HttpClient httpClient;

        public Form1()
        {
            InitializeComponent();
            //
            httpClient = new HttpClient() { BaseAddress = new Uri(textBox2.Text), };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // !!!
            Cursor = Cursors.WaitCursor;
            button2.Enabled = false;
            button3.Enabled = false;
            try
            {
                textBox1.Text = "";
                int i = 1;
                int cnt = int.Parse(textBox4.Text);
                while (i <= cnt)
                {
                    var res = httpClient.GetAsync(textBox3.Text).Result;
                    textBox1.Text += "i=" + i.ToString();
                    textBox1.Text += System.Environment.NewLine;
                    textBox1.Text += res.Content.ReadAsStringAsync().Result;
                    textBox1.Text += System.Environment.NewLine;
                    i = i + 1;
                    
                    /*
                    using (HttpClient httpClient = new HttpClient() { BaseAddress = new Uri(textBox2.Text), })
                    {
                    
                    }
                    */
                }
                Cursor = Cursors.Default;
                button2.Enabled = true;
                button3.Enabled = true;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                textBox1.Text += GlobalUtil.ExceptionInfo(ex);
                button2.Enabled = true;
                button3.Enabled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            button2.Enabled = false;
            button3.Enabled = false;
            try
            {
                //JavaScriptSerializer serializer = new JavaScriptSerializer();
                //var jsonString = serializer.Serialize(textBox1.Text);
                var jsonString = textBox5.Text;                
                textBox1.Text = "";
                int i = 1;
                int cnt = int.Parse(textBox4.Text);                
                while (i <= cnt)
                {
                    var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
                    var res = httpClient.PostAsync("", content).Result;                    
                    textBox1.Text += "i=" + i.ToString();
                    textBox1.Text += System.Environment.NewLine;
                    textBox1.Text += res.StatusCode.ToString();
                    textBox1.Text += System.Environment.NewLine;
                    i = i + 1;
                }
                Cursor = Cursors.Default;
                button2.Enabled = true;
                button3.Enabled = true;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                textBox1.Text += GlobalUtil.ExceptionInfo(ex);
                button2.Enabled = true;
                button3.Enabled = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /*
            Test2 test2 = new Test2("aaa");
            var s = test2.Name;
            */
            /*
            var singleton = Singleton.getInstance();
            var crtDate = singleton.crtDate;
            */
            var singleton = Singleton2.Instance;
            var crtDate = singleton.crtDate;
        }
    }
}
