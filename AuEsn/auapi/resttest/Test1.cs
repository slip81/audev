﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace resttest
{
    class Test1
    {
        public Test1()
        {

        }

        public Test1(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }
    }

    class Test2 : Test1
    {
        public Test2(string name):base()
        {
            this.Name = name + " [child]";
        }
    }

    class Singleton
    {
        private static Singleton instance;

        public DateTime crtDate { get; internal set; }
        private static object syncRoot = new Object();


        private Singleton()
        {
            crtDate = DateTime.Now;
        }
        
        public static Singleton getInstance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    instance = new Singleton();
                }
            }
            return instance;
        }
    }

    public sealed class Singleton2
    {
        public DateTime crtDate { get; internal set; }

        private static readonly Singleton2 instance = new Singleton2();

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static Singleton2()
        { }

        private Singleton2()
        {
            crtDate = DateTime.Now;
        }

        public static Singleton2 Instance { get { return instance; } }
    }
}
