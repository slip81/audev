﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace apicaller
{
    class Program
    {
        static void Main(string[] args)
        {
            // !!!
            string url = "http://esn.aptekaural.ru/api/goods/GetGoodSzFromSite";
            //if (Debugger.IsAttached)
            if (0 == 1)
            {
                url = "http://localhost:61303/goods/GetGoodSzFromSite";
            }

            string apiCallResult = "";
            bool isResponseOk = false;
            HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get,
                };

                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode;
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }

                        var jsonTask = response.Content.ReadAsStringAsync();
                        jsonTask.Wait();
                        apiCallResult = jsonTask.Result;
                    });
                task.Wait();
            }
        }
    }
}
