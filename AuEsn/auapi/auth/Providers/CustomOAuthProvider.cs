﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using auesn.util;

namespace auesn.auth
{

    // http://bitoftech.net/2014/10/27/json-web-token-asp-net-web-api-2-jwt-owin-authorization-server/


    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            /*
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            string symmetricKeyAsBase64 = string.Empty;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            if (context.ClientId == null)
            {
                context.SetError("invalid_clientId", "client_Id is not set");
                return Task.FromResult<object>(null);
            }
            
            var audience = AudiencesStore.FindAudience(context.ClientId);            
            if (audience == null)
            {
                context.SetError("invalid_clientId", string.Format("Invalid client_id '{0}'", context.ClientId));
                return Task.FromResult<object>(null);
            }
            */            

            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });            

            var client = ClientStore.FindClient(context.UserName, context.Password);
            if (client == null)
            {
                context.SetError("invalid_grant", "Неверный логин или пароль");
                return Task.FromResult<object>(null);
            }

            // !!!
            context.Options.AccessTokenExpireTimeSpan = client.workplace_type == AuEsnConst.WORKPLACE_TYPE_ESN ? TimeSpan.FromDays(365) : TimeSpan.FromHours(24);

            var identity = new ClaimsIdentity("JWT");
            
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, context.UserName));
            identity.AddClaim(new Claim("cci", client.client_id.ToString()));
            identity.AddClaim(new Claim("csi", client.sales_id.GetValueOrDefault(0).ToString()));
            identity.AddClaim(new Claim("sub", context.UserName));
            //identity.AddClaim(new Claim(ClaimTypes.Role, "Manager"));
            //identity.AddClaim(new Claim(ClaimTypes.Role, "Supervisor"));            

            // !!!
            //var audience = AudiencesStore.FindAudience(context.ClientId);
            var audience = AudiencesStore.FindAudience(System.Configuration.ConfigurationManager.AppSettings["audienceid"]);            
            var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                         // !!!
                         //"audience", (context.ClientId == null) ? string.Empty : context.ClientId
                         "audience", (audience == null) ? string.Empty : audience.AudienceId
                    }
                });

            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);
            return Task.FromResult<object>(null);
        }

    }
}