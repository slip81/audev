﻿using auesn.auth.Models;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace auesn.auth
{
    public static class AudiencesStore
    {
        public static ConcurrentDictionary<string, Audience> AudiencesList = new ConcurrentDictionary<string, Audience>();

        static AudiencesStore()
        {
            string audienceId = System.Configuration.ConfigurationManager.AppSettings["audienceid"];
            string audienceSecret = System.Configuration.ConfigurationManager.AppSettings["audiencesecret"];
            string audienceName = System.Configuration.ConfigurationManager.AppSettings["audiencename"];

            AudiencesList.TryAdd(audienceId,
                                new Audience
                                {                                    
                                    AudienceId = audienceId,
                                    Base64Secret = audienceSecret,
                                    Name = audienceName
                                });
        }

        public static Audience FindAudience(string clientId)
        {
            Audience audience = null;
            if (AudiencesList.TryGetValue(clientId, out audience))
            {
                return audience;
            }
            return null;
        }
    }
}