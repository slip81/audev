﻿using auesn.auth.Entities;
using auesn.auth.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace auesn.auth
{
    public static class ClientStore
    {
        public static ClientModel FindClient(string login, string workplace)
        {
            if (String.IsNullOrWhiteSpace(login))
                return null;
            if (String.IsNullOrWhiteSpace(workplace))
                return null;

            using (var context = new AuMainDb())
            {
                ClientModel result = context.vw_license2
                    .Where(ss => ss.user_name.Trim().ToLower().Equals(login.Trim().ToLower())
                    && ss.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())                    
                    && ss.service_id == 64
                    )
                    .Select(ss => new ClientModel()
                    {
                        client_id = ss.client_id,
                        sales_id = ss.sales_id,
                        service_id = ss.service_id,
                        version_id = ss.version_id,
                        workplace_id = ss.workplace_id,
                        user_name = ss.user_name,
                        registration_key = ss.registration_key,
                        workplace_type = ss.workplace_type,
                    }
                    )
                    .FirstOrDefault();

                if ((result == null) && (login.Contains("/")))
                {
                    var splittedLogin = login.Split(new[] { '/' });
                    if (splittedLogin.Length != 2)
                        return result;
                    string loginPart_clientId = splittedLogin[0];
                    string loginPart_compName = splittedLogin[1];
                    if ((String.IsNullOrWhiteSpace(loginPart_clientId)) || (String.IsNullOrWhiteSpace(loginPart_compName)))
                        return result;

                    int clientId = 0;
                    if (!int.TryParse(loginPart_clientId, out clientId))
                        clientId = 0;
                    if (clientId <= 0)
                        return result;

                    result = context.vw_service_esn2
                        .Where(ss => ss.client_id == clientId
                        && ss.comp_name != null
                        && ss.comp_name.Trim().ToLower().Equals(loginPart_compName.Trim().ToLower())
                        && ss.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())
                        && ss.service_id == 64
                        )
                        .Select(ss => new ClientModel()
                        {
                            client_id = ss.client_id,
                            sales_id = ss.sales_id,
                            service_id = ss.service_id,
                            version_id = 0,
                            workplace_id = ss.workplace_id,
                            //user_name = login,
                            registration_key = ss.registration_key,
                            workplace_type = ss.workplace_type,
                        })
                        .FirstOrDefault();

                    if (result != null)
                        result.user_name = login;
                }

                return result;
            }
        }
    }
}