﻿using auesn.auth.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace auesn.auth.Models
{
    public class ClientModel
    {

        public ClientModel()
        {
            //vw_license2
        }
        
        public int client_id { get; set; }
        public Nullable<int> workplace_id { get; set; }
        public string registration_key { get; set; }
        public Nullable<int> sales_id { get; set; }
        public int service_id { get; set; }
        public int version_id { get; set; }
        public string user_name { get; set; }
        public Nullable<int> workplace_type { get; set; }
    }
}