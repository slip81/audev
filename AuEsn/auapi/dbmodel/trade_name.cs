//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace auesn.dbmodel
{
    using System;
    using System.Collections.Generic;
    
    public partial class trade_name
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public trade_name()
        {
            this.product = new HashSet<product>();
        }
    
        public long trade_name_id { get; set; }
        public System.Guid trade_name_uuid { get; set; }
        public string trade_name_name { get; set; }
        public System.DateTime crt_date { get; set; }
        public string crt_user { get; set; }
        public System.DateTime upd_date { get; set; }
        public string upd_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
        public Nullable<long> inn_source_id { get; set; }
    
        public virtual inn_source inn_source { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<product> product { get; set; }
    }
}
