//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace auesn.dbmodel
{
    using System;
    using System.Collections.Generic;
    
    public partial class supplier
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public supplier()
        {
            this.supplier_good = new HashSet<supplier_good>();
        }
    
        public int supplier_id { get; set; }
        public System.Guid supplier_uuid { get; set; }
        public string supplier_name { get; set; }
        public string labeling_code { get; set; }
        public System.DateTime crt_date { get; set; }
        public string crt_user { get; set; }
        public System.DateTime upd_date { get; set; }
        public string upd_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<supplier_good> supplier_good { get; set; }
    }
}
