﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace auesn.dbmodel
{
    public static class DbContextUtil
    {
        // Fastest Way of Inserting in Entity Framework
        // http://stackoverflow.com/questions/5940225/fastest-way-of-inserting-in-entity-framework

        public static AuEsnDb AddToContext<TEntity>(AuEsnDb context,
            TEntity entity, int count, int commitCount, bool recreateContext)
            where TEntity : class
        {
            context.Set<TEntity>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuEsnDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }
    }
}
