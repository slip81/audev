//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace auesn.dbmodel
{
    using System;
    using System.Collections.Generic;
    
    public partial class good_sz_log
    {
        public long good_sz_log_id { get; set; }
        public System.Guid good_sz_log_uuid { get; set; }
        public Nullable<System.DateTime> download_date { get; set; }
        public Nullable<int> cnt { get; set; }
        public string mess { get; set; }
        public bool is_success { get; set; }
        public System.DateTime crt_date { get; set; }
        public string crt_user { get; set; }
        public System.DateTime upd_date { get; set; }
        public string upd_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
    }
}
