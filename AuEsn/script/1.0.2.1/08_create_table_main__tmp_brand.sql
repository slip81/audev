﻿/*
	CREATE TABLE main._tmp_brand
*/

-- DROP TABLE main._tmp_brand;

CREATE TABLE main._tmp_brand
(
  "Брэнды" character varying
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main._tmp_brand OWNER TO pavlov;
