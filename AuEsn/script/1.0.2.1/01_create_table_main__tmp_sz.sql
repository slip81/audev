﻿/*
	CREATE TABLE main._tmp_sz
*/

-- DROP TABLE main._tmp_sz;

CREATE TABLE main._tmp_sz
(
  "Код СЗИ" character varying,
  "Код СЗ" character varying,
  "Наименование" character varying,
  "Код Изг" character varying,
  "Изготовитель" character varying,
  "Группа товаров" character varying,
  "ЖНВЛП" character varying,
  "ПКУ" character varying,
  "3 мес." character varying,
  "Код МНН" character varying,
  "МНН" character varying,
  "Создан" character varying,
  "Штрихкод" bigint
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main._tmp_sz OWNER TO pavlov;
