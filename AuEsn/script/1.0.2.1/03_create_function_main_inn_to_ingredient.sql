﻿/*
	CREATE OR REPLACE FUNCTION main.inn_to_ingredient
*/

-- DROP FUNCTION main.inn_to_ingredient(integer, character varying);

CREATE OR REPLACE FUNCTION main.inn_to_ingredient(    
    _client_id integer,
    _user_name character varying
    )
  RETURNS void AS
$BODY$
BEGIN
  /*
    Разбивка МНН по действующим веществам для указанного client_source_id
  */

  with inn_cte1 as
  (
  select distinct trim(trim(trailing ')' from trim(trailing ']' from trim(leading '[' from trim(leading '(' from unnest(string_to_array(t1.inn_name, '+'))))))) as ingredient_name
  from main.inn t1
  where t1.client_source_id = _client_id
  and t1.is_deleted = false
  and not exists (select x1.inn_id from main.inn_ingredient x1 where t1.inn_id = x1.inn_id and x1.is_deleted = false)  
  order by 1
  )
  insert into main.ingredient (ingredient_name, upd_user)
  select distinct t1.ingredient_name, _user_name
  from inn_cte1 t1
  where not exists (select x1.ingredient_id from main.ingredient x1 where trim(coalesce(x1.ingredient_name, '')) = trim(coalesce(t1.ingredient_name, '')) and x1.is_deleted = false);

  with inn_cte2 as
  (
  select distinct t1.inn_id, trim(trim(trailing ')' from trim(trailing ']' from trim(leading '[' from trim(leading '(' from unnest(string_to_array(t1.inn_name, '+'))))))) as ingredient_name
  from main.inn t1
  where t1.client_source_id = _client_id
  and t1.is_deleted = false
  and not exists (select x1.inn_id from main.inn_ingredient x1 where t1.inn_id = x1.inn_id and x1.is_deleted = false)
  order by 1
  )  
  insert into main.inn_ingredient (inn_id, ingredient_id, upd_user)
  select distinct t1.inn_id, t2.ingredient_id, _user_name
  from inn_cte2 t1
  inner join main.ingredient t2 on trim(coalesce(t1.ingredient_name, '')) = trim(coalesce(t2.ingredient_name, '')) and t2.is_deleted = false;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.inn_to_ingredient(integer, character varying) OWNER TO pavlov;
