﻿/*
	insert into main.brand
*/

insert into main.brand (brand_name, upd_user)
select distinct trim(coalesce(t1."Брэнды", '')), 'upd_user'
from main._tmp_brand t1
where not exists (select x1.brand_id from main.brand x1 where trim(coalesce(x1.brand_name, '')) = trim(coalesce(t1."Брэнды", '')));

-- select * from main.brand