﻿/*
	CREATE OR REPLACE FUNCTION main.gr_to_inn
*/

-- DROP FUNCTION main.gr_to_inn(integer, character varying);

CREATE OR REPLACE FUNCTION main.gr_to_inn(
    _client_id integer,
    _user_name character varying)
  RETURNS void AS
$BODY$
BEGIN
  /*
    Перенос МНН из таблицы _tmp_gr в таблицу inn для указанного client_source_id
  */

  insert into main.inn (inn_name, client_source_id, upd_user)
  select distinct t1.f8, _client_id, _user_name
  from main._tmp_gr t1 
  left join main.inn t2 on coalesce(trim(t1.f8), '') = coalesce(trim(t2.inn_name), '') 
  and t2.client_source_id = _client_id
  and t2.is_deleted = false
  where coalesce(trim(t1.f8), '') != '' 
  and t2.inn_id is null;

  PERFORM main.inn_to_ingredient(_client_id, _user_name);

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.gr_to_inn(integer, character varying) OWNER TO pavlov;
