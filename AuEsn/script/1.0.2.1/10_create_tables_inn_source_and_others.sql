﻿/*
	CREATE TABLE main.inn_source
*/

-----------------------------------------------------------------------------------------

DROP VIEW main.vw_trade_name;
DROP VIEW main.vw_inn;
DROP VIEW main.vw_inn_ingredient;
DROP VIEW main.vw_client_good_info;

DROP FUNCTION main.gr_to_inn(integer, character varying);
DROP FUNCTION main.sz_to_inn(integer, character varying);
DROP FUNCTION main.sz_to_trade_name(character varying);
DROP FUNCTION main.inn_to_ingredient(integer, character varying);

-----------------------------------------------------------------------------------------

-- DROP TABLE main.inn_source;

CREATE TABLE main.inn_source
(
  inn_source_id bigserial NOT NULL,
  inn_source_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  inn_source_name character varying NOT NULL,
  client_source_id integer,  
  inn_id bigint,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,  
  CONSTRAINT inn_source_pkey PRIMARY KEY (inn_source_id),
  CONSTRAINT inn_source_client_source_id_fkey FOREIGN KEY (client_source_id)
      REFERENCES main.client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT inn_source_inn_id_fkey FOREIGN KEY (inn_id)
      REFERENCES main.inn (inn_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.inn_source OWNER TO pavlov;

COMMENT ON TABLE main.inn_source IS 'МНН из источников';

-- DROP TRIGGER main_inn_source_set_date_and_user_trg ON main.inn_source;

CREATE TRIGGER main_inn_source_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.inn_source
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

-----------------------------------------------------------------------------------------

-- DROP TABLE main.inn_source_ingredient;

CREATE TABLE main.inn_source_ingredient
(
  inn_source_ingredient_id bigserial NOT NULL,
  inn_source_ingredient_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  inn_source_id bigint NOT NULL,
  ingredient_id bigint NOT NULL,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT inn_source_ingredient_pkey PRIMARY KEY (inn_source_ingredient_id),
  CONSTRAINT inn_source_ingredient_ingredient_id_fkey FOREIGN KEY (ingredient_id)
      REFERENCES main.ingredient (ingredient_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT inn_source_ingredient_inn_source_id_fkey FOREIGN KEY (inn_source_id)
      REFERENCES main.inn_source (inn_source_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.inn_source_ingredient OWNER TO pavlov;

COMMENT ON TABLE main.inn_source_ingredient IS 'Действующие вещества МНН';

-- DROP TRIGGER main_inn_source_ingredient_set_date_and_user_trg ON main.inn_source_ingredient;

CREATE TRIGGER main_inn_source_ingredient_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.inn_source_ingredient
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

-----------------------------------------------------------------------------------------

INSERT INTO main.inn_source (
  inn_source_id,
  inn_source_uuid,
  inn_source_name,
  client_source_id,  
  inn_id,
  upd_user
)
SELECT
  inn_id,
  inn_uuid,
  inn_name,
  client_source_id,  
  null,
  'pavlov'
FROM main.inn;

select setval('main.inn_source_inn_source_id_seq', (select max(inn_source_id) + 1 from main.inn_source), false);

INSERT INTO main.inn_source_ingredient (
  inn_source_ingredient_id,
  inn_source_ingredient_uuid,
  inn_source_id,
  ingredient_id,
  upd_user
)
SELECT
  inn_ingredient_id,
  inn_ingredient_uuid,
  inn_id,
  ingredient_id,
  'pavlov'
FROM main.inn_ingredient;

select setval('main.inn_source_ingredient_inn_source_ingredient_id_seq', (select max(inn_source_ingredient_id) + 1 from main.inn_source_ingredient), false);

-----------------------------------------------------------------------------------------

DROP TABLE main.inn_ingredient;

-----------------------------------------------------------------------------------------

DELETE FROM main.inn;

-----------------------------------------------------------------------------------------

ALTER TABLE main.trade_name ADD COLUMN inn_source_id bigint;

ALTER TABLE main.trade_name
  ADD CONSTRAINT trade_name_inn_source_id_fkey FOREIGN KEY (inn_source_id)
      REFERENCES main.inn_source (inn_source_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE main.trade_name SET inn_source_id = inn_id;

-----------------------------------------------------------------------------------------

ALTER TABLE main.trade_name DROP CONSTRAINT trade_name_inn_id_fkey;

ALTER TABLE main.trade_name DROP COLUMN inn_id;

-----------------------------------------------------------------------------------------

CREATE INDEX main_trade_name_inn_source_id1_idx
  ON main.trade_name
  USING btree
  (inn_source_id);

-----------------------------------------------------------------------------------------

CREATE INDEX main_product_trade_name_id1_idx
  ON main.product
  USING btree
  (trade_name_id);

-----------------------------------------------------------------------------------------
  
ALTER TABLE main.inn DROP CONSTRAINT inn_client_source_id_fkey;

ALTER TABLE main.inn DROP COLUMN client_source_id;

-----------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW main.vw_trade_name AS 
 SELECT t1.trade_name_id,
    t1.trade_name_uuid,
    t1.trade_name_name,
    t1.inn_source_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t11.inn_source_uuid
   FROM main.trade_name t1
     LEFT JOIN main.inn_source t11 ON t1.inn_source_id = t11.inn_source_id;

ALTER TABLE main.vw_trade_name OWNER TO pavlov;

-----------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW main.vw_inn_source_ingredient AS 
 SELECT t1.inn_source_ingredient_id,
    t1.inn_source_ingredient_uuid,
    t1.inn_source_id,
    t1.ingredient_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.inn_source_uuid,
    t3.ingredient_uuid
   FROM main.inn_source_ingredient t1
     JOIN main.inn_source t2 ON t1.inn_source_id = t2.inn_source_id
     JOIN main.ingredient t3 ON t1.ingredient_id = t3.ingredient_id;

ALTER TABLE main.vw_inn_source_ingredient OWNER TO pavlov;

-----------------------------------------------------------------------------------------  

CREATE OR REPLACE VIEW main.vw_inn_source AS 
 SELECT t1.inn_source_id,
    t1.inn_source_uuid,
    t1.inn_source_name,
    t1.client_source_id,
    t1.inn_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,    
    t11.client_uuid AS client_source_uuid,
    t12.inn_uuid
   FROM main.inn_source t1
     LEFT JOIN main.client t11 ON t1.client_source_id = t11.client_id
     LEFT JOIN main.inn t12 ON t1.inn_id = t12.inn_id;

ALTER TABLE main.vw_inn_source  OWNER TO pavlov;

-----------------------------------------------------------------------------------------  

CREATE OR REPLACE VIEW main.vw_client_good_info AS 
 SELECT t1.client_good_id,
    t1.client_id,
    t1.good_id,
    t1.client_code,
    t1.client_name,
    t2.product_id,
    t2.mfr_id,
    t2.barcode,
    t3.product_name,
    t4.mfr_name,
    t22.country_name,
    t5.product_type_name,    
    t12.atc_name,
    t12.atc_code,
    t13.dosage_name,
    t14.form_name,
    t14.short_name AS form_short_name,
    t14.group_name AS form_group_name,
    t15.pharm_ther_group_name,
    t16.okpd2_name,
    t16.okpd2_code,
    t17.brand_name,
    t18.recipe_type_name,
    t19.registry_num,
    t19.registry_date,
    t20.baa_type_name,
    t21.metaname_name,
    t23.trade_name_name
   FROM main.client_good t1
     JOIN main.good t2 ON t1.good_id = t2.good_id
     JOIN main.product t3 ON t2.product_id = t3.product_id
     JOIN main.mfr t4 ON t2.mfr_id = t4.mfr_id
     JOIN main.product_type t5 ON t3.product_type_id = t5.product_type_id
     LEFT JOIN main.atc t12 ON t3.atc_id = t12.atc_id
     LEFT JOIN main.dosage t13 ON t3.dosage_id = t13.dosage_id
     LEFT JOIN main.form t14 ON t3.form_id = t14.form_id
     LEFT JOIN main.pharm_ther_group t15 ON t3.pharm_ther_group_id = t15.pharm_ther_group_id
     LEFT JOIN main.okpd2 t16 ON t3.okpd2_id = t16.okpd2_id
     LEFT JOIN main.brand t17 ON t3.brand_id = t17.brand_id
     LEFT JOIN main.recipe_type t18 ON t3.recipe_type_id = t18.recipe_type_id
     LEFT JOIN main.registry t19 ON t3.registry_id = t19.registry_id
     LEFT JOIN main.baa_type t20 ON t3.baa_type_id = t20.baa_type_id
     LEFT JOIN main.metaname t21 ON t3.metaname_id = t21.metaname_id
     LEFT JOIN main.country t22 ON t4.country_id = t22.country_id
     LEFT JOIN main.trade_name t23 ON t3.trade_name_id = t23.trade_name_id
     ;

ALTER TABLE main.vw_client_good_info OWNER TO pavlov;

-----------------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION main.inn_source_to_ingredient(
    _client_id integer,
    _user_name character varying)
  RETURNS void AS
$BODY$
BEGIN
  /*
    Разбивка МНН источников по действующим веществам для указанного client_source_id
  */

  with inn_cte1 as
  (
  select distinct trim(trim(trailing '*' from trim(trailing ')' from trim(trailing ']' from trim(leading '*' from trim(leading '[' from trim(leading '(' from unnest(string_to_array(t1.inn_source_name, '+'))))))))) as ingredient_name
  from main.inn_source t1
  where t1.client_source_id = _client_id
  and t1.is_deleted = false
  and not exists (select x1.inn_source_id from main.inn_source_ingredient x1 where t1.inn_source_id = x1.inn_source_id and x1.is_deleted = false)  
  order by 1
  )
  insert into main.ingredient (ingredient_name, upd_user)
  select distinct t1.ingredient_name, _user_name
  from inn_cte1 t1
  where not exists (select x1.ingredient_id from main.ingredient x1 where trim(coalesce(x1.ingredient_name, '')) = trim(coalesce(t1.ingredient_name, '')) and x1.is_deleted = false);

  with inn_cte2 as
  (
  select distinct t1.inn_source_id, trim(trim(trailing '*' from trim(trailing ')' from trim(trailing ']' from trim(leading '*' from trim(leading '[' from trim(leading '(' from unnest(string_to_array(t1.inn_source_name, '+'))))))))) as ingredient_name
  from main.inn_source t1
  where t1.client_source_id = _client_id
  and t1.is_deleted = false
  and not exists (select x1.inn_source_id from main.inn_source_ingredient x1 where t1.inn_source_id = x1.inn_source_id and x1.is_deleted = false)
  order by 1
  )  
  insert into main.inn_source_ingredient (inn_source_id, ingredient_id, upd_user)
  select distinct t1.inn_source_id, t2.ingredient_id, _user_name
  from inn_cte2 t1
  inner join main.ingredient t2 on trim(coalesce(t1.ingredient_name, '')) = trim(coalesce(t2.ingredient_name, '')) and t2.is_deleted = false;

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.inn_source_to_ingredient(integer, character varying) OWNER TO pavlov;

-----------------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION main.gr_to_inn_source(
    _client_id integer,
    _user_name character varying)
  RETURNS void AS
$BODY$
BEGIN
  /*
    Перенос МНН из таблицы _tmp_gr в таблицу inn_source для указанного client_source_id
  */

  insert into main.inn_source (inn_name, client_source_id, upd_user)
  select distinct t1.f8, _client_id, _user_name
  from main._tmp_gr t1 
  left join main.inn_source t2 on coalesce(trim(t1.f8), '') = coalesce(trim(t2.inn_source_name), '') 
  and t2.client_source_id = _client_id
  and t2.is_deleted = false
  where coalesce(trim(t1.f8), '') != '' 
  and t2.inn_source_id is null;

  PERFORM main.inn_source_to_ingredient(_client_id, _user_name);

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.gr_to_inn_source(integer, character varying) OWNER TO pavlov;

-----------------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION main.sz_to_inn_source(
    _client_id integer,
    _user_name character varying)
  RETURNS void AS
$BODY$
BEGIN
  /*
    Перенос МНН из таблицы _tmp_sz в таблицу inn_source для указанного client_source_id
  */

  insert into main.inn_source (inn_source_name, client_source_id, upd_user)
  select distinct t1.МНН, _client_id, _user_name
  from main._tmp_sz t1 
  left join main.inn_source t2 on coalesce(trim(t1.МНН), '') = coalesce(trim(t2.inn_source_name), '') 
  and t2.client_source_id = _client_id
  and t2.is_deleted = false
  where coalesce(trim(t1.МНН), '') != '' 
  and t2.inn_source_id is null;

  PERFORM main.inn_source_to_ingredient(_client_id, _user_name);

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.sz_to_inn_source(integer, character varying) OWNER TO pavlov;

-----------------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION main.sz_to_trade_name(_client_id integer,_user_name character varying)
  RETURNS void AS
$BODY$
BEGIN
  /*
    Перенос торговых наименований из таблицы _tmp_sz в таблицу trade_name
  */

  insert into main.trade_name (trade_name_name, inn_source_id, upd_user)
  select distinct t1.Наименование, t3.inn_source_id, _user_name
  from main._tmp_sz t1 
  left join main.trade_name t2 on coalesce(trim(t1.Наименование), '') = coalesce(trim(t2.trade_name_name), '')   
  and t2.is_deleted = false
  left join main.inn_source t3 on t3.client_source_id = _client_id
  and trim(coalesce(t3.inn_source_name, '')) = trim(coalesce(t1.МНН, ''))
  where coalesce(trim(t1.Наименование), '') != '' 
  and t2.trade_name_id is null;  

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.sz_to_trade_name(integer, character varying) OWNER TO pavlov;

-----------------------------------------------------------------------------------------  

CREATE OR REPLACE FUNCTION main.gr_to_trade_name(
    _client_id integer,
    _user_name character varying)
  RETURNS void AS
$BODY$
BEGIN
  /*
    Перенос торговых наименований из таблицы _tmp_gr в таблицу trade_name
  */

  insert into main.trade_name (trade_name_name, inn_source_id, upd_user)
  select distinct t1.f7, t3.inn_source_id, _user_name
  from main._tmp_gr t1 
  left join main.trade_name t2 on coalesce(trim(t1.f7), '') = coalesce(trim(t2.trade_name_name), '')   
  and t2.is_deleted = false
  left join main.inn_source t3 on t3.client_source_id = _client_id
  and trim(coalesce(t3.inn_source_name, '')) = trim(coalesce(t1.f8, ''))
  where coalesce(trim(t1.f7), '') != '' 
  and t2.trade_name_id is null;  

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.gr_to_trade_name(integer, character varying) OWNER TO pavlov;

-----------------------------------------------------------------------------------------  

-- SELECT * FROM adm.main_table order by main_table_id

DELETE FROM adm.main_table WHERE main_table_id = 50;

UPDATE adm.main_table SET view_name = null WHERE main_table_id = 20;

INSERT INTO adm.main_table (
  main_table_id,
  main_table_name,
  allow_select,
  allow_insert,
  allow_update,
  allow_delete,
  is_logged,
  view_name
)
VALUES (
  54,
  'inn_source',
  true,
  true,
  true,
  true,
  true,
  'vw_inn_source'
);

INSERT INTO adm.main_table (
  main_table_id,
  main_table_name,
  allow_select,
  allow_insert,
  allow_update,
  allow_delete,
  is_logged,
  view_name
)
VALUES (
  55,
  'inn_source_ingredient',
  true,
  true,
  true,
  true,
  true,
  'vw_inn_source_ingredient'
);