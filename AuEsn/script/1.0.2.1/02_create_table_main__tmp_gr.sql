﻿/*
	CREATE TABLE main._tmp_gr
*/

-- DROP TABLE main._tmp_gr;

CREATE TABLE main._tmp_gr
(
  f1 character varying,
  f2 character varying,
  f3 character varying,
  f4 character varying,
  f5 character varying,
  f6 character varying,
  f7 character varying,
  f8 character varying,
  f9 character varying,
  f10 character varying,
  f11 character varying,
  f12 character varying,
  f13 character varying
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main._tmp_gr OWNER TO pavlov;
