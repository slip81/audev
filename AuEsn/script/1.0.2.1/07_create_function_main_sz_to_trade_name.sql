﻿/*
	CREATE OR REPLACE FUNCTION main.sz_to_trade_name
*/

-- DROP FUNCTION main.sz_to_trade_name(character varying);

CREATE OR REPLACE FUNCTION main.sz_to_trade_name(    
    _user_name character varying)
  RETURNS void AS
$BODY$
BEGIN
  /*
    Перенос торговых наименований из таблицы _tmp_sz в таблицу trade_name
  */

  insert into main.trade_name (trade_name_name, upd_user)
  select distinct t1.Наименование, _user_name
  from main._tmp_sz t1 
  left join main.trade_name t2 on coalesce(trim(t1.Наименование), '') = coalesce(trim(t2.trade_name_name), '')   
  and t2.is_deleted = false
  where coalesce(trim(t1.Наименование), '') != '' 
  and t2.trade_name_id is null;  

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.sz_to_trade_name(character varying) OWNER TO pavlov;
