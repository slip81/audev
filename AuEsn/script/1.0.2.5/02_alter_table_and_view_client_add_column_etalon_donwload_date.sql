﻿
ALTER TABLE main.client ADD COLUMN etalon_download_date timestamp without time zone;
COMMENT ON COLUMN main.client.etalon_download_date IS 'когда скачан эталонный справочник';

-- DROP VIEW main.vw_client;

CREATE OR REPLACE VIEW main.vw_client AS 
 SELECT t1.client_id,
    t1.client_uuid,
    t1.client_name,
    t1.parent_id,
    t1.labeling_code,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.client_type_id,
    t1.cab_client_code,
    t1.cab_sales_code,
    t11.client_uuid AS parent_uuid,
    t12.client_type_uuid,
    t1.etalon_download_date
   FROM main.client t1
     LEFT JOIN main.client t11 ON t1.parent_id = t11.client_id
     LEFT JOIN main.client_type t12 ON t1.client_type_id = t12.client_type_id;

ALTER TABLE main.vw_client OWNER TO pavlov;

-- select * from main.client order by client_id