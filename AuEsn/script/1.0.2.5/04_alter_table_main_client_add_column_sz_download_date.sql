﻿/*
	ALTER TABLE main.client ADD COLUMN sz_download_date
*/

ALTER TABLE main.client ADD COLUMN sz_download_date timestamp without time zone;