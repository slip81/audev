﻿/*
	CREATE TABLE main.good_sz_log
*/

-- DROP TABLE main.good_sz_log;

CREATE TABLE main.good_sz_log
(
  good_sz_log_id bigserial NOT NULL,
  good_sz_log_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  download_date timestamp without time zone,
  cnt integer,
  mess character varying,
  is_success boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,  
  CONSTRAINT good_sz_log_pkey PRIMARY KEY (good_sz_log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.good_sz_log OWNER TO pavlov;

-- DROP TRIGGER main_good_sz_log_set_date_and_user_trg ON good_sz;

CREATE TRIGGER main_good_sz_log_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.good_sz_log
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

