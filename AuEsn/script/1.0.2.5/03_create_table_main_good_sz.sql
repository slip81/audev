﻿/*
	CREATE TABLE main.good_sz
*/

-- DROP TABLE main.good_sz;

CREATE TABLE main.good_sz
(
  good_sz_id bigserial NOT NULL,
  good_sz_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  tov_id integer,
  kat_id integer,
  kat character varying,
  izg_id integer,
  izg character varying,
  mnn_id integer,
  mnn character varying,
  gr_id integer,
  gr character varying,
  gvls integer,
  pku integer,
  pku3m integer,
  assort integer,
  modify_date timestamp without time zone,
  eans_list character varying,  
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,  
  CONSTRAINT good_sz_pkey PRIMARY KEY (good_sz_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.good_sz OWNER TO pavlov;

-- DROP TRIGGER main_good_sz_set_date_and_user_trg ON good;

CREATE TRIGGER main_good_sz_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.good_sz
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();
