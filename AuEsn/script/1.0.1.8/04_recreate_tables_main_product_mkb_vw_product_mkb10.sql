﻿/*
	RECREATE TABLE main.product_mkb10 AND main.vw_product_mkb10
*/

DROP VIEW main.vw_product_mkb10;
DROP TABLE main.product_mkb10;

-- DROP TABLE main.product_mkb10;

CREATE TABLE main.product_mkb10
(
  product_mkb10_id bigserial NOT NULL,
  product_mkb10_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  mkb10_id integer NOT NULL,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT product_mkb10_pkey PRIMARY KEY (product_mkb10_id),
  CONSTRAINT product_mkb10_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_mkb10_mkb10_id_fkey FOREIGN KEY (mkb10_id)
      REFERENCES main.mkb10 (mkb10_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_mkb10 OWNER TO pavlov;

COMMENT ON TABLE main.product_mkb10 IS 'Классификация препарата по МКБ-10';


-- DROP TRIGGER main_product_mkb10_set_date_and_user_trg ON main.product_mkb10;

CREATE TRIGGER main_product_mkb10_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_mkb10
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

-- DROP VIEW main.vw_product_mkb10;

CREATE OR REPLACE VIEW main.vw_product_mkb10 AS 
 SELECT t1.product_mkb10_id,
    t1.product_mkb10_uuid,
    t1.product_id,
    t1.mkb10_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.product_uuid,
    t3.mkb10_uuid
   FROM main.product_mkb10 t1
     JOIN main.product t2 ON t1.product_id = t2.product_id
     JOIN main.mkb10 t3 ON t1.mkb10_id = t3.mkb10_id;

ALTER TABLE main.vw_product_mkb10 OWNER TO pavlov;