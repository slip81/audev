﻿/*
	CREATE OR REPLACE VIEW main.vw_mkb10
*/

-- DROP VIEW main.vw_mkb10;

CREATE OR REPLACE VIEW main.vw_mkb10 AS 
 SELECT t1.mkb10_id,
  t1.mkb10_uuid,
  t1.mkb10_name,
  t1.mkb10_code,
  t1.parent_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t11.mkb10_uuid AS parent_uuid
   FROM main.mkb10 t1
     LEFT JOIN main.mkb10 t11 ON t1.parent_id = t11.mkb10_id;

ALTER TABLE main.vw_mkb10 OWNER TO pavlov;
