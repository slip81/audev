﻿/*
	CREATE TABLE main.inn_ingredient
*/

-- DROP TABLE main.inn_ingredient;

CREATE TABLE main.inn_ingredient
(
  inn_ingredient_id bigserial NOT NULL,
  inn_ingredient_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  inn_id bigint NOT NULL,
  ingredient_id bigint NOT NULL,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT inn_ingredient_pkey PRIMARY KEY (inn_ingredient_id),
  CONSTRAINT inn_ingredient_inn_id_fkey FOREIGN KEY (inn_id)
      REFERENCES main.inn (inn_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT inn_ingredient_ingredient_id_fkey FOREIGN KEY (ingredient_id)
      REFERENCES main.ingredient (ingredient_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION     
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.inn_ingredient OWNER TO pavlov;

COMMENT ON TABLE main.inn_ingredient IS 'Действующие вещества МНН';

-- DROP TRIGGER main_inn_ingredient_set_date_and_user_trg ON main.inn_ingredient;

CREATE TRIGGER main_inn_ingredient_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.inn_ingredient
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();
