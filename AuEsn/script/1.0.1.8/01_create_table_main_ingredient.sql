﻿/*
	CREATE TABLE main.ingredient
*/

-- DROP TABLE main.ingredient;

CREATE TABLE main.ingredient
(
  ingredient_id bigserial NOT NULL,
  ingredient_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  ingredient_name character varying NOT NULL,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT ingredient_pkey PRIMARY KEY (ingredient_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.ingredient OWNER TO pavlov;

COMMENT ON TABLE main.ingredient IS 'Справочник действующих веществ';

-- DROP TRIGGER main_ingredient_set_date_and_user_trg ON main.ingredient;

CREATE TRIGGER main_ingredient_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.ingredient
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

