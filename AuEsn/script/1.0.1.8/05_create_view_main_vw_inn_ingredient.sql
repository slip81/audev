﻿/*
	CREATE OR REPLACE VIEW main.vw_inn_ingredient
*/

-- DROP VIEW main.vw_inn_ingredient;

CREATE OR REPLACE VIEW main.vw_inn_ingredient AS 
 SELECT t1.inn_ingredient_id,
  t1.inn_ingredient_uuid,
  t1.inn_id,
  t1.ingredient_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.inn_uuid,
  t3.ingredient_uuid
   FROM main.inn_ingredient t1
     JOIN main.inn t2 ON t1.inn_id = t2.inn_id
     JOIN main.ingredient t3 ON t1.ingredient_id = t3.ingredient_id;

ALTER TABLE main.vw_inn_ingredient OWNER TO pavlov;
