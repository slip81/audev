﻿/*
	insert into adm.main_table
*/

insert into adm.main_table (main_table_id, main_table_name, allow_select, allow_insert, allow_update, allow_delete, is_logged, view_name)
values (49, 'ingredient', true, true, true, true, true, null);

insert into adm.main_table (main_table_id, main_table_name, allow_select, allow_insert, allow_update, allow_delete, is_logged, view_name)
values (50, 'inn_ingredient', true, true, true, true, true, 'vw_inn_ingredient');

insert into adm.main_table (main_table_id, main_table_name, allow_select, allow_insert, allow_update, allow_delete, is_logged, view_name)
values (51, 'mkb10', true, true, true, true, true, 'vw_mkb10');

-- select * from adm.main_table order by main_table_id