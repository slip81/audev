﻿/*
	CREATE TABLE main.mkb10
*/

-- DROP TABLE main.mkb10;

CREATE TABLE main.mkb10
(
  mkb10_id serial NOT NULL,
  mkb10_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),  
  mkb10_name character varying NOT NULL,
  mkb10_code character varying,
  parent_id integer,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT mkb10_pkey PRIMARY KEY (mkb10_id),
  CONSTRAINT mkb10_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES main.mkb10 (mkb10_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE main.mkb10 OWNER TO pavlov;

COMMENT ON TABLE main.mkb10 IS 'Справочник МКБ-10';

-- DROP TRIGGER main_inn_set_date_and_user_trg ON main.mkb10;

CREATE TRIGGER main_mkb10_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.mkb10
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();
