﻿----------------------------------------
DROP VIEW main.vw_client_good_info;
DROP VIEW main.vw_good_info;
----------------------------------------

-- DROP TABLE main.vw_client_good_info;

CREATE TABLE main.vw_client_good_info
(
  client_good_id bigint NOT NULL,
  client_id integer NOT NULL,
  good_id bigint NOT NULL,
  client_code character varying,
  client_name character varying,
  product_id bigint NOT NULL,
  mfr_id bigint NOT NULL,
  barcode character varying,
  product_name character varying,
  storage_period character varying,
  storage_condition character varying,
  indication character varying,
  contraindication character varying,
  storage_condition_short character varying,
  indication_short character varying,
  contraindication_short character varying,
  applic_and_dose character varying,
  mfr_name character varying,
  country_name character varying,
  product_type_name character varying,  
  atc_name character varying,
  atc_code character varying,
  dosage_name character varying,
  form_name character varying,
  form_short_name character varying,
  form_group_name character varying,
  pharm_ther_group_name character varying,
  okpd2_name character varying,
  okpd2_code character varying,
  brand_name character varying,
  recipe_type_name character varying,
  registry_num character varying,
  registry_date date,
  baa_type_name character varying,
  metaname_name character varying,
  trade_name_name character varying,
  inn_id bigint,
  inn_name character varying,
  CONSTRAINT vw_client_good_info_pkey PRIMARY KEY (client_good_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.vw_client_good_info OWNER TO pavlov;

----------------------------------------

-- DROP TABLE main.vw_good_info;

CREATE TABLE main.vw_good_info
(
    good_uuid uuid NOT NULL,
    barcode character varying,
    gtin character varying,
    is_perfect boolean,
    crt_date timestamp without time zone NOT NULL DEFAULT now(),
    crt_user character varying,
    upd_date timestamp without time zone NOT NULL DEFAULT now(),
    upd_user character varying,
    is_deleted boolean NOT NULL DEFAULT false,
    del_date timestamp without time zone,
    del_user character varying,    
    product_uuid uuid NOT NULL,
    product_name character varying,
    storage_period character varying,
    storage_condition character varying,
    is_vital boolean,
    is_drug2 boolean,
    is_drug3 boolean,
    is_precursor boolean,
    is_alcohol boolean,
    is_imm_bio boolean,
    is_mandatory boolean,
    is_defect_exists boolean,
    indication character varying,
    contraindication character varying,
    drug_interaction character varying,
    storage_condition_short character varying,
    indication_short character varying,
    contraindication_short character varying,
    applic_and_dose character varying,
    mfr_uuid uuid,
    mfr_name character varying,
    country_uuid uuid,
    country_name character varying,
    product_type_uuid uuid,
    product_type_name character varying,
    atc_uuid uuid,
    atc_name character varying,
    atc_code character varying,
    dosage_uuid uuid,
    dosage_name character varying,
    form_uuid uuid,
    form_name character varying,
    form_short_name character varying,
    form_group_name character varying,
    pharm_ther_group_uuid uuid,
    pharm_ther_group_name character varying,
    okpd2_uuid uuid,
    okpd2_name character varying,
    okpd2_code character varying,
    brand_uuid uuid,
    brand_name character varying,
    recipe_type_uuid uuid,
    recipe_type_name character varying,
    registry_uuid uuid,
    registry_num character varying,
    registry_date date,
    baa_type_uuid uuid,
    baa_type_name character varying,
    metaname_uuid uuid,
    metaname_name character varying,
    trade_name_uuid uuid,
    trade_name_name character varying,
    inn_uuid uuid,
    inn_name character varying,
  CONSTRAINT vw_good_info_pkey PRIMARY KEY (good_uuid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.vw_good_info OWNER TO pavlov;

----------------------------------------
