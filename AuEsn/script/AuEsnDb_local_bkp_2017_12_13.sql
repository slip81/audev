﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.0
-- Dumped by pg_dump version 9.4.0
-- Started on 2017-12-13 10:28:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2523 (class 1262 OID 156412)
-- Name: AuEsnDb; Type: DATABASE; Schema: -; Owner: pavlov
--

--CREATE DATABASE "AuEsnDb" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
--ALTER DATABASE "AuEsnDb" OWNER TO pavlov;
--\connect "AuEsnDb"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 6 (class 2615 OID 156413)
-- Name: main; Type: SCHEMA; Schema: -; Owner: pavlov
--

CREATE SCHEMA main;


ALTER SCHEMA main OWNER TO pavlov;

--
-- TOC entry 258 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2526 (class 0 OID 0)
-- Dependencies: 258
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 259 (class 3079 OID 156414)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 2527 (class 0 OID 0)
-- Dependencies: 259
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = main, pg_catalog;

--
-- TOC entry 282 (class 1255 OID 156425)
-- Name: id_generator(); Type: FUNCTION; Schema: main; Owner: pavlov
--

CREATE FUNCTION id_generator(OUT result bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
DECLARE
    our_epoch bigint := 1314220021721;
    seq_id bigint;
    now_millis bigint;
    shard_id int := 1;
BEGIN
    SELECT nextval('main.global_id_sequence') % 1024 INTO seq_id;

SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000) INTO now_millis;
    result := (now_millis - our_epoch) << 23;
    result := result | (shard_id << 10);
    result := result | (seq_id);
END;
$$;


ALTER FUNCTION main.id_generator(OUT result bigint) OWNER TO pavlov;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 156426)
-- Name: atc; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE atc (
    atc_id bigint NOT NULL,
    atc_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    atc_name character varying NOT NULL
);


ALTER TABLE atc OWNER TO pavlov;

--
-- TOC entry 2528 (class 0 OID 0)
-- Dependencies: 173
-- Name: TABLE atc; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE atc IS 'Справочник АТХ';


--
-- TOC entry 174 (class 1259 OID 156433)
-- Name: atc_atc_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE atc_atc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE atc_atc_id_seq OWNER TO pavlov;

--
-- TOC entry 2529 (class 0 OID 0)
-- Dependencies: 174
-- Name: atc_atc_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE atc_atc_id_seq OWNED BY atc.atc_id;


--
-- TOC entry 175 (class 1259 OID 156435)
-- Name: baa_type; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE baa_type (
    baa_type_id integer NOT NULL,
    baa_type_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    baa_type_name character varying NOT NULL
);


ALTER TABLE baa_type OWNER TO pavlov;

--
-- TOC entry 2530 (class 0 OID 0)
-- Dependencies: 175
-- Name: TABLE baa_type; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE baa_type IS 'Справочник типов БАД';


--
-- TOC entry 176 (class 1259 OID 156442)
-- Name: brand; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE brand (
    brand_id bigint NOT NULL,
    brand_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    brand_name character varying NOT NULL
);


ALTER TABLE brand OWNER TO pavlov;

--
-- TOC entry 2531 (class 0 OID 0)
-- Dependencies: 176
-- Name: TABLE brand; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE brand IS 'Справочник брэндов';


--
-- TOC entry 177 (class 1259 OID 156449)
-- Name: brand_brand_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE brand_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE brand_brand_id_seq OWNER TO pavlov;

--
-- TOC entry 2532 (class 0 OID 0)
-- Dependencies: 177
-- Name: brand_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE brand_brand_id_seq OWNED BY brand.brand_id;


--
-- TOC entry 178 (class 1259 OID 156451)
-- Name: client; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE client (
    client_id integer NOT NULL,
    client_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    client_name character varying,
    parent_id integer,
    labeling_code character varying,
    cab_client_id integer,
    cab_sales_id integer
);


ALTER TABLE client OWNER TO pavlov;

--
-- TOC entry 2533 (class 0 OID 0)
-- Dependencies: 178
-- Name: TABLE client; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE client IS 'Клиенты';


--
-- TOC entry 2534 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN client.client_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client.client_id IS 'PK';


--
-- TOC entry 2535 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN client.client_uuid; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client.client_uuid IS 'BK';


--
-- TOC entry 2536 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN client.client_name; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client.client_name IS 'наименование клиента';


--
-- TOC entry 2537 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN client.parent_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client.parent_id IS 'FK код родительского клиента';


--
-- TOC entry 2538 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN client.cab_client_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client.cab_client_id IS 'код клиента из ЛК';


--
-- TOC entry 2539 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN client.cab_sales_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client.cab_sales_id IS 'код ТТ из ЛК';


--
-- TOC entry 2540 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN client.labeling_code; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client.labeling_code IS 'код в программе маркировки';


--
-- TOC entry 179 (class 1259 OID 156458)
-- Name: client_client_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE client_client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_client_id_seq OWNER TO pavlov;

--
-- TOC entry 2541 (class 0 OID 0)
-- Dependencies: 179
-- Name: client_client_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE client_client_id_seq OWNED BY client.client_id;


--
-- TOC entry 180 (class 1259 OID 156460)
-- Name: client_doc_type; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE client_doc_type (
    client_doc_type_id integer NOT NULL,
    client_doc_type_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    client_id integer NOT NULL,
    doc_type_id integer NOT NULL,
    valid_date date
);


ALTER TABLE client_doc_type OWNER TO pavlov;

--
-- TOC entry 2542 (class 0 OID 0)
-- Dependencies: 180
-- Name: TABLE client_doc_type; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE client_doc_type IS 'Типы документов клиентов';


--
-- TOC entry 181 (class 1259 OID 156464)
-- Name: client_doc_type_client_doc_type_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE client_doc_type_client_doc_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_doc_type_client_doc_type_id_seq OWNER TO pavlov;

--
-- TOC entry 2543 (class 0 OID 0)
-- Dependencies: 181
-- Name: client_doc_type_client_doc_type_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE client_doc_type_client_doc_type_id_seq OWNED BY client_doc_type.client_doc_type_id;


--
-- TOC entry 182 (class 1259 OID 156466)
-- Name: client_good; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE client_good (
    client_good_id bigint NOT NULL,
    client_good_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    client_id integer NOT NULL,
    good_id bigint NOT NULL,
    client_code character varying,
    client_name character varying,
    crt_date timestamp without time zone,
    crt_user character varying,
    upd_date timestamp without time zone,
    upd_user character varying,
    is_deleted boolean DEFAULT false NOT NULL,
    del_date timestamp without time zone,
    del_user character varying
);


ALTER TABLE client_good OWNER TO pavlov;

--
-- TOC entry 2544 (class 0 OID 0)
-- Dependencies: 182
-- Name: TABLE client_good; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE client_good IS 'Связь между товарами клиентов и товарами ЕСН';


--
-- TOC entry 2545 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.client_good_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.client_good_id IS 'PK';


--
-- TOC entry 2546 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.client_good_uuid; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.client_good_uuid IS 'BK';


--
-- TOC entry 2547 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.client_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.client_id IS 'FK код клиента';


--
-- TOC entry 2548 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.good_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.good_id IS 'FK код товара';


--
-- TOC entry 2549 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.client_code; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.client_code IS 'код товара у клиента';


--
-- TOC entry 2550 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.client_name; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.client_name IS 'наименование товара у клиента';


--
-- TOC entry 2551 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.crt_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.crt_date IS 'когда создана запись';


--
-- TOC entry 2552 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.crt_user; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.crt_user IS 'кем создана запись';


--
-- TOC entry 2553 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.upd_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.upd_date IS 'когда изменена запись';


--
-- TOC entry 2554 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.is_deleted; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.is_deleted IS 'запись удалена';


--
-- TOC entry 2555 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.del_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.del_date IS 'когда удалена запись';


--
-- TOC entry 2556 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN client_good.del_user; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN client_good.del_user IS 'кем удалена запись';


--
-- TOC entry 183 (class 1259 OID 156474)
-- Name: client_good_client_good_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE client_good_client_good_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_good_client_good_id_seq OWNER TO pavlov;

--
-- TOC entry 2557 (class 0 OID 0)
-- Dependencies: 183
-- Name: client_good_client_good_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE client_good_client_good_id_seq OWNED BY client_good.client_good_id;


--
-- TOC entry 184 (class 1259 OID 156476)
-- Name: client_service; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE client_service (
    client_service_id integer NOT NULL,
    client_service_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    client_id integer NOT NULL,
    service_id integer NOT NULL
);


ALTER TABLE client_service OWNER TO pavlov;

--
-- TOC entry 2558 (class 0 OID 0)
-- Dependencies: 184
-- Name: TABLE client_service; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE client_service IS 'Типы операций клиентов';


--
-- TOC entry 185 (class 1259 OID 156480)
-- Name: client_service_client_service_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE client_service_client_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_service_client_service_id_seq OWNER TO pavlov;

--
-- TOC entry 2559 (class 0 OID 0)
-- Dependencies: 185
-- Name: client_service_client_service_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE client_service_client_service_id_seq OWNED BY client_service.client_service_id;


--
-- TOC entry 186 (class 1259 OID 156482)
-- Name: consumer; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE consumer (
    consumer_id integer NOT NULL,
    consumer_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    consumer_name character varying NOT NULL
);


ALTER TABLE consumer OWNER TO pavlov;

--
-- TOC entry 2560 (class 0 OID 0)
-- Dependencies: 186
-- Name: TABLE consumer; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE consumer IS 'Справочник групп потребителей';


--
-- TOC entry 187 (class 1259 OID 156489)
-- Name: consumer_consumer_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE consumer_consumer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consumer_consumer_id_seq OWNER TO pavlov;

--
-- TOC entry 2561 (class 0 OID 0)
-- Dependencies: 187
-- Name: consumer_consumer_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE consumer_consumer_id_seq OWNED BY consumer.consumer_id;


--
-- TOC entry 188 (class 1259 OID 156491)
-- Name: country; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE country (
    country_id integer NOT NULL,
    country_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    country_name character varying NOT NULL,
    short_name character varying,
    aplpha2 character varying,
    aplpha3 character varying
);


ALTER TABLE country OWNER TO pavlov;

--
-- TOC entry 2562 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE country; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE country IS 'Справочник стран';


--
-- TOC entry 189 (class 1259 OID 156498)
-- Name: country_country_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE country_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE country_country_id_seq OWNER TO pavlov;

--
-- TOC entry 2563 (class 0 OID 0)
-- Dependencies: 189
-- Name: country_country_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE country_country_id_seq OWNED BY country.country_id;


--
-- TOC entry 190 (class 1259 OID 156500)
-- Name: doc; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE doc (
    doc_id bigint NOT NULL,
    doc_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    doc_num character varying,
    doc_name character varying,
    doc_date date,
    doc_type_id integer NOT NULL,
    doc_state_id integer NOT NULL,
    doc_path character varying,
    client_from_id integer NOT NULL,
    client_to_id integer,
    crt_date timestamp without time zone,
    crt_user character varying,
    upd_date timestamp without time zone,
    upd_user character varying,
    is_deleted boolean DEFAULT false NOT NULL,
    del_date timestamp without time zone,
    del_user character varying
);


ALTER TABLE doc OWNER TO pavlov;

--
-- TOC entry 2564 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE doc; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE doc IS 'Документы';


--
-- TOC entry 2565 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.doc_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.doc_id IS 'PK';


--
-- TOC entry 2566 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.doc_uuid; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.doc_uuid IS 'BK';


--
-- TOC entry 2567 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.doc_num; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.doc_num IS '№ документа';


--
-- TOC entry 2568 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.doc_name; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.doc_name IS 'название документа';


--
-- TOC entry 2569 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.doc_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.doc_date IS 'дата документа';


--
-- TOC entry 2570 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.doc_type_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.doc_type_id IS 'FK тип документа';


--
-- TOC entry 2571 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.doc_state_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.doc_state_id IS 'FK статус документа';


--
-- TOC entry 2572 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.doc_path; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.doc_path IS 'путь к файлу документа';


--
-- TOC entry 2573 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.client_from_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.client_from_id IS 'FK от кого документ';


--
-- TOC entry 2574 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.client_to_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.client_to_id IS 'FK кому документ';


--
-- TOC entry 2575 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.crt_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.crt_date IS 'когда создана запись';


--
-- TOC entry 2576 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.crt_user; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.crt_user IS 'кем создана запись';


--
-- TOC entry 2577 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.upd_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.upd_date IS 'когда изменена запись';


--
-- TOC entry 2578 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.is_deleted; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.is_deleted IS 'запись удалена';


--
-- TOC entry 2579 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.del_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.del_date IS 'когда удалена запись';


--
-- TOC entry 2580 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN doc.del_user; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc.del_user IS 'кем удалена запись';


--
-- TOC entry 191 (class 1259 OID 156508)
-- Name: doc_doc_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE doc_doc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE doc_doc_id_seq OWNER TO pavlov;

--
-- TOC entry 2581 (class 0 OID 0)
-- Dependencies: 191
-- Name: doc_doc_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE doc_doc_id_seq OWNED BY doc.doc_id;


--
-- TOC entry 192 (class 1259 OID 156510)
-- Name: doc_period; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE doc_period (
    doc_period_id bigint NOT NULL,
    doc_period_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    doc_id bigint,
    doc_state_id integer NOT NULL,
    crt_date timestamp without time zone
);


ALTER TABLE doc_period OWNER TO pavlov;

--
-- TOC entry 2582 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE doc_period; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE doc_period IS 'История изменений статусов док-тов';


--
-- TOC entry 2583 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN doc_period.doc_period_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc_period.doc_period_id IS 'PK';


--
-- TOC entry 2584 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN doc_period.doc_period_uuid; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc_period.doc_period_uuid IS 'BK';


--
-- TOC entry 2585 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN doc_period.doc_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc_period.doc_id IS 'FK код документа';


--
-- TOC entry 2586 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN doc_period.doc_state_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc_period.doc_state_id IS 'FK статус документа';


--
-- TOC entry 2587 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN doc_period.crt_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN doc_period.crt_date IS 'дата статуса документа';


--
-- TOC entry 193 (class 1259 OID 156514)
-- Name: doc_period_doc_period_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE doc_period_doc_period_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE doc_period_doc_period_id_seq OWNER TO pavlov;

--
-- TOC entry 2588 (class 0 OID 0)
-- Dependencies: 193
-- Name: doc_period_doc_period_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE doc_period_doc_period_id_seq OWNED BY doc_period.doc_period_id;


--
-- TOC entry 194 (class 1259 OID 156516)
-- Name: doc_state; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE doc_state (
    doc_state_id integer NOT NULL,
    doc_state_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    doc_state_name character varying
);


ALTER TABLE doc_state OWNER TO pavlov;

--
-- TOC entry 2589 (class 0 OID 0)
-- Dependencies: 194
-- Name: TABLE doc_state; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE doc_state IS 'Справочник статусов док-тов';


--
-- TOC entry 195 (class 1259 OID 156523)
-- Name: doc_state_doc_state_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE doc_state_doc_state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE doc_state_doc_state_id_seq OWNER TO pavlov;

--
-- TOC entry 2590 (class 0 OID 0)
-- Dependencies: 195
-- Name: doc_state_doc_state_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE doc_state_doc_state_id_seq OWNED BY doc_state.doc_state_id;


--
-- TOC entry 196 (class 1259 OID 156525)
-- Name: doc_type; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE doc_type (
    doc_type_id integer NOT NULL,
    doc_type_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    doc_type_name character varying
);


ALTER TABLE doc_type OWNER TO pavlov;

--
-- TOC entry 2591 (class 0 OID 0)
-- Dependencies: 196
-- Name: TABLE doc_type; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE doc_type IS 'Справочник типов док-тов';


--
-- TOC entry 197 (class 1259 OID 156532)
-- Name: doc_type_doc_type_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE doc_type_doc_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE doc_type_doc_type_id_seq OWNER TO pavlov;

--
-- TOC entry 2592 (class 0 OID 0)
-- Dependencies: 197
-- Name: doc_type_doc_type_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE doc_type_doc_type_id_seq OWNED BY doc_type.doc_type_id;


--
-- TOC entry 198 (class 1259 OID 156534)
-- Name: dosage; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE dosage (
    dosage_id integer NOT NULL,
    dosage_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    dosage_name character varying NOT NULL
);


ALTER TABLE dosage OWNER TO pavlov;

--
-- TOC entry 2593 (class 0 OID 0)
-- Dependencies: 198
-- Name: TABLE dosage; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE dosage IS 'Справочник дозировок';


--
-- TOC entry 199 (class 1259 OID 156541)
-- Name: dosage_dosage_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE dosage_dosage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dosage_dosage_id_seq OWNER TO pavlov;

--
-- TOC entry 2594 (class 0 OID 0)
-- Dependencies: 199
-- Name: dosage_dosage_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE dosage_dosage_id_seq OWNED BY dosage.dosage_id;


--
-- TOC entry 200 (class 1259 OID 156543)
-- Name: form; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE form (
    form_id integer NOT NULL,
    form_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    form_name character varying NOT NULL
);


ALTER TABLE form OWNER TO pavlov;

--
-- TOC entry 2595 (class 0 OID 0)
-- Dependencies: 200
-- Name: TABLE form; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE form IS 'Справочник форм выпуска';


--
-- TOC entry 201 (class 1259 OID 156550)
-- Name: form_form_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE form_form_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE form_form_id_seq OWNER TO pavlov;

--
-- TOC entry 2596 (class 0 OID 0)
-- Dependencies: 201
-- Name: form_form_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE form_form_id_seq OWNED BY form.form_id;


--
-- TOC entry 202 (class 1259 OID 156552)
-- Name: global_id_sequence; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE global_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE global_id_sequence OWNER TO pavlov;

--
-- TOC entry 203 (class 1259 OID 156554)
-- Name: good; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE good (
    good_id bigint NOT NULL,
    good_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_id bigint NOT NULL,
    mfr_id bigint NOT NULL,
    barcode character varying,
    gtin character varying,
    crt_date timestamp without time zone,
    crt_user character varying,
    upd_date timestamp without time zone,
    upd_user character varying,
    is_deleted boolean DEFAULT false NOT NULL,
    del_date timestamp without time zone,
    del_user character varying    
);


ALTER TABLE good OWNER TO pavlov;

--
-- TOC entry 2597 (class 0 OID 0)
-- Dependencies: 203
-- Name: TABLE good; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE good IS 'Товары';


--
-- TOC entry 2598 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.good_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.good_id IS 'PK';


--
-- TOC entry 2599 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.good_uuid; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.good_uuid IS 'BK';


--
-- TOC entry 2600 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.product_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.product_id IS 'FK код препарата';


--
-- TOC entry 2601 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.mfr_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.mfr_id IS 'FK код основного производителя';


--
-- TOC entry 2602 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.barcode; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.barcode IS 'штрих-код';


--
-- TOC entry 2603 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.crt_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.crt_date IS 'когда создана запись';


--
-- TOC entry 2604 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.crt_user; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.crt_user IS 'кем создана запись';


--
-- TOC entry 2605 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.upd_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.upd_date IS 'когда изменена запись';


--
-- TOC entry 2606 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.is_deleted; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.is_deleted IS 'запись удалена';


--
-- TOC entry 2607 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.del_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.del_date IS 'когда удалена запись';


--
-- TOC entry 2608 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.del_user; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.del_user IS 'кем удалена запись';


--
-- TOC entry 2609 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN good.gtin; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good.gtin IS 'GTIN';


--
-- TOC entry 204 (class 1259 OID 156562)
-- Name: good_defect; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE good_defect (
    good_defect_id bigint NOT NULL,
    good_defect_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    good_id bigint NOT NULL,
    defect_series character varying
);


ALTER TABLE good_defect OWNER TO pavlov;

--
-- TOC entry 2610 (class 0 OID 0)
-- Dependencies: 204
-- Name: TABLE good_defect; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE good_defect IS 'Забракованные серии товара';


--
-- TOC entry 205 (class 1259 OID 156569)
-- Name: good_defect_good_defect_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE good_defect_good_defect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE good_defect_good_defect_id_seq OWNER TO pavlov;

--
-- TOC entry 2611 (class 0 OID 0)
-- Dependencies: 205
-- Name: good_defect_good_defect_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE good_defect_good_defect_id_seq OWNED BY good_defect.good_defect_id;


--
-- TOC entry 206 (class 1259 OID 156571)
-- Name: good_good_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE good_good_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE good_good_id_seq OWNER TO pavlov;

--
-- TOC entry 2612 (class 0 OID 0)
-- Dependencies: 206
-- Name: good_good_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE good_good_id_seq OWNED BY good.good_id;


--
-- TOC entry 207 (class 1259 OID 156573)
-- Name: good_mfr; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE good_mfr (
    good_mfr_id bigint NOT NULL,
    good_mfr_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    good_id bigint NOT NULL,
    mfr_id bigint NOT NULL,
    mfr_role_id integer
);


ALTER TABLE good_mfr OWNER TO pavlov;

--
-- TOC entry 2613 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE good_mfr; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE good_mfr IS 'Производители товара (по стадиям производства)';


--
-- TOC entry 2614 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN good_mfr.good_mfr_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good_mfr.good_mfr_id IS 'PK';


--
-- TOC entry 2615 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN good_mfr.good_mfr_uuid; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good_mfr.good_mfr_uuid IS 'BK';


--
-- TOC entry 2616 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN good_mfr.good_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good_mfr.good_id IS 'FK код товара';


--
-- TOC entry 2617 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN good_mfr.mfr_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good_mfr.mfr_id IS 'FK код производителя';


--
-- TOC entry 2618 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN good_mfr.mfr_role_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN good_mfr.mfr_role_id IS 'FK код роли производителя';


--
-- TOC entry 208 (class 1259 OID 156577)
-- Name: good_mfr_good_mfr_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE good_mfr_good_mfr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE good_mfr_good_mfr_id_seq OWNER TO pavlov;

--
-- TOC entry 2619 (class 0 OID 0)
-- Dependencies: 208
-- Name: good_mfr_good_mfr_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE good_mfr_good_mfr_id_seq OWNED BY good_mfr.good_mfr_id;


--
-- TOC entry 209 (class 1259 OID 156579)
-- Name: good_register_price; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE good_register_price (
    good_register_price_id bigint NOT NULL,
    good_register_price_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    good_id bigint NOT NULL,
    register_price numeric
);


ALTER TABLE good_register_price OWNER TO pavlov;

--
-- TOC entry 2620 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE good_register_price; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE good_register_price IS 'Цены гос.реестра товара';


--
-- TOC entry 210 (class 1259 OID 156586)
-- Name: good_register_price_good_register_price_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE good_register_price_good_register_price_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE good_register_price_good_register_price_id_seq OWNER TO pavlov;

--
-- TOC entry 2621 (class 0 OID 0)
-- Dependencies: 210
-- Name: good_register_price_good_register_price_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE good_register_price_good_register_price_id_seq OWNED BY good_register_price.good_register_price_id;


--
-- TOC entry 211 (class 1259 OID 156588)
-- Name: inn; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE inn (
    inn_id bigint NOT NULL,
    inn_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    inn_name character varying NOT NULL
);


ALTER TABLE inn OWNER TO pavlov;

--
-- TOC entry 2622 (class 0 OID 0)
-- Dependencies: 211
-- Name: TABLE inn; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE inn IS 'Справочник МНН';


--
-- TOC entry 212 (class 1259 OID 156595)
-- Name: inn_inn_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE inn_inn_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inn_inn_id_seq OWNER TO pavlov;

--
-- TOC entry 2623 (class 0 OID 0)
-- Dependencies: 212
-- Name: inn_inn_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE inn_inn_id_seq OWNED BY inn.inn_id;


--
-- TOC entry 213 (class 1259 OID 156597)
-- Name: metaname; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE metaname (
    metaname_id bigint NOT NULL,
    metaname_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    metaname_name character varying NOT NULL,
    crt_date timestamp without time zone,
    crt_user character varying,
    upd_date timestamp without time zone,
    upd_user character varying,
    is_deleted boolean DEFAULT false NOT NULL,
    del_date timestamp without time zone,
    del_user character varying
);


ALTER TABLE metaname OWNER TO pavlov;

--
-- TOC entry 2624 (class 0 OID 0)
-- Dependencies: 213
-- Name: TABLE metaname; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE metaname IS 'Сгруппированные ("идеальные") наименования продуктов';


--
-- TOC entry 214 (class 1259 OID 156605)
-- Name: metaname_metaname_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE metaname_metaname_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metaname_metaname_id_seq OWNER TO pavlov;

--
-- TOC entry 2625 (class 0 OID 0)
-- Dependencies: 214
-- Name: metaname_metaname_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE metaname_metaname_id_seq OWNED BY metaname.metaname_id;


--
-- TOC entry 215 (class 1259 OID 156607)
-- Name: mfr; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE mfr (
    mfr_id bigint NOT NULL,
    mfr_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    mfr_name character varying NOT NULL,
    country_id integer,
    labeling_code character varying
);


ALTER TABLE mfr OWNER TO pavlov;

--
-- TOC entry 2626 (class 0 OID 0)
-- Dependencies: 215
-- Name: TABLE mfr; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE mfr IS 'Производители';


--
-- TOC entry 2627 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN mfr.labeling_code; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN mfr.labeling_code IS 'код в программе маркировки';


--
-- TOC entry 216 (class 1259 OID 156614)
-- Name: mfr_alias; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE mfr_alias (
    mfr_alias_id bigint NOT NULL,
    mfr_alias_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    mfr_id bigint NOT NULL,
    mfr_alias_name character varying NOT NULL
);


ALTER TABLE mfr_alias OWNER TO pavlov;

--
-- TOC entry 2628 (class 0 OID 0)
-- Dependencies: 216
-- Name: TABLE mfr_alias; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE mfr_alias IS 'Синонимы производителей';


--
-- TOC entry 217 (class 1259 OID 156621)
-- Name: mfr_alias_mfr_alias_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE mfr_alias_mfr_alias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mfr_alias_mfr_alias_id_seq OWNER TO pavlov;

--
-- TOC entry 2629 (class 0 OID 0)
-- Dependencies: 217
-- Name: mfr_alias_mfr_alias_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE mfr_alias_mfr_alias_id_seq OWNED BY mfr_alias.mfr_alias_id;


--
-- TOC entry 218 (class 1259 OID 156623)
-- Name: mfr_mfr_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE mfr_mfr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mfr_mfr_id_seq OWNER TO pavlov;

--
-- TOC entry 2630 (class 0 OID 0)
-- Dependencies: 218
-- Name: mfr_mfr_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE mfr_mfr_id_seq OWNED BY mfr.mfr_id;


--
-- TOC entry 219 (class 1259 OID 156625)
-- Name: mfr_role; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE mfr_role (
    mfr_role_id integer NOT NULL,
    mfr_role_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    mfr_role_name character varying NOT NULL
);


ALTER TABLE mfr_role OWNER TO pavlov;

--
-- TOC entry 2631 (class 0 OID 0)
-- Dependencies: 219
-- Name: TABLE mfr_role; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE mfr_role IS 'Справочник ролей производителей';


--
-- TOC entry 220 (class 1259 OID 156632)
-- Name: okpd2; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE okpd2 (
    okpd2_id integer NOT NULL,
    okpd2_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    okpd2_name character varying NOT NULL
);


ALTER TABLE okpd2 OWNER TO pavlov;

--
-- TOC entry 2632 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE okpd2; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE okpd2 IS 'Справочник ОКПД-2';


--
-- TOC entry 221 (class 1259 OID 156639)
-- Name: okpd2_okpd2_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE okpd2_okpd2_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE okpd2_okpd2_id_seq OWNER TO pavlov;

--
-- TOC entry 2633 (class 0 OID 0)
-- Dependencies: 221
-- Name: okpd2_okpd2_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE okpd2_okpd2_id_seq OWNED BY okpd2.okpd2_id;


--
-- TOC entry 222 (class 1259 OID 156641)
-- Name: pack; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE pack (
    pack_id integer NOT NULL,
    pack_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    pack_name character varying NOT NULL
);


ALTER TABLE pack OWNER TO pavlov;

--
-- TOC entry 2634 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE pack; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE pack IS 'Справочник упаковок';


--
-- TOC entry 223 (class 1259 OID 156648)
-- Name: pack_pack_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE pack_pack_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pack_pack_id_seq OWNER TO pavlov;

--
-- TOC entry 2635 (class 0 OID 0)
-- Dependencies: 223
-- Name: pack_pack_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE pack_pack_id_seq OWNED BY pack.pack_id;


--
-- TOC entry 224 (class 1259 OID 156650)
-- Name: pharm_group; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE pharm_group (
    pharm_group_id integer NOT NULL,
    pharm_group_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    pharm_group_name character varying NOT NULL
);


ALTER TABLE pharm_group OWNER TO pavlov;

--
-- TOC entry 2636 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE pharm_group; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE pharm_group IS 'Справочник фармацевтических групп';


--
-- TOC entry 225 (class 1259 OID 156657)
-- Name: pharm_group_pharm_group_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE pharm_group_pharm_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pharm_group_pharm_group_id_seq OWNER TO pavlov;

--
-- TOC entry 2637 (class 0 OID 0)
-- Dependencies: 225
-- Name: pharm_group_pharm_group_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE pharm_group_pharm_group_id_seq OWNED BY pharm_group.pharm_group_id;


--
-- TOC entry 226 (class 1259 OID 156659)
-- Name: pharm_ther_group; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE pharm_ther_group (
    pharm_ther_group_id integer NOT NULL,
    pharm_ther_group_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    pharm_ther_group_name character varying
);


ALTER TABLE pharm_ther_group OWNER TO pavlov;

--
-- TOC entry 2638 (class 0 OID 0)
-- Dependencies: 226
-- Name: TABLE pharm_ther_group; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE pharm_ther_group IS 'Справочник фармако-терапевтических групп';


--
-- TOC entry 227 (class 1259 OID 156666)
-- Name: pharm_ther_group_pharm_ther_group_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE pharm_ther_group_pharm_ther_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pharm_ther_group_pharm_ther_group_id_seq OWNER TO pavlov;

--
-- TOC entry 2639 (class 0 OID 0)
-- Dependencies: 227
-- Name: pharm_ther_group_pharm_ther_group_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE pharm_ther_group_pharm_ther_group_id_seq OWNED BY pharm_ther_group.pharm_ther_group_id;


--
-- TOC entry 228 (class 1259 OID 156668)
-- Name: product; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product (
    product_id bigint NOT NULL,
    product_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_name character varying NOT NULL,
    product_type_id integer NOT NULL,
    inn_id bigint,
    atc_id bigint,
    dosage_id integer,
    form_id integer,
    pharm_ther_group_id integer,
    okpd2_id integer,
    brand_id bigint,
    recipe_type_id integer,
    registry_id bigint,
    baa_type_id integer,
    metaname_id bigint,
    storage_period character varying,
    storage_condition character varying,
    is_vital boolean,
    is_drug2 boolean,
    is_drug3 boolean,
    is_precursor boolean,
    is_alcohol boolean,
    is_imm_bio boolean,
    is_mandatory boolean,
    is_defect_exists boolean,
    indication character varying,
    contraindication character varying,
    drug_interaction character varying,
    crt_date timestamp without time zone,
    crt_user character varying,
    upd_date timestamp without time zone,
    upd_user character varying,
    is_deleted boolean DEFAULT false NOT NULL,
    del_date timestamp without time zone,
    del_user character varying
);


ALTER TABLE product OWNER TO pavlov;

--
-- TOC entry 2640 (class 0 OID 0)
-- Dependencies: 228
-- Name: TABLE product; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product IS 'Продукты';


--
-- TOC entry 2641 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.product_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.product_id IS 'PK';


--
-- TOC entry 2642 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.product_uuid; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.product_uuid IS 'BK';


--
-- TOC entry 2643 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.product_name; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.product_name IS 'торговое наименование';


--
-- TOC entry 2644 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.product_type_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.product_type_id IS 'FK тип продукта';


--
-- TOC entry 2645 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.inn_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.inn_id IS 'FK МНН';


--
-- TOC entry 2646 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.atc_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.atc_id IS 'FK АТХ';


--
-- TOC entry 2647 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.dosage_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.dosage_id IS 'FK дозировка';


--
-- TOC entry 2648 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.form_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.form_id IS 'FK форма выпуска';


--
-- TOC entry 2649 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.pharm_ther_group_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.pharm_ther_group_id IS 'FK фармако-терапевтическая группа';


--
-- TOC entry 2650 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.okpd2_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.okpd2_id IS 'FK ОКПД-2';


--
-- TOC entry 2651 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.brand_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.brand_id IS 'FK брэнд';


--
-- TOC entry 2652 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.recipe_type_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.recipe_type_id IS 'FK тип рецепта';


--
-- TOC entry 2653 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.registry_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.registry_id IS 'FK свидетельство о регистрации';


--
-- TOC entry 2654 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.baa_type_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.baa_type_id IS 'FK тип БАД';


--
-- TOC entry 2655 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.metaname_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.metaname_id IS 'FK сгруппированное наименование';


--
-- TOC entry 2656 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.storage_period; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.storage_period IS 'срок хранения';


--
-- TOC entry 2657 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.storage_condition; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.storage_condition IS 'условия хранения';


--
-- TOC entry 2658 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.is_vital; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.is_vital IS 'ЖВ';


--
-- TOC entry 2659 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.is_drug2; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.is_drug2 IS 'наркотик списка 2';


--
-- TOC entry 2660 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.is_drug3; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.is_drug3 IS 'наркотик списка 3';


--
-- TOC entry 2661 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.is_precursor; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.is_precursor IS 'прекурсор';


--
-- TOC entry 2662 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.is_alcohol; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.is_alcohol IS 'спиртосодержащее';


--
-- TOC entry 2663 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.is_imm_bio; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.is_imm_bio IS 'имунно-биологический';


--
-- TOC entry 2664 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.is_mandatory; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.is_mandatory IS 'обязательный ассортимент';


--
-- TOC entry 2665 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.is_defect_exists; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.is_defect_exists IS 'есть бракованные серии';


--
-- TOC entry 2666 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.indication; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.indication IS 'показания';


--
-- TOC entry 2667 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.contraindication; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.contraindication IS 'противопоказания';


--
-- TOC entry 2668 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.drug_interaction; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.drug_interaction IS 'лекарственное взаимодействие';


--
-- TOC entry 2669 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.crt_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.crt_date IS 'когда создана запись';


--
-- TOC entry 2670 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.crt_user; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.crt_user IS 'кем создана запись';


--
-- TOC entry 2671 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.upd_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.upd_date IS 'когда изменена запись';


--
-- TOC entry 2672 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.is_deleted; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.is_deleted IS 'запись удалена';


--
-- TOC entry 2673 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.del_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.del_date IS 'когда удалена запись';


--
-- TOC entry 2674 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN product.del_user; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN product.del_user IS 'кем удалена запись';


--
-- TOC entry 229 (class 1259 OID 156676)
-- Name: product_adjacent; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product_adjacent (
    product_adjacent_id bigint NOT NULL,
    product_adjacent_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_id bigint NOT NULL,
    adjacent_id bigint NOT NULL
);


ALTER TABLE product_adjacent OWNER TO pavlov;

--
-- TOC entry 2675 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE product_adjacent; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product_adjacent IS 'Сопутствующие товары';


--
-- TOC entry 230 (class 1259 OID 156680)
-- Name: product_adjacent_product_adjacent_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE product_adjacent_product_adjacent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_adjacent_product_adjacent_id_seq OWNER TO pavlov;

--
-- TOC entry 2676 (class 0 OID 0)
-- Dependencies: 230
-- Name: product_adjacent_product_adjacent_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE product_adjacent_product_adjacent_id_seq OWNED BY product_adjacent.product_adjacent_id;


--
-- TOC entry 231 (class 1259 OID 156682)
-- Name: product_consumer; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product_consumer (
    product_consumer_id bigint NOT NULL,
    product_consumer_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_id bigint NOT NULL,
    consumer_id integer NOT NULL
);


ALTER TABLE product_consumer OWNER TO pavlov;

--
-- TOC entry 2677 (class 0 OID 0)
-- Dependencies: 231
-- Name: TABLE product_consumer; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product_consumer IS 'Группы потребителей препарата';


--
-- TOC entry 232 (class 1259 OID 156686)
-- Name: product_consumer_product_consumer_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE product_consumer_product_consumer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_consumer_product_consumer_id_seq OWNER TO pavlov;

--
-- TOC entry 2678 (class 0 OID 0)
-- Dependencies: 232
-- Name: product_consumer_product_consumer_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE product_consumer_product_consumer_id_seq OWNED BY product_consumer.product_consumer_id;


--
-- TOC entry 233 (class 1259 OID 156688)
-- Name: product_mkb10; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product_mkb10 (
    product_mkb10_id bigint NOT NULL,
    product_mkb10_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_id bigint NOT NULL,
    mkb10_code character varying
);


ALTER TABLE product_mkb10 OWNER TO pavlov;

--
-- TOC entry 2679 (class 0 OID 0)
-- Dependencies: 233
-- Name: TABLE product_mkb10; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product_mkb10 IS 'Классификация препарата по МКБ-10';


--
-- TOC entry 234 (class 1259 OID 156695)
-- Name: product_mkb10_product_mkb10_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE product_mkb10_product_mkb10_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_mkb10_product_mkb10_id_seq OWNER TO pavlov;

--
-- TOC entry 2680 (class 0 OID 0)
-- Dependencies: 234
-- Name: product_mkb10_product_mkb10_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE product_mkb10_product_mkb10_id_seq OWNED BY product_mkb10.product_mkb10_id;


--
-- TOC entry 235 (class 1259 OID 156697)
-- Name: product_normative; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product_normative (
    product_normative_id bigint NOT NULL,
    product_normative_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_id bigint NOT NULL,
    doc_num character varying,
    doc_year integer,
    doc_change_num character varying
);


ALTER TABLE product_normative OWNER TO pavlov;

--
-- TOC entry 2681 (class 0 OID 0)
-- Dependencies: 235
-- Name: TABLE product_normative; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product_normative IS 'Нормативная документация препарата';


--
-- TOC entry 236 (class 1259 OID 156704)
-- Name: product_normative_product_normative_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE product_normative_product_normative_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_normative_product_normative_id_seq OWNER TO pavlov;

--
-- TOC entry 2682 (class 0 OID 0)
-- Dependencies: 236
-- Name: product_normative_product_normative_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE product_normative_product_normative_id_seq OWNED BY product_normative.product_normative_id;


--
-- TOC entry 237 (class 1259 OID 156706)
-- Name: product_pack; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product_pack (
    product_pack_id bigint NOT NULL,
    product_pack_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_id bigint NOT NULL,
    pack_id integer NOT NULL
);


ALTER TABLE product_pack OWNER TO pavlov;

--
-- TOC entry 2683 (class 0 OID 0)
-- Dependencies: 237
-- Name: TABLE product_pack; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product_pack IS 'Упаковки препарата';


--
-- TOC entry 238 (class 1259 OID 156710)
-- Name: product_pack_product_pack_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE product_pack_product_pack_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_pack_product_pack_id_seq OWNER TO pavlov;

--
-- TOC entry 2684 (class 0 OID 0)
-- Dependencies: 238
-- Name: product_pack_product_pack_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE product_pack_product_pack_id_seq OWNED BY product_pack.product_pack_id;


--
-- TOC entry 239 (class 1259 OID 156712)
-- Name: product_pharm_group; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product_pharm_group (
    product_pharm_group_id bigint NOT NULL,
    product_pharm_group_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_id bigint NOT NULL,
    pharm_group_id integer NOT NULL
);


ALTER TABLE product_pharm_group OWNER TO pavlov;

--
-- TOC entry 2685 (class 0 OID 0)
-- Dependencies: 239
-- Name: TABLE product_pharm_group; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product_pharm_group IS 'Фармацевтические группы препарата';


--
-- TOC entry 240 (class 1259 OID 156716)
-- Name: product_pharm_group_product_pharm_group_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE product_pharm_group_product_pharm_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_pharm_group_product_pharm_group_id_seq OWNER TO pavlov;

--
-- TOC entry 2686 (class 0 OID 0)
-- Dependencies: 240
-- Name: product_pharm_group_product_pharm_group_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE product_pharm_group_product_pharm_group_id_seq OWNED BY product_pharm_group.product_pharm_group_id;


--
-- TOC entry 241 (class 1259 OID 156718)
-- Name: product_product_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE product_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_product_id_seq OWNER TO pavlov;

--
-- TOC entry 2687 (class 0 OID 0)
-- Dependencies: 241
-- Name: product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE product_product_id_seq OWNED BY product.product_id;


--
-- TOC entry 242 (class 1259 OID 156720)
-- Name: product_substance; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product_substance (
    product_substance_id bigint NOT NULL,
    product_substance_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_id bigint NOT NULL,
    substance_id bigint NOT NULL
);


ALTER TABLE product_substance OWNER TO pavlov;

--
-- TOC entry 2688 (class 0 OID 0)
-- Dependencies: 242
-- Name: TABLE product_substance; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product_substance IS 'Субстанции, входящие в препарата';


--
-- TOC entry 243 (class 1259 OID 156724)
-- Name: product_substance_product_substance_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE product_substance_product_substance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_substance_product_substance_id_seq OWNER TO pavlov;

--
-- TOC entry 2689 (class 0 OID 0)
-- Dependencies: 243
-- Name: product_substance_product_substance_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE product_substance_product_substance_id_seq OWNED BY product_substance.product_substance_id;


--
-- TOC entry 244 (class 1259 OID 156726)
-- Name: product_type; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product_type (
    product_type_id integer NOT NULL,
    product_type_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_type_name character varying NOT NULL
);


ALTER TABLE product_type OWNER TO pavlov;

--
-- TOC entry 2690 (class 0 OID 0)
-- Dependencies: 244
-- Name: TABLE product_type; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product_type IS 'Типы препарата (ЛС, БАД, ИМН, прочее)';


--
-- TOC entry 245 (class 1259 OID 156733)
-- Name: product_variant; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE product_variant (
    product_variant_id bigint NOT NULL,
    product_variant_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    product_id bigint NOT NULL,
    variant_id bigint NOT NULL
);


ALTER TABLE product_variant OWNER TO pavlov;

--
-- TOC entry 2691 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE product_variant; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE product_variant IS 'Товары на замену';


--
-- TOC entry 246 (class 1259 OID 156737)
-- Name: product_variant_product_variant_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE product_variant_product_variant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_variant_product_variant_id_seq OWNER TO pavlov;

--
-- TOC entry 2692 (class 0 OID 0)
-- Dependencies: 246
-- Name: product_variant_product_variant_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE product_variant_product_variant_id_seq OWNED BY product_variant.product_variant_id;


--
-- TOC entry 247 (class 1259 OID 156739)
-- Name: recipe_type; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE recipe_type (
    recipe_type_id integer NOT NULL,
    recipe_type_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    recipe_type_name character varying NOT NULL
);


ALTER TABLE recipe_type OWNER TO pavlov;

--
-- TOC entry 2693 (class 0 OID 0)
-- Dependencies: 247
-- Name: TABLE recipe_type; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE recipe_type IS 'Справочник типов рецептов';


--
-- TOC entry 248 (class 1259 OID 156746)
-- Name: registry; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE registry (
    registry_id bigint NOT NULL,
    registry_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    registry_num character varying NOT NULL,
    registry_date date NOT NULL,
    renewal_date date,
    mfr_id bigint
);


ALTER TABLE registry OWNER TO pavlov;

--
-- TOC entry 2694 (class 0 OID 0)
-- Dependencies: 248
-- Name: TABLE registry; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE registry IS 'Свидетельство о регистрации';


--
-- TOC entry 2695 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN registry.registry_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN registry.registry_id IS 'PK';


--
-- TOC entry 2696 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN registry.registry_uuid; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN registry.registry_uuid IS 'BK';


--
-- TOC entry 2697 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN registry.registry_num; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN registry.registry_num IS '№ рег. удостоверения';


--
-- TOC entry 2698 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN registry.registry_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN registry.registry_date IS 'дата РУ';


--
-- TOC entry 2699 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN registry.renewal_date; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN registry.renewal_date IS 'дата переоформления РУ';


--
-- TOC entry 2700 (class 0 OID 0)
-- Dependencies: 248
-- Name: COLUMN registry.mfr_id; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN registry.mfr_id IS 'FK владелец РУ';


--
-- TOC entry 249 (class 1259 OID 156753)
-- Name: registry_registry_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE registry_registry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE registry_registry_id_seq OWNER TO pavlov;

--
-- TOC entry 2701 (class 0 OID 0)
-- Dependencies: 249
-- Name: registry_registry_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE registry_registry_id_seq OWNED BY registry.registry_id;


--
-- TOC entry 250 (class 1259 OID 156755)
-- Name: service; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE service (
    service_id integer NOT NULL,
    service_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    service_name character varying,
    service_descr character varying,
    doc_type_id integer NOT NULL,
    service_logic text
);


ALTER TABLE service OWNER TO pavlov;

--
-- TOC entry 2702 (class 0 OID 0)
-- Dependencies: 250
-- Name: TABLE service; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE service IS 'Типы операций над типами документов';


--
-- TOC entry 251 (class 1259 OID 156762)
-- Name: service_service_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE service_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE service_service_id_seq OWNER TO pavlov;

--
-- TOC entry 2703 (class 0 OID 0)
-- Dependencies: 251
-- Name: service_service_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE service_service_id_seq OWNED BY service.service_id;


--
-- TOC entry 252 (class 1259 OID 156764)
-- Name: substance; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE substance (
    substance_id bigint NOT NULL,
    substance_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    substance_name character varying NOT NULL,
    substance_trade_name character varying NOT NULL,
    storage_period character varying,
    storage_condition character varying,
    doc_name character varying,
    is_drug boolean,
    mfr_id bigint
);


ALTER TABLE substance OWNER TO pavlov;

--
-- TOC entry 2704 (class 0 OID 0)
-- Dependencies: 252
-- Name: TABLE substance; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE substance IS 'Справочник субстанций';


--
-- TOC entry 253 (class 1259 OID 156771)
-- Name: substance_substance_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE substance_substance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE substance_substance_id_seq OWNER TO pavlov;

--
-- TOC entry 2705 (class 0 OID 0)
-- Dependencies: 253
-- Name: substance_substance_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE substance_substance_id_seq OWNED BY substance.substance_id;


--
-- TOC entry 254 (class 1259 OID 156773)
-- Name: supplier; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE supplier (
    supplier_id integer NOT NULL,
    supplier_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    supplier_name character varying NOT NULL,
    labeling_code character varying
);


ALTER TABLE supplier OWNER TO pavlov;

--
-- TOC entry 2706 (class 0 OID 0)
-- Dependencies: 254
-- Name: TABLE supplier; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE supplier IS 'Справочник поставщиков';


--
-- TOC entry 2707 (class 0 OID 0)
-- Dependencies: 254
-- Name: COLUMN supplier.labeling_code; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON COLUMN supplier.labeling_code IS 'код в программе маркировки';


--
-- TOC entry 255 (class 1259 OID 156780)
-- Name: supplier_good; Type: TABLE; Schema: main; Owner: pavlov; Tablespace: 
--

CREATE TABLE supplier_good (
    supplier_good_id integer NOT NULL,
    supplier_good_uuid uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    supplier_id integer NOT NULL,
    good_id bigint NOT NULL,
    price numeric,
    quantity numeric
);


ALTER TABLE supplier_good OWNER TO pavlov;

--
-- TOC entry 2708 (class 0 OID 0)
-- Dependencies: 255
-- Name: TABLE supplier_good; Type: COMMENT; Schema: main; Owner: pavlov
--

COMMENT ON TABLE supplier_good IS 'Товары в наличии у поставщиков';


--
-- TOC entry 256 (class 1259 OID 156787)
-- Name: supplier_good_supplier_good_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE supplier_good_supplier_good_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE supplier_good_supplier_good_id_seq OWNER TO pavlov;

--
-- TOC entry 2709 (class 0 OID 0)
-- Dependencies: 256
-- Name: supplier_good_supplier_good_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE supplier_good_supplier_good_id_seq OWNED BY supplier_good.supplier_good_id;


--
-- TOC entry 257 (class 1259 OID 156789)
-- Name: supplier_supplier_id_seq; Type: SEQUENCE; Schema: main; Owner: pavlov
--

CREATE SEQUENCE supplier_supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE supplier_supplier_id_seq OWNER TO pavlov;

--
-- TOC entry 2710 (class 0 OID 0)
-- Dependencies: 257
-- Name: supplier_supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: pavlov
--

ALTER SEQUENCE supplier_supplier_id_seq OWNED BY supplier.supplier_id;


--
-- TOC entry 2181 (class 2604 OID 156791)
-- Name: atc_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY atc ALTER COLUMN atc_id SET DEFAULT nextval('atc_atc_id_seq'::regclass);


--
-- TOC entry 2184 (class 2604 OID 156792)
-- Name: brand_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY brand ALTER COLUMN brand_id SET DEFAULT nextval('brand_brand_id_seq'::regclass);


--
-- TOC entry 2186 (class 2604 OID 156793)
-- Name: client_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client ALTER COLUMN client_id SET DEFAULT nextval('client_client_id_seq'::regclass);


--
-- TOC entry 2188 (class 2604 OID 156794)
-- Name: client_doc_type_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client_doc_type ALTER COLUMN client_doc_type_id SET DEFAULT nextval('client_doc_type_client_doc_type_id_seq'::regclass);


--
-- TOC entry 2191 (class 2604 OID 156795)
-- Name: client_good_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client_good ALTER COLUMN client_good_id SET DEFAULT nextval('client_good_client_good_id_seq'::regclass);


--
-- TOC entry 2193 (class 2604 OID 156796)
-- Name: client_service_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client_service ALTER COLUMN client_service_id SET DEFAULT nextval('client_service_client_service_id_seq'::regclass);


--
-- TOC entry 2195 (class 2604 OID 156797)
-- Name: consumer_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY consumer ALTER COLUMN consumer_id SET DEFAULT nextval('consumer_consumer_id_seq'::regclass);


--
-- TOC entry 2197 (class 2604 OID 156798)
-- Name: country_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY country ALTER COLUMN country_id SET DEFAULT nextval('country_country_id_seq'::regclass);


--
-- TOC entry 2200 (class 2604 OID 156799)
-- Name: doc_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc ALTER COLUMN doc_id SET DEFAULT nextval('doc_doc_id_seq'::regclass);


--
-- TOC entry 2202 (class 2604 OID 156800)
-- Name: doc_period_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc_period ALTER COLUMN doc_period_id SET DEFAULT nextval('doc_period_doc_period_id_seq'::regclass);


--
-- TOC entry 2204 (class 2604 OID 156801)
-- Name: doc_state_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc_state ALTER COLUMN doc_state_id SET DEFAULT nextval('doc_state_doc_state_id_seq'::regclass);


--
-- TOC entry 2206 (class 2604 OID 156802)
-- Name: doc_type_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc_type ALTER COLUMN doc_type_id SET DEFAULT nextval('doc_type_doc_type_id_seq'::regclass);


--
-- TOC entry 2208 (class 2604 OID 156803)
-- Name: dosage_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY dosage ALTER COLUMN dosage_id SET DEFAULT nextval('dosage_dosage_id_seq'::regclass);


--
-- TOC entry 2210 (class 2604 OID 156804)
-- Name: form_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY form ALTER COLUMN form_id SET DEFAULT nextval('form_form_id_seq'::regclass);


--
-- TOC entry 2213 (class 2604 OID 156805)
-- Name: good_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good ALTER COLUMN good_id SET DEFAULT nextval('good_good_id_seq'::regclass);


--
-- TOC entry 2215 (class 2604 OID 156806)
-- Name: good_defect_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good_defect ALTER COLUMN good_defect_id SET DEFAULT nextval('good_defect_good_defect_id_seq'::regclass);


--
-- TOC entry 2217 (class 2604 OID 156807)
-- Name: good_mfr_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good_mfr ALTER COLUMN good_mfr_id SET DEFAULT nextval('good_mfr_good_mfr_id_seq'::regclass);


--
-- TOC entry 2219 (class 2604 OID 156808)
-- Name: good_register_price_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good_register_price ALTER COLUMN good_register_price_id SET DEFAULT nextval('good_register_price_good_register_price_id_seq'::regclass);


--
-- TOC entry 2221 (class 2604 OID 156809)
-- Name: inn_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY inn ALTER COLUMN inn_id SET DEFAULT nextval('inn_inn_id_seq'::regclass);


--
-- TOC entry 2224 (class 2604 OID 156810)
-- Name: metaname_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY metaname ALTER COLUMN metaname_id SET DEFAULT nextval('metaname_metaname_id_seq'::regclass);


--
-- TOC entry 2226 (class 2604 OID 156811)
-- Name: mfr_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY mfr ALTER COLUMN mfr_id SET DEFAULT nextval('mfr_mfr_id_seq'::regclass);


--
-- TOC entry 2228 (class 2604 OID 156812)
-- Name: mfr_alias_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY mfr_alias ALTER COLUMN mfr_alias_id SET DEFAULT nextval('mfr_alias_mfr_alias_id_seq'::regclass);


--
-- TOC entry 2231 (class 2604 OID 156813)
-- Name: okpd2_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY okpd2 ALTER COLUMN okpd2_id SET DEFAULT nextval('okpd2_okpd2_id_seq'::regclass);


--
-- TOC entry 2233 (class 2604 OID 156814)
-- Name: pack_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY pack ALTER COLUMN pack_id SET DEFAULT nextval('pack_pack_id_seq'::regclass);


--
-- TOC entry 2235 (class 2604 OID 156815)
-- Name: pharm_group_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY pharm_group ALTER COLUMN pharm_group_id SET DEFAULT nextval('pharm_group_pharm_group_id_seq'::regclass);


--
-- TOC entry 2237 (class 2604 OID 156816)
-- Name: pharm_ther_group_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY pharm_ther_group ALTER COLUMN pharm_ther_group_id SET DEFAULT nextval('pharm_ther_group_pharm_ther_group_id_seq'::regclass);


--
-- TOC entry 2240 (class 2604 OID 156817)
-- Name: product_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product ALTER COLUMN product_id SET DEFAULT nextval('product_product_id_seq'::regclass);


--
-- TOC entry 2242 (class 2604 OID 156818)
-- Name: product_adjacent_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_adjacent ALTER COLUMN product_adjacent_id SET DEFAULT nextval('product_adjacent_product_adjacent_id_seq'::regclass);


--
-- TOC entry 2244 (class 2604 OID 156819)
-- Name: product_consumer_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_consumer ALTER COLUMN product_consumer_id SET DEFAULT nextval('product_consumer_product_consumer_id_seq'::regclass);


--
-- TOC entry 2246 (class 2604 OID 156820)
-- Name: product_mkb10_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_mkb10 ALTER COLUMN product_mkb10_id SET DEFAULT nextval('product_mkb10_product_mkb10_id_seq'::regclass);


--
-- TOC entry 2248 (class 2604 OID 156821)
-- Name: product_normative_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_normative ALTER COLUMN product_normative_id SET DEFAULT nextval('product_normative_product_normative_id_seq'::regclass);


--
-- TOC entry 2250 (class 2604 OID 156822)
-- Name: product_pack_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_pack ALTER COLUMN product_pack_id SET DEFAULT nextval('product_pack_product_pack_id_seq'::regclass);


--
-- TOC entry 2252 (class 2604 OID 156823)
-- Name: product_pharm_group_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_pharm_group ALTER COLUMN product_pharm_group_id SET DEFAULT nextval('product_pharm_group_product_pharm_group_id_seq'::regclass);


--
-- TOC entry 2254 (class 2604 OID 156824)
-- Name: product_substance_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_substance ALTER COLUMN product_substance_id SET DEFAULT nextval('product_substance_product_substance_id_seq'::regclass);


--
-- TOC entry 2257 (class 2604 OID 156825)
-- Name: product_variant_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_variant ALTER COLUMN product_variant_id SET DEFAULT nextval('product_variant_product_variant_id_seq'::regclass);


--
-- TOC entry 2260 (class 2604 OID 156826)
-- Name: registry_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY registry ALTER COLUMN registry_id SET DEFAULT nextval('registry_registry_id_seq'::regclass);


--
-- TOC entry 2262 (class 2604 OID 156827)
-- Name: service_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY service ALTER COLUMN service_id SET DEFAULT nextval('service_service_id_seq'::regclass);


--
-- TOC entry 2264 (class 2604 OID 156828)
-- Name: substance_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY substance ALTER COLUMN substance_id SET DEFAULT nextval('substance_substance_id_seq'::regclass);


--
-- TOC entry 2266 (class 2604 OID 156829)
-- Name: supplier_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY supplier ALTER COLUMN supplier_id SET DEFAULT nextval('supplier_supplier_id_seq'::regclass);


--
-- TOC entry 2268 (class 2604 OID 156830)
-- Name: supplier_good_id; Type: DEFAULT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY supplier_good ALTER COLUMN supplier_good_id SET DEFAULT nextval('supplier_good_supplier_good_id_seq'::regclass);


--
-- TOC entry 2270 (class 2606 OID 156832)
-- Name: atc_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY atc
    ADD CONSTRAINT atc_pkey PRIMARY KEY (atc_id);


--
-- TOC entry 2272 (class 2606 OID 156834)
-- Name: baa_type_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY baa_type
    ADD CONSTRAINT baa_type_pkey PRIMARY KEY (baa_type_id);


--
-- TOC entry 2274 (class 2606 OID 156836)
-- Name: brand_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY brand
    ADD CONSTRAINT brand_pkey PRIMARY KEY (brand_id);


--
-- TOC entry 2278 (class 2606 OID 156838)
-- Name: client_doc_type_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY client_doc_type
    ADD CONSTRAINT client_doc_type_pkey PRIMARY KEY (client_doc_type_id);


--
-- TOC entry 2280 (class 2606 OID 156840)
-- Name: client_good_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY client_good
    ADD CONSTRAINT client_good_pkey PRIMARY KEY (client_good_id);


--
-- TOC entry 2276 (class 2606 OID 156842)
-- Name: client_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (client_id);


--
-- TOC entry 2282 (class 2606 OID 156844)
-- Name: client_service_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY client_service
    ADD CONSTRAINT client_service_pkey PRIMARY KEY (client_service_id);


--
-- TOC entry 2284 (class 2606 OID 156846)
-- Name: consumer_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY consumer
    ADD CONSTRAINT consumer_pkey PRIMARY KEY (consumer_id);


--
-- TOC entry 2286 (class 2606 OID 156848)
-- Name: country_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY country
    ADD CONSTRAINT country_pkey PRIMARY KEY (country_id);


--
-- TOC entry 2290 (class 2606 OID 156850)
-- Name: doc_period_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY doc_period
    ADD CONSTRAINT doc_period_pkey PRIMARY KEY (doc_period_id);


--
-- TOC entry 2288 (class 2606 OID 156852)
-- Name: doc_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY doc
    ADD CONSTRAINT doc_pkey PRIMARY KEY (doc_id);


--
-- TOC entry 2292 (class 2606 OID 156854)
-- Name: doc_state_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY doc_state
    ADD CONSTRAINT doc_state_pkey PRIMARY KEY (doc_state_id);


--
-- TOC entry 2294 (class 2606 OID 156856)
-- Name: doc_type_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY doc_type
    ADD CONSTRAINT doc_type_pkey PRIMARY KEY (doc_type_id);


--
-- TOC entry 2296 (class 2606 OID 156858)
-- Name: dosage_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY dosage
    ADD CONSTRAINT dosage_pkey PRIMARY KEY (dosage_id);


--
-- TOC entry 2298 (class 2606 OID 156860)
-- Name: form_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY form
    ADD CONSTRAINT form_pkey PRIMARY KEY (form_id);


--
-- TOC entry 2302 (class 2606 OID 156862)
-- Name: good_defect_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY good_defect
    ADD CONSTRAINT good_defect_pkey PRIMARY KEY (good_defect_id);


--
-- TOC entry 2304 (class 2606 OID 156864)
-- Name: good_mfr_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY good_mfr
    ADD CONSTRAINT good_mfr_pkey PRIMARY KEY (good_mfr_id);


--
-- TOC entry 2300 (class 2606 OID 156866)
-- Name: good_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY good
    ADD CONSTRAINT good_pkey PRIMARY KEY (good_id);


--
-- TOC entry 2306 (class 2606 OID 156868)
-- Name: good_register_price_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY good_register_price
    ADD CONSTRAINT good_register_price_pkey PRIMARY KEY (good_register_price_id);


--
-- TOC entry 2308 (class 2606 OID 156870)
-- Name: inn_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY inn
    ADD CONSTRAINT inn_pkey PRIMARY KEY (inn_id);


--
-- TOC entry 2310 (class 2606 OID 156872)
-- Name: metaname_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY metaname
    ADD CONSTRAINT metaname_pkey PRIMARY KEY (metaname_id);


--
-- TOC entry 2314 (class 2606 OID 156874)
-- Name: mfr_alias_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY mfr_alias
    ADD CONSTRAINT mfr_alias_pkey PRIMARY KEY (mfr_alias_id);


--
-- TOC entry 2312 (class 2606 OID 156876)
-- Name: mfr_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY mfr
    ADD CONSTRAINT mfr_pkey PRIMARY KEY (mfr_id);


--
-- TOC entry 2316 (class 2606 OID 156878)
-- Name: mfr_role_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY mfr_role
    ADD CONSTRAINT mfr_role_pkey PRIMARY KEY (mfr_role_id);


--
-- TOC entry 2318 (class 2606 OID 156880)
-- Name: okpd2_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY okpd2
    ADD CONSTRAINT okpd2_pkey PRIMARY KEY (okpd2_id);


--
-- TOC entry 2320 (class 2606 OID 156882)
-- Name: pack_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY pack
    ADD CONSTRAINT pack_pkey PRIMARY KEY (pack_id);


--
-- TOC entry 2322 (class 2606 OID 156884)
-- Name: pharm_group_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY pharm_group
    ADD CONSTRAINT pharm_group_pkey PRIMARY KEY (pharm_group_id);


--
-- TOC entry 2324 (class 2606 OID 156886)
-- Name: pharm_ther_group_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY pharm_ther_group
    ADD CONSTRAINT pharm_ther_group_pkey PRIMARY KEY (pharm_ther_group_id);


--
-- TOC entry 2328 (class 2606 OID 156888)
-- Name: product_adjacent_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product_adjacent
    ADD CONSTRAINT product_adjacent_pkey PRIMARY KEY (product_adjacent_id);


--
-- TOC entry 2330 (class 2606 OID 156890)
-- Name: product_consumer_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product_consumer
    ADD CONSTRAINT product_consumer_pkey PRIMARY KEY (product_consumer_id);


--
-- TOC entry 2332 (class 2606 OID 156892)
-- Name: product_mkb10_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product_mkb10
    ADD CONSTRAINT product_mkb10_pkey PRIMARY KEY (product_mkb10_id);


--
-- TOC entry 2334 (class 2606 OID 156894)
-- Name: product_normative_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product_normative
    ADD CONSTRAINT product_normative_pkey PRIMARY KEY (product_normative_id);


--
-- TOC entry 2336 (class 2606 OID 156896)
-- Name: product_pack_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product_pack
    ADD CONSTRAINT product_pack_pkey PRIMARY KEY (product_pack_id);


--
-- TOC entry 2338 (class 2606 OID 156898)
-- Name: product_pharm_group_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product_pharm_group
    ADD CONSTRAINT product_pharm_group_pkey PRIMARY KEY (product_pharm_group_id);


--
-- TOC entry 2326 (class 2606 OID 156900)
-- Name: product_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (product_id);


--
-- TOC entry 2340 (class 2606 OID 156902)
-- Name: product_substance_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product_substance
    ADD CONSTRAINT product_substance_pkey PRIMARY KEY (product_substance_id);


--
-- TOC entry 2342 (class 2606 OID 156904)
-- Name: product_type_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product_type
    ADD CONSTRAINT product_type_pkey PRIMARY KEY (product_type_id);


--
-- TOC entry 2344 (class 2606 OID 156906)
-- Name: product_variant_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY product_variant
    ADD CONSTRAINT product_variant_pkey PRIMARY KEY (product_variant_id);


--
-- TOC entry 2346 (class 2606 OID 156908)
-- Name: recipe_type_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY recipe_type
    ADD CONSTRAINT recipe_type_pkey PRIMARY KEY (recipe_type_id);


--
-- TOC entry 2348 (class 2606 OID 156910)
-- Name: registry_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY registry
    ADD CONSTRAINT registry_pkey PRIMARY KEY (registry_id);


--
-- TOC entry 2350 (class 2606 OID 156912)
-- Name: service_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_pkey PRIMARY KEY (service_id);


--
-- TOC entry 2352 (class 2606 OID 156914)
-- Name: substance_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY substance
    ADD CONSTRAINT substance_pkey PRIMARY KEY (substance_id);


--
-- TOC entry 2356 (class 2606 OID 156916)
-- Name: supplier_good_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY supplier_good
    ADD CONSTRAINT supplier_good_pkey PRIMARY KEY (supplier_good_id);


--
-- TOC entry 2354 (class 2606 OID 156918)
-- Name: supplier_pkey; Type: CONSTRAINT; Schema: main; Owner: pavlov; Tablespace: 
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (supplier_id);


--
-- TOC entry 2358 (class 2606 OID 156919)
-- Name: client_doc_type_client_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client_doc_type
    ADD CONSTRAINT client_doc_type_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(client_id);


--
-- TOC entry 2359 (class 2606 OID 156924)
-- Name: client_doc_type_doc_type_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client_doc_type
    ADD CONSTRAINT client_doc_type_doc_type_id_fkey FOREIGN KEY (doc_type_id) REFERENCES doc_type(doc_type_id);


--
-- TOC entry 2360 (class 2606 OID 156929)
-- Name: client_good_client_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client_good
    ADD CONSTRAINT client_good_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(client_id);


--
-- TOC entry 2361 (class 2606 OID 156934)
-- Name: client_good_good_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client_good
    ADD CONSTRAINT client_good_good_id_fkey FOREIGN KEY (good_id) REFERENCES good(good_id);


--
-- TOC entry 2357 (class 2606 OID 156939)
-- Name: client_parent_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES client(client_id);


--
-- TOC entry 2362 (class 2606 OID 156944)
-- Name: client_service_client_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client_service
    ADD CONSTRAINT client_service_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(client_id);


--
-- TOC entry 2363 (class 2606 OID 156949)
-- Name: client_service_service_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY client_service
    ADD CONSTRAINT client_service_service_id_fkey FOREIGN KEY (service_id) REFERENCES service(service_id);


--
-- TOC entry 2364 (class 2606 OID 156954)
-- Name: doc_client_from_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc
    ADD CONSTRAINT doc_client_from_id_fkey FOREIGN KEY (client_from_id) REFERENCES client(client_id);


--
-- TOC entry 2365 (class 2606 OID 156959)
-- Name: doc_client_to_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc
    ADD CONSTRAINT doc_client_to_id_fkey FOREIGN KEY (client_to_id) REFERENCES client(client_id);


--
-- TOC entry 2366 (class 2606 OID 156964)
-- Name: doc_doc_state_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc
    ADD CONSTRAINT doc_doc_state_id_fkey FOREIGN KEY (doc_state_id) REFERENCES doc_state(doc_state_id);


--
-- TOC entry 2367 (class 2606 OID 156969)
-- Name: doc_doc_type_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc
    ADD CONSTRAINT doc_doc_type_id_fkey FOREIGN KEY (doc_type_id) REFERENCES doc_type(doc_type_id);


--
-- TOC entry 2368 (class 2606 OID 156974)
-- Name: doc_period_doc_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc_period
    ADD CONSTRAINT doc_period_doc_id_fkey FOREIGN KEY (doc_id) REFERENCES doc(doc_id);


--
-- TOC entry 2369 (class 2606 OID 156979)
-- Name: doc_period_doc_state_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY doc_period
    ADD CONSTRAINT doc_period_doc_state_id_fkey FOREIGN KEY (doc_state_id) REFERENCES doc_state(doc_state_id);


--
-- TOC entry 2372 (class 2606 OID 156984)
-- Name: good_defect_good_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good_defect
    ADD CONSTRAINT good_defect_good_id_fkey FOREIGN KEY (good_id) REFERENCES good(good_id);


--
-- TOC entry 2373 (class 2606 OID 156989)
-- Name: good_mfr_good_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good_mfr
    ADD CONSTRAINT good_mfr_good_id_fkey FOREIGN KEY (good_id) REFERENCES good(good_id);


--
-- TOC entry 2370 (class 2606 OID 156994)
-- Name: good_mfr_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good
    ADD CONSTRAINT good_mfr_id_fkey FOREIGN KEY (mfr_id) REFERENCES mfr(mfr_id);


--
-- TOC entry 2374 (class 2606 OID 156999)
-- Name: good_mfr_mfr_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good_mfr
    ADD CONSTRAINT good_mfr_mfr_id_fkey FOREIGN KEY (mfr_id) REFERENCES mfr(mfr_id);


--
-- TOC entry 2375 (class 2606 OID 157004)
-- Name: good_mfr_mfr_role_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good_mfr
    ADD CONSTRAINT good_mfr_mfr_role_id_fkey FOREIGN KEY (mfr_role_id) REFERENCES mfr_role(mfr_role_id);


--
-- TOC entry 2371 (class 2606 OID 157009)
-- Name: good_product_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good
    ADD CONSTRAINT good_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(product_id);


--
-- TOC entry 2376 (class 2606 OID 157014)
-- Name: good_register_price_good_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY good_register_price
    ADD CONSTRAINT good_register_price_good_id_fkey FOREIGN KEY (good_id) REFERENCES good(good_id);


--
-- TOC entry 2378 (class 2606 OID 157019)
-- Name: mfr_alias_mfr_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY mfr_alias
    ADD CONSTRAINT mfr_alias_mfr_id_fkey FOREIGN KEY (mfr_id) REFERENCES mfr(mfr_id);


--
-- TOC entry 2377 (class 2606 OID 157024)
-- Name: mfr_country_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY mfr
    ADD CONSTRAINT mfr_country_id_fkey FOREIGN KEY (country_id) REFERENCES country(country_id);


--
-- TOC entry 2391 (class 2606 OID 157029)
-- Name: product_adjacent_adjacent_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_adjacent
    ADD CONSTRAINT product_adjacent_adjacent_id_fkey FOREIGN KEY (adjacent_id) REFERENCES product(product_id);


--
-- TOC entry 2392 (class 2606 OID 157034)
-- Name: product_adjacent_product_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_adjacent
    ADD CONSTRAINT product_adjacent_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(product_id);


--
-- TOC entry 2379 (class 2606 OID 157039)
-- Name: product_atc_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_atc_id_fkey FOREIGN KEY (atc_id) REFERENCES atc(atc_id);


--
-- TOC entry 2380 (class 2606 OID 157044)
-- Name: product_baa_type_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_baa_type_id_fkey FOREIGN KEY (baa_type_id) REFERENCES baa_type(baa_type_id);


--
-- TOC entry 2381 (class 2606 OID 157049)
-- Name: product_brand_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_brand_id_fkey FOREIGN KEY (brand_id) REFERENCES brand(brand_id);


--
-- TOC entry 2394 (class 2606 OID 163844)
-- Name: product_consumer_consumer_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_consumer
    ADD CONSTRAINT product_consumer_consumer_id_fkey FOREIGN KEY (consumer_id) REFERENCES consumer(consumer_id);


--
-- TOC entry 2393 (class 2606 OID 157059)
-- Name: product_consumer_product_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_consumer
    ADD CONSTRAINT product_consumer_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(product_id);


--
-- TOC entry 2382 (class 2606 OID 157064)
-- Name: product_dosage_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_dosage_id_fkey FOREIGN KEY (dosage_id) REFERENCES dosage(dosage_id);


--
-- TOC entry 2383 (class 2606 OID 157069)
-- Name: product_form_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_form_id_fkey FOREIGN KEY (form_id) REFERENCES form(form_id);


--
-- TOC entry 2384 (class 2606 OID 157074)
-- Name: product_inn_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_inn_id_fkey FOREIGN KEY (inn_id) REFERENCES inn(inn_id);


--
-- TOC entry 2385 (class 2606 OID 157079)
-- Name: product_metaname_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_metaname_id_fkey FOREIGN KEY (metaname_id) REFERENCES metaname(metaname_id);


--
-- TOC entry 2395 (class 2606 OID 157084)
-- Name: product_mkb10_product_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_mkb10
    ADD CONSTRAINT product_mkb10_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(product_id);


--
-- TOC entry 2396 (class 2606 OID 157089)
-- Name: product_normative_product_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_normative
    ADD CONSTRAINT product_normative_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(product_id);


--
-- TOC entry 2386 (class 2606 OID 157094)
-- Name: product_okpd2_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_okpd2_id_fkey FOREIGN KEY (okpd2_id) REFERENCES okpd2(okpd2_id);


--
-- TOC entry 2397 (class 2606 OID 157099)
-- Name: product_pack_pack_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_pack
    ADD CONSTRAINT product_pack_pack_id_fkey FOREIGN KEY (pack_id) REFERENCES pack(pack_id);


--
-- TOC entry 2398 (class 2606 OID 157104)
-- Name: product_pack_product_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_pack
    ADD CONSTRAINT product_pack_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(product_id);


--
-- TOC entry 2400 (class 2606 OID 163853)
-- Name: product_pharm_group_pharm_group_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_pharm_group
    ADD CONSTRAINT product_pharm_group_pharm_group_id_fkey FOREIGN KEY (pharm_group_id) REFERENCES pharm_group(pharm_group_id);


--
-- TOC entry 2399 (class 2606 OID 157114)
-- Name: product_pharm_group_product_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_pharm_group
    ADD CONSTRAINT product_pharm_group_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(product_id);


--
-- TOC entry 2387 (class 2606 OID 157119)
-- Name: product_pharm_ther_group_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pharm_ther_group_id_fkey FOREIGN KEY (pharm_ther_group_id) REFERENCES pharm_ther_group(pharm_ther_group_id);


--
-- TOC entry 2388 (class 2606 OID 157124)
-- Name: product_product_type_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_product_type_id_fkey FOREIGN KEY (product_type_id) REFERENCES product_type(product_type_id);


--
-- TOC entry 2389 (class 2606 OID 157129)
-- Name: product_recipe_type_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_recipe_type_id_fkey FOREIGN KEY (recipe_type_id) REFERENCES recipe_type(recipe_type_id);


--
-- TOC entry 2390 (class 2606 OID 157134)
-- Name: product_registry_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_registry_id_fkey FOREIGN KEY (registry_id) REFERENCES registry(registry_id);


--
-- TOC entry 2401 (class 2606 OID 157139)
-- Name: product_substance_product_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_substance
    ADD CONSTRAINT product_substance_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(product_id);


--
-- TOC entry 2402 (class 2606 OID 157144)
-- Name: product_substance_substance_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_substance
    ADD CONSTRAINT product_substance_substance_id_fkey FOREIGN KEY (substance_id) REFERENCES substance(substance_id);


--
-- TOC entry 2403 (class 2606 OID 157149)
-- Name: product_variant_product_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_variant
    ADD CONSTRAINT product_variant_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(product_id);


--
-- TOC entry 2404 (class 2606 OID 157154)
-- Name: product_variant_variant_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY product_variant
    ADD CONSTRAINT product_variant_variant_id_fkey FOREIGN KEY (variant_id) REFERENCES product(product_id);


--
-- TOC entry 2405 (class 2606 OID 157159)
-- Name: registry_mfr_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY registry
    ADD CONSTRAINT registry_mfr_id_fkey FOREIGN KEY (mfr_id) REFERENCES mfr(mfr_id);


--
-- TOC entry 2406 (class 2606 OID 157164)
-- Name: service_doc_type_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY service
    ADD CONSTRAINT service_doc_type_id_fkey FOREIGN KEY (doc_type_id) REFERENCES doc_type(doc_type_id);


--
-- TOC entry 2407 (class 2606 OID 157169)
-- Name: substance_mfr_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY substance
    ADD CONSTRAINT substance_mfr_id_fkey FOREIGN KEY (mfr_id) REFERENCES mfr(mfr_id);


--
-- TOC entry 2408 (class 2606 OID 157174)
-- Name: supplier_good_good_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY supplier_good
    ADD CONSTRAINT supplier_good_good_id_fkey FOREIGN KEY (good_id) REFERENCES good(good_id);


--
-- TOC entry 2409 (class 2606 OID 157179)
-- Name: supplier_good_supplier_id_fkey; Type: FK CONSTRAINT; Schema: main; Owner: pavlov
--

ALTER TABLE ONLY supplier_good
    ADD CONSTRAINT supplier_good_supplier_id_fkey FOREIGN KEY (supplier_id) REFERENCES supplier(supplier_id);


--
-- TOC entry 2525 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-12-13 10:28:59

--
-- PostgreSQL database dump complete
--

