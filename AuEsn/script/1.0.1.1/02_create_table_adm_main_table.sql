﻿/*
	CREATE TABLE adm.main_table
*/

-- DROP TABLE adm.main_table;

CREATE TABLE adm.main_table
(
  main_table_id bigserial NOT NULL,  
  main_table_name character varying NOT NULL,
  allow_select boolean NOT NULL DEFAULT true,
  allow_insert boolean NOT NULL DEFAULT true,
  allow_update boolean NOT NULL DEFAULT true,
  allow_delete boolean NOT NULL DEFAULT true,
  CONSTRAINT main_table_pkey PRIMARY KEY (main_table_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE adm.main_table OWNER TO pavlov;
