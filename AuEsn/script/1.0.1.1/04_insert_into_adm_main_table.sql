﻿/*
	INSERT INTO adm.main_table
*/

-- delete from adm.main_table;

INSERT INTO adm.main_table (main_table_name)
SELECT table_name
FROM information_schema.tables
WHERE table_schema = 'main'
ORDER BY table_name;

-- select * from adm.main_table ORDER BY main_table_name;