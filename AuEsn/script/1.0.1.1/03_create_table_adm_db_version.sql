﻿/*
	CREATE TABLE adm.db_version
*/

-- DROP TABLE adm.version_db;

CREATE TABLE adm.db_version
(  
  db_ver_id integer NOT NULL,
  db_ver_num character varying,
  db_ver_date date,
  db_ver_descr character varying,
  CONSTRAINT db_version_pkey PRIMARY KEY (db_ver_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE adm.db_version OWNER TO pavlov;
