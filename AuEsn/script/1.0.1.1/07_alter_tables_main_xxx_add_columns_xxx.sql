﻿/*
	ALTER TABLE main.xxx ADD COLUMNs xxx
*/

ALTER TABLE main.client ADD COLUMN crt_date timestamp without time zone;
COMMENT ON COLUMN main.client.crt_date IS 'когда создана запись';

ALTER TABLE main.client ADD COLUMN crt_user character varying;
COMMENT ON COLUMN main.client.crt_user IS 'кем создана запись';

ALTER TABLE main.client ADD COLUMN upd_date timestamp without time zone;
COMMENT ON COLUMN main.client.upd_date IS 'когда изменена запись';

ALTER TABLE main.client ADD COLUMN upd_user character varying;
COMMENT ON COLUMN main.client.upd_user IS 'кем изменена запись';

ALTER TABLE main.client ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
COMMENT ON COLUMN main.client.is_deleted IS 'запись удалена';

ALTER TABLE main.client ADD COLUMN del_date timestamp without time zone;
COMMENT ON COLUMN main.client.del_date IS 'когда удалена запись';

ALTER TABLE main.client ADD COLUMN del_user character varying;
COMMENT ON COLUMN main.client.del_user IS 'кем удалена запись';


COMMENT ON COLUMN main.doc.upd_user IS 'кем изменена запись';

COMMENT ON COLUMN main.good.upd_user IS 'кем изменена запись';

--COMMENT ON COLUMN main.metaname.upd_user IS 'кем изменена запись';

--COMMENT ON COLUMN main.metaname.upd_user IS '';

COMMENT ON COLUMN main.product.upd_user IS 'кем изменена запись';

ALTER TABLE main.mfr ADD COLUMN crt_date timestamp without time zone;
COMMENT ON COLUMN main.mfr.crt_date IS 'когда создана запись';

ALTER TABLE main.mfr ADD COLUMN crt_user character varying;
COMMENT ON COLUMN main.mfr.crt_user IS 'кем создана запись';

ALTER TABLE main.mfr ADD COLUMN upd_date timestamp without time zone;
COMMENT ON COLUMN main.mfr.upd_date IS 'когда изменена запись';

ALTER TABLE main.mfr ADD COLUMN upd_user character varying;
COMMENT ON COLUMN main.mfr.upd_user IS 'кем изменена запись';

ALTER TABLE main.mfr ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
COMMENT ON COLUMN main.mfr.is_deleted IS 'запись удалена';

ALTER TABLE main.mfr ADD COLUMN del_date timestamp without time zone;
COMMENT ON COLUMN main.mfr.del_date IS 'когда удалена запись';

ALTER TABLE main.mfr ADD COLUMN del_user character varying;
COMMENT ON COLUMN main.mfr.del_user IS 'кем удалена запись';

ALTER TABLE main.supplier ADD COLUMN crt_date timestamp without time zone;
COMMENT ON COLUMN main.supplier.crt_date IS 'когда создана запись';

ALTER TABLE main.supplier ADD COLUMN crt_user character varying;
COMMENT ON COLUMN main.supplier.crt_user IS 'кем создана запись';

ALTER TABLE main.supplier ADD COLUMN upd_date timestamp without time zone;
COMMENT ON COLUMN main.supplier.upd_date IS 'когда изменена запись';

ALTER TABLE main.supplier ADD COLUMN upd_user character varying;
COMMENT ON COLUMN main.supplier.upd_user IS 'кем изменена запись';

ALTER TABLE main.supplier ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
COMMENT ON COLUMN main.supplier.is_deleted IS 'запись удалена';

ALTER TABLE main.supplier ADD COLUMN del_date timestamp without time zone;
COMMENT ON COLUMN main.supplier.del_date IS 'когда удалена запись';

ALTER TABLE main.supplier ADD COLUMN del_user character varying;
COMMENT ON COLUMN main.supplier.del_user IS 'кем удалена запись';
