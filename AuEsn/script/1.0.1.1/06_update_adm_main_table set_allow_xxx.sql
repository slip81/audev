﻿/*
	update adm.main_table set allow
*/

-- select * from adm.main_table order by main_table_name

update adm.main_table set allow_select = true, allow_insert = true, allow_update = true, allow_delete = true;

update adm.main_table set allow_insert = false, allow_update = false, allow_delete = false where main_table_id in
(
2,
11,
12,
37,
39,
41
);