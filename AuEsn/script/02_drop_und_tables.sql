﻿/*
	DROP und tables
*/

DROP VIEW und.vw_asna_order_row;
DROP VIEW und.vw_asna_order_row_state;
DROP VIEW und.vw_asna_order_state;
DROP VIEW und.vw_asna_price_row_linked;
DROP VIEW und.vw_asna_stock_row_linked;
DROP VIEW und.vw_doc_import_match_variant;
DROP VIEW und.vw_doc_import_match;
DROP VIEW und.vw_doc;
DROP VIEW und.vw_doc_import;
DROP VIEW und.vw_doc_product_filter;
DROP VIEW und.vw_product_and_good;
DROP VIEW und.vw_product;
DROP VIEW und.vw_partner_good_filter;
DROP VIEW und.vw_partner_good;
DROP VIEW und.vw_wa_task_lc;
drop table und.asna_link_row;
drop table und.asna_link;
drop table und.asna_order_row_state;
drop table und.asna_order_row;
drop table und.asna_order_state;
drop table und.asna_order;
drop table und.asna_order_state_type;
drop table und.asna_price_row;
drop table und.asna_price_row_actual;
drop table und.asna_price_batch_in;
drop table und.asna_price_batch_out;
drop table und.asna_price_chain;
drop table und.asna_stock_row_actual;
drop table und.asna_stock_row;
drop table und.asna_schedule;
drop table und.asna_schedule_interval_type;
drop table und.asna_schedule_time_type;
drop table und.asna_schedule_type;
drop table und.asna_stock_batch_in;
drop table und.asna_stock_batch_out;
drop table und.asna_stock_chain;
drop table und.asna_store_info_schedule;
drop table und.asna_store_info;
drop table und.asna_user_log;
drop table und.asna_user;

drop table und.doc_row_field;
drop table und.doc_row;
drop table und.doc_lc;
drop table und.doc_import_row;
drop table und.doc_import_match_variant;
drop table und.doc_import_match;
drop table und.doc_import_log;
drop table und.doc_import_log_type;
drop table und.doc_import_good;
drop table und.doc_import;
drop table und.doc_import_config_type_xml;
drop table und.doc_import_config_type_excel;
drop table und.doc_import_config_type;
drop table und.doc_data;

drop table und.stock_row;
drop table und.stock_batch;
drop table und.stock_sales_download;
drop table und.stock;

drop table und.wa_worker_task;
drop table und.wa_worker;
drop table und.wa_task_param;
drop table und.wa_task_lc;
drop table und.wa_task;
drop table und.wa_task_state;
drop table und.wa_request_type;
drop table und.wa_request_type_group2;
drop table und.wa_request_type_group1;
drop table und.wa;

ALTER TABLE und.apply_group DROP COLUMN crt_date;
ALTER TABLE und.apply_group DROP COLUMN crt_user;
ALTER TABLE und.apply_group DROP COLUMN upd_date;
ALTER TABLE und.apply_group DROP COLUMN upd_user;
ALTER TABLE und.apply_group DROP COLUMN del_date;
ALTER TABLE und.apply_group DROP COLUMN del_user;
ALTER TABLE und.apply_group DROP COLUMN is_deleted;
ALTER TABLE und.apply_group DROP COLUMN sysrowstamp;
ALTER TABLE und.apply_group DROP COLUMN sysrowuid;

ALTER TABLE und.area DROP COLUMN crt_date;
ALTER TABLE und.area DROP COLUMN crt_user;
ALTER TABLE und.area DROP COLUMN upd_date;
ALTER TABLE und.area DROP COLUMN upd_user;
ALTER TABLE und.area DROP COLUMN del_date;
ALTER TABLE und.area DROP COLUMN del_user;
ALTER TABLE und.area DROP COLUMN is_deleted;
ALTER TABLE und.area DROP COLUMN sysrowstamp;
ALTER TABLE und.area DROP COLUMN sysrowuid;

ALTER TABLE und.atc DROP COLUMN crt_date;
ALTER TABLE und.atc DROP COLUMN crt_user;
ALTER TABLE und.atc DROP COLUMN upd_date;
ALTER TABLE und.atc DROP COLUMN upd_user;
ALTER TABLE und.atc DROP COLUMN del_date;
ALTER TABLE und.atc DROP COLUMN del_user;
ALTER TABLE und.atc DROP COLUMN is_deleted;
ALTER TABLE und.atc DROP COLUMN sysrowstamp;
ALTER TABLE und.atc DROP COLUMN sysrowuid;