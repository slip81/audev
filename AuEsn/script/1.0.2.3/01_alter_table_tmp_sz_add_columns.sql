﻿/*
	ALTER TABLE main._tmp_sz ADD COLUMNs
*/

ALTER TABLE main._tmp_sz ADD COLUMN mfr_id bigint;
ALTER TABLE main._tmp_sz ADD COLUMN inn_source_id bigint;
ALTER TABLE main._tmp_sz ADD COLUMN inn_id bigint;
ALTER TABLE main._tmp_sz ADD COLUMN trade_name_id bigint;
ALTER TABLE main._tmp_sz ADD COLUMN product_type_id integer;
ALTER TABLE main._tmp_sz ADD COLUMN is_precursor boolean NOT NULL DEFAULT false
