﻿/*
	CREATE OR REPLACE VIEW main.vw_good_info
*/

-- DROP VIEW main.vw_good_info;

CREATE OR REPLACE VIEW main.vw_good_info AS 
 SELECT
  t2.good_uuid,
  t2.barcode,
  t2.gtin,
  t2.is_perfect,    
  t2.crt_date,
  t2.crt_user,
  t2.upd_date,
  t2.upd_user,
  t2.is_deleted,
  t2.del_date,
  t2.del_user,  
  t3.product_uuid,
  t3.product_name,
  t3.storage_period,
  t3.storage_condition,
  t3.is_vital,
  t3.is_drug2,
  t3.is_drug3,
  t3.is_precursor,
  t3.is_alcohol,
  t3.is_imm_bio,
  t3.is_mandatory,
  t3.is_defect_exists,
  t3.indication,
  t3.contraindication,
  t3.drug_interaction,  
  t3.storage_condition_short,
  t3.indication_short,
  t3.contraindication_short,
  t4.mfr_uuid,
  t4.mfr_name,
  t22.country_uuid,
  t22.country_name,
  t5.product_type_uuid,
  t5.product_type_name,
  t12.atc_uuid,
  t12.atc_name,
  t12.atc_code,
  t13.dosage_uuid,
  t13.dosage_name,
  t14.form_uuid,
  t14.form_name,
  t14.short_name AS form_short_name,
  t14.group_name AS form_group_name,
  t15.pharm_ther_group_uuid,
  t15.pharm_ther_group_name,
  t16.okpd2_uuid,
  t16.okpd2_name,
  t16.okpd2_code,
  t17.brand_uuid,
  t17.brand_name,
  t18.recipe_type_uuid,
  t18.recipe_type_name,
  t19.registry_uuid,
  t19.registry_num,
  t19.registry_date,
  t20.baa_type_uuid,
  t20.baa_type_name,
  t21.metaname_uuid,
  t21.metaname_name,
  t23.trade_name_uuid,
  t23.trade_name_name,
  t25.inn_uuid,
  t25.inn_name
   FROM main.good t2
     JOIN main.product t3 ON t2.product_id = t3.product_id
     JOIN main.mfr t4 ON t2.mfr_id = t4.mfr_id
     JOIN main.product_type t5 ON t3.product_type_id = t5.product_type_id
     LEFT JOIN main.atc t12 ON t3.atc_id = t12.atc_id
     LEFT JOIN main.dosage t13 ON t3.dosage_id = t13.dosage_id
     LEFT JOIN main.form t14 ON t3.form_id = t14.form_id
     LEFT JOIN main.pharm_ther_group t15 ON t3.pharm_ther_group_id = t15.pharm_ther_group_id
     LEFT JOIN main.okpd2 t16 ON t3.okpd2_id = t16.okpd2_id
     LEFT JOIN main.brand t17 ON t3.brand_id = t17.brand_id
     LEFT JOIN main.recipe_type t18 ON t3.recipe_type_id = t18.recipe_type_id
     LEFT JOIN main.registry t19 ON t3.registry_id = t19.registry_id
     LEFT JOIN main.baa_type t20 ON t3.baa_type_id = t20.baa_type_id
     LEFT JOIN main.metaname t21 ON t3.metaname_id = t21.metaname_id
     LEFT JOIN main.country t22 ON t4.country_id = t22.country_id
     LEFT JOIN main.trade_name t23 ON t3.trade_name_id = t23.trade_name_id
     LEFT JOIN main.inn_source t24 ON t23.inn_source_id = t24.inn_source_id
     LEFT JOIN main.inn t25 ON t24.inn_id = t25.inn_id;

ALTER TABLE main.vw_good_info OWNER TO pavlov;
