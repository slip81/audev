﻿/*
	copy _tmp_sz to good, product
*/

-- select * from main._tmp_sz limit 100

-- update main._tmp_sz t1 set mfr_id = null;

update main._tmp_sz t1
set mfr_id = t2.mfr_id
from main.mfr t2
where trim(lower(t2.mfr_name)) = trim(lower(t1."Изготовитель")) 
and t2.is_deleted = false
and t1.mfr_id is null;


insert into main.mfr (mfr_name, upd_user)
select distinct main._tmp_sz."Изготовитель", 'pavlov'
from main._tmp_sz
where mfr_id is null;

update main._tmp_sz t1
set mfr_id = t2.mfr_id
from main.mfr t2
where trim(lower(t2.mfr_name)) = trim(lower(t1."Изготовитель")) 
and t2.is_deleted = false
and t1.mfr_id is null;


-- select * from main.client where is_deleted = false order by client_id

update main._tmp_sz t1
set inn_source_id = t2.inn_source_id, inn_id = t2.inn_id
from main.inn_source t2
where trim(lower(t2.inn_source_name)) = trim(lower(t1."МНН")) 
and t2.is_deleted = false and t2.client_source_id = 10
and t1.inn_source_id is null;

update main._tmp_sz t1
set trade_name_id = t2.trade_name_id
from main.trade_name t2
where t1.inn_source_id = t2.inn_source_id
and t2.is_deleted = false
and t1.trade_name_id is null;

update main._tmp_sz
set product_type_id = 4
where trim("Группа товаров") = 'н/д';

update main._tmp_sz
set product_type_id = 1
where trim("Группа товаров") = 'ЛС';

update main._tmp_sz
set product_type_id = 2
where trim("Группа товаров") = 'БАД';

update main._tmp_sz
set product_type_id = 4
where trim("Группа товаров") = 'ПР';

---------------------------

delete from main.client_good;
delete from main.good;

--select * from main.product limit 100

--select * from main.product_type
/*
1"Лекарственное средство"
2"БАД"
3"ИМН"
4"Прочее"
*/

--select * from main.baa_type
--select distinct "Группа товаров" from main._tmp_sz
/*
1  "н/д"
2  "ЛС"
3  "БАД"
4  "ПР"
*/


-- delete from main.product where trade_name_id is not null;


insert into main.product (product_name, product_type_id, is_vital, is_precursor, trade_name_id, upd_user)
select distinct "Наименование", product_type_id, case when trim(coalesce("ЖНВЛП", '')) != '' then true else false end, 
case when trim(coalesce("ПКУ", '')) != '' then true else false end, trade_name_id, 'pavlov'
from main._tmp_sz
where trade_name_id is not null;

--select * from main.good limit 100

insert into main.good (product_id, mfr_id, barcode, is_perfect, upd_user)
select t2.product_id, t1.mfr_id, t1."Штрихкод", true, 'pavlov'
from main._tmp_sz t1
inner join main.product t2 on t1."Наименование" = t2.product_name and  t1.trade_name_id = t2.trade_name_id
where t1.trade_name_id is not null;

insert into main.client_good (client_id, good_id, client_code, client_name, upd_user)
select distinct 10, t3.good_id, t1."Код СЗИ", t1."Наименование", 'pavlov'
from main._tmp_sz t1
inner join main.product t2 on t1."Наименование" = t2.product_name and  t1.trade_name_id = t2.trade_name_id
inner join main.good t3 on t2.product_id = t3.product_id
where t1.trade_name_id is not null;