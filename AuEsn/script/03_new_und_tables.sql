﻿/*
DROP TABLE main.product_pharm_group;
DROP TABLE main.product_package_kind;
DROP TABLE main.product_okdp;
DROP TABLE main.product_category;
DROP TABLE main.partner_good;
DROP TABLE main.remain;
DROP TABLE main.vital;
DROP TABLE main.good;
DROP TABLE main.product;
DROP TABLE main.product_form;
DROP TABLE main.space;
DROP TABLE main.pharm_market;
DROP TABLE main.pharm_group;
DROP TABLE main.package_kind;
DROP TABLE main.doc;
DROP TABLE main.partner_wh;
DROP TABLE main.partner_func;
DROP TABLE main.partner_doc_type;
DROP TABLE main.partner;
DROP TABLE main.okdp;
DROP TABLE main.misprint;
DROP TABLE main.metaname;
DROP TABLE main.manufacturer_alias;
DROP TABLE main.manufacturer;
DROP TABLE main.inn;
DROP TABLE main.func;
DROP TABLE main.dosage;
DROP TABLE main.measure_unit;
DROP TABLE main.category;
DROP TABLE main.brand;
DROP TABLE main.atc;
DROP TABLE main.area;
DROP TABLE main.apply_group;
*/
---------------------
-- DROP SEQUENCE main.global_id_sequence;

CREATE SEQUENCE main.global_id_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE main.global_id_sequence OWNER TO pavlov;

-- DROP FUNCTION main.id_generator();

CREATE OR REPLACE FUNCTION main.id_generator(OUT result bigint)
  RETURNS bigint AS
$BODY$
DECLARE
    our_epoch bigint := 1314220021721;
    seq_id bigint;
    now_millis bigint;
    shard_id int := 1;
BEGIN
    SELECT nextval('main.global_id_sequence') % 1024 INTO seq_id;

SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000) INTO now_millis;
    result := (now_millis - our_epoch) << 23;
    result := result | (shard_id << 10);
    result := result | (seq_id);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.id_generator() OWNER TO pavlov;


-- tables:
-- product_form
-- product_dosage
-- product_pack
-- product_normative
-- product_substance
-- product_mkb10
-- product_register_price
-- product_defect
-- product_pharm_group
-- product_consumer
-- product_variant
-- product_adjacent

-- supplier
-- supplier_good

-- good_mfr

-------------------------------------------
-- DROP TABLE main.client;

CREATE TABLE main.client
(
  client_id serial NOT NULL,
  client_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  client_name character varying,  
  parent_id integer,
  CONSTRAINT client_pkey PRIMARY KEY (client_id),
  CONSTRAINT client_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES main.client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.client OWNER TO pavlov;

-- DROP TABLE main.doc_type;

CREATE TABLE main.doc_type
(
  doc_type_id serial NOT NULL,
  doc_type_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  doc_type_name character varying,
  client_from_id integer NOT NULL,
  client_to_id integer NOT NULL,
  CONSTRAINT doc_type_pkey PRIMARY KEY (doc_type_id),
  CONSTRAINT doc_type_client_from_id_fkey FOREIGN KEY (client_from_id)
      REFERENCES main.client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT doc_type_client_to_id_fkey FOREIGN KEY (client_to_id)
      REFERENCES main.client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.doc_type OWNER TO pavlov;


-- DROP TABLE main.country;

CREATE TABLE main.country
(
  country_id serial NOT NULL,
  country_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  country_name character varying NOT NULL,
  short_name character varying,
  aplpha2 character varying,
  aplpha3 character varying,  
  CONSTRAINT country_pkey PRIMARY KEY (country_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.country OWNER TO pavlov;


-- DROP TABLE main.mfr;

CREATE TABLE main.mfr
(
  mfr_id bigserial NOT NULL,
  mfr_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  mfr_name character varying NOT NULL,
  country_id integer,
  CONSTRAINT mfr_pkey PRIMARY KEY (mfr_id),
  CONSTRAINT mfr_country_id_fkey FOREIGN KEY (country_id)
      REFERENCES main.country (country_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.mfr OWNER TO pavlov;


-- DROP TABLE main.doc_state;

CREATE TABLE main.doc_state
(
  doc_state_id serial NOT NULL,
  doc_state_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  doc_state_name character varying,
  CONSTRAINT doc_state_pkey PRIMARY KEY (doc_state_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE main.doc_state OWNER TO pavlov;

-- DROP TABLE main.doc;

CREATE TABLE main.doc
(
  doc_id bigserial NOT NULL,
  doc_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  doc_num character varying,
  doc_name character varying,
  doc_date date,
  doc_type_id integer NOT NULL,
  doc_state_id integer NOT NULL,
  doc_path character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT doc_pkey PRIMARY KEY (doc_id),
  CONSTRAINT doc_doc_state_id_fkey FOREIGN KEY (doc_state_id)
      REFERENCES main.doc_state (doc_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT doc_doc_type_id_fkey FOREIGN KEY (doc_type_id)
      REFERENCES main.doc_type (doc_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.doc OWNER TO pavlov;

-- DROP TABLE main.doc;

CREATE TABLE main.doc_period
(
  doc_period_id bigserial NOT NULL,
  doc_period_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  doc_id bigint,
  doc_state_id integer NOT NULL,
  crt_date timestamp without time zone,  
  CONSTRAINT doc_period_pkey PRIMARY KEY (doc_period_id),
  CONSTRAINT doc_period_doc_state_id_fkey FOREIGN KEY (doc_state_id)
      REFERENCES main.doc_state (doc_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.doc OWNER TO pavlov;
-------------------------------------------

-- DROP TABLE main.mfr_alias;

CREATE TABLE main.mfr_alias
(
  mfr_alias_id bigserial NOT NULL,
  mfr_alias_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  mfr_id bigint NOT NULL,
  mfr_alias_name character varying NOT NULL,
  CONSTRAINT mfr_alias_pkey PRIMARY KEY (mfr_alias_id),
  CONSTRAINT mfr_alias_mfr_id_fkey FOREIGN KEY (mfr_id)
      REFERENCES main.mfr (mfr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.mfr_alias OWNER TO pavlov;

-- DROP TABLE main.product_type;

CREATE TABLE main.product_type
(
  product_type_id integer NOT NULL,  -- PK
  product_type_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(), -- BK  
  product_type_name character varying NOT NULL, -- наименование типа
  CONSTRAINT product_type_pkey PRIMARY KEY (product_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_type OWNER TO pavlov;

-- DROP TABLE main.baa_type;

CREATE TABLE main.baa_type
(
  baa_type_id integer NOT NULL,
  baa_type_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  baa_type_name character varying NOT NULL,
  CONSTRAINT baa_type_pkey PRIMARY KEY (baa_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.baa_type OWNER TO pavlov;

-- DROP TABLE main.inn;

CREATE TABLE main.inn
(
  inn_id bigserial NOT NULL,
  inn_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  inn_name character varying NOT NULL,
  CONSTRAINT inn_pkey PRIMARY KEY (inn_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.inn OWNER TO pavlov;

-- DROP TABLE main.registry;

CREATE TABLE main.registry
(
  registry_id bigserial NOT NULL,
  registry_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  registry_num character varying NOT NULL,
  registry_date date NOT NULL,
  renewal_date date,
  mfr_id bigint,
  CONSTRAINT registry_pkey PRIMARY KEY (registry_id),
  CONSTRAINT registry_mfr_id_fkey FOREIGN KEY (mfr_id)
      REFERENCES main.mfr (mfr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.registry OWNER TO pavlov;

-- DROP TABLE main.pharm_ther_group;

CREATE TABLE main.pharm_ther_group
(
  pharm_ther_group_id serial NOT NULL,
  pharm_ther_group_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  pharm_ther_group_name character varying,
  CONSTRAINT pharm_ther_group_pkey PRIMARY KEY (pharm_ther_group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.pharm_ther_group OWNER TO pavlov;


-- DROP TABLE main.atc;

CREATE TABLE main.atc
(
  atc_id bigserial NOT NULL,
  atc_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  atc_name character varying NOT NULL,
  CONSTRAINT atc_pkey PRIMARY KEY (atc_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.atc OWNER TO pavlov;

-- DROP TABLE main.recipe_type;

CREATE TABLE main.recipe_type
(
  recipe_type_id integer NOT NULL,
  recipe_type_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  recipe_type_name character varying NOT NULL,
  CONSTRAINT recipe_type_pkey PRIMARY KEY (recipe_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.recipe_type OWNER TO pavlov;

-- DROP TABLE main.okpd2;

CREATE TABLE main.okpd2
(
  okpd2_id serial NOT NULL,
  okpd2_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  okpd2_name character varying NOT NULL,
  CONSTRAINT okpd2_pkey PRIMARY KEY (okpd2_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.okpd2 OWNER TO pavlov;

-- DROP TABLE main.brand;

CREATE TABLE main.brand
(
  brand_id bigserial NOT NULL,
  brand_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  brand_name character varying NOT NULL,
  CONSTRAINT brand_pkey PRIMARY KEY (brand_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.brand OWNER TO pavlov;

-- DROP TABLE main.metaname;

CREATE TABLE main.metaname
(
  metaname_id bigserial NOT NULL,
  metaname_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  metaname_name character varying NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT metaname_pkey PRIMARY KEY (metaname_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.metaname OWNER TO pavlov;


-- DROP TABLE main.product;

CREATE TABLE main.product
(
  product_id bigserial NOT NULL,  -- PK
  product_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(), -- BK (business key)
  product_type_id integer NOT NULL, -- тип записи (ЛС, БАД, ИМН, прочее)
  product_name character varying NOT NULL, -- наименование
  inn_id bigint,  -- код МНН
  registry_id bigint, -- код свидетельства о регистрации
  storage_period character varying, -- срок хранения
  storage_condition character varying, -- условия хранения
  pharm_ther_group_id integer, -- код фармако-терапевтической группа
  atc_id bigint, -- код АТХ
  is_vital boolean, -- признак: ЖВ
  is_drug2 boolean, -- признак: принадлежит к списку 2 нарко и писхо веществ
  is_drug3 boolean, -- признак: принадлежит к списку 3 нарко и писхо веществ
  is_precursor boolean, -- признак: принадлежит к списку прекурсоров
  recipe_type_id integer, -- код типа рецепта
  is_alcohol boolean, -- признак: спиртосодержащее
  is_imm_bio boolean, -- признак: иммунобиологический препарат
  is_mandatory boolean, -- признак: обязательный ассортимент
  okpd2_id integer, -- код ОКДП-2
  is_defect_exists boolean, -- признак: есть забракованные серии (тогда см. product_defect)
  brand_id bigint, -- код брэнда
  indication character varying, -- показания к применению
  contraindication character varying, -- противопоказания к применению
  drug_interaction character varying, -- лекарственное взаимодействие 
  baa_type_id integer, -- для БАД - тип БАДа
  metaname_id bigint, -- сгруппированное наименование   
  crt_date timestamp without time zone, -- когда создано
  crt_user character varying, -- кем создано
  upd_date timestamp without time zone, -- когда изменено
  upd_user character varying, -- кем изменено
  is_deleted boolean NOT NULL DEFAULT false, -- признак: строка удалена
  del_date timestamp without time zone, -- когда удалена
  del_user character varying, -- кем удалена
  CONSTRAINT product_pkey PRIMARY KEY (product_id),
  CONSTRAINT product_product_type_id_fkey FOREIGN KEY (product_type_id)
      REFERENCES main.product_type (product_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_inn_id_fkey FOREIGN KEY (inn_id)
      REFERENCES main.inn (inn_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_registry_id_fkey FOREIGN KEY (registry_id)
      REFERENCES main.registry (registry_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT product_pharm_ther_group_id_fkey FOREIGN KEY (pharm_ther_group_id)
      REFERENCES main.pharm_ther_group (pharm_ther_group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION, 
  CONSTRAINT product_atc_id_fkey FOREIGN KEY (atc_id)
      REFERENCES main.atc (atc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,                
  CONSTRAINT product_recipe_type_id_fkey FOREIGN KEY (recipe_type_id)
      REFERENCES main.recipe_type (recipe_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_okpd2_id_fkey FOREIGN KEY (okpd2_id)
      REFERENCES main.okpd2 (okpd2_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT product_brand_id_fkey FOREIGN KEY (brand_id)
      REFERENCES main.brand (brand_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT product_metaname_id_fkey FOREIGN KEY (metaname_id)
      REFERENCES main.metaname (metaname_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product OWNER TO pavlov;

-- DROP TABLE main.good;

CREATE TABLE main.good
(
  good_id bigserial NOT NULL,
  good_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  mfr_id bigint NOT NULL,
  barcode character varying,
  is_monitor boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT good_pkey PRIMARY KEY (good_id),
  CONSTRAINT good_mfr_id_fkey FOREIGN KEY (mfr_id)
      REFERENCES main.mfr (mfr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT good_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.good OWNER TO pavlov;

-- DROP TABLE main.client_good;

CREATE TABLE main.client_good
(
  client_good_id bigserial NOT NULL,
  client_good_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  client_id integer NOT NULL,
  good_id bigint NOT NULL,
  client_code character varying,
  client_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT client_good_pkey PRIMARY KEY (client_good_id),
  CONSTRAINT client_good_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES main.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT client_good_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES main.client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.client_good OWNER TO pavlov;

----------------------------------------------------

-- DROP TABLE main.form;

CREATE TABLE main.form
(
  form_id serial NOT NULL,
  form_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  form_name character varying NOT NULL,
  CONSTRAINT form_pkey PRIMARY KEY (form_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.form OWNER TO pavlov;

-- DROP TABLE main.dosage;

CREATE TABLE main.dosage
(
  dosage_id serial NOT NULL,
  dosage_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  dosage_name character varying NOT NULL,
  CONSTRAINT dosage_pkey PRIMARY KEY (dosage_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.dosage OWNER TO pavlov;

-- DROP TABLE main.pack;

CREATE TABLE main.pack
(
  pack_id serial NOT NULL,
  pack_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  pack_name character varying NOT NULL,
  CONSTRAINT pack_pkey PRIMARY KEY (pack_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.pack OWNER TO pavlov;

-- DROP TABLE main.substance;

CREATE TABLE main.substance
(
  substance_id bigserial NOT NULL,
  substance_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  substance_name character varying NOT NULL,
  substance_trade_name character varying NOT NULL,
  storage_period character varying,
  storage_condition character varying,
  doc_name character varying,
  is_drug boolean,
  CONSTRAINT substance_pkey PRIMARY KEY (substance_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.substance OWNER TO pavlov;

-- DROP TABLE main.substance_mfr;

CREATE TABLE main.substance_mfr
(
  substance_mfr_id bigserial NOT NULL,
  substance_mfr_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  substance_id bigint NOT NULL,
  mfr_id bigint NOT NULL,  
  CONSTRAINT substance_mfr_pkey PRIMARY KEY (substance_mfr_id),
  CONSTRAINT substance_mfr_substance_id_fkey FOREIGN KEY (substance_id)
      REFERENCES main.substance (substance_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT substance_mfr_mfr_id_fkey FOREIGN KEY (mfr_id)
      REFERENCES main.mfr (mfr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.substance_mfr OWNER TO pavlov;

-- DROP TABLE main.pharm_group;

CREATE TABLE main.pharm_group
(
  pharm_group_id serial NOT NULL,
  pharm_group_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  pharm_group_name character varying NOT NULL,
  CONSTRAINT pharm_group_pkey PRIMARY KEY (pharm_group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.pharm_group OWNER TO pavlov;

-- DROP TABLE main.consumer;

CREATE TABLE main.consumer
(
  consumer_id serial NOT NULL,
  consumer_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  consumer_name character varying NOT NULL,
  CONSTRAINT consumer_pkey PRIMARY KEY (consumer_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.consumer OWNER TO pavlov;

-- tables:
-- product_form
-- product_dosage
-- product_pack
-- product_normative
-- product_substance
-- product_mkb10
-- product_register_price
-- product_defect
-- product_pharm_group
-- product_consumer
-- product_variant
-- product_adjacent


-- DROP TABLE main.product_form;

CREATE TABLE main.product_form
(
  product_form_id bigserial NOT NULL,
  product_form_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  form_id integer NOT NULL,
  CONSTRAINT product_form_pkey PRIMARY KEY (product_form_id),
  CONSTRAINT product_form_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT product_form_form_id_fkey FOREIGN KEY (form_id)
      REFERENCES main.form (form_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_form OWNER TO pavlov;


-- DROP TABLE main.product_dosage;

CREATE TABLE main.product_dosage
(
  product_dosage_id bigserial NOT NULL,
  product_dosage_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  dosage_id integer NOT NULL,
  CONSTRAINT product_dosage_pkey PRIMARY KEY (product_dosage_id),
  CONSTRAINT product_dosage_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT product_dosage_dosage_id_fkey FOREIGN KEY (dosage_id)
      REFERENCES main.dosage (dosage_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_dosage OWNER TO pavlov;

-- DROP TABLE main.product_pack;

CREATE TABLE main.product_pack
(
  product_pack_id bigserial NOT NULL,
  product_pack_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  pack_id integer NOT NULL,
  CONSTRAINT product_pack_pkey PRIMARY KEY (product_pack_id),
  CONSTRAINT product_pack_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT product_pack_pack_id_fkey FOREIGN KEY (pack_id)
      REFERENCES main.pack (pack_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_pack OWNER TO pavlov;

-- DROP TABLE main.product_normative;

CREATE TABLE main.product_normative
(
  product_normative_id bigserial NOT NULL,
  product_normative_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  doc_num character varying,
  doc_year integer,
  doc_change_num character varying,
  CONSTRAINT product_normative_pkey PRIMARY KEY (product_normative_id),
  CONSTRAINT product_normative_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION   
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_normative OWNER TO pavlov;

-- DROP TABLE main.product_substance;

CREATE TABLE main.product_substance
(
  product_substance_id bigserial NOT NULL,
  product_substance_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  substance_id bigint NOT NULL,
  CONSTRAINT product_substance_pkey PRIMARY KEY (product_substance_id),
  CONSTRAINT product_substance_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,   
  CONSTRAINT product_substance_substance_id_fkey FOREIGN KEY (substance_id)
      REFERENCES main.substance (substance_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_substance OWNER TO pavlov;

-- DROP TABLE main.product_normative;

CREATE TABLE main.product_mkb10
(
  product_mkb10_id bigserial NOT NULL,
  product_mkb10_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  mkb10_code character varying,
  CONSTRAINT product_mkb10_pkey PRIMARY KEY (product_mkb10_id),
  CONSTRAINT product_mkb10_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION   
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_mkb10 OWNER TO pavlov;

-- DROP TABLE main.product_register_price;

CREATE TABLE main.product_register_price
(
  product_register_price_id bigserial NOT NULL,
  product_register_price_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  register_price numeric,
  CONSTRAINT product_register_price_pkey PRIMARY KEY (product_register_price_id),
  CONSTRAINT product_register_price_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION   
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_register_price OWNER TO pavlov;

-- DROP TABLE main.product_defect;

CREATE TABLE main.product_defect
(
  product_defect_id bigserial NOT NULL,
  product_defect_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  defect_series character varying,
  CONSTRAINT product_defect_pkey PRIMARY KEY (product_defect_id),
  CONSTRAINT product_defect_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION   
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_defect OWNER TO pavlov;

-- DROP TABLE main.product_pharm_group;

CREATE TABLE main.product_pharm_group
(
  product_pharm_group_id bigserial NOT NULL,
  product_pharm_group_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  pharm_group_id bigint NOT NULL,
  CONSTRAINT product_pharm_group_pkey PRIMARY KEY (product_pharm_group_id),
  CONSTRAINT product_pharm_group_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,   
  CONSTRAINT product_pharm_group_pharm_group_id_fkey FOREIGN KEY (pharm_group_id)
      REFERENCES main.pharm_group (pharm_group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_pharm_group OWNER TO pavlov;


-- DROP TABLE main.product_consumer;

CREATE TABLE main.product_consumer
(
  product_consumer_id bigserial NOT NULL,
  product_consumer_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  consumer_id bigint NOT NULL,
  CONSTRAINT product_consumer_pkey PRIMARY KEY (product_consumer_id),
  CONSTRAINT product_consumer_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,   
  CONSTRAINT product_consumer_consumer_id_fkey FOREIGN KEY (consumer_id)
      REFERENCES main.consumer (consumer_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_consumer OWNER TO pavlov;

-- DROP TABLE main.product_variant;

CREATE TABLE main.product_variant
(
  product_variant_id bigserial NOT NULL,
  product_variant_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  variant_id bigint NOT NULL,
  CONSTRAINT product_variant_pkey PRIMARY KEY (product_variant_id),
  CONSTRAINT product_variant_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,   
  CONSTRAINT product_variant_variant_id_fkey FOREIGN KEY (variant_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_variant OWNER TO pavlov;

-- DROP TABLE main.product_adjacent;

CREATE TABLE main.product_adjacent
(
  product_adjacent_id bigserial NOT NULL,
  product_adjacent_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  adjacent_id bigint NOT NULL,
  CONSTRAINT product_adjacent_pkey PRIMARY KEY (product_adjacent_id),
  CONSTRAINT product_adjacent_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,   
  CONSTRAINT product_adjacent_adjacent_id_fkey FOREIGN KEY (adjacent_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_adjacent OWNER TO pavlov;


-- DROP TABLE main.supplier;

CREATE TABLE main.supplier
(
  supplier_id serial NOT NULL,
  supplier_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  supplier_name character varying NOT NULL, 
  CONSTRAINT supplier_pkey PRIMARY KEY (supplier_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.supplier OWNER TO pavlov;

-- DROP TABLE main.supplier_good;

CREATE TABLE main.supplier_good
(
  supplier_good_id serial NOT NULL,
  supplier_good_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  supplier_id integer NOT NULL, 
  good_id bigint NOT NULL,
  price numeric,
  quantity numeric,
  CONSTRAINT supplier_good_pkey PRIMARY KEY (supplier_good_id),
  CONSTRAINT supplier_good_supplier_id_fkey FOREIGN KEY (supplier_id)
      REFERENCES main.supplier (supplier_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,    
  CONSTRAINT supplier_good_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES main.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.supplier_good OWNER TO pavlov;

-- DROP TABLE main.mfr_role;

CREATE TABLE main.mfr_role
(
  mfr_role_id integer NOT NULL,
  mfr_role_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  mfr_role_name character varying NOT NULL,  
  CONSTRAINT mfr_role_pkey PRIMARY KEY (mfr_role_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.mfr OWNER TO pavlov;

-- DROP TABLE main.good_mfr;

CREATE TABLE main.good_mfr
(
  good_mfr_id bigserial NOT NULL,
  good_mfr_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),
  good_id bigint NOT NULL, 
  mfr_id bigint NOT NULL, 
  mfr_role_id integer,
  CONSTRAINT good_mfr_pkey PRIMARY KEY (good_mfr_id),
  CONSTRAINT good_mfr_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES main.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,    
  CONSTRAINT good_mfr_mfr_id_fkey FOREIGN KEY (mfr_id)
      REFERENCES main.mfr (mfr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT good_mfr_mfr_role_id_fkey FOREIGN KEY (mfr_role_id)
      REFERENCES main.mfr_role (mfr_role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.good_mfr OWNER TO pavlov;