﻿/*
	transfer from ESN to AuEsnDb
*/

select count(*) from public.atx;
-- 1816

ALTER TABLE public.atx ADD COLUMN id bigserial NOT NULL;

INSERT INTO main.atc (atc_id, atc_name, atc_code)
SELECT id, atxname, atx
FROM public.atx;

select * from main.atc;

------------------------------

select count(*) from public.countries;
select * from public.countries;
-- 252

ALTER TABLE public.countries ADD COLUMN id serial NOT NULL;

INSERT INTO main.country (
  country_id,  
  country_name,
  short_name,
  aplpha2,
  aplpha3,
  country_code
)
SELECT id, coalesce(fullname,shortname), shortname, alpha2, alpha3, countrycode
FROM public.countries;

select * from main.country;

------------------------------

select count(*) from public.dosage;
select * from public.dosage;
-- 2478

INSERT INTO main.dosage (
  dosage_id,  
  dosage_name
)
SELECT dosageid, trim((weight::character varying) || ' ' || trim(coalesce(weightunit, '')))
FROM public.dosage
where weight is not null;

select * from main.dosage;

------------------------------

select count(*) from public.inn;
select * from public.inn;
-- 2230

ALTER TABLE public.inn ADD COLUMN id bigserial NOT NULL;

INSERT INTO main.inn (
  inn_id,  
  inn_name
)
SELECT id, inn
FROM public.inn;

select * from main.inn;

------------------------------

select count(*) from public.manufacturers;
select * from public.manufacturers;
-- 7880

INSERT INTO main.mfr (
  mfr_id,
  mfr_name,
  country_id,  
  crt_date,
  crt_user,
  upd_date,
  upd_user
)
SELECT t1.manufacturer, t1.manufacturername, t2.id, current_timestamp, 'adm', current_timestamp, 'adm'
FROM public.manufacturers t1
INNER JOIN public.countries t2 ON t1.countrycode = t2.countrycode
;

select * from main.mfr;

------------------------------

select count(*) from public.manufactureraliases;
select * from public.manufactureraliases;
-- 10173

ALTER TABLE public.manufactureraliases ADD COLUMN id bigserial NOT NULL;

INSERT INTO main.mfr_alias (
  mfr_alias_id,  
  mfr_id,
  mfr_alias_name
)
SELECT id, manufacturer, aka
FROM public.manufactureraliases;

select * from main.mfr_alias;

------------------------------

select count(*) from public.metanames;
select * from public.metanames;
-- 9656

ALTER TABLE public.metanames ADD COLUMN id bigserial NOT NULL;

INSERT INTO main.metaname (
  metaname_id,  
  metaname_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user
)
SELECT id, metaname, current_timestamp, 'adm', current_timestamp, 'adm'
FROM public.metanames;

------------------------------

select count(*) from public.okdp;
select * from public.okdp;
-- 44430

ALTER TABLE public.okdp ADD COLUMN id serial NOT NULL;

INSERT INTO main.okpd2 (
  okpd2_id, 
  okpd2_name,
  okdp_code
)
SELECT id, okdp_text, okdp_id
FROM public.okdp;

select * from main.okpd2;

------------------------------

select count(*) from public.packagekinds;
select * from public.packagekinds;
-- 786

INSERT INTO main.pack (
  pack_id,  
  pack_name,
  short_name
)
SELECT packageid, packagenamefull, packagename
FROM public.packagekinds;

select * from main.pack;

------------------------------

select count(*) from public.pharmgroups;
select * from public.pharmgroups;
-- 303

INSERT INTO main.pharm_group (
  pharm_group_id,  
  pharm_group_name  
)
SELECT pharmgroupid, pharmgroupname
FROM public.pharmgroups;

select * from main.pharm_group;

------------------------------

select count(*) from public.productforms;
select * from public.productforms;
-- 653

ALTER TABLE public.productforms ADD COLUMN id serial NOT NULL;

INSERT INTO main.form (
  form_id,  
  form_name,
  short_name,
  group_name
)
SELECT id, productformfull, productform, groupform
FROM public.productforms;

select * from main.form;

------------------------------

ALTER TABLE public.pharmmarket ADD COLUMN id serial NOT NULL;

------------------------------

insert into main.product_type (product_type_id, product_type_name)
values (1, 'Лекарственное средство');

insert into main.product_type (product_type_id, product_type_name)
values (2, 'БАД');

insert into main.product_type (product_type_id, product_type_name)
values (3, 'ИМН');

insert into main.product_type (product_type_id, product_type_name)
values (4, 'Прочее');

------------------------------

insert into main.recipe_type  (recipe_type_id, recipe_type_name)
values (1, 'Рецепт остается в аптеке');

insert into main.recipe_type  (recipe_type_id, recipe_type_name)
values (2, 'Рецепт не остается в аптеке');

select * from main.recipe_type;

------------------------------
insert into main.baa_type  (baa_type_id, baa_type_name)
values (1, 'Другие БАДы');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (2, 'БАДы - бальзамы, чаи, взвары, сборы');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (3, 'БАДы - белки, аминокислоты и их производные');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (4, 'БАДы - витаминно-минеральные комплексы');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (5, 'БАДы - витамины, витаминоподобные вещества и коферменты');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (6, 'БАДы - естественные метаболиты');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (7, 'БАДы - жиры, жироподобные вещества и их производные');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (8, 'БАДы - макро- и микроэлементы');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (9, 'БАДы - полифенольные соединения');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (10, 'БАДы - продукты пчеловодства');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (11, 'БАДы - пробиотики и пребиотики');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (12, 'БАДы - продукты растительного, животного или минерального происхождения');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (13, 'БАДы - углеводы и продукты их переработки');

insert into main.baa_type  (baa_type_id, baa_type_name)
values (14, 'БАДы - ферменты растительного или микробного происхождения');

select * from main.baa_type;
------------------------------

select count(*) from public.products;
select * from public.products order by productid desc limit 100;
-- 144474

delete from main.product;

insert into main.product (
  product_id, 
  product_name,
  product_type_id,
  inn_id,
  atc_id,
  dosage_id,
  form_id,
  pharm_ther_group_id,
  okpd2_id,
  brand_id,
  recipe_type_id,
  registry_id,
  baa_type_id,
  metaname_id,
  storage_period,
  storage_condition,
  is_vital,
  is_drug2,
  is_drug3,
  is_precursor,
  is_alcohol,
  is_imm_bio,
  is_mandatory,
  is_defect_exists,
  indication,
  contraindication,
  drug_interaction,
  crt_date,
  crt_user,
  upd_date,
  upd_user
)
select 
  t1.productid,
  t1.productname,
  case t2.id when 2 then 2 else 1 end,
  t3.id,
  t4.id,
  t8.dosage_id,
  t5.id,
  null,
  t6.okdp_id,
  null,
  case t1.receiptdemand when true then 1 else null end,
  null,
  case t2.id when 2 then 1 else null end,
  t7.id,
  t1.storageperiod::character varying,
  null,
  case t1.vital when true then true else false end,
  case t1.drug when 1 then true else false end,
  false,
  false,
  false,
  false,
  false,
  false,
  null,
  null,
  null,
  current_timestamp, 
  'adm', 
  current_timestamp, 
  'adm'  
from public.products t1
left join public.pharmmarket t2 on t1.pharmmarketproduct = t2.pharmmarketproduct
left join public.inn t3 on t1.inn = t3.inn
left join public.atx t4 on t3.atx = t4.atx
left join public.productforms t5 on t1.productform = t5.productform
left join (
select max(x2.id) as okdp_id, x1.productid 
from public.okdpofproduct x1 
inner join public.okdp x2 on x1.okdp_id = x2.okdp_id
group by x1.productid
) t6 on t1.productid = t6.productid
left join public.metanames t7 on t1.metaname = t7.metaname
left join main.dosage t8 on t1.dosageid = t8.dosage_id
;

select count(*) from main.product;
------------------------------

select count(*) from public.productpacks;
select * from public.productpacks order by productid desc limit 100;
-- 245320

ALTER TABLE public.productpacks ADD COLUMN id bigserial NOT NULL;

INSERT INTO main.product_pack (
  product_pack_id,  
  product_id,
  pack_id
)
SELECT id, productid, packageid
FROM public.productpacks;

select count(*) from main.product_pack;

------------------------------

select count(*) from public.pharmgroupsofproduct;
select * from public.pharmgroupsofproduct;
-- 303

ALTER TABLE public.pharmgroupsofproduct ADD COLUMN id bigserial NOT NULL;

INSERT INTO main.product_pharm_group (
  product_pharm_group_id,  
  product_id,
  pharm_group_id
)
SELECT id, productid, pharmgroupid
FROM public.pharmgroupsofproduct;

select count(*) from main.product_pharm_group;

-- !!!
-- update sequences
