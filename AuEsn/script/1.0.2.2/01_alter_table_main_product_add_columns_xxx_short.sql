﻿/*
	ALTER TABLE main.product ADD COLUMNs xxx_short
*/

ALTER TABLE main.product ADD COLUMN storage_condition_short character varying;
COMMENT ON COLUMN main.product.storage_condition_short IS 'условия хранения (кратко)';

ALTER TABLE main.product ADD COLUMN indication_short character varying;
COMMENT ON COLUMN main.product.indication_short IS 'показания (кратко)';

ALTER TABLE main.product ADD COLUMN contraindication_short character varying;
COMMENT ON COLUMN main.product.contraindication_short IS 'противопоказания (кратко)';