﻿/*
	CREATE OR REPLACE VIEW main.vw_product
*/

DROP VIEW main.vw_product;

CREATE OR REPLACE VIEW main.vw_product AS 
 SELECT t1.product_id,
    t1.product_uuid,
    t1.product_name,
    t1.product_type_id,
    t1.atc_id,
    t1.dosage_id,
    t1.form_id,
    t1.pharm_ther_group_id,
    t1.okpd2_id,
    t1.brand_id,
    t1.recipe_type_id,
    t1.registry_id,
    t1.baa_type_id,
    t1.metaname_id,
    t1.storage_period,
    t1.storage_condition,
    t1.is_vital,
    t1.is_drug2,
    t1.is_drug3,
    t1.is_precursor,
    t1.is_alcohol,
    t1.is_imm_bio,
    t1.is_mandatory,
    t1.is_defect_exists,
    t1.indication,
    t1.contraindication,
    t1.drug_interaction,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.trade_name_id,
    t1.storage_condition_short,
    t1.indication_short,
    t1.contraindication_short,
    t2.product_type_uuid,
    t12.atc_uuid,
    t13.dosage_uuid,
    t14.form_uuid,
    t15.pharm_ther_group_uuid,
    t16.okpd2_uuid,
    t17.brand_uuid,
    t18.recipe_type_uuid,
    t19.registry_uuid,
    t20.baa_type_uuid,
    t21.metaname_uuid,
    t22.trade_name_uuid
   FROM main.product t1
     JOIN main.product_type t2 ON t1.product_type_id = t2.product_type_id
     LEFT JOIN main.atc t12 ON t1.atc_id = t12.atc_id
     LEFT JOIN main.dosage t13 ON t1.dosage_id = t13.dosage_id
     LEFT JOIN main.form t14 ON t1.form_id = t14.form_id
     LEFT JOIN main.pharm_ther_group t15 ON t1.pharm_ther_group_id = t15.pharm_ther_group_id
     LEFT JOIN main.okpd2 t16 ON t1.okpd2_id = t16.okpd2_id
     LEFT JOIN main.brand t17 ON t1.brand_id = t17.brand_id
     LEFT JOIN main.recipe_type t18 ON t1.recipe_type_id = t18.recipe_type_id
     LEFT JOIN main.registry t19 ON t1.registry_id = t19.registry_id
     LEFT JOIN main.baa_type t20 ON t1.baa_type_id = t20.baa_type_id
     LEFT JOIN main.metaname t21 ON t1.metaname_id = t21.metaname_id
     LEFT JOIN main.trade_name t22 ON t1.trade_name_id = t22.trade_name_id;

ALTER TABLE main.vw_product OWNER TO pavlov;
