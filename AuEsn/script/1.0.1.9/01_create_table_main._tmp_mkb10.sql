﻿/*
	CREATE TABLE main._tmp_mkb10
*/

-- DROP TABLE main._tmp_mkb10;

CREATE TABLE main._tmp_mkb10
(
  mkb_cod character varying,
  mkb_name character varying,
  class_id character varying
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main._tmp_mkb10 OWNER TO pavlov;
