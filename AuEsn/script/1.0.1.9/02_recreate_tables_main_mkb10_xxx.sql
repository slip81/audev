﻿/*
	RECREATE TABLEs main.mkb10_xxx
*/

-- DROP TABLE main.mkb10_class;

CREATE TABLE main.mkb10_class
(
  mkb10_class_id serial NOT NULL,
  mkb10_class_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),  
  mkb10_class_code character varying NOT NULL,
  mkb10_class_name character varying,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT mkb10_class_pkey PRIMARY KEY (mkb10_class_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.mkb10_class OWNER TO pavlov;

COMMENT ON TABLE main.mkb10_class IS 'Справочник классов МКБ-10';

-- DROP TRIGGER main_mkb10_class_set_date_and_user_trg ON main.mkb10_class;

CREATE TRIGGER main_mkb10_class_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.mkb10_class
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

---------------------------------------------------

INSERT INTO main.mkb10_class (mkb10_class_code, mkb10_class_name, upd_user)
SELECT DISTINCT class_id, class_id, 'pavlov'
FROM main._tmp_mkb10
ORDER BY class_id;

---------------------------------------------------

ALTER TABLE main.product_mkb10 DROP CONSTRAINT product_mkb10_mkb10_id_fkey;
DROP VIEW main.vw_product_mkb10;
DROP VIEW main.vw_mkb10;
DROP TABLE main.mkb10;

CREATE TABLE main.mkb10
(
  mkb10_id serial NOT NULL,
  mkb10_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  mkb10_name character varying NOT NULL,
  mkb10_code character varying,
  mkb10_class_id integer,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT mkb10_pkey PRIMARY KEY (mkb10_id),
  CONSTRAINT mkb10_mkb10_class_id_fkey FOREIGN KEY (mkb10_class_id)
      REFERENCES main.mkb10_class (mkb10_class_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.mkb10 OWNER TO pavlov;

COMMENT ON TABLE main.mkb10 IS 'Справочник МКБ-10';

-- DROP TRIGGER main_mkb10_set_date_and_user_trg ON main.mkb10;

CREATE TRIGGER main_mkb10_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.mkb10
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

---------------------------------------------------
INSERT INTO main.mkb10 (mkb10_name, mkb10_code, upd_user)
SELECT DISTINCT mkb_name, mkb_cod, 'pavlov'
FROM main._tmp_mkb10
ORDER BY mkb_cod;

UPDATE main.mkb10 t1
SET mkb10_class_id = (SELECT MAX(x1.mkb10_class_id) FROM main.mkb10_class x1 WHERE t2.class_id = x1.mkb10_class_code)
FROM main._tmp_mkb10 t2
WHERE t1.mkb10_code = t2.mkb_cod;
---------------------------------------------------

CREATE OR REPLACE VIEW main.vw_mkb10 AS 
 SELECT t1.mkb10_id,
    t1.mkb10_uuid,
    t1.mkb10_name,
    t1.mkb10_code,
    t1.mkb10_class_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t11.mkb10_class_uuid
   FROM main.mkb10 t1
     LEFT JOIN main.mkb10_class t11 ON t1.mkb10_class_id = t11.mkb10_class_id;

ALTER TABLE main.vw_mkb10 OWNER TO pavlov;

---------------------------------------------------

CREATE OR REPLACE VIEW main.vw_product_mkb10 AS 
 SELECT t1.product_mkb10_id,
    t1.product_mkb10_uuid,
    t1.product_id,
    t1.mkb10_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.product_uuid,
    t3.mkb10_uuid
   FROM main.product_mkb10 t1
     JOIN main.product t2 ON t1.product_id = t2.product_id
     JOIN main.mkb10 t3 ON t1.mkb10_id = t3.mkb10_id;

ALTER TABLE main.vw_product_mkb10 OWNER TO pavlov;

---------------------------------------------------

ALTER TABLE main.product_mkb10
  ADD CONSTRAINT product_mkb10_mkb10_id_fkey FOREIGN KEY (mkb10_id)
      REFERENCES main.mkb10 (mkb10_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
