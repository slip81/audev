﻿/*
	CREATE TABLE main.doc_client_to
*/

-- DROP TABLE main.doc_client_to;

CREATE TABLE main.doc_client_to
(
  doc_client_to_id bigserial NOT NULL,
  doc_client_to_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  doc_id bigint NOT NULL,
  client_id integer NOT NULL,
  doc_state_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT doc_client_to_pkey PRIMARY KEY (doc_client_to_id),  
  CONSTRAINT doc_client_to_doc_id_fkey FOREIGN KEY (doc_id)
      REFERENCES main.doc (doc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT doc_client_to_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES main.client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT doc_client_to_doc_state_id_fkey FOREIGN KEY (doc_state_id)
      REFERENCES main.doc_state (doc_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.doc_client_to OWNER TO pavlov;

COMMENT ON TABLE main.doc_client_to
  IS 'Получатели документов';
