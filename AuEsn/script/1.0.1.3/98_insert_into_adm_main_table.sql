﻿/*
	insert into adm.main_table
*/

insert into adm.main_table (main_table_name, allow_select, allow_insert, allow_update, allow_delete)
values ('doc_client_to', true, true, true, true);

insert into adm.main_table (main_table_name, allow_select, allow_insert, allow_update, allow_delete)
values ('client_rel', true, true, true, true);

-- select * from adm.main_table order by main_table_id