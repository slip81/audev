﻿/*
	ALTER TABLEs main.xxx ADD COLUMNs is_deleted, del_date, del_user
*/

ALTER TABLE main.atc ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.atc ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.atc ADD COLUMN del_user character varying;

ALTER TABLE main.brand ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.brand ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.brand ADD COLUMN del_user character varying;

ALTER TABLE main.consumer ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.consumer ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.consumer ADD COLUMN del_user character varying;

ALTER TABLE main.country ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.country ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.country ADD COLUMN del_user character varying;

ALTER TABLE main.doc_type ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.doc_type ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.doc_type ADD COLUMN del_user character varying;

ALTER TABLE main.dosage ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.dosage ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.dosage ADD COLUMN del_user character varying;

ALTER TABLE main.form ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.form ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.form ADD COLUMN del_user character varying;

ALTER TABLE main.inn ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.inn ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.inn ADD COLUMN del_user character varying;

ALTER TABLE main.okpd2 ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.okpd2 ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.okpd2 ADD COLUMN del_user character varying;

ALTER TABLE main.pack ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.pack ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.pack ADD COLUMN del_user character varying;

ALTER TABLE main.pharm_group ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.pharm_group ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.pharm_group ADD COLUMN del_user character varying;

ALTER TABLE main.pharm_ther_group ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.pharm_ther_group ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.pharm_ther_group ADD COLUMN del_user character varying;

ALTER TABLE main.substance ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.substance ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.substance ADD COLUMN del_user character varying;
