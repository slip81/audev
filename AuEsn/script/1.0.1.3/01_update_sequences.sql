﻿/*
	update sequences
*/

select setval('main.atc_atc_id_seq', (select max(atc_id) + 1 from main.atc), false);
select setval('main.country_country_id_seq', (select max(country_id) + 1 from main.country), false);
select setval('main.dosage_dosage_id_seq', (select max(dosage_id) + 1 from main.dosage), false);
select setval('main.inn_inn_id_seq', (select max(inn_id) + 1 from main.inn), false);
select setval('main.mfr_mfr_id_seq', (select max(mfr_id) + 1 from main.mfr), false);
select setval('main.mfr_alias_mfr_alias_id_seq', (select max(mfr_alias_id) + 1 from main.mfr_alias), false);
select setval('main.metaname_metaname_id_seq', (select max(metaname_id) + 1 from main.metaname), false);
select setval('main.okpd2_okpd2_id_seq', (select max(okpd2_id) + 1 from main.okpd2), false);
select setval('main.pack_pack_id_seq', (select max(pack_id) + 1 from main.pack), false);
select setval('main.pharm_group_pharm_group_id_seq', (select max(pharm_group_id) + 1 from main.pharm_group), false);
select setval('main.form_form_id_seq', (select max(form_id) + 1 from main.form), false);
select setval('main.product_product_id_seq', (select max(product_id) + 1 from main.product), false);
select setval('main.product_pack_product_pack_id_seq', (select max(product_pack_id) + 1 from main.product_pack), false);
select setval('main.product_pharm_group_product_pharm_group_id_seq', (select max(product_pharm_group_id) + 1 from main.product_pharm_group), false);
