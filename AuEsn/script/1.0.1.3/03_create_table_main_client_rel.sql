﻿/*
	CREATE TABLE main.client_rel
*/

-- DROP TABLE main.client_rel;

CREATE TABLE main.client_rel
(
  client_rel_id serial NOT NULL,
  client_rel_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  client1_id integer NOT NULL,
  client2_id integer NOT NULL,
  CONSTRAINT client_rel_pkey PRIMARY KEY (client_rel_id),
  CONSTRAINT client_rel_client1_id_fkey FOREIGN KEY (client1_id)
      REFERENCES main.client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT client_rel_client2_id_fkey FOREIGN KEY (client2_id)
      REFERENCES main.client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.client_rel OWNER TO pavlov;

COMMENT ON TABLE main.client_rel IS 'Доступные клиенты для клиента';
