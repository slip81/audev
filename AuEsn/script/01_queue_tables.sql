﻿/*
	Примерный вариант таблиц для очереди заданий
*/



-- DROP TABLE test.queue_type;

-- типы заданий, справочник
CREATE TABLE test.queue_type
(
  q_type_id integer NOT NULL,		-- PK
  q_type_name character varying,	-- наименование типа
  CONSTRAINT queue_type_pkey PRIMARY KEY (q_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE test.queue_type OWNER TO pavlov;



-- DROP TABLE test.queue_state_type;

-- типы статусов обработки заданий, справочник
CREATE TABLE test.queue_state_type
(
  q_state_type_id integer NOT NULL,		-- PK
  q_state_type_name character varying,		-- наименование типа
  CONSTRAINT queue_state_type_pkey PRIMARY KEY (q_state_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE test.queue_state_type OWNER TO pavlov;



-- DROP TABLE test.queue;

-- очередь заданий
CREATE TABLE test.queue
(
  queue_id bigint NOT NULL DEFAULT test.id_generator(),		-- PK
  queue_uuid uuid NOT NULL DEFAULT uuid_generate_v1mc(),	-- как бы тоже PK
  user_id integer,						-- инициатор, нужно ли ? (FK)
  q_type_id integer NOT NULL,					-- тип задания (FK)
  q_state_type_id integer NOT NULL,				-- тип статуса обработки задания (FK)
  crt_date timestamp without time zone NOT NULL,		-- когда создано
  state_date timestamp without time zone NOT NULL,		-- когда изменен статус
  CONSTRAINT queue_pkey PRIMARY KEY (queue_id),
  CONSTRAINT queue_q_type_id_fkey FOREIGN KEY (q_type_id)
      REFERENCES test.queue_type (q_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION, 
  CONSTRAINT queue_q_state_type_id_fkey FOREIGN KEY (q_state_type_id)
      REFERENCES test.queue_state_type (q_state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION   
)
WITH (
  OIDS=FALSE
);

ALTER TABLE test.queue OWNER TO pavlov;



-- DROP TABLE test.queue_data;

-- на всякий случай предусмотрена таблица-расширение для хранения каких-то доп. данных заданий
CREATE TABLE test.queue_data
(
  queue_id bigint NOT NULL,		-- PK, FK, связь 1 к 1
  queue_data text,			-- доп. данные задания (binary ? text ?)
  CONSTRAINT queue_data_pkey PRIMARY KEY (queue_id),
  CONSTRAINT queue_data_queue_id_fkey FOREIGN KEY (queue_id)
      REFERENCES test.queue (queue_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE test.queue_data OWNER TO pavlov;



-- DROP TABLE test.queue_receiver;

-- получатели результатов по заданиям
CREATE TABLE test.queue_receiver
(
  item_id serial,					-- PK
  queue_id bigint NOT NULL,  				-- код задания (FK)
  user_id integer NOT NULL,				-- код пользователя - получатедя задания (FK)
  is_received boolean NOT NULL DEFAULT false,		-- 1 - результат задания получен пользователем, 0 - нет
  CONSTRAINT queue_receiver_pkey PRIMARY KEY (item_id),
  CONSTRAINT queue_receiver_queue_id_fkey FOREIGN KEY (queue_id)
      REFERENCES test.queue (queue_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION    
)
WITH (
  OIDS=FALSE
);

ALTER TABLE test.queue_receiver OWNER TO pavlov;