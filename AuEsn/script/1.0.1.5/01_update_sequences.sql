﻿/*
	update sequences
*/

select setval('main.doc_type_doc_type_id_seq', (select max(doc_type_id) + 1 from main.doc_type), false);
select setval('main.doc_state_doc_state_id_seq', (select max(doc_state_id) + 1 from main.doc_state), false);
