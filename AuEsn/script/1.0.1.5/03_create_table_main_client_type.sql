﻿/*
	CREATE TABLE main.client_type
*/

-- DROP TABLE main.client_type;

CREATE TABLE main.client_type
(
  client_type_id serial NOT NULL,
  client_type_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  client_type_name character varying,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT client_type_pkey PRIMARY KEY (client_type_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE main.client_type OWNER TO pavlov;

COMMENT ON TABLE main.client_type
  IS 'Справочник типов клиентов';

-- DROP TRIGGER main_client_type_set_date_and_user_trg ON main.client_type;

CREATE TRIGGER main_client_type_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.client_type
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

INSERT INTO main.client_type (client_type_name)
VALUES ('ЕСН');

-- select * from main.client_type;