﻿/*
	insert into adm.main_table
*/

insert into adm.main_table (main_table_name, allow_select, allow_insert, allow_update, allow_delete, is_logged, view_name)
values ('product_alias', true, true, true, true, true, 'vw_product_alias');

insert into adm.main_table (main_table_name, allow_select, allow_insert, allow_update, allow_delete, is_logged, view_name)
values ('client_type', true, false, false, false, true, null);


-- select * from adm.main_table order by main_table_id