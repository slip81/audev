﻿/*
	CREATE TABLE main.product_alias
*/

-- DROP TABLE main.product_alias;

CREATE TABLE main.product_alias
(
  product_alias_id bigserial NOT NULL,
  product_alias_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  product_id bigint NOT NULL,
  product_alias_name character varying NOT NULL,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT product_alias_pkey PRIMARY KEY (product_alias_id),
  CONSTRAINT product_alias_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES main.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.product_alias OWNER TO pavlov;

COMMENT ON TABLE main.product_alias
  IS 'Синонимы продкутов';

-- DROP TRIGGER main_product_alias_set_date_and_user_trg ON main.product_alias;

CREATE TRIGGER main_product_alias_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_alias
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

