﻿/*
	CREATE OR REPLACE VIEW main.vw_product_alias
*/

-- DROP VIEW main.vw_product_alias;

CREATE OR REPLACE VIEW main.vw_product_alias AS 
 SELECT t1.product_alias_id,
    t1.product_alias_uuid,
    t1.product_id,
    t1.product_alias_name,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.product_uuid
   FROM main.product_alias t1
     JOIN main.product t2 ON t1.product_id = t2.product_id;

ALTER TABLE main.vw_product_alias OWNER TO pavlov;
