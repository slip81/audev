﻿/*
	ALTER TABLE main.client ADD COLUMN client_type_id
*/

-- ALTER TABLE main.client DROP COLUMN client_type_id;

ALTER TABLE main.client ADD COLUMN client_type_id integer;
COMMENT ON COLUMN main.client.client_type_id IS 'FK тип клиента';

ALTER TABLE main.client
  ADD CONSTRAINT client_client_type_id_fkey FOREIGN KEY (client_type_id)
      REFERENCES main.client_type (client_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
