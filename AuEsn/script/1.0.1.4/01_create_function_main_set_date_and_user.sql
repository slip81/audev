﻿/*
	CREATE OR REPLACE FUNCTION main.set_date_and_user()
*/

-- DROP FUNCTION main.set_date_and_user();

CREATE OR REPLACE FUNCTION main.set_date_and_user()
  RETURNS trigger AS
$BODY$
BEGIN   
    IF TG_OP = 'INSERT' THEN
	NEW.crt_user := NEW.upd_user;
	RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN	
	IF (NEW.is_deleted != true) THEN
		NEW.upd_date := current_timestamp;
	END IF;
	IF ((NEW.is_deleted = true) AND (OLD.is_deleted != true)) THEN
		NEW.del_date := current_timestamp;
		NEW.del_user := NEW.upd_user;
	ELSIF ((NEW.is_deleted != true) AND (OLD.is_deleted = true)) THEN
		NEW.del_date := null;
		NEW.del_user := null;	
	END IF;    	
	RETURN NEW;
    END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION main.set_date_and_user() OWNER TO pavlov;
