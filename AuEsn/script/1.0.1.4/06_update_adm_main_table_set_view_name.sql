﻿/*
	update adm.main_table set view_name
*/

update adm.main_table set view_name = 'vw_' || main_table_name where main_table_name in
(
'client',
'client_doc_type',
'client_good',
'client_rel',
'doc',
'doc_client_to',
'doc_period',
'good',
'good_defect',
'good_mfr',
'good_register_price',
'mfr',
'mfr_alias',
'product',
'product_adjacent',
'product_consumer',
'product_mkb10',
'product_normative',
'product_pack',
'product_pharm_group',
'product_substance',
'product_variant',
'registry',
'substance',
'supplier_good'
);

-- select * from adm.main_table order by main_table_name;