﻿/*
	ALTER TABLEs main.xxx ADD COLUMNs xxx
*/

-----------------------------------------------

ALTER TABLE main.atc DROP COLUMN is_deleted;
ALTER TABLE main.atc DROP COLUMN del_date;
ALTER TABLE main.atc DROP COLUMN del_user;

ALTER TABLE main.atc ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.atc ADD COLUMN crt_user character varying;
ALTER TABLE main.atc ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.atc ADD COLUMN upd_user character varying;
ALTER TABLE main.atc ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.atc ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.atc ADD COLUMN del_user character varying;

CREATE TRIGGER main_atc_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.atc
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

-----------------------------------------------

ALTER TABLE main.baa_type ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.baa_type ADD COLUMN crt_user character varying;
ALTER TABLE main.baa_type ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.baa_type ADD COLUMN upd_user character varying;
ALTER TABLE main.baa_type ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.baa_type ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.baa_type ADD COLUMN del_user character varying;

CREATE TRIGGER main_baa_type_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.baa_type
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

-----------------------------------------------

ALTER TABLE main.brand DROP COLUMN is_deleted;
ALTER TABLE main.brand DROP COLUMN del_date;
ALTER TABLE main.brand DROP COLUMN del_user;

ALTER TABLE main.brand ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.brand ADD COLUMN crt_user character varying;
ALTER TABLE main.brand ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.brand ADD COLUMN upd_user character varying;
ALTER TABLE main.brand ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.brand ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.brand ADD COLUMN del_user character varying;

CREATE TRIGGER main_brand_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.brand
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

-----------------------------------------------

ALTER TABLE main.client DROP COLUMN crt_date;
ALTER TABLE main.client DROP COLUMN crt_user;
ALTER TABLE main.client DROP COLUMN upd_date;
ALTER TABLE main.client DROP COLUMN upd_user;
ALTER TABLE main.client DROP COLUMN is_deleted;
ALTER TABLE main.client DROP COLUMN del_date;
ALTER TABLE main.client DROP COLUMN del_user;

ALTER TABLE main.client ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client ADD COLUMN crt_user character varying;
ALTER TABLE main.client ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client ADD COLUMN upd_user character varying;
ALTER TABLE main.client ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.client ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.client ADD COLUMN del_user character varying;

CREATE TRIGGER main_client_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.client
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();  

-----------------------------------------------

ALTER TABLE main.client_doc_type ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client_doc_type ADD COLUMN crt_user character varying;
ALTER TABLE main.client_doc_type ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client_doc_type ADD COLUMN upd_user character varying;
ALTER TABLE main.client_doc_type ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.client_doc_type ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.client_doc_type ADD COLUMN del_user character varying;

CREATE TRIGGER main_client_doc_type_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.client_doc_type
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();    

-----------------------------------------------

ALTER TABLE main.client_good DROP COLUMN crt_date;
ALTER TABLE main.client_good DROP COLUMN crt_user;
ALTER TABLE main.client_good DROP COLUMN upd_date;
ALTER TABLE main.client_good DROP COLUMN upd_user;
ALTER TABLE main.client_good DROP COLUMN is_deleted;
ALTER TABLE main.client_good DROP COLUMN del_date;
ALTER TABLE main.client_good DROP COLUMN del_user;

ALTER TABLE main.client_good ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client_good ADD COLUMN crt_user character varying;
ALTER TABLE main.client_good ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client_good ADD COLUMN upd_user character varying;
ALTER TABLE main.client_good ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.client_good ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.client_good ADD COLUMN del_user character varying;

CREATE TRIGGER main_client_good_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.client_good
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();  

-----------------------------------------------

ALTER TABLE main.client_rel ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client_rel ADD COLUMN crt_user character varying;
ALTER TABLE main.client_rel ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client_rel ADD COLUMN upd_user character varying;
ALTER TABLE main.client_rel ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.client_rel ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.client_rel ADD COLUMN del_user character varying;

CREATE TRIGGER main_client_rel_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.client_rel
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

-----------------------------------------------

ALTER TABLE main.client_service ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client_service ADD COLUMN crt_user character varying;
ALTER TABLE main.client_service ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.client_service ADD COLUMN upd_user character varying;
ALTER TABLE main.client_service ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.client_service ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.client_service ADD COLUMN del_user character varying;

CREATE TRIGGER main_client_service_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.client_service
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();  

-----------------------------------------------

ALTER TABLE main.consumer DROP COLUMN is_deleted;
ALTER TABLE main.consumer DROP COLUMN del_date;
ALTER TABLE main.consumer DROP COLUMN del_user;

ALTER TABLE main.consumer ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.consumer ADD COLUMN crt_user character varying;
ALTER TABLE main.consumer ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.consumer ADD COLUMN upd_user character varying;
ALTER TABLE main.consumer ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.consumer ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.consumer ADD COLUMN del_user character varying;

CREATE TRIGGER main_consumer_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.consumer
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();    

-----------------------------------------------

ALTER TABLE main.country DROP COLUMN is_deleted;
ALTER TABLE main.country DROP COLUMN del_date;
ALTER TABLE main.country DROP COLUMN del_user;

ALTER TABLE main.country ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.country ADD COLUMN crt_user character varying;
ALTER TABLE main.country ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.country ADD COLUMN upd_user character varying;
ALTER TABLE main.country ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.country ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.country ADD COLUMN del_user character varying;

CREATE TRIGGER main_country_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.country
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();      

-----------------------------------------------

ALTER TABLE main.doc DROP COLUMN crt_date;
ALTER TABLE main.doc DROP COLUMN crt_user;
ALTER TABLE main.doc DROP COLUMN upd_date;
ALTER TABLE main.doc DROP COLUMN upd_user;
ALTER TABLE main.doc DROP COLUMN is_deleted;
ALTER TABLE main.doc DROP COLUMN del_date;
ALTER TABLE main.doc DROP COLUMN del_user;

ALTER TABLE main.doc ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc ADD COLUMN crt_user character varying;
ALTER TABLE main.doc ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc ADD COLUMN upd_user character varying;
ALTER TABLE main.doc ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.doc ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.doc ADD COLUMN del_user character varying;

CREATE TRIGGER main_doc_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.doc
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();        

-----------------------------------------------

ALTER TABLE main.doc_client_to DROP COLUMN crt_date;
ALTER TABLE main.doc_client_to DROP COLUMN crt_user;
ALTER TABLE main.doc_client_to DROP COLUMN upd_date;
ALTER TABLE main.doc_client_to DROP COLUMN upd_user;
ALTER TABLE main.doc_client_to DROP COLUMN is_deleted;
ALTER TABLE main.doc_client_to DROP COLUMN del_date;
ALTER TABLE main.doc_client_to DROP COLUMN del_user;

ALTER TABLE main.doc_client_to ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc_client_to ADD COLUMN crt_user character varying;
ALTER TABLE main.doc_client_to ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc_client_to ADD COLUMN upd_user character varying;
ALTER TABLE main.doc_client_to ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.doc_client_to ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.doc_client_to ADD COLUMN del_user character varying;

CREATE TRIGGER main_doc_client_to_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.doc_client_to
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();    

-----------------------------------------------

ALTER TABLE main.doc_period DROP COLUMN crt_date;

ALTER TABLE main.doc_period ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc_period ADD COLUMN crt_user character varying;
ALTER TABLE main.doc_period ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc_period ADD COLUMN upd_user character varying;
ALTER TABLE main.doc_period ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.doc_period ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.doc_period ADD COLUMN del_user character varying;

CREATE TRIGGER main_doc_period_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.doc_period
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();   

-----------------------------------------------

ALTER TABLE main.doc_state ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc_state ADD COLUMN crt_user character varying;
ALTER TABLE main.doc_state ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc_state ADD COLUMN upd_user character varying;
ALTER TABLE main.doc_state ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.doc_state ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.doc_state ADD COLUMN del_user character varying;

CREATE TRIGGER main_doc_state_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.doc_state
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();     

-----------------------------------------------

ALTER TABLE main.doc_type DROP COLUMN is_deleted;
ALTER TABLE main.doc_type DROP COLUMN del_date;
ALTER TABLE main.doc_type DROP COLUMN del_user;

ALTER TABLE main.doc_type ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc_type ADD COLUMN crt_user character varying;
ALTER TABLE main.doc_type ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.doc_type ADD COLUMN upd_user character varying;
ALTER TABLE main.doc_type ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.doc_type ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.doc_type ADD COLUMN del_user character varying;

CREATE TRIGGER main_doc_type_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.doc_type
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();    

-----------------------------------------------

ALTER TABLE main.dosage DROP COLUMN is_deleted;
ALTER TABLE main.dosage DROP COLUMN del_date;
ALTER TABLE main.dosage DROP COLUMN del_user;

ALTER TABLE main.dosage ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.dosage ADD COLUMN crt_user character varying;
ALTER TABLE main.dosage ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.dosage ADD COLUMN upd_user character varying;
ALTER TABLE main.dosage ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.dosage ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.dosage ADD COLUMN del_user character varying;

CREATE TRIGGER main_dosage_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.dosage
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();      

-----------------------------------------------

ALTER TABLE main.form DROP COLUMN is_deleted;
ALTER TABLE main.form DROP COLUMN del_date;
ALTER TABLE main.form DROP COLUMN del_user;

ALTER TABLE main.form ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.form ADD COLUMN crt_user character varying;
ALTER TABLE main.form ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.form ADD COLUMN upd_user character varying;
ALTER TABLE main.form ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.form ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.form ADD COLUMN del_user character varying;

CREATE TRIGGER main_form_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.form
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();        

-----------------------------------------------

ALTER TABLE main.good DROP COLUMN crt_date;
ALTER TABLE main.good DROP COLUMN crt_user;
ALTER TABLE main.good DROP COLUMN upd_date;
ALTER TABLE main.good DROP COLUMN upd_user;
ALTER TABLE main.good DROP COLUMN is_deleted;
ALTER TABLE main.good DROP COLUMN del_date;
ALTER TABLE main.good DROP COLUMN del_user;

ALTER TABLE main.good ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.good ADD COLUMN crt_user character varying;
ALTER TABLE main.good ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.good ADD COLUMN upd_user character varying;
ALTER TABLE main.good ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.good ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.good ADD COLUMN del_user character varying;

CREATE TRIGGER main_good_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.good
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();

-----------------------------------------------

ALTER TABLE main.good_defect ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.good_defect ADD COLUMN crt_user character varying;
ALTER TABLE main.good_defect ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.good_defect ADD COLUMN upd_user character varying;
ALTER TABLE main.good_defect ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.good_defect ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.good_defect ADD COLUMN del_user character varying;

CREATE TRIGGER main_good_defect_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.good_defect
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();   

-----------------------------------------------

ALTER TABLE main.good_mfr ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.good_mfr ADD COLUMN crt_user character varying;
ALTER TABLE main.good_mfr ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.good_mfr ADD COLUMN upd_user character varying;
ALTER TABLE main.good_mfr ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.good_mfr ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.good_mfr ADD COLUMN del_user character varying;

CREATE TRIGGER main_good_mfr_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.good_mfr
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();     

-----------------------------------------------

ALTER TABLE main.good_register_price ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.good_register_price ADD COLUMN crt_user character varying;
ALTER TABLE main.good_register_price ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.good_register_price ADD COLUMN upd_user character varying;
ALTER TABLE main.good_register_price ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.good_register_price ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.good_register_price ADD COLUMN del_user character varying;

CREATE TRIGGER main_good_register_price_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.good_register_price
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();       

-----------------------------------------------

ALTER TABLE main.inn DROP COLUMN is_deleted;
ALTER TABLE main.inn DROP COLUMN del_date;
ALTER TABLE main.inn DROP COLUMN del_user;

ALTER TABLE main.inn ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.inn ADD COLUMN crt_user character varying;
ALTER TABLE main.inn ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.inn ADD COLUMN upd_user character varying;
ALTER TABLE main.inn ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.inn ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.inn ADD COLUMN del_user character varying;

CREATE TRIGGER main_inn_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.inn
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();    

-----------------------------------------------

ALTER TABLE main.metaname DROP COLUMN crt_date;
ALTER TABLE main.metaname DROP COLUMN crt_user;
ALTER TABLE main.metaname DROP COLUMN upd_date;
ALTER TABLE main.metaname DROP COLUMN upd_user;
ALTER TABLE main.metaname DROP COLUMN is_deleted;
ALTER TABLE main.metaname DROP COLUMN del_date;
ALTER TABLE main.metaname DROP COLUMN del_user;

ALTER TABLE main.metaname ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.metaname ADD COLUMN crt_user character varying;
ALTER TABLE main.metaname ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.metaname ADD COLUMN upd_user character varying;
ALTER TABLE main.metaname ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.metaname ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.metaname ADD COLUMN del_user character varying;

CREATE TRIGGER main_metaname_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.metaname
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();      

-----------------------------------------------

ALTER TABLE main.mfr DROP COLUMN crt_date;
ALTER TABLE main.mfr DROP COLUMN crt_user;
ALTER TABLE main.mfr DROP COLUMN upd_date;
ALTER TABLE main.mfr DROP COLUMN upd_user;
ALTER TABLE main.mfr DROP COLUMN is_deleted;
ALTER TABLE main.mfr DROP COLUMN del_date;
ALTER TABLE main.mfr DROP COLUMN del_user;

ALTER TABLE main.mfr ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.mfr ADD COLUMN crt_user character varying;
ALTER TABLE main.mfr ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.mfr ADD COLUMN upd_user character varying;
ALTER TABLE main.mfr ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.mfr ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.mfr ADD COLUMN del_user character varying;

CREATE TRIGGER main_mfr_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.mfr
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();        

-----------------------------------------------

ALTER TABLE main.mfr_alias ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.mfr_alias ADD COLUMN crt_user character varying;
ALTER TABLE main.mfr_alias ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.mfr_alias ADD COLUMN upd_user character varying;
ALTER TABLE main.mfr_alias ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.mfr_alias ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.mfr_alias ADD COLUMN del_user character varying;

CREATE TRIGGER main_mfr_alias_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.mfr_alias
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();          

-----------------------------------------------

ALTER TABLE main.mfr_role ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.mfr_role ADD COLUMN crt_user character varying;
ALTER TABLE main.mfr_role ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.mfr_role ADD COLUMN upd_user character varying;
ALTER TABLE main.mfr_role ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.mfr_role ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.mfr_role ADD COLUMN del_user character varying;

CREATE TRIGGER main_mfr_role_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.mfr_role
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();            

-----------------------------------------------

ALTER TABLE main.okpd2 DROP COLUMN is_deleted;
ALTER TABLE main.okpd2 DROP COLUMN del_date;
ALTER TABLE main.okpd2 DROP COLUMN del_user;

ALTER TABLE main.okpd2 ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.okpd2 ADD COLUMN crt_user character varying;
ALTER TABLE main.okpd2 ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.okpd2 ADD COLUMN upd_user character varying;
ALTER TABLE main.okpd2 ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.okpd2 ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.okpd2 ADD COLUMN del_user character varying;

CREATE TRIGGER main_okpd2_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.okpd2
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();              

-----------------------------------------------

ALTER TABLE main.pack DROP COLUMN is_deleted;
ALTER TABLE main.pack DROP COLUMN del_date;
ALTER TABLE main.pack DROP COLUMN del_user;

ALTER TABLE main.pack ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.pack ADD COLUMN crt_user character varying;
ALTER TABLE main.pack ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.pack ADD COLUMN upd_user character varying;
ALTER TABLE main.pack ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.pack ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.pack ADD COLUMN del_user character varying;

CREATE TRIGGER main_pack_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.pack
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                

-----------------------------------------------

ALTER TABLE main.pharm_group DROP COLUMN is_deleted;
ALTER TABLE main.pharm_group DROP COLUMN del_date;
ALTER TABLE main.pharm_group DROP COLUMN del_user;

ALTER TABLE main.pharm_group ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.pharm_group ADD COLUMN crt_user character varying;
ALTER TABLE main.pharm_group ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.pharm_group ADD COLUMN upd_user character varying;
ALTER TABLE main.pharm_group ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.pharm_group ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.pharm_group ADD COLUMN del_user character varying;

CREATE TRIGGER main_pharm_group_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.pharm_group
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                  

-----------------------------------------------

ALTER TABLE main.pharm_ther_group DROP COLUMN is_deleted;
ALTER TABLE main.pharm_ther_group DROP COLUMN del_date;
ALTER TABLE main.pharm_ther_group DROP COLUMN del_user;

ALTER TABLE main.pharm_ther_group ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.pharm_ther_group ADD COLUMN crt_user character varying;
ALTER TABLE main.pharm_ther_group ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.pharm_ther_group ADD COLUMN upd_user character varying;
ALTER TABLE main.pharm_ther_group ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.pharm_ther_group ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.pharm_ther_group ADD COLUMN del_user character varying;

CREATE TRIGGER main_pharm_ther_group_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.pharm_ther_group
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                    

-----------------------------------------------

ALTER TABLE main.product DROP COLUMN crt_date;
ALTER TABLE main.product DROP COLUMN crt_user;
ALTER TABLE main.product DROP COLUMN upd_date;
ALTER TABLE main.product DROP COLUMN upd_user;
ALTER TABLE main.product DROP COLUMN is_deleted;
ALTER TABLE main.product DROP COLUMN del_date;
ALTER TABLE main.product DROP COLUMN del_user;

ALTER TABLE main.product ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product ADD COLUMN crt_user character varying;
ALTER TABLE main.product ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product ADD COLUMN upd_user character varying;
ALTER TABLE main.product ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();      

-----------------------------------------------

ALTER TABLE main.product_adjacent ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_adjacent ADD COLUMN crt_user character varying;
ALTER TABLE main.product_adjacent ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_adjacent ADD COLUMN upd_user character varying;
ALTER TABLE main.product_adjacent ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product_adjacent ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product_adjacent ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_adjacent_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_adjacent
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();        

-----------------------------------------------

ALTER TABLE main.product_consumer ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_consumer ADD COLUMN crt_user character varying;
ALTER TABLE main.product_consumer ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_consumer ADD COLUMN upd_user character varying;
ALTER TABLE main.product_consumer ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product_consumer ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product_consumer ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_consumer_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_consumer
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();          

-----------------------------------------------

ALTER TABLE main.product_mkb10 ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_mkb10 ADD COLUMN crt_user character varying;
ALTER TABLE main.product_mkb10 ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_mkb10 ADD COLUMN upd_user character varying;
ALTER TABLE main.product_mkb10 ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product_mkb10 ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product_mkb10 ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_mkb10_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_mkb10
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();            

-----------------------------------------------

ALTER TABLE main.product_normative ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_normative ADD COLUMN crt_user character varying;
ALTER TABLE main.product_normative ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_normative ADD COLUMN upd_user character varying;
ALTER TABLE main.product_normative ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product_normative ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product_normative ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_normative_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_normative
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();              

-----------------------------------------------

ALTER TABLE main.product_pack ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_pack ADD COLUMN crt_user character varying;
ALTER TABLE main.product_pack ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_pack ADD COLUMN upd_user character varying;
ALTER TABLE main.product_pack ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product_pack ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product_pack ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_pack_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_pack
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                

-----------------------------------------------

ALTER TABLE main.product_pharm_group ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_pharm_group ADD COLUMN crt_user character varying;
ALTER TABLE main.product_pharm_group ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_pharm_group ADD COLUMN upd_user character varying;
ALTER TABLE main.product_pharm_group ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product_pharm_group ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product_pharm_group ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_pharm_group_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_pharm_group
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                  

-----------------------------------------------

ALTER TABLE main.product_substance ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_substance ADD COLUMN crt_user character varying;
ALTER TABLE main.product_substance ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_substance ADD COLUMN upd_user character varying;
ALTER TABLE main.product_substance ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product_substance ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product_substance ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_substance_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_substance
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                    

-----------------------------------------------

ALTER TABLE main.product_type ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_type ADD COLUMN crt_user character varying;
ALTER TABLE main.product_type ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_type ADD COLUMN upd_user character varying;
ALTER TABLE main.product_type ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product_type ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product_type ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_type_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_type
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                      

-----------------------------------------------

ALTER TABLE main.product_variant ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_variant ADD COLUMN crt_user character varying;
ALTER TABLE main.product_variant ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.product_variant ADD COLUMN upd_user character varying;
ALTER TABLE main.product_variant ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.product_variant ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.product_variant ADD COLUMN del_user character varying;

CREATE TRIGGER main_product_variant_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.product_variant
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                        

-----------------------------------------------

ALTER TABLE main.recipe_type ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.recipe_type ADD COLUMN crt_user character varying;
ALTER TABLE main.recipe_type ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.recipe_type ADD COLUMN upd_user character varying;
ALTER TABLE main.recipe_type ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.recipe_type ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.recipe_type ADD COLUMN del_user character varying;

CREATE TRIGGER main_recipe_type_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.recipe_type
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                          

-----------------------------------------------

ALTER TABLE main.registry ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.registry ADD COLUMN crt_user character varying;
ALTER TABLE main.registry ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.registry ADD COLUMN upd_user character varying;
ALTER TABLE main.registry ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.registry ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.registry ADD COLUMN del_user character varying;

CREATE TRIGGER main_registry_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.registry
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                            

-----------------------------------------------

ALTER TABLE main.service ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.service ADD COLUMN crt_user character varying;
ALTER TABLE main.service ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.service ADD COLUMN upd_user character varying;
ALTER TABLE main.service ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.service ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.service ADD COLUMN del_user character varying;

CREATE TRIGGER main_service_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.service
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                              

-----------------------------------------------

ALTER TABLE main.substance DROP COLUMN is_deleted;
ALTER TABLE main.substance DROP COLUMN del_date;
ALTER TABLE main.substance DROP COLUMN del_user;

ALTER TABLE main.substance ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.substance ADD COLUMN crt_user character varying;
ALTER TABLE main.substance ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.substance ADD COLUMN upd_user character varying;
ALTER TABLE main.substance ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.substance ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.substance ADD COLUMN del_user character varying;

CREATE TRIGGER main_substance_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.substance
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();                                

-----------------------------------------------

ALTER TABLE main.supplier DROP COLUMN crt_date;
ALTER TABLE main.supplier DROP COLUMN crt_user;
ALTER TABLE main.supplier DROP COLUMN upd_date;
ALTER TABLE main.supplier DROP COLUMN upd_user;
ALTER TABLE main.supplier DROP COLUMN is_deleted;
ALTER TABLE main.supplier DROP COLUMN del_date;
ALTER TABLE main.supplier DROP COLUMN del_user;

ALTER TABLE main.supplier ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.supplier ADD COLUMN crt_user character varying;
ALTER TABLE main.supplier ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.supplier ADD COLUMN upd_user character varying;
ALTER TABLE main.supplier ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.supplier ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.supplier ADD COLUMN del_user character varying;

CREATE TRIGGER main_supplier_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.supplier
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();   

-----------------------------------------------

ALTER TABLE main.supplier_good ADD COLUMN crt_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.supplier_good ADD COLUMN crt_user character varying;
ALTER TABLE main.supplier_good ADD COLUMN upd_date timestamp without time zone NOT NULL DEFAULT current_timestamp;
ALTER TABLE main.supplier_good ADD COLUMN upd_user character varying;
ALTER TABLE main.supplier_good ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE main.supplier_good ADD COLUMN del_date timestamp without time zone;
ALTER TABLE main.supplier_good ADD COLUMN del_user character varying;

CREATE TRIGGER main_supplier_good_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.supplier_good
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();   

    