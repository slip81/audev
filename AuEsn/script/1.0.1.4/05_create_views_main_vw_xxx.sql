﻿/*
	CREATE VIEWS main.vw_xxx
*/

------------------------------------------------------

-- DROP VIEW main.vw_client;

CREATE OR REPLACE VIEW main.vw_client AS 
 SELECT t1.client_id,
  t1.client_uuid,
  t1.client_name,
  t1.parent_id,
  t1.labeling_code,
  t1.cab_client_id,
  t1.cab_sales_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t11.client_uuid as parent_uuid
   FROM main.client t1
   LEFT JOIN main.client t11 ON t1.parent_id = t11.client_id;

ALTER TABLE main.vw_client OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_client_doc_type;

CREATE OR REPLACE VIEW main.vw_client_doc_type AS 
 SELECT t1.client_doc_type_id,
  t1.client_doc_type_uuid,
  t1.client_id,
  t1.doc_type_id,
  t1.valid_date,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.client_uuid,
  t3.doc_type_uuid
   FROM main.client_doc_type t1
   INNER JOIN main.client t2 ON t1.client_id = t2.client_id
   INNER JOIN main.doc_type t3 ON t1.doc_type_id = t3.doc_type_id
   ;

ALTER TABLE main.vw_client_doc_type OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_client_good;

CREATE OR REPLACE VIEW main.vw_client_good AS 
 SELECT t1.client_good_id,
  t1.client_good_uuid,
  t1.client_id,
  t1.good_id,
  t1.client_code,
  t1.client_name,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.client_uuid,
  t3.good_uuid
   FROM main.client_good t1
   INNER JOIN main.client t2 ON t1.client_id = t2.client_id   
   INNER JOIN main.good t3 ON t1.good_id = t3.good_id   
   ;

ALTER TABLE main.vw_client_good OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_client_rel;

CREATE OR REPLACE VIEW main.vw_client_rel AS 
 SELECT t1.client_rel_id,
  t1.client_rel_uuid,
  t1.client1_id,
  t1.client2_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.client_uuid as client1_uuid,
  t3.client_uuid as client2_uuid
   FROM main.client_rel t1
   INNER JOIN main.client t2 ON t1.client1_id = t2.client_id   
   INNER JOIN main.client t3 ON t1.client2_id = t3.client_id      
   ;

ALTER TABLE main.vw_client_rel OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_doc;

CREATE OR REPLACE VIEW main.vw_doc AS 
 SELECT t1.doc_id,
  t1.doc_uuid,
  t1.doc_num,
  t1.doc_name,
  t1.doc_date,
  t1.doc_type_id,
  t1.doc_state_id,
  t1.doc_path,
  t1.client_from_id,
  t1.client_to_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.doc_type_uuid,
  t3.doc_state_uuid,
  t4.client_uuid as client_from_uuid,
  t11.client_uuid as client_to_uuid
   FROM main.doc t1
   INNER JOIN main.doc_type t2 ON t1.doc_type_id = t2.doc_type_id
   INNER JOIN main.doc_state t3 ON t1.doc_state_id = t3.doc_state_id
   INNER JOIN main.client t4 ON t1.client_from_id = t4.client_id   
   LEFT JOIN main.client t11 ON t1.client_to_id = t11.client_id      
   ;

ALTER TABLE main.vw_doc OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_doc_client_to;

CREATE OR REPLACE VIEW main.vw_doc_client_to AS 
 SELECT t1.doc_client_to_id,
  t1.doc_client_to_uuid,
  t1.doc_id,
  t1.client_id,
  t1.doc_state_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.doc_uuid,  
  t3.client_uuid,
  t4.doc_state_uuid  
   FROM main.doc_client_to t1
   INNER JOIN main.doc t2 ON t1.doc_id = t2.doc_id
   INNER JOIN main.client t3 ON t1.client_id = t3.client_id   
   INNER JOIN main.doc_state t4 ON t1.doc_state_id = t4.doc_state_id   
   ;

ALTER TABLE main.vw_doc_client_to OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_doc_period;

CREATE OR REPLACE VIEW main.vw_doc_period AS 
 SELECT t1.doc_period_id,
  t1.doc_period_uuid,
  t1.doc_id,
  t1.doc_state_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.doc_uuid,
  t3.doc_state_uuid
   FROM main.doc_period t1
   INNER JOIN main.doc t2 ON t1.doc_id = t2.doc_id
   INNER JOIN main.doc_state t3 ON t1.doc_state_id = t3.doc_state_id
   ;

ALTER TABLE main.vw_doc_period OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_good;

CREATE OR REPLACE VIEW main.vw_good AS 
 SELECT t1.good_id,
  t1.good_uuid,
  t1.product_id,
  t1.mfr_id,
  t1.barcode,
  t1.gtin,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_uuid,
  t3.mfr_uuid
   FROM main.good t1
   INNER JOIN main.product t2 ON t1.product_id = t2.product_id
   INNER JOIN main.mfr t3 ON t1.mfr_id = t3.mfr_id
   ;

ALTER TABLE main.vw_good OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_good_defect;

CREATE OR REPLACE VIEW main.vw_good_defect AS 
 SELECT t1.good_defect_id,
  t1.good_defect_uuid,
  t1.good_id,
  t1.defect_series,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.good_uuid
   FROM main.good_defect t1
   INNER JOIN main.good t2 ON t1.good_id = t2.good_id
   ;

ALTER TABLE main.vw_good_defect OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_good_mfr;

CREATE OR REPLACE VIEW main.vw_good_mfr AS 
 SELECT t1.good_mfr_id,
  t1.good_mfr_uuid,
  t1.good_id,
  t1.mfr_id,
  t1.mfr_role_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.good_uuid,
  t3.mfr_uuid,
  t11.mfr_role_uuid
   FROM main.good_mfr t1
   INNER JOIN main.good t2 ON t1.good_id = t2.good_id
   INNER JOIN main.mfr t3 ON t1.mfr_id = t3.mfr_id
   LEFT JOIN main.mfr_role t11 ON t1.mfr_role_id = t1.mfr_role_id
   ;

ALTER TABLE main.vw_good_mfr OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_good_register_price;

CREATE OR REPLACE VIEW main.vw_good_register_price AS 
 SELECT t1.good_register_price_id,
  t1.good_register_price_uuid,
  t1.good_id,
  t1.register_price,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.good_uuid
   FROM main.good_register_price t1
   INNER JOIN main.good t2 ON t1.good_id = t2.good_id
   ;

ALTER TABLE main.vw_good_register_price OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_mfr;

CREATE OR REPLACE VIEW main.vw_mfr AS 
 SELECT t1.mfr_id,
  t1.mfr_uuid,
  t1.mfr_name,
  t1.country_id,
  t1.labeling_code,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t11.country_uuid
   FROM main.mfr t1
   LEFT JOIN main.country t11 ON t1.country_id = t11.country_id
   ;

ALTER TABLE main.vw_mfr OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_mfr_alias;

CREATE OR REPLACE VIEW main.vw_mfr_alias AS 
 SELECT t1.mfr_alias_id,
  t1.mfr_alias_uuid,
  t1.mfr_id,
  t1.mfr_alias_name,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.mfr_uuid
   FROM main.mfr_alias t1
   INNER JOIN main.mfr t2 ON t1.mfr_id = t2.mfr_id
   ;

ALTER TABLE main.vw_mfr_alias OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_product;

CREATE OR REPLACE VIEW main.vw_product AS 
 SELECT t1.product_id,
  t1.product_uuid,
  t1.product_name,
  t1.product_type_id,
  t1.inn_id,
  t1.atc_id,
  t1.dosage_id,
  t1.form_id,
  t1.pharm_ther_group_id,
  t1.okpd2_id,
  t1.brand_id,
  t1.recipe_type_id,
  t1.registry_id,
  t1.baa_type_id,
  t1.metaname_id,
  t1.storage_period,
  t1.storage_condition,
  t1.is_vital,
  t1.is_drug2,
  t1.is_drug3,
  t1.is_precursor,
  t1.is_alcohol,
  t1.is_imm_bio,
  t1.is_mandatory,
  t1.is_defect_exists,
  t1.indication,
  t1.contraindication,
  t1.drug_interaction,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_type_uuid,
  t11.inn_uuid,
  t12.atc_uuid,
  t13.dosage_uuid,
  t14.form_uuid,
  t15.pharm_ther_group_uuid,
  t16.okpd2_uuid,
  t17.brand_uuid,
  t18.recipe_type_uuid,
  t19.registry_uuid,
  t20.baa_type_uuid,
  t21.metaname_uuid  
   FROM main.product t1
   INNER JOIN main.product_type t2 ON t1.product_type_id = t2.product_type_id
   LEFT JOIN main.inn t11 ON t1.inn_id = t11.inn_id
   LEFT JOIN main.atc t12 ON t1.atc_id = t12.atc_id
   LEFT JOIN main.dosage t13 ON t1.dosage_id = t13.dosage_id
   LEFT JOIN main.form t14 ON t1.form_id = t14.form_id
   LEFT JOIN main.pharm_ther_group t15 ON t1.pharm_ther_group_id = t15.pharm_ther_group_id
   LEFT JOIN main.okpd2 t16 ON t1.okpd2_id = t16.okpd2_id
   LEFT JOIN main.brand t17 ON t1.brand_id = t17.brand_id
   LEFT JOIN main.recipe_type t18 ON t1.recipe_type_id = t18.recipe_type_id
   LEFT JOIN main.registry t19 ON t1.registry_id = t19.registry_id
   LEFT JOIN main.baa_type t20 ON t1.baa_type_id = t20.baa_type_id
   LEFT JOIN main.metaname t21 ON t1.metaname_id = t21.metaname_id   
   ;

ALTER TABLE main.vw_product OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_product_adjacent;

CREATE OR REPLACE VIEW main.vw_product_adjacent AS 
 SELECT t1.product_adjacent_id,
  t1.product_adjacent_uuid,
  t1.product_id,
  t1.adjacent_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_uuid,
  t3.product_uuid as adjacent_uuiid
   FROM main.product_adjacent t1
   INNER JOIN main.product t2 ON t1.product_id = t2.product_id
   INNER JOIN main.product t3 ON t1.adjacent_id = t3.product_id
   ;

ALTER TABLE main.vw_product_adjacent OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_product_consumer;

CREATE OR REPLACE VIEW main.vw_product_consumer AS 
 SELECT t1.product_consumer_id,
  t1.product_consumer_uuid,
  t1.product_id,
  t1.consumer_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_uuid,
  t3.consumer_uuid
   FROM main.product_consumer t1
   INNER JOIN main.product t2 ON t1.product_id = t2.product_id
   INNER JOIN main.consumer t3 ON t1.consumer_id = t3.consumer_id
   ;

ALTER TABLE main.vw_product_consumer OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_product_mkb10;

CREATE OR REPLACE VIEW main.vw_product_mkb10 AS 
 SELECT t1.product_mkb10_id,
  t1.product_mkb10_uuid,
  t1.product_id,
  t1.mkb10_code,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_uuid  
   FROM main.product_mkb10 t1
   INNER JOIN main.product t2 ON t1.product_id = t2.product_id   
   ;

ALTER TABLE main.vw_product_mkb10 OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_product_normative;

CREATE OR REPLACE VIEW main.vw_product_normative AS 
 SELECT t1.product_normative_id,
  t1.product_normative_uuid,
  t1.product_id,
  t1.doc_num,
  t1.doc_year,
  t1.doc_change_num,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_uuid  
   FROM main.product_normative t1
   INNER JOIN main.product t2 ON t1.product_id = t2.product_id   
   ;

ALTER TABLE main.vw_product_normative OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_product_pack;

CREATE OR REPLACE VIEW main.vw_product_pack AS 
 SELECT t1.product_pack_id,
  t1.product_pack_uuid,
  t1.product_id,
  t1.pack_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_uuid,
  t3.pack_uuid
   FROM main.product_pack t1
   INNER JOIN main.product t2 ON t1.product_id = t2.product_id
   INNER JOIN main.pack t3 ON t1.pack_id = t3.pack_id   
   ;

ALTER TABLE main.vw_product_pack OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_product_pharm_group;

CREATE OR REPLACE VIEW main.vw_product_pharm_group AS 
 SELECT t1.product_pharm_group_id,
  t1.product_pharm_group_uuid,
  t1.product_id,
  t1.pharm_group_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_uuid,
  t3.pharm_group_uuid
   FROM main.product_pharm_group t1
   INNER JOIN main.product t2 ON t1.product_id = t2.product_id
   INNER JOIN main.pharm_group t3 ON t1.pharm_group_id = t3.pharm_group_id   
   ;

ALTER TABLE main.vw_product_pharm_group OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_product_substance;

CREATE OR REPLACE VIEW main.vw_product_substance AS 
 SELECT t1.product_substance_id,
  t1.product_substance_uuid,
  t1.product_id,
  t1.substance_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_uuid,
  t3.substance_uuid
   FROM main.product_substance t1
   INNER JOIN main.product t2 ON t1.product_id = t2.product_id
   INNER JOIN main.substance t3 ON t1.substance_id = t3.substance_id   
   ;

ALTER TABLE main.vw_product_substance OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_product_variant;

CREATE OR REPLACE VIEW main.vw_product_variant AS 
 SELECT t1.product_variant_id,
  t1.product_variant_uuid,
  t1.product_id,
  t1.variant_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.product_uuid,
  t3.product_uuid as variant_uuid
   FROM main.product_variant t1
   INNER JOIN main.product t2 ON t1.product_id = t2.product_id
   INNER JOIN main.product t3 ON t1.variant_id = t3.product_id   
   ;

ALTER TABLE main.vw_product_variant OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_registry;

CREATE OR REPLACE VIEW main.vw_registry AS 
 SELECT t1.registry_id,
  t1.registry_uuid,
  t1.registry_num,
  t1.registry_date,
  t1.renewal_date,
  t1.mfr_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t11.mfr_uuid  
   FROM main.registry t1
   LEFT JOIN main.mfr t11 ON t1.mfr_id = t11.mfr_id   
   ;

ALTER TABLE main.vw_registry OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_substance;

CREATE OR REPLACE VIEW main.vw_substance AS 
 SELECT t1.substance_id,
  t1.substance_uuid,
  t1.substance_name,
  t1.substance_trade_name,
  t1.storage_period,
  t1.storage_condition,
  t1.doc_name,
  t1.is_drug,
  t1.mfr_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t11.mfr_uuid  
   FROM main.substance t1
   LEFT JOIN main.mfr t11 ON t1.mfr_id = t11.mfr_id   
   ;

ALTER TABLE main.vw_substance OWNER TO pavlov;

------------------------------------------------------

-- DROP VIEW main.vw_supplier_good;

CREATE OR REPLACE VIEW main.vw_supplier_good AS 
 SELECT t1.supplier_good_id,
  t1.supplier_good_uuid,
  t1.supplier_id,
  t1.good_id,
  t1.price,
  t1.quantity,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.supplier_uuid,
  t3.good_uuid
   FROM main.supplier_good t1
   INNER JOIN main.supplier t2 ON t1.supplier_id = t2.supplier_id  
   INNER JOIN main.good t3 ON t1.good_id = t3.good_id  
   ;

ALTER TABLE main.vw_supplier_good OWNER TO pavlov;

------------------------------------------------------