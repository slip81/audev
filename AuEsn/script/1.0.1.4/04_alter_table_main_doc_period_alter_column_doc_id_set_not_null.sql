﻿/*
	ALTER TABLE main.doc_period ALTER COLUMN doc_id SET NOT NULL
*/

ALTER TABLE main.doc_period ALTER COLUMN doc_id SET NOT NULL;