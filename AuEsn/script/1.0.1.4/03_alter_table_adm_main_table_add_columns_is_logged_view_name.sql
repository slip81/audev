﻿/*
	ALTER TABLE adm.main_table ADD COLUMNs is_logged, view_name
*/

ALTER TABLE adm.main_table ADD COLUMN is_logged boolean NOT NULL DEFAULT true;
ALTER TABLE adm.main_table ADD COLUMN view_name character varying;
