﻿/*
	ALTER TABLE main.okpd2 ADD COLUMN okpd2_code
*/

ALTER TABLE main.okpd2 ADD COLUMN okpd2_code character varying NOT NULL;