﻿/*
	ALTER TABLE main.pack ADD COLUMN short_name
*/

ALTER TABLE main.pack ADD COLUMN short_name character varying;