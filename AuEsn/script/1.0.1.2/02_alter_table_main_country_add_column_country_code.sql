﻿/*
	ALTER TABLE main.country ADD COLUMN country_code
*/

ALTER TABLE main.country ADD COLUMN country_code character varying;