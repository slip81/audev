﻿/*
	ALTER TABLE main.atc ADD COLUMN atc_code
*/

ALTER TABLE main.atc ADD COLUMN atc_code character varying NOT NULL;