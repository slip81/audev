﻿/*
	ALTER TABLE main.form ADD COLUMNs short_name, group_name
*/

ALTER TABLE main.form ADD COLUMN short_name character varying;
ALTER TABLE main.form ADD COLUMN group_name character varying;