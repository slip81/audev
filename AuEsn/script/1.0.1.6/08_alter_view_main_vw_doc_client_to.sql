﻿/*
	CREATE OR REPLACE VIEW main.vw_doc_client_to
*/

DROP VIEW main.vw_doc_client_to;

CREATE OR REPLACE VIEW main.vw_doc_client_to AS 
 SELECT t1.doc_client_to_id,
    t1.doc_client_to_uuid,
    t1.doc_id,
    t1.client_id,
    t1.doc_state_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.is_success,
    t1.mess,
    t2.doc_uuid,
    t3.client_uuid,
    t4.doc_state_uuid
   FROM main.doc_client_to t1
     JOIN main.doc t2 ON t1.doc_id = t2.doc_id
     JOIN main.client t3 ON t1.client_id = t3.client_id
     JOIN main.doc_state t4 ON t1.doc_state_id = t4.doc_state_id;

ALTER TABLE main.vw_doc_client_to OWNER TO pavlov;
