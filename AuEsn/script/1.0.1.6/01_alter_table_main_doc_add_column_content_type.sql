﻿/*
	ALTER TABLE main.doc ADD COLUMN content_type
*/

-- ALTER TABLE main.doc DROP COLUMN content_type;

ALTER TABLE main.doc ADD COLUMN content_type character varying;
COMMENT ON COLUMN main.doc.content_type IS 'тип содержимого';
