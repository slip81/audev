﻿/*
	ALTER TABLE adm.main_table ALTER COLUMN main_table_id DROP DEFAULT
*/

ALTER TABLE adm.main_table ALTER COLUMN main_table_id DROP DEFAULT;