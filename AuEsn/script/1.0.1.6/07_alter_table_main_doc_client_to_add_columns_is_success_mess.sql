﻿/*
	ALTER TABLE main.doc_client_to ADD COLUMNs is_success, mess
*/

ALTER TABLE main.doc_client_to ADD COLUMN is_success boolean NOT NULL DEFAULT false;
ALTER TABLE main.doc_client_to ADD COLUMN mess character varying;
