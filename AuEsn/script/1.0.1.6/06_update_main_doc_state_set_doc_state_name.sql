﻿/*
	update main.doc_state set doc_state_name
*/

delete from main.doc_period;
delete from main.doc_client_to;
delete from main.doc;

update main.doc_state set doc_state_name = 'В очереди отправителя', upd_user = 'pavlov' where doc_state_id = 1;
update main.doc_state set doc_state_name = 'Обрабатывается веб-сервисом', upd_user = 'pavlov' where doc_state_id = 2;
update main.doc_state set doc_state_name = 'В очереди сервиса ЕСН', upd_user = 'pavlov' where doc_state_id = 3;
update main.doc_state set doc_state_name = 'Обрабатывается сервисом ЕСН', upd_user = 'pavlov' where doc_state_id = 4;
update main.doc_state set doc_state_name = 'В очереди получателя', upd_user = 'pavlov' where doc_state_id = 5;

insert into main.doc_state (doc_state_id, doc_state_name, upd_user)
values (6, 'Доставлено получателю', 'pavlov');

select setval('main.doc_state_doc_state_id_seq', 7, false);

-- select * from main.doc_state order by doc_state_id
