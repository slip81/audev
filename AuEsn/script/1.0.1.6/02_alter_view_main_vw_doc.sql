﻿/*
	CREATE OR REPLACE VIEW main.vw_doc
*/

DROP VIEW main.vw_doc;

CREATE OR REPLACE VIEW main.vw_doc AS 
 SELECT t1.doc_id,
    t1.doc_uuid,
    t1.doc_num,
    t1.doc_name,
    t1.doc_date,
    t1.doc_type_id,
    t1.doc_state_id,
    t1.doc_path,
    t1.client_from_id,
    t1.client_to_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.content_type,
    t2.doc_type_uuid,
    t3.doc_state_uuid,
    t4.client_uuid AS client_from_uuid,
    t11.client_uuid AS client_to_uuid
   FROM main.doc t1
     JOIN main.doc_type t2 ON t1.doc_type_id = t2.doc_type_id
     JOIN main.doc_state t3 ON t1.doc_state_id = t3.doc_state_id
     JOIN main.client t4 ON t1.client_from_id = t4.client_id
     LEFT JOIN main.client t11 ON t1.client_to_id = t11.client_id;

ALTER TABLE main.vw_doc OWNER TO pavlov;
