﻿/*
	ALTER TABLE main.product ADD COLUMN applic_and_dose
*/

ALTER TABLE main.product ADD COLUMN applic_and_dose character varying;
COMMENT ON COLUMN main.product.applic_and_dose IS 'способ применеия и дозы';
