﻿/*
	ALTER TABLE main.doc ADD COLUMN is_need_reply
*/

-- ALTER TABLE main.doc DROP COLUMN is_need_reply;

ALTER TABLE main.doc ADD COLUMN is_need_reply boolean NOT NULL DEFAULT true;
COMMENT ON COLUMN main.doc.is_need_reply IS 'нужен ответ';
