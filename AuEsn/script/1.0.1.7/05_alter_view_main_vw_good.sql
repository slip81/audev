﻿/*
	CREATE OR REPLACE VIEW main.vw_good
*/

DROP VIEW main.vw_good;

CREATE OR REPLACE VIEW main.vw_good AS 
 SELECT t1.good_id,
    t1.good_uuid,
    t1.product_id,
    t1.mfr_id,
    t1.barcode,
    t1.gtin,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.is_perfect,
    t2.product_uuid,
    t3.mfr_uuid
   FROM main.good t1
     JOIN main.product t2 ON t1.product_id = t2.product_id
     JOIN main.mfr t3 ON t1.mfr_id = t3.mfr_id;

ALTER TABLE main.vw_good OWNER TO pavlov;
