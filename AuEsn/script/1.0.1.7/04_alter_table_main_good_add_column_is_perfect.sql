﻿/*
	ALTER TABLE main.good ADD COLUMN is_perfect
*/

ALTER TABLE main.good ADD COLUMN is_perfect boolean NOT NULL DEFAULT false;