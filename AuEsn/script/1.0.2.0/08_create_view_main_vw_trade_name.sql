﻿/*
	CREATE OR REPLACE VIEW main.vw_trade_name
*/

-- DROP VIEW main.vw_trade_name;

CREATE OR REPLACE VIEW main.vw_trade_name AS 
 SELECT t1.trade_name_id,
  t1.trade_name_uuid,
  t1.trade_name_name,
  t1.inn_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t11.inn_uuid
   FROM main.trade_name t1
     LEFT JOIN main.inn t11 ON t1.inn_id = t11.inn_id;

ALTER TABLE main.vw_trade_name OWNER TO pavlov;
