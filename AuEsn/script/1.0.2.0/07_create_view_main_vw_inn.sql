﻿/*
	CREATE OR REPLACE VIEW main.vw_inn
*/

-- DROP VIEW main.vw_inn;

CREATE OR REPLACE VIEW main.vw_inn AS 
 SELECT t1.inn_id,
  t1.inn_uuid uuid,
  t1.inn_name,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t1.client_source_id,
  t11.client_uuid as client_source_uuid
   FROM main.inn t1
     LEFT JOIN main.client t11 ON t1.client_source_id = t11.client_id;

ALTER TABLE main.vw_inn OWNER TO pavlov;
