﻿/*
	insert into adm.main_table 50
*/

update adm.main_table
set view_name = 'vw_inn'
where main_table_id = 20;

insert into adm.main_table (main_table_id, main_table_name, allow_select, allow_insert, allow_update, allow_delete, is_logged, view_name)
values (53, 'trade_name', true, true, true, true, true, 'vw_trade_name');

-- select * from adm.main_table order by main_table_id