﻿/*
	ALTER TABLE main.product ADD COLUMN trade_name_id
*/


-- ALTER TABLE main.product DROP COLUMN trade_name_id

ALTER TABLE main.product ADD COLUMN trade_name_id bigint;
COMMENT ON COLUMN main.product.trade_name_id IS 'FK торговое наименование';

ALTER TABLE main.product
  ADD CONSTRAINT product_trade_name_id_fkey FOREIGN KEY (trade_name_id)
      REFERENCES main.trade_name (trade_name_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;