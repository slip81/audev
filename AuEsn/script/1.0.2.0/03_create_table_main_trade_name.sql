﻿/*
	CREATE TABLE main.trade_name
*/

-- DROP TABLE main.trade_name;

CREATE TABLE main.trade_name
(
  trade_name_id bigserial NOT NULL,
  trade_name_uuid uuid NOT NULL DEFAULT main.uuid_generate_v1mc(),
  trade_name_name character varying NOT NULL,
  inn_id bigint,
  crt_date timestamp without time zone NOT NULL DEFAULT now(),
  crt_user character varying,
  upd_date timestamp without time zone NOT NULL DEFAULT now(),
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  CONSTRAINT trade_name_pkey PRIMARY KEY (trade_name_id),
  CONSTRAINT trade_name_inn_id_fkey FOREIGN KEY (inn_id)
      REFERENCES main.inn (inn_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE main.trade_name OWNER TO pavlov;

COMMENT ON TABLE main.trade_name IS 'Справочник торговых наименований';

-- DROP TRIGGER main_trade_name_set_date_and_user_trg ON main.trade_name;

CREATE TRIGGER main_trade_name_set_date_and_user_trg
  BEFORE INSERT OR UPDATE
  ON main.trade_name
  FOR EACH ROW
  EXECUTE PROCEDURE main.set_date_and_user();
