﻿/*
	ALTER TABLE main.inn ADD COLUMN client_source_id
*/

ALTER TABLE main.inn ADD COLUMN client_source_id integer;

ALTER TABLE main.inn
  ADD CONSTRAINT inn_client_source_id_fkey FOREIGN KEY (client_source_id)
      REFERENCES main.client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

-- update main.inn set client_source_id = (select max(client_id) from main.client where client_name =  'Консоль ЕСН') where client_source_id is null;