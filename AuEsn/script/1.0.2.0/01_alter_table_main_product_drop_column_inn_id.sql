﻿/*
	ALTER TABLE main.product DROP COLUMN inn_id
*/

DROP VIEW main.vw_client_good_info;
DROP VIEW main.vw_product;

ALTER TABLE main.product DROP COLUMN inn_id;
