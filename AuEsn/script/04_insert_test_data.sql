﻿/*
	insert test data
*/


select setval('main.client_client_id_seq', 1, false);
select setval('main.doc_type_doc_type_id_seq', 1, false);
select setval('main.doc_state_doc_state_id_seq', 1, false);
select setval('main.doc_doc_id_seq', 1, false);
select setval('main.doc_period_doc_period_id_seq', 1, false);

-- delete from main.client;

-- select * from main.client;

insert into main.client (client_name, cab_client_id)
values ('Клиент 1', 5219);

insert into main.client (client_name)
values ('Клиент 2');

-- select * from main.doc_type;

-- select * from main.doc_state;

insert into main.doc_type (doc_type_id, doc_type_name)
values (1, 'Список номенклатуры для сопоставления');

insert into main.doc_type (doc_type_id, doc_type_name)
values (2, 'Остатки товаров');

insert into main.doc_type (doc_type_id, doc_type_name)
values (3, 'Товарная накладная');

insert into main.doc_state (doc_state_id, doc_state_name)
values (1, 'В очереди');

insert into main.doc_state (doc_state_id, doc_state_name)
values (2, 'Обрабатывается');

insert into main.doc_state (doc_state_id, doc_state_name)
values (3, 'Обработан успешно');

insert into main.doc_state (doc_state_id, doc_state_name)
values (4, 'Обработан с ошибками');

-- select * from main.doc;

insert into main.doc (doc_num, doc_name, doc_date, doc_type_id, doc_state_id, doc_path, client_from_id, client_to_id, crt_date, crt_user, upd_date, upd_user, is_deleted)
values ('111', 'Документ 1', current_date, 1, 3, null, 1, 1, current_timestamp, 'api', current_timestamp, 'api', false);

insert into main.doc (doc_num, doc_name, doc_date, doc_type_id, doc_state_id, doc_path, client_from_id, client_to_id, crt_date, crt_user, upd_date, upd_user, is_deleted)
values ('222', 'Документ 2', current_date, 1, 4, null, 1, 1, current_timestamp, 'api', current_timestamp, 'api', false);

--select * from main.doc_period

insert into main.doc_period (doc_id, doc_state_id, crt_date)
select doc_id, 1, current_timestamp from main.doc;

insert into main.doc_period (doc_id, doc_state_id, crt_date)
select doc_id, 2, current_timestamp from main.doc;

insert into main.doc_period (doc_id, doc_state_id, crt_date)
select doc_id, 3, current_timestamp from main.doc where doc_state_id = 3;

insert into main.doc_period (doc_id, doc_state_id, crt_date)
select doc_id, 4, current_timestamp from main.doc where doc_state_id = 4;

-- update main.doc set is_deleted = false, del_date = null, del_user = null;